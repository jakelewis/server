#ifndef _included_HttpRequest_h_
#define _included_HttpRequest_h_

#include "Connection.h"
#include <string>
#include <string.h>
#include <vector>
#include <time.h> // for clock() and clock_t

class HttpRequest
{
  Connection *m_conn;
  typedef std::vector<std::string> HeaderList;
  HeaderList m_headers;
  int m_good,m_complete,m_processed;
  std::string m_uri,m_verb,m_params,m_postData;
  int m_contentLength;
  static void fixEscapedChars( std::string &out, std::string &in );
  void readAll( Connection *c, std::string &data, int len );
#ifndef _WIN32
  struct timespec m_start;
#else
  clock_t m_start;
#endif
  
  typedef enum
  {
    MAX_LIFETIME = 5000
  } EValues;
public:
  HttpRequest( Connection *c );
  int IsConnectionLocal();
  signed long TimeElapsed();
  void Dump();
  // returns completion: 1 == complete, 0==still need more data
  int Service();
  int IsGood();
  void SetProcessed(); // mark this request as processed
  int WasProcessed(); // see if this request was processed
  const char *Uri();
  const char *Verb();
  int GetParamFrom( const char *params, const char *name, std::string &outVal );
  int GetParam( const char *name, std::string &outVal );
  int GetCookie( const char *name, std::string &val );
  int GetHeader( const char *name, std::string &val );
};

#endif //def _included_HttpRequest_h_
