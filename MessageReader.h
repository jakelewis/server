#ifndef _included_MessageReader_h_
#define _included_MessageReader_h_

#include "Logging.h"
#include "Stream.h"
#include <vector>

class MessageReader
{
  unsigned char m_buf[512];
  std::vector<unsigned char> m_rawbuf;
  unsigned int m_bufLen;
  int m_status;
  int m_cc;
  int m_endc; // what character terminated the sentence
  Logging *m_myLog;
  typedef enum
  {
  	STAT_WAITING_START = 0,
  	STAT_READING_MSG   = 1,
  	STAT_AWAITING_CC_EOT   = 4,
  	STAT_AWAITING_CC_ETX   = 3,
  	STAT_COMPLETE      = 2
  } EStatus;

public:
  typedef enum
  {
  	STX = 0x02,
  	ETX = 0x03,
  	EOT = 0x04,
  } ESPECIALCHARS;

  class Callback
  {
  public:
    virtual void OnProtocolError( int err ) = 0;
  };
  MessageReader( Logging *myLog );
  typedef enum
  { 
    //MSG_OK = 1, any positive number means the message length
    MSG_NONE = 0,
    MSG_OK_BUT_EMPTY = 99999,
    ERR_DISCONNECTED = -1,
    ERR_BADCRC = -2,        // bad CRC was received
    ERR_BUFFEROVERRUN = -3, // message was too long for rx buffer
    ERR_RESTART = -4,       // previous message was interrupted by a new STX
    ERR_NOCRC = -5,         // message restarted while awaiting CRC 

    ERR_UNKNOWN=-99
  } ErrorCodes;
  int Read( IStream *pStm, int usesChecksum, Callback * pCallback );
  int ended_with_eot();
  unsigned char *get();
  unsigned int size();
  void reset();
};

#endif//ndef _included_MessageReader_h_
