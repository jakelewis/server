#include "ErrorLog.h"
#include <stdio.h>
#include <stdarg.h>
#include <string.h>

std::vector< std::string > ErrorLog::sm_vErrors;
int ErrorLog::dup_count = 0;
std::string ErrorLog::sm_prev; // the last string that was logged.  If new one is identical, just increment dup count

void ErrorLog::logf( const char *fmt ... )
{
  char buf[500];
  make_timestamp(buf);
  int len = strlen(buf);
  char *p = &buf[len];
  *p++ = ' '; ++len; // put in a space
  va_list args;
  va_start( args, fmt );
  len += vsnprintf( p, sizeof(buf)-len, fmt, args );
  if ( !sm_prev.compare( p ) )
  {
    ++dup_count;
    // don't append to my list
  } else
  {
    if ( dup_count ) // previous string was repeated this many times
    {
      char buf2[100];
      sprintf( buf2, "[previous line repeated %d times]", dup_count );
      sm_vErrors.push_back( buf2 );
      dup_count = 0;
    }
    sm_vErrors.push_back( std::string(buf) );
    sm_prev = p;
    while ( sm_vErrors.size() > 100 ) // limit this to the most recent 100 errors
      sm_vErrors.erase( sm_vErrors.begin() );
  }

  // still want this to go out STDOUT even if it's a dup -- that's how fail2ban will decide to ban somebody  

  // this stuff sends the error via STDOUT, which goes to a log, which can be monitored by fail2ban
  printf( "%s\n", buf ); //m_connection.GetAddressAndPort(addr) );
  fflush(stdout); // so that fail2ban will see it right away
}

const char *ErrorLog::make_list( std::string &outstr, const char *linesep )
{
  std::vector< std::string >::iterator it;
  for ( it=sm_vErrors.begin(); it != sm_vErrors.end(); it++ )
  {
    outstr += it->c_str();
    outstr += linesep;
  }
  return outstr.c_str();
}
