/*
 * Connection.cpp
 *
 *  Created on: Oct 3, 2012
 *      Author: todd
 */

#include "predef.h"
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include "Connection.h"
#include <algorithm>

int Connection::m_bCommLogging = 0;

#if 0
void Connection::run()
{
	while ( !m_killThread )
	{
		unsigned char c=0;
		int n = recv(m_sock,(char*)&c,1,0);
		if ( n < 0 )
		{
		  if ( WSAGetLastError() == WSAEWOULDBLOCK )
		  {
		    OSTimeDly(1);
		    continue;
		  }
			m_err = n;
			break;
		}
		else if ( !n )
		{
			m_err = 0;
			break;
		}
		int ov;
		if ( (ov=m_fifo.write( c ) ) )
		{
			iprintf( "Connection FIFO overflow: %d\n", ov );
		} //else
			//iprintf( "%c", c );
	}
  return;
}
#endif
#if 0
Connection::Connection( IPADDR ip, int port, int *piConnected )
: m_thread(this),    // does nothing but store the pointer for when the thread is started later
  m_killThread(0),
  m_err(0),
  m_sock(INVALID_SOCKET),
  m_pLogFile(NULL)
{
  *piConnected = 0;
  SOCKET sock = INVALID_SOCKET;
  do
  {
    sock = socket( AF_INET, SOCK_STREAM, IPPROTO_TCP );
    if ( INVALID_SOCKET == sock )
      break;
    SOCKADDR_IN sa;
    memset(&sa,0,sizeof(sa));
    sa.sin_family = AF_INET;
    sa.sin_port = htons(port);
    ADDR_FROM_SOCKADDR(sa) = ip;
    if ( connect( sock, (sockaddr*)&sa, sizeof(sa) ) )
      break;
    if ( ! m_thread.Start() )
      break;
    m_sock = sock;
    *piConnected = 1;
 	} while ( 0 );
 	if ( INVALID_SOCKET != sock && sock != m_sock )
 	  closesocket(sock);
}
#endif

struct changeBadChars
{
  void operator()(char &c) { if ( c == '.' ) c = '-'; else if ( c == ':' ) c = '_'; }
};
void Connection::OnDataTransferred( int received, const void *data, int length ) // for verbose logging 
{
  if ( !m_bCommLogging )
    return;
  // enable to make a file describing all the data transferred on the connection
  if ( !m_pLogFile )
  {
    std::string fn;

    GetAddressAndPort(fn);
    if ( fn.compare( 0, 4, "127." ) ) // don't do this for localhost communications (web connections)
    {
      std::for_each( fn.begin(), fn.end(), changeBadChars() );

      m_pLogFile = fopen( fn.c_str(), "w" );
      if ( !m_pLogFile )
        printf( "Failed to open log file %s\n", fn.c_str() );
      else
        printf( "Opened log file %s\n", fn.c_str() );
    }
  }
  FILE *out = m_pLogFile;
  //if ( !out )
    //out = stdout;
  if ( out )
  {
    int i;
    unsigned char *pb = (unsigned char*)data;
    char ascii[20];
    long elapsed = Elapsed(&m_start);
    fprintf( out, "%6d.%03d\n", elapsed/1000, elapsed % 1000 );
    for ( i=0; i<length; ++i )
    {
      if ( !(i%16) )
      {
        if ( !i )
          fprintf( out, "%6d %c ", (int)elapsed, i==0 ? (received ? '>' : '<') : ' '  );
        else
          fprintf( out, "         " );
      }
      ascii[i%16] = isprint((char)pb[i]) ? pb[i] : '.';
      fprintf( out, "%02X ", (int)pb[i] );
      if ( !((i+1)%16) )
      {
        ascii[16] = 0;
        fprintf( out, " '%.16s'\n", ascii );
      }
    }
    if ( i%16 )
    {
      while ( i%16 )
      {
        ascii[i%16] = 0;
        fprintf( out, "   " );
        ++i;
      }
      fprintf( out, " '%.16s'\n", ascii );
    }
  }

}

Connection::Connection( Socket *sock )
: m_sock(sock),
  m_pLogFile(NULL)
{
  GET_CURRENT_TIME(m_start);

  // make it finish sending if the socket it closed before it's done
  // this makes it so when the HTTP connections finish sending their data, they can call close immediately
  m_sock->SetLinger(1);
  
  // so if a connection drops while there's no activity, we should get an error 
  m_sock->SetKeepalive(1);

  m_sock->SetNonblocking(1);
  
  sock->SetCallback(this);

//std::string out;
//GetAddressAndPort( out );
//printf( "NEW %s CONNECTION: %s\n", !strncmp("127.0.0.1", out.c_str(), 9 ) ? "LOCAL" : "REMOTE", out.c_str()  );   
}

Connection::~Connection()
{
  if ( m_pLogFile )
    fclose( m_pLogFile );
	delete m_sock;
}

const char *Connection::GetAddressAndPort( std::string &out )
{
  char buf[100];
  SOCKADDR_IN *sa = m_sock->GetAddress();
  sprintf( buf, "%s:%d", inet_ntoa(sa->sin_addr), ntohs(sa->sin_port) );
  out = buf;
  return out.c_str();
}

void Connection::OnDataAvailable( Stream *pSock )
{
  INSTRUMENT_THIS_FUNCTION
  int res;
  unsigned char buf[8192];
  // read all bytes into the FIFO
  int len = pSock->DataReadable();
//printf( "Connection: %d bytes readable\n", len );
  if ( len < 0 ) len = sizeof(buf);
  while ( len > 0 )
  {
    int toRead = len;
    if ( toRead > sizeof(buf) )
      toRead = sizeof(buf);
    while ( (res=pSock->Read( buf, toRead ) ) > 0 ) // should be fine if non-blocking
    {
      OnDataTransferred( 1, buf, res );
      m_fifo.write( buf, res );
    }
    if ( res <= 0 ) // graceful close or error
    {
      if ( WSAGetLastError() != WSAEWOULDBLOCK && m_sock )
      {
        printf( "Connection closing because recv returned %d (last error %d)\n", res, WSAGetLastError() );
        m_sock->Close();
      }
      break;
    }
  }
}

void Connection::Close()
{
  m_sock->Close();
}

void Connection::OnException( Stream *pSock, int events )
{
 // printf( "Connection::OnException\n" );
  if ( events & POLLHUP ) // other end hung up
  {
//    printf( "Connection::OnException closing fd %d\n", pSock->GetFD() );
 //   pSock->Close();
  }
}

int Connection::IsOpen()
{
  return m_sock->IsOpen();
}

int Connection::Available()
{
  if ( !m_sock->IsOpen() ) // see if the socket has been closed
  {
    printf( "Connection::Available returning -1, socket says it's closed!\n" );
  	return -1;
  }
	if ( !m_fifo.available() )
	{
    return 0;
	}
	return m_fifo.available();
}
int Connection::Read( void *data, int numBytes )
{
	if ( !m_fifo.available() )
	{
	  if ( !m_sock->IsOpen() )
		  return -1;
		return 0;
	}
	unsigned char *pb = (unsigned char*)data;
	int nRead = 0;
	int c = 0;
	while ( nRead < numBytes && (c=m_fifo.read()) >= 0 )
	{
		pb[nRead++] = c;
	}
	//if ( c <  0 )
		//iprintf( "fifo read returned %d\n", c );
	return nRead; // this will stop when there's no more data available, or we filled up the buffer
}
int Connection::ReadStr( std::string &out, int numBytes )
{
	if ( !m_fifo.available() )
	{
	  if ( !m_sock->IsOpen() )
		  return -1;
		return 0;
	}
	int nRead = 0;
	int c = 0;
	while ( nRead < numBytes && (c=m_fifo.read()) >= 0 )
	{
		out += (char)c;
		++nRead;
	}
	//if ( c <  0 )
		//iprintf( "fifo read returned %d\n", c );
	return nRead; // this will stop when there's no more data available, or we filled up the buffer
}
int Connection::Write( const void *data, int numBytes )
{
  if ( !m_sock )
    return -1;
	if ( !m_sock->IsOpen() )
	{
		return -1;
	}
  OnDataTransferred( 0, data, numBytes );
	int nWrote = m_sock->Write(data,numBytes);
  if ( nWrote < numBytes )
  {
    printf( "Connection closing because only %d bytes of %d were written\n", nWrote, numBytes );
    Close(); // so all future calls will fail
  }
  return nWrote;
}

int Connection::WriteStr( const char *str )
{
  return Write( str, (int)strlen(str) );
}
