#include "predef.h"

#ifndef _NO_DBCONN
#define _CRT_SECURE_NO_WARNINGS

#include "DBConn.h"
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>
#ifdef _LINUX_
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <linux/if_link.h>
#include <ifaddrs.h>
#endif

extern "C" {
#include "sha1.h"
}

int DBConn::have_server_ip = 0;
char DBConn::server_ip[100] = "";

// one environment for everything in this process

DBConn::SQLEnv DBConn::hEnv;

int DBConn::Initialize(const char *dsn, const char *un, const char *pw)
{
  if ( have_server_ip )
    return 0;
  int do_disconnect = 0;
  struct ifaddrs *ifap = 0L;
  if ( !getifaddrs( &ifap ) && ifap )
  {
    for ( struct ifaddrs *cur = ifap; cur; cur = cur->ifa_next )
    {
      if ( !cur->ifa_addr ) 
      {
        printf( "  [no address included for %s]\n", cur->ifa_name );
        continue;
      }
      int family = cur->ifa_addr->sa_family;
      if ( !strncmp( "lo", cur->ifa_name,2 ) )
        continue; // don't want localhost
      if ( !strncmp( "dummy", cur->ifa_name,5 ) )
        continue; // don't want localhost
      if ( AF_INET != family ) continue;
      printf("%-8s %s (%d)\n",
                      cur->ifa_name,
                      (family == AF_PACKET) ? "AF_PACKET" :
                      (family == AF_INET) ? "AF_INET" :
                      (family == AF_INET6) ? "AF_INET6" : "???",
                      family);
      sockaddr_in *sa = (sockaddr_in*)cur->ifa_addr;
/*      printf( "%s\n", inet_ntoa( sa->sin_addr ) );
      printf( "%u %u %u %u %u %u", 
                (int)(unsigned char)cur->ifa_addr->sa_data[0],
                (int)(unsigned char)cur->ifa_addr->sa_data[1],
                (int)(unsigned char)cur->ifa_addr->sa_data[2],
                (int)(unsigned char)cur->ifa_addr->sa_data[3],
                (int)(unsigned char)cur->ifa_addr->sa_data[4],
                (int)(unsigned char)cur->ifa_addr->sa_data[5] );*/
//      char host[NI_MAXHOST];
//      int s;
//      if ( !(s == getnameinfo( cur->ifa_addr, sizeof(struct sockaddr_in),
//                        host, NI_MAXHOST, NULL, 0, NI_NUMERICHOST ) ) )
      {
        strcpy( server_ip, inet_ntoa( sa->sin_addr ) );
//        strcpy( server_ip, host );
        have_server_ip = 1;
        printf( "Using server IP %s for %s\n", server_ip, cur->ifa_name );
        do_disconnect = 1;
        break;
      } //else
      {
//        printf( "  No name info: %s\n", gai_strerror(s) );
      }
    }
    freeifaddrs( ifap );
  }
  if ( do_disconnect )
  {
    DBConn conn(dsn,un,pw);
    conn.clearControllerList();
  }
  return 0;
}

DBConn::DBConn( const char *dsn, const char *un, const char *pw )
: m_connected(0)
{
  SQLRETURN retCode ;
  
  int retries = 5;
  do
  {
    // create env handle
//printf( "Allocating Env handle\n" );
//    if ( !hEnv.Alloc( SQL_HANDLE_ENV, SQL_NULL_HANDLE ) )
//      break;
    
    // tell it we want ODBC v3
//printf( "requesting ODBC ver 3\n" );
//    retCode = SQLSetEnvAttr( hEnv, SQL_ATTR_ODBC_VERSION, (void*)SQL_OV_ODBC3, 0 ) ;
//    if ( !SQL_SUCCEEDED(retCode) )
//      break; 

    // allocate actual connection handle
//printf( "alloc conn handle\n" );
    if ( !m_hConn.Alloc( SQL_HANDLE_DBC, hEnv ) )
      break;

#if 1
    SQLCHAR connStrOut[200];
    SQLSMALLINT numOut;
    // Open database connection.
    retCode = SQLDriverConnectA(
      
      m_hConn,
      
      NULL, // HWND
      (SQLCHAR*)dsn,  // name of data source we are connecting to,
      // AS PER REGISTERED IN ODBC Data Source Administrator.

      // If you are on a 64-bit machine, and you
      // DO NOT USE THE 64-bit driver.  As long as
      // your compiler publishes a 32-bit .exe (which
      // Visual Studio does), you'll keep getting:

      // [Microsoft][ODBC Driver Manager] Data source name not found and no default driver specified

      // SO DON'T USE THE 64-BIT DRIVERS!  Instead, install
      // the 32-bit driver, and then managing/using
      // your 32-bit datasources in
      // c:\windows\syswow64\odbcad32.exe

      // Note that on a 64-bit windows machine, the 32-bit
      // drivers and the 64-bit drivers are managed
      // from COMPLETELY SEPARATE, BUT IDENTICAL-LOOKING
      // windows.  Its really weird.

      // On a 64-bit machine:
      // c:\windows\system32\odbcad32.exe    // 64-bit version [even though it SAYS system32_ in the path, this is the __64__ bit version on a 64-bit machine]
      // c:\windows\syswow64\odbcad32.exe    // 32-bit version [even though it SAYS syswow64_ in the path]

      // Call it stupid, scream, pull your hair out,
      // that's what it is.
      // http://stackoverflow.com/questions/949959/why-do-64bit-dlls-go-to-system32-and-32bit-dlls-to-syswow64-on-64bit-windows

      // and

      // http://blogs.sepago.de/helge/2008/04/20/windows-x64-all-the-same-yet-very-different-part-7/

      // Thanks again, Microsoft,
      // for making the 64-bit programming experience
      // such a pleasure.
      SQL_NTS,  // the DSN name is a NULL TERMINATED STRING, so "count it yourself"

      connStrOut,
      sizeof(connStrOut),  // userid is a null-terminated string
      &numOut,
      SQL_DRIVER_NOPROMPT 
      
    ) ;  
#else
    // Open database connection.
    retCode = SQLConnectA(
      
      m_hConn,
      
      (SQLCHAR*)dsn,  // name of data source we are connecting to,
      // AS PER REGISTERED IN ODBC Data Source Administrator.

      // If you are on a 64-bit machine, and you
      // DO NOT USE THE 64-bit driver.  As long as
      // your compiler publishes a 32-bit .exe (which
      // Visual Studio does), you'll keep getting:

      // [Microsoft][ODBC Driver Manager] Data source name not found and no default driver specified

      // SO DON'T USE THE 64-BIT DRIVERS!  Instead, install
      // the 32-bit driver, and then managing/using
      // your 32-bit datasources in
      // c:\windows\syswow64\odbcad32.exe

      // Note that on a 64-bit windows machine, the 32-bit
      // drivers and the 64-bit drivers are managed
      // from COMPLETELY SEPARATE, BUT IDENTICAL-LOOKING
      // windows.  Its really weird.

      // On a 64-bit machine:
      // c:\windows\system32\odbcad32.exe    // 64-bit version [even though it SAYS system32_ in the path, this is the __64__ bit version on a 64-bit machine]
      // c:\windows\syswow64\odbcad32.exe    // 32-bit version [even though it SAYS syswow64_ in the path]

      // Call it stupid, scream, pull your hair out,
      // that's what it is.
      // http://stackoverflow.com/questions/949959/why-do-64bit-dlls-go-to-system32-and-32bit-dlls-to-syswow64-on-64bit-windows

      // and

      // http://blogs.sepago.de/helge/2008/04/20/windows-x64-all-the-same-yet-very-different-part-7/

      // Thanks again, Microsoft,
      // for making the 64-bit programming experience
      // such a pleasure.
      strlen(dsn),  // the DSN name is a NULL TERMINATED STRING, so "count it yourself"

      (SQLCHAR*)un,
      strlen(un),  // userid is a null-terminated string
      
      (SQLCHAR*)pw,
      strlen(pw)   // password is a null terminated string
      
    ) ;  
#endif
    if ( !SQL_SUCCEEDED(retCode) )
    {
      printf( "Connection failed:\n" );
      ShowStatus(m_hConn);
      if ( !retries ) 
      {
        printf( "Max connection retries exceeded... aborting process!\n" );
        exit(99);
      }
      printf( "Waiting 5 sec to try again...\n" );
      sleep(5);
      --retries;
      continue; 
    }
    m_connected = 1;
    break;
  } while ( 1 );
//printf( "DBC+ %p\n", this );
}

DBConn::~DBConn()
{
  SQLDisconnect( m_hConn );
//printf( "DBC- %p\n", this );
}

void DBConn::ShowStatus( SQLHandle &hConn )
{
  SQLCHAR sqlState[6];
  SQLINTEGER nativeError;
  SQLCHAR msgStr[2048];
  SQLSMALLINT returnedChars=0; // the number of characters that msgStr buffer was TOO SHORT..
  
  // http://msdn.microsoft.com/en-us/library/ms716256(VS.85).aspx
  // This must be the WEIRDEST ERROR REPORTING FUNCTION I've EVER seen.
  // It requires 8 parameters, and its actually pretty .. silly
  // about the amount of state information it expects YOU to keep track of.

  // It isn't so much a "GetLastError()" function
  // as it is a "GetMeStatus( something very, very specific )" function.
  
  SQLRETURN retCode ;
  
  for( int i = 1 ; i < 20 ; i++ )
  {
    retCode = SQLGetDiagRecA(
      
      hConn.Type(),  // the type of object you're checking the status of
      hConn,   // handle to the actual object you want the status of
      
      i, // WHICH status message you want.  The "Comments" section at the 
      // bottom of http://msdn.microsoft.com/en-us/library/ms716256(VS.85).aspx
      // seems to explain this part well.

      sqlState,    // OUT:  gives back 5 characters (the HY*** style error code)
      &nativeError,// numerical error number
      msgStr,      // buffer to store the DESCRIPTION OF THE ERROR.
      // This is the MOST important one, I suppose

      sizeof(msgStr),         // the number of characters in msgStr, so that
      // the function doesn't do a buffer overrun in case it
      // has A LOT to tell you
      &returnedChars      // again in case the function has A LOT to tell you,
      // the 255 character size buffer we passed might not be large
      // enough to hold the entire error message.  If that happens
      // the error message will truncate and the 'overBy' variable
      // will have a value > 0 (it will measure number of characters
      // that you 'missed seeing' in the error message).
      
    ) ;

    if( SQL_SUCCEEDED( retCode ) )
    {
      printf( "  [%s][%d] %s\n", sqlState, (int)nativeError, msgStr ) ;
    }
    else
    {
      // Stop looping when retCode comes back
      // as a failure, because it means there are
      // no more messages to tell you
      if ( !i ) 
        printf( "  [No status reported]\n" );
      break ;
    }
  }
}

// tell whether we have a working database connection
int DBConn::IsConnectionOK()
{
  if ( !m_connected )
    return 0;
  // check that the actual connection is OK
  SQLUINTEGER outval;
  SQLINTEGER outsize;
  SQLRETURN ret;
  ret = SQLGetConnectAttr( m_hConn, SQL_ATTR_CONNECTION_DEAD, (SQLPOINTER)&outval, sizeof(outval), &outsize );
  if ( SQL_INVALID_HANDLE == ret )
    return 0;
  if ( !SQL_SUCCEEDED(ret) )
  {
printf( "Could not query connection status: %d!\n", ret );
    return 1; // maybe not supported on this version or system?
  }
  return outval == SQL_CD_FALSE;
}

const char *DBConn::MakeNewSalt( std::string &out )
{
  static int seeded = 0;
  if ( !seeded )
  {
#ifndef _WIN32
    struct timespec ts;
    clock_gettime( GTOD_CLOCK, &ts );
    srand( ts.tv_nsec );
#else
    srand( clock() );
#endif
    ++seeded;
  }
  int salt = (rand() ^ (rand()<<16))&0xfffff;
  char tmp[8];
  sprintf( tmp, "%05x", salt );
  out = tmp;
  return out.c_str();
}

const char *DBConn::MakePasswordString( std::string &out, const char *salt, const char *pw )
{
  out = "sha1$";
  out.append( salt, &salt[5] );
  out += "$";
  std::string shastr( salt, &salt[5] );
  shastr.append( pw );

  // run the string through sha
  SHA1Context sha1;
  SHA1Reset( &sha1 );
  SHA1Input( &sha1, (uint8_t*)shastr.c_str(), (unsigned int)shastr.length() );
  uint8_t hash[SHA1HashSize];
  SHA1Result( &sha1, hash );
  
  for ( int i=0; i<SHA1HashSize; ++i )
  {
    char tmp[10];
    sprintf( tmp, "%02x", hash[i] );
    out += tmp;
  }
  return out.c_str();
}


int DBConn::CheckPasswordMatch( const char *pw, const char *dbInfo )
{
  do
  {
    // first field tells us what kind of hash was used
    if ( strncmp( "sha1$", dbInfo, 5 ) )
      break; // not a known hash
    
    // next field is "salt", put that at the beginning of what we'll hash
    std::string salt( &dbInfo[5], &dbInfo[10] );
    
    std::string hash;
    MakePasswordString( hash, salt.c_str(), pw );
    
    return !hash.compare( dbInfo );
    
  } while ( 0 );
  return 0; 
}

// get the UserId of the user matching the given username and password
int DBConn::GetUserId( const char *username, const char *password )
{
  if ( !IsConnectionOK() )
    return NO_CONNECTION;
  do
  {
    Select hStmt( m_hConn );
    
    // 7.  Form a query to run and attach it to the hStmt
    // this basically connects the hStmt up with
    // some results.
    char query[200];
    sprintf( query, "SELECT id,password from auth_user WHERE username='%s'", username );
    int numRows;
    if ( 1 != (numRows = hStmt.Execute( query ) ) )
      break;

    char buf[256];
    SQLLEN numBytes;
    long userId;
    if ( !hStmt.GetLong( 1, &userId ) )
      break;
    if ( !SQL_SUCCEEDED( hStmt.GetText( 2, buf, sizeof(buf), &numBytes ) ) )
      break;
    
    // now looks like this for admin:
    // "sha1$255ce$f142833240a9479016db11829ec12e19129a426c"
    if ( !CheckPasswordMatch( password, buf ) )
      break;

    return userId;
  } while ( 0 );
  
  return 0;
}

// ask if the given user is marked as an administrator
int DBConn::IsAdminUser( int userId )
{
  if ( !IsConnectionOK() )
    return NO_CONNECTION;

  // needs to execute a statement that looks like this:
  // SELECT COUNT() 
  //                      FROM auth_user _user
  //  WHERE _user.id={userId} AND
  //        _user.is_superuser 
  //
  // After all that, if it returns more than one row, then it means access is ALLOWED
  Select hStmt( m_hConn );
  
  char query[2000];
  sprintf( query, "SELECT DISTINCT _user.id "
                  "FROM auth_user _user "
                  "WHERE _user.id=%d AND "
                  "      _user.is_superuser ",
                  userId );
//printf( "%s\n", query );
  if ( !hStmt.Execute( query ) )
    return 0;

  long count = 0;
  SQLLEN numBytes;
  if ( !hStmt.GetLong( 1, &count ) )
    return 0;

  return count > 0;
}

// ask if the given Controller is accessible by the given user ID
int DBConn::ControllerIsAccessibleBy( int unitId, const char *controller, int userId )
{
  if ( !IsConnectionOK() )
    return NO_CONNECTION;

  // needs to execute a statement that looks like this:
  // SELECT COUNT() 
  //                      FROM auth_user _user,
  //                           core_unit_users _unit_users,
  //                           core_unit _unit,
  //                           core_networkcontroller _cont
  //  WHERE _user.id={userId} AND
  //       ( _user.is_superuser OR
  //        ( _user.id=_unit_users.user_id AND
  //          _unit_users.unit_id=_unit.id AND
  //          _cont.id=_unit.controller_id AND
  //          _cont.identifier={controller->GetID()} ) )
  //
  // After all that, if it returns more than one row, then it means access is ALLOWED
  Select hStmt( m_hConn );
  
  char query[2000];
  sprintf( query, "SELECT DISTINCT _unit.id,_cont.id "
                  "FROM auth_user _user, "
                  "     core_unit_users _unit_users,"
                  "     core_unit _unit,"
                  "     core_networkcontroller _cont "
                  "WHERE _user.id=%d AND "
                  "      ( _user.is_superuser OR "
                  "        ( _user.id=_unit_users.user_id AND "
                  "          _unit_users.unit_id=_unit.id AND "
                  "          _unit.subunitid=%d AND "
                  "          _cont.id=_unit.controller_id AND "
                  "          _cont.identifier='%s' "
                  "        )"
                  "      )",
                  userId, unitId, controller );
//printf( "%s\n", query );
  if ( !hStmt.Execute( query ) )
    return 0;

  long count = 0;
  SQLLEN numBytes;
  if ( !SQL_SUCCEEDED( hStmt.GetLong( 1, &count ) ) )
    return 0;

  return count > 0;
}

int DBConn::ScrapeControllerInfo( Select &hStmt, ControllerUnitInfo *info )
{
    
  char buf[256];
  SQLLEN numBytes;
  int fieldsGood=0;
  if ( SQL_SUCCEEDED( hStmt.GetText(1,buf,sizeof(buf), &numBytes ) ) )
  {
    info->identifier = buf;
    ++fieldsGood;
  }

  long lval;
  if ( hStmt.GetLong(2,&lval) )
  {
    info->unitId = lval;
    ++fieldsGood;
  }
    
  if ( SQL_SUCCEEDED( hStmt.GetText( 3, buf, sizeof(buf), &numBytes ) ) )
  {
    info->msidsn = buf;
    ++fieldsGood;
  }

  if ( SQL_SUCCEEDED( hStmt.GetText( 4, buf, sizeof(buf), &numBytes ) ) )
  {
    info->name = buf;
    ++fieldsGood;
  }

  if ( hStmt.GetLong( 5, &lval ) )
  {
    info->unitNum = lval;
    ++fieldsGood;
  }

  if ( hStmt.GetLong( 6, &lval ) )
  {
    info->scanfreq = lval;
    ++fieldsGood;
  }
  if ( SQL_SUCCEEDED( hStmt.GetText( 7, buf, sizeof(buf), &numBytes ) ) )
  {
#ifdef _DO_SYNC
    info->syncInfo.tz = buf;
#endif
    ++fieldsGood;
  }

  return fieldsGood;    
}

int DBConn::GetControllerInfo( const char *identifier, std::vector<ControllerUnitInfo> &infos )
{
  if ( !IsConnectionOK() )
  {
    printf( "GetControllerInfo: No DB connection\n" );
    return NO_CONNECTION;
  }
  Select hStmt( m_hConn );
  
  char query[2000];
  sprintf( query, "SELECT DISTINCT _cont.identifier,_unit.id,_cont.msisdn,_cont.name,_unit.subunitid,_unit.scanfreq,_loc.tz "
                  "FROM core_unit _unit,"
                  "     core_networkcontroller _cont, "
                  "     core_location _loc "
                  "WHERE _cont.id=_unit.controller_id  "
                  "  AND _cont.location_id=_loc.id  "
                  "  AND _cont.identifier='%s'", identifier
                  );

  int numRows;
  if ( (numRows = hStmt.Execute( query ) ) <= 0 )
  {
    printf( "GetControllerInfo: Found no controller with identifier %s\n", identifier );
    return 0;
  }

  do
  {
    ControllerUnitInfo info;
    ScrapeControllerInfo( hStmt, &info );

    infos.push_back(info);
  } while (SQLFetch(hStmt) == SQL_SUCCESS);   

  return infos.size();
}

#ifdef _DO_SYNC
// load startGapDate,stopGapDate,gapHours from database
int DBConn::GetControllerSyncInfo( const char *identifier, int subunit, int scanfreq, SyncInfo &info )
{
#ifdef _DO_SYNC
  if ( info.known ) return 1; // already have this

  char query[2000];
  Select hStmt2( m_hConn );
  
  sprintf( query, "select DATE_FORMAT(CONVERT_TZ(s.ts,'+00:00',l.tz),'%%m%%d%%y') as start_date,"
       "DATE_FORMAT(CONVERT_TZ(UTC_TIMESTAMP(),'+00:00',l.tz),'%%m%%d%%y') as stop_date,"
       "TIMESTAMPDIFF(MINUTE,s.ts,UTC_TIMESTAMP()) as gap, s.scandata, "

       "DATE_FORMAT(CONVERT_TZ(s.ts,'+00:00',l.tz),'%%H%%i' ) as start_time, "
       "DATE_FORMAT(CONVERT_TZ(UTC_TIMESTAMP(),'+00:00',l.tz),'%%H%%i') as stop_time "
 " FROM core_unit_scans s, core_unit u, core_networkcontroller c, core_location l"
" WHERE s.unit_id=u.id AND u.subunitid=%d AND l.id=c.location_id AND l.tz IS NOT NULL"
"   AND u.controller_id=c.id AND c.identifier='%s'"
"   ORDER BY s.ts DESC LIMIT 1;"
, subunit, identifier );

  int numRows;
  if ( (numRows = hStmt2.Execute( query ) ) > 0 )
  {
    // get start and stop date and gap in hours for this controller
    SQLLEN numBytes;

    long val;
    if ( hStmt2.GetLong( 3, &val ) && scanfreq > 0 )
    {
      info.scansMissing = (int)val/scanfreq;
      if ( info.scansMissing > 0 )
      {
        if ( hStmt2.GetLong( 1, &val ) )
          info.startGapDate = val;
        if ( hStmt2.GetLong( 2, &val ) )
          info.stopGapDate = val;
        if ( hStmt2.GetLong( 5, &val ) )
          info.startGapTime = val;
        if ( hStmt2.GetLong( 6, &val ) )
          info.stopGapTime = val;
      }
    }
//printf( "Loaded gap info 1: %d %d %d %d %d?=%d\n", info.startGapDate, info.stopGapDate, (int)val, info.scanfreq, info.scansMissing, (int)val/info.scanfreq );
    if ( SQL_SUCCEEDED( hStmt2.GetText( 4, query, sizeof(query), &numBytes ) ) )
      info.lastScanInDB = query;
//printf( "Loaded gap info 2: %d %d %d %d '%s' (%d)\n", info.startGapDate, info.stopGapDate, info.scanfreq, info.scansMissing, query, numBytes );
  }
  info.known = 1; // we now know the sync info for this controller
#endif
  return 1;
}
#endif

// ask if the given Controller is accessible by the given user ID
int DBConn::FindControllersAccessibleBy( int userId, std::vector<ControllerUnitInfo> &controllers, std::string *outSQL )
{
  if ( !IsConnectionOK() )
    return NO_CONNECTION;

  // needs to execute a statement that looks like this:
  // SELECT COUNT() 
  //                      FROM auth_user _user,
  //                           core_unit_users _unit_users,
  //                           core_unit _unit,
  //                           core_networkcontroller _cont
  //  WHERE _user.id={userId} AND
  //       ( _user.is_superuser OR
  //        ( _user.id=_unit_users.user_id AND
  //          _unit_users.unit_id=_unit.id AND
  //          _cont.id=_unit.controller_id AND
  //          _cont.identifier={controller->GetID()} ) )
  //
  // After all that, if it returns more than one row, then it means access is ALLOWED
  Select hStmt( m_hConn );
  
  char query[2000];
  sprintf( query, "SELECT DISTINCT _cont.identifier,_unit.id,_cont.msisdn,_cont.name,_unit.subunitid,_unit.scanfreq "
                  "FROM auth_user _user, "
                  "     core_unit_users _unit_users,"
                  "     core_unit _unit,"
                  "     core_networkcontroller _cont "
                  "WHERE _user.id=%d AND "
                  "      ( _user.is_superuser OR "
                  "        ( _user.id=_unit_users.user_id AND "
                  "          _unit_users.unit_id=_unit.id )) "
                  "        AND  _cont.id=_unit.controller_id  "
                  "        "
                  "      ",
                  userId );

  if ( outSQL )
    *outSQL = query;
  int numRows;
  if ( (numRows = hStmt.Execute( query ) ) <= 0 )
    return 0;

  do
  {
    ControllerUnitInfo info;

    ScrapeControllerInfo( hStmt, &info );
    
    controllers.push_back( info );

  } while (SQLFetch(hStmt) == SQL_SUCCESS);   

  return controllers.size();
}

int DBConn::VerifyUserPassword( int userId, const char *pw )
{
  if ( !IsConnectionOK() )
    return NO_CONNECTION;

  do
  {
    Select hStmt( m_hConn );
    
    char query[200];
    sprintf( query, "SELECT password from auth_user WHERE id=%d", userId );
    int numRows;
    if ( 1 != (numRows = hStmt.Execute( query ) ) )
      break;

    char buf[256];
    SQLLEN numBytes;
    if ( !SQL_SUCCEEDED( hStmt.GetText( 1, buf, sizeof(buf), &numBytes ) ) )
      break;
    
    // now looks like this for admin:
    // "sha1$255ce$f142833240a9479016db11829ec12e19129a426c"
    if ( !CheckPasswordMatch( pw, buf ) )
      break;

    return 1;
  } while ( 0 );
  return 0;
}

// this removes any controllers from the database that might be lingering there
// if a controller server had crashed
void DBConn::clearControllerList( )
{
  if ( !IsConnectionOK() )
    return;

  do
  {
    Update hStmt( m_hConn );
    char query[200];
printf( "Marking ALL on %s as DISCONNECTED\n", server_ip );

    sprintf( query, "UPDATE core_connection_status SET ip=NULL,disconnected=UTC_TIMESTAMP() WHERE ip='%s' AND disconnected is NULL", server_ip );

    hStmt.Execute( query );
  } while ( 0 );
}
// change a user's password
int DBConn::ChangeUserPassword( int userId, const char *pw )
{
  if ( !IsConnectionOK() )
    return NO_CONNECTION;

  do
  {
    Update hStmt( m_hConn );
    
    std::string hash,salt;
    MakePasswordString( hash, MakeNewSalt(salt), pw );
    
    char query[200];
    sprintf( query, "UPDATE auth_user SET password='%s' WHERE id=%d", hash.c_str(), userId );

    if ( hStmt.Execute( query ) <= 0 )
      break;
    return 1;
  } while ( 0 );
  return 0;
}

/*
Looks like we should log it to this table, core_alarm
CREATE TABLE `core_alarm` (
  `id` int(11) NOT NULL auto_increment,
  `source_id` int(11) NOT NULL,
  `text` longtext,
  `when` datetime NOT NULL,
  `sent` tinyint(1) NOT NULL,
  `viewed` tinyint(1) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `core_alarm_source_id` (`source_id`)
) ENGINE=MyISAM AUTO_INCREMENT=168 ;

DROP TABLE IF EXISTS `alarm_log`;
CREATE TABLE `alarm_log` (
  id int(11) NOT NULL auto_increment,
  timestamp datetime NOT NULL,
  identifier varchar(51) NOT NULL,
  unit_num SMALLINT NOT NULL,
  type VARCHAR(20) NOT NULL,
  message longtext NOT NULL,
  dispatched TINYINT DEFAULT 0,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 ;
*/
const char *escape( const char *in, std::string &out )
{
  for ( ; *in; ++in )
  {
    switch ( *in )
    {
      case '\\' :
        out += "\\\\"; break;
      case '\'' :
        out += "\\'"; break;
      case '"' :
        out += "\\\""; break;
      case '%' :
        out += "\\%"; break;
      case '_' :
        out += "\\_"; break;
      case '\t' :
        out += "\\t"; break;
      case '\x1a' :
        out += "\\Z"; break;
      case '\b' :
        out += "\\b"; break;
      case '\n' :
        out += "\\n"; break;
      case '\r' :
        out += "\\r"; break;
      default :
	out += *in;
    }
  }
  return out.c_str();
}

int DBConn::LogAlarm( const char *mac, const char *typ, int unitNum, const char *msg, const char *minInterval )
{
  if ( !IsConnectionOK() )
    return NO_CONNECTION;

  if ( minInterval && strlen(msg) < 4000 )
  {
    Select sel( m_hConn );
    char buf[5000];
    std::string id_str,msg_str,int_str;
    sprintf( buf, "SELECT COUNT(id) from alarm_log WHERE "
                  "dispatched!=2 AND "
                  "identifier='%s' AND unit_num=%d AND message='%s' AND "
                  "(timestamp+INTERVAL %s) > UTC_TIMESTAMP()",
                  escape(mac,id_str), unitNum, escape(msg,msg_str), escape(minInterval,int_str) );
    int numRows;
    if ( 0 < (numRows = sel.Execute( buf) ) )
    {
      SQLLEN numBytes;
      if ( SQL_SUCCEEDED( sel.GetText( 1, buf, sizeof(buf), &numBytes ) ) )
      {
        int num = strtoul( buf, 0, 10 );
        if ( num > 0 ) // there were non-skipped previous alarms in the database already
        {
//printf("Skipping entry of duplicate alarm '%s' for '%s'\n", msg, mac );
          return 1; // say it worked even though we didn't enter it
        }
      }
    }
  }
  do
  {
    Update hStmt( m_hConn );
    
    std::string query = "INSERT INTO alarm_log "
                    "(timestamp,identifier,unit_num,type,message) "
                    "VALUES (UTC_TIMESTAMP(),'";
    std::string stmp;
    query += escape(mac,stmp);
    query += "',";
    char tmp[50];
    sprintf( tmp, "%d,'%s','", unitNum, typ );
    query += tmp;
    query += escape(msg,stmp);
    query += "')";

    if ( hStmt.Execute( query.c_str() ) <= 0 )
      break;
    return 1;
  } while ( 0 );
  return 0;
}

int DBConn::LogScan( int unitId, const char *datascan )
{
  if ( !IsConnectionOK() )
    return NO_CONNECTION;

  if ( unitId <= 0 ) 
    return 0; // no valid unit ID for this one

  do
  {
    Update hStmt( m_hConn );
    
    std::string query = "INSERT INTO core_unit_scans "
                    "(unit_id,ts,scandata) "
                    "VALUES (";
    char tmp[50];
    sprintf( tmp, "%d", unitId );
    query += tmp;
    query += ",UTC_TIMESTAMP(),'";
    query += datascan;
    query += "')";

    if ( hStmt.Execute( query.c_str() ) <= 0 )
      break;
    return 1;
  } while ( 0 );
  return 0;
}

// this routine will return 1 if MSISDN is not null in this database entry.
// returns 0 if database has MSIDSN as NULL
// return -1 if this controller is not in the database at all.
// this is here so Controller object does not talk directly to database.  Only this server object knows the database login info
int DBConn::IsControllerOnGSM( const char *identifier )
{
  if ( !IsConnectionOK() )
    return -1;

  { // scope to force statment to be freed first
    Select hStmt( m_hConn );
    
    // 7.  Form a query to run and attach it to the hStmt
    // this basically connects the hStmt up with
    // some results.
    char query[200];
    sprintf( query, "SELECT msisdn from core_networkcontroller WHERE identifier='%s'", identifier );
    int numRows;
    if ( 1 != (numRows = hStmt.Execute( query ) ) )
      return -1;

    char buf[256];
    SQLLEN numBytes;
    if ( !SQL_SUCCEEDED( hStmt.GetText(1,buf,sizeof(buf),&numBytes) ) )
      return -1;
    if ( strlen(buf) < 5 )
      return 0;
    return 1;
  }
}

void DBConn::UpdateUnitOwner( const char *identifier, int unitnum, int owner )
{
  if ( !IsConnectionOK() || !have_server_ip )
    return;
  Update hStmt( m_hConn );
    
  std::string query;
  char tmp[50];
  query = "UPDATE core_connection_status "
          "SET owner=";
  sprintf( tmp, "%d", owner );
  query += tmp;
  query += " WHERE identifier='";
  query += identifier;
  query += "'";// AND unitnum=";
//  sprintf( tmp, "%d", unitnum );
//  query += tmp;
  hStmt.Execute( query.c_str() );
}
void DBConn::UpdateConnectionStatus( const char *identifier, int unitnum, int connected, int flags, const char *conn_from_ip )
{
  if ( !IsConnectionOK() )
  {
    printf("UpdateConn failed, conn!=ok\n");
    return;
  }
  if ( !have_server_ip )
  {
    printf("UpdateConn failed, no server ip\n");
    return;
  }
//printf( "%s_%d UpdateConn(%d,%d,%s)\n", identifier, unitnum, connected, flags, conn_from_ip ? conn_from_ip : "" );
  Update hStmt( m_hConn );
    
  std::string query;
  char tmp[50];
  sprintf( tmp, "%d", unitnum );
  if ( connected )
  {
    // do an update only, if we're just modifying flags
    if ( !conn_from_ip && flags >= 0 )
    {
      // this always updates the server_ip and turns off disconnected,
      // since sometimes there is a reconnect where db things appear to
      // happen in the wrong order
      // UPDATE: when updating flags, it only does it if the unit is already
      // on the current server.  Theory is that when a unit moves to a different
      // server, the updates could happen in the wrong order and then the 
      // database would have the system connected to the wrong server.
      query = "UPDATE core_connection_status "
              "SET disconnected=NULL,flags=";
      sprintf(tmp,"%d",flags);
      query += tmp;
      query += " WHERE identifier='";
      query += identifier;
      query += "' AND unitnum=";
      sprintf(tmp,"%d",unitnum);
      query += tmp;
      query += " AND ip='";
      query += server_ip;
      query += "'";
    } else
    {
//printf( "Marking %s CONNECTED\n", identifier );
      query = "REPLACE INTO core_connection_status "
                      "(identifier,unitnum,disconnected";
      query += ",connected,ip,owner";
      if ( conn_from_ip ) query += ",from_ip";
      if ( flags >= 0 ) query += ",flags";
      query += ") VALUES ('";
      query += identifier;
      query += "',";
      query += tmp;
      query += ",NULL,UTC_TIMESTAMP(),'";
      query += server_ip;
      query += "',0";
      if ( conn_from_ip )
      {
//printf( "UpdateConn from_ip=%s\n", conn_from_ip );
        query += ",'";
        query += conn_from_ip;
        query += "'";
      }
      if ( flags >= 0 ) 
      {
        sprintf( tmp, ",%d", flags );
        query += tmp;
      }
      query += ")";
    }
  } else
  {
//printf( "Marking %s DISCONNECTED\n", identifier );
    query = "UPDATE core_connection_status SET disconnected=UTC_TIMESTAMP(),ip=NULL "
            "WHERE disconnected is NULL AND identifier='";
    query += identifier;
    query += "' AND unitnum=";
    query += tmp;
    query += " AND ip='";
    query += server_ip;
    query += "'";
  }

  hStmt.Execute( query.c_str() );
}

void DBConn::DisconnectTunnel( const char *identifier )
{
  if ( !IsConnectionOK() )
  {
    printf("DisconnectTunnel failed, conn!=ok\n");
    return;
  }
  Select hStmt( m_hConn );
  char query[200];
  sprintf( query, "SELECT pid from core_tunnel_status WHERE identifier='%s'", identifier );
  int numRows;
  if ( !(numRows = hStmt.Execute( query ) ) )
    return;

  char buf[256];
  SQLLEN numBytes;
  if ( !SQL_SUCCEEDED( hStmt.GetText(1,buf,sizeof(buf),&numBytes ) ) )
    return;
  int pid = strtoul( buf, 0, 10 );
  sprintf( buf, "kill -INT %d", pid );
  system(buf);
}
#ifdef _DO_SYNC
void DBConn::SyncDatalog( int unit_dbid, int scanfreq, const char *log, SyncInfo *syncInfo )
{
  // datalog will look like this:
  //                CHEMTROL(TM)    DATA LOG
  //     Date      Time     ORP   ORPRT    San    SanRT     pH     pHRT  ...
  // 03/02/17      09:57    200       0    0.0        0    4.95       0  ...
  //
  // for each line that has a date and time, we check it against the range
  //  in syncInfo, and if it falls within the gap, insert it into the database
  // we should probably make up a timestamp based on the time the scan was 
  // taken, rather than NOW() (the time it is inserted)

  const char *line = log;
  const char *eol;
  // rearrange MMDDYY to be YYMMDD so they can be numerically compared
  int gapStartDate = (syncInfo->startGapDate%100)*10000 + 
                     (syncInfo->startGapDate/100)%100 +
                     (syncInfo->startGapDate/10000)*100;
  int gapStopDate =  (syncInfo->stopGapDate%100)*10000 + 
                     (syncInfo->stopGapDate/100)%100 +
                     (syncInfo->stopGapDate/10000)*100;
  std::string hdr;
  while ( line && *line )
  {
    const char *cr = strchr( line, '\r' );
    const char *lf = strchr( line, '\n' );
    if ( lf && lf < cr ) // take minimum of CR or LF as EOL
      eol = lf;
    else
      eol = cr;
    if ( !eol )
      eol = &line[strlen(line)];
    if ( line == eol ) break;
    const char *spc = line;
    while ( isspace(*spc) )
      ++spc;
    int mo,dy,yr,hr,mn;
    if ( 5 == sscanf( spc, "%d/%d/%d\t%d:%d", &mo, &dy, &yr, &hr, &mn ) )
    {
      int dt = (yr*10000)+(mo*100)+dy;
      int tim = hr*100 + mn;
      if ( dt >= gapStartDate && tim > syncInfo->startGapTime &
           dt <= gapStopDate  && tim < syncInfo->stopGapTime  )
      {
//        printf( "Would insert at %d %d using TZ %s\n", dt, tim, syncInfo->tz.c_str() );
        std::string strline;
        strline += hdr;
        strline += "\r\n";
        strline.append( line, eol );
 //       printf( "==> '%s'\n", strline.c_str() );
        Update hStmt( m_hConn );

        std::string query = "INSERT INTO core_unit_scans "
                            "(unit_id,ts,scandata) "
                            "VALUES (";
        char tmp[50];
        sprintf( tmp, "%d", unit_dbid );
        query += tmp;  query += ",";
// TODO: figure out what this should do, since a single scan is 
// recorded in the database using NOW() for the ts value
        query += "CONVERT_TZ('";
        sprintf( tmp, "%04d-%02d-%02d %02d:%02d:00", 
                      yr+2000,mo,dy,hr,mn );
        query += tmp;
        query += "','";
        query += syncInfo->tz.c_str();
        query += "','+00:00'),'";
        query += strline;
        query += "')";

//printf( "SQL '%s'\n",query.c_str() );
        if ( hStmt.Execute( query.c_str() ) <= 0 )
        {
          printf( "Failed to insert sync'd history!\n==>'%s'\n",query.c_str() );
//          break;
        }

      } else
      {
        std::string strline;
        strline.append( line, eol );
 //       printf( "==> '%s'\n", strline.c_str() );
//printf( "Skipping datalog entry: '%s'\n", strline.c_str() );
      }
           
    } else
    {
//printf("Could not interpret: '%.*s'\n", (int)(eol-line), line );
      if ( !strncasecmp("Date",spc,4) )
      {
        hdr.assign( line, eol );
        printf( "HDR '%s'\n", hdr.c_str() );
      }
    }
    
    line = eol;
    while ( line && *line && ( *line == '\r' || *line == '\n' ) )
      ++line;
  }
}
#endif

#endif // ndef _NO_DBCONN
