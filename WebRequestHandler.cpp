#include "predef.h"
#include "WebRequestHandler.h"

WebRequestHandler::WebRequestHandler( Connection *c, 
                    RequestHandler **handlers, int nHandlers, // per-file handlers
                    ScriptHandler **scriptHandler, int nScriptHandlers // insertion handlers
                      )
: m_conn(c),
  m_req(c),
  m_resp(c,&m_req),
  m_finished(0),
  m_handlers(handlers),
  m_nHandlers(nHandlers),
  m_scriptHandlers(scriptHandler),
  m_nScriptHandlers(nScriptHandlers),
  m_status(0),m_index(-1)
{
  GET_CURRENT_TIME(m_timeStarted);
}
WebRequestHandler::~WebRequestHandler()
{
  //assert( m_finished );
  delete m_conn;
}
  
int WebRequestHandler::IsReq( HttpRequest *req )
{
  return &m_req == req;
}

// print out the current state of this request to stdout (which will be console or log)
void WebRequestHandler::DumpState( void (*poutfcn)( void *vparam, const char *fmt ... ), void *vparam )
{
	std::string str;
	if ( !poutfcn ) { poutfcn = (void (*)( void *vparam, const char *fmt ... ))fprintf; vparam = (void*)stdout; }
	if ( !m_req.IsGood() )
	{
	  poutfcn( vparam, "WebReq(%s): req not complete (%s) (Elapsed %d)\n", m_conn->GetAddressAndPort(str), m_req.Uri(), m_req.TimeElapsed() );
	} else
	{
	  poutfcn( vparam, "WebReq(%s): %s.  Status:%d,Index:%d,Finished:%d\n", m_conn->GetAddressAndPort(str), m_req.Uri(), m_status,m_index,m_finished );
	}
}
	
int WebRequestHandler::Service()
{
  INSTRUMENT_THIS_FUNCTION
  // incrementally read everything, then send out response, then done
  if ( !m_req.Service() )
    return 0; // request is still reading headers stuff
      
  if ( !m_req.IsGood() || m_finished )
  {
    m_finished = 1;
    return -1;
  }

  if ( 0 ) //!m_req.IsConnectionLocal() ) // we will always redirect to same servername, with a specific URI
  {
    std::string url = "http://";
    std::string host;
    if ( !m_req.GetHeader( "Host", host ) ) // will come back with port number too, need to remove that
    {
      m_resp.ReturnError( 404, "Invalid request (no Host header)" );
      m_status = RETURNED_INVALID_ERROR;
      return -1;
    }
    url += host.substr(0,strcspn( host.c_str(), ":" ) );
    url += "/cs/"; // note, this must be correct for whatever sysytem config says.  On sa2.us, URI is /cs/ on new linode, it would just be /
    m_resp.SetCookie("UserID", "" ); // clear any possible login cookies we might have had in place
    m_resp.SetCookie("UnitID", "" );
    m_resp.SendRedirect( url.c_str() );
    m_resp.Append( "<HTML><BODY>You are being redirected to an updated URL:<BR>" );
    m_resp.Append( url.c_str() );
    m_resp.Append( "</BODY></HTML>" );
    m_resp.Finish();
    m_finished = 1;
    return -1;
  }
      
  const char *uri = m_req.Uri();

//m_req.Dump();
  const char *ext = strrchr( uri,'.');
  if ( strchr( uri, '\\' ) )
  {
    m_resp.ReturnError( 404, "Invalid request" );
    m_status = RETURNED_INVALID_ERROR;
  } else
  {
    m_status = CHECKING_HANDLERS;
    for ( m_index=0; m_index<m_nHandlers; ++m_index )
    {
      if ( m_handlers[m_index]->HandleRequest( &m_req, &m_resp ) )
      {
        m_status = HANDLER_USED;
        m_finished = m_resp.IsComplete();
        if ( !m_finished && Elapsed(&m_timeStarted) > MAX_REQUEST_TIMEOUT_MS )
        {
          printf( "WebRequestHandler timing out, unfinished: %s\n", m_req.Uri() );
          return -1; // just kill me now!
        }
        return m_finished ? -1 : 0; // somebody handled the request, but may not be done with it yet
      }
    }
    if ( '/' == uri[0] && !strchr( uri+1, '/' ) && !strchr( uri+1, '\\' ))
    {
      // look for actual files in html subdirectory, must be flat, no subdirs
      char fn[MAX_PATH];
      if ( 0 == uri[1] )
        sprintf( fn, "html" OS_SLASH_STR "%s", "index.htm" );
      else      
        sprintf( fn, "html" OS_SLASH_STR "%s", uri+1 );
      m_status = SENDING_FILE_CONTENTS;
      m_resp.SendFileContents( fn, m_scriptHandlers, m_nScriptHandlers );
      m_status = SENT_FILE_CONTENTS;
    } else
    {
      m_req.Dump();
      m_resp.ReturnError( 404, "Not Found" );
      m_status = RETURNED_NOTFOUND_ERROR;
    }
  }
  m_finished = 1;
    
  return m_finished ? -1 : 0;

}
