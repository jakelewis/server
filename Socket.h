#ifndef _included_Socket_h_
#define _included_Socket_h_

#include "predef.h"
#ifdef _WIN32
#include <stdio.h>
#define _WINSOCKAPI_
#include <winsock2.h>
#else
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/poll.h>
#include <sys/socket.h>
#include <netinet/tcp.h>
#include <netdb.h> // for gethostbyname
#include <string.h>
#define ioctlsocket ioctl
#endif
#include <vector>
#include "Stream.h"

// this puts all the socket stuff into one class, so it can 
// have a global list of sockets for a select statement
class Socket : public Stream
{
	int m_listening;
	
  SOCKADDR_IN m_addr;
  	
	// called by accept
  Socket( SOCKET sock, SOCKADDR_IN *addr );
  
public:
  // to create a socket listening on the given port
  Socket( int listenPort, int maxQueue, unsigned long listenaddr=INADDR_ANY );

  // to create outbound connection
  Socket( const char *addr, int port );
  
  virtual ~Socket();

  SOCKADDR_IN *GetAddress();
  int GetRemotePort() { return ntohs(m_addr.sin_port); }
  
  void Close(); // need to override
  
	// not valid for a listening socket
	int Read( void *data, int numBytes );
	int Write( const void *data, int numBytes );
  int StreamType() { return STREAMTYPE_TCP; }
  
  static int64_t GetEth0MAC();
  static int64_t GetWlan0MAC();
  // this is for the pi to communicate actual DHCP-received network settings back to the controller for its display
  static int GetCurrentIPInfo( sockaddr_in *addr, sockaddr_in *mask, sockaddr_in *gw, sockaddr_in *dns );

  // how many bytes are available to be read
  int DataReadable();
  
  // only valid for a listening socket
  Socket *Accept();  

  int SetNonblocking( u_long on )
  {
    return !ioctlsocket(GetFD(),FIONBIO,&on ) ? 1 : -errno;
  }
  
  int SetLinger(int on)
  {
    struct linger ling = { on ? 1 : 0, on ? 1 : 0 };
    return !setsockopt( GetFD(), SOL_SOCKET, SO_LINGER, (const char *)&ling, sizeof(ling) ) ? 1 : -errno;
  }

  int SetKeepalive( int on )
  {
    socklen_t optlen = sizeof(on);
    return !setsockopt(GetFD(), SOL_SOCKET, SO_KEEPALIVE, (const char *)&on, optlen) ? 1 : -errno; 
  }
  int SetNoDelay( int on )
  {
    socklen_t optlen = sizeof(on);
    return !setsockopt(GetFD(), IPPROTO_TCP, TCP_NODELAY, (const char *)&on, optlen) ? 1 : -errno; 
  }
};

#endif //ndef _included_Socket_h_

