#include "predef.h"
#include "HttpRequest.h"

void HttpRequest::fixEscapedChars( std::string &out, std::string &in )
{
  const char *cur = in.c_str();
  out.clear();
  const char *pct;
  while ( pct = strchr( cur, '%' ) )
  {
    out.append( cur, (int)(pct-cur) );
    cur = pct+1;
    if ( cur[0] && cur[1] )
    {
      int val;
      if ( sscanf( cur, "%02X", &val ) )
        out += (char)val;
      cur += 2;
    } else
      break;
  }
  if ( cur[0] )
    out.append( cur );
}
void HttpRequest::readAll( Connection *c, std::string &data, int len )
{
  while ( (int)data.length() < len ) 
  {
    char buf[1];
    int num = c->Read( buf, 1 );
    if ( num < 0 )
      break;
    if ( !num )
    {
      OSTimeDly(0);
      continue;
    }
    if ( !data.length() && buf[0] == 0x0a ) // remnant LF
      continue;
    data += buf[0];
  }
}
HttpRequest::HttpRequest( Connection *c )
: m_conn(c),
  m_good(0),
  m_complete(0),
  m_contentLength(0),
  m_processed(0)
{
#ifndef _WIN32
  clock_gettime(GTOD_CLOCK, &m_start);
#else
  m_start = clock();
#endif
}
int HttpRequest::IsConnectionLocal()
{
  std::string str;
  if ( !strncmp( m_conn->GetAddressAndPort( str ), "127.0.0.1", 9 ) )
    return 1;
  if ( !strncmp( m_conn->GetAddressAndPort( str ), "50.173.15.83", 12 ) ) // Todd's IP, for direct testing with the controller server
    return 1;
  if ( !strncmp( m_conn->GetAddressAndPort( str ), "207.154.78.1", 12 ) ) // SBCS IP, for direct testing with the controller server
    return 1;
  return 0;
}
signed long HttpRequest::TimeElapsed()
{
  signed long elapsed_ms;
#ifndef _WIN32    
  struct timespec now;
  clock_gettime(GTOD_CLOCK, &now);
  elapsed_ms = now.tv_sec*1000 + now.tv_nsec/1000000 
              - (m_start.tv_sec*1000 + m_start.tv_nsec/1000000);
#else
  clock_t now = clock();
  elapsed_ms = (now-m_start)*1000/CLOCKS_PER_SEC;
#endif
  return elapsed_ms;
}
void HttpRequest::Dump()
{
  return;
  if ( !m_complete )
  {
    printf( "[Incomplete request]\n" );
    return;
  }
  // dump the request to stdout
  for ( HeaderList::iterator it = m_headers.begin(); it != m_headers.end(); it++ )
  {
    printf( "%s\n", it->c_str() );
  }
  printf( "%s?%s\n", Uri(), m_params.c_str() );
  if ( m_postData.length() )
    printf( "POST DATA:\n%s\n", m_postData.c_str() );
}
// returns completion: 1 == complete, 0==still need more data
int HttpRequest::Service()
{
  signed long elap = TimeElapsed();
  if ( elap >= MAX_LIFETIME )
  {
//printf( "WRH timed out!\n" );
    return 1; // we've timed out
  }
  if ( m_complete )
    return 1; // no timeout yet, but we've got a complete request
            
  // read request and headers from this connection
  std::string line;
  char buf[1];
  // will use this to timeout the request if 5 seconds pass without
  //     a GET/POST, 0+ headers, and an empty line
  int numAvail;
  while ( (numAvail = m_conn->Available() ) > 0 )
  {
    int num = m_conn->Read( buf, 1 );
    if ( num < 0 )
    {
      m_complete = 1;
      return 1; // done, connection broken
    }
    if ( !num )
    {
      return 0;
    }
    if ( 0x0d == buf[0] ) // line completed
    {
      if ( !line.compare("") ) // empty line at end of headers
      {
          
        if ( !m_verb.compare("POST") && m_contentLength > 0 )
        {
          readAll( m_conn, m_postData, m_contentLength );
          //printf( "[POSTDATA] %s\n", m_postData.c_str() );
        }
        m_complete = 1;
        break;
      }
          
      if ( 0 == m_headers.size() )
      {
        const char *http = line.c_str() + (line.length()-9);
        // protect against bad requests
        m_good = line.length() >= 14 && 
                  ( !strncmp( line.c_str(), "GET ", 4 ) || !strncmp( line.c_str(), "POST ", 5 ) ) && 
                  !strncmp( http, " HTTP/", 6 );
        if ( !m_good )
        {
          m_complete = 1;
          break;
        }
          
        // first space ends verb
        int space = (int)line.find(' ');
        m_verb.append( line.c_str(), line.c_str()+space );
          
        std::string tmp( line.c_str()+1+space, line.length()-10-space );
          
        // fix any %xx chars in the URI
        fixEscapedChars(m_uri,tmp);
          
        int qmark = (int)m_uri.find('?');
        if ( qmark != m_uri.npos )
        {
          m_params = m_uri.substr(qmark+1);
            
          // go through parameters and put in a list? 
            
          m_uri.erase( qmark );
        } else
          m_params.clear();
        std::string addrPort;
        //printf( "[%s %s] %s" " ? %s" "\n", m_conn->GetAddressAndPort(addrPort), m_verb.c_str(), m_uri.c_str(), m_params.c_str() );
//          printf( "[%s] %s" /*" ? %s"*/ "\n", m_verb.c_str(), m_uri.c_str() /*, m_params.c_str()*/ );
          
      }

      m_headers.push_back(line);
      //printf( "   %s\n", line.c_str() ); // uncomment this to see the headers sent by the client
      if ( !_strnicmp( line.c_str(), "Content-Length: ", 16 ) )
        m_contentLength = strtoul( line.c_str()+16, 0, 10 ); 
      if ( m_headers.size() > 50 )
      {
        m_good = 0;
        m_complete = 1;
        break;
      }

      //printf( "[RHdr] %s\n", line.c_str() );
      line.clear();
    } else if ( 0x0a != buf[0] ) // some things might send LF
      line += buf[0];
        
    if ( line.length() > 500 ) // protect from too long of a header
    {
      line.clear();
      m_complete = 1;
      m_good = 0;
      break;
    }
  }
  if ( numAvail < 0 )
  {
printf( "HttpRequest got numAvail=%d\n", numAvail );
    m_complete = 1; // just say it's completed so it will quit
  }
  return m_complete;
}
int HttpRequest::IsGood()
{
  return m_good;
}
void HttpRequest::SetProcessed() // mark this request as processed
{
  m_processed = 1;
}
int HttpRequest::WasProcessed() // see if this request was processed
{
  return m_processed;
}
const char *HttpRequest::Uri()
{
  return m_uri.c_str();
}
const char *HttpRequest::Verb()
{
  return m_verb.c_str();
}
int HttpRequest::GetParamFrom( const char *params, const char *name, std::string &outVal )
{
  const char *ofs = strstr( params, name );
  int nameLen = (int)strlen(name);
  if ( !ofs )
    return 0; // not found at all
  if ( '=' != ofs[nameLen] )  // must have = at end of name
    return GetParamFrom(ofs+nameLen, name,outVal); // might exist later in the params
  if ( ofs > params && ofs[-1] != '&' )
    return GetParamFrom(ofs+nameLen,name,outVal); // if not first, must have & before var name, but might exist later in the line
  outVal.clear();
  ofs += nameLen+1; // skip name and = sign
  std::string tmp;
  while ( *ofs && *ofs != '&' )
    tmp += *ofs++;
  fixEscapedChars( outVal, tmp );
  return 1;
}
int HttpRequest::GetParam( const char *name, std::string &outVal )
{
  if ( !GetParamFrom( m_params.c_str(), name, outVal ) )
    return GetParamFrom( m_postData.c_str(), name, outVal );
  return 1;
}
int HttpRequest::GetCookie( const char *name, std::string &val )
{
  // look through received headers for the cookie of the given name
  std::string prefix;
  prefix += name;
  prefix += "=";
  val.clear();
  for ( HeaderList::iterator it = m_headers.begin(); it != m_headers.end(); it++ )
  {
    if ( !strncmp( it->c_str(), "Cookie: ", 8 ) )
    {
      const char *found = strstr( it->c_str()+8, prefix.c_str() );
      if ( found )
      {
        found += prefix.length();
        const char *end = strchr( found, ';' );
        if ( !end )
          end = &found[strlen(found)];
        val.assign( found, end ); 
      }
      // keep going, because the last one set is the correct one
    }
  }
  return (int)val.length();
}
int HttpRequest::GetHeader( const char *name, std::string &val )
{
  // look through received headers for the header of the given name
  std::string prefix = name;
  prefix += ": ";
  int pfxlen = prefix.length();
  val.clear();
  for ( HeaderList::iterator it = m_headers.begin(); it != m_headers.end(); it++ )
  {
    if ( !strncmp( it->c_str(), prefix.c_str(), pfxlen ) )
    {
      const char *found = it->c_str()+pfxlen;
      const char *end = &found[strlen(found)];
      val.assign( found, end ); 
      return val.length();
    }
  }
  return 0;
}
