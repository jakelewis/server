#ifndef _included_Logging_h_
#define _included_Logging_h_

class Logging
{
public:
  virtual void LogPreSTXChar( unsigned int c ) = 0;
  virtual void OnSTX() = 0; // called when STX received, to show any non-STX chars
  virtual void LogReceivedMsg( const unsigned char *msgContents, int len, const char *msgComment, int hl ) = 0;
  virtual void LogSentMsg( const unsigned char *msgContents, int len ) = 0;
};

#endif//ndef _included_Logging_h_
