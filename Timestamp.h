// OS-independent class that gets time when created, and can format it for output
#ifndef _included_Timestamp_h_
#define _included_Timestamp_h_


#include <time.h>
class Timestamp
{
#ifdef _LINUX_
  struct timespec ts;
#else
    __int64 now,freq;
#endif
public:
  Timestamp()
  {
  #ifdef _LINUX_
    clock_gettime( CLOCK_MONOTONIC, &ts );
  #else
    QueryPerformanceCounter( (LARGE_INTEGER*)&now );
    QueryPerformanceFrequency( (LARGE_INTEGER*)&freq );
  #endif
  }
  char *toString( char *buff )
  {
  #ifdef _LINUX_
    sprintf( buff,"%d.%06d", ts.tv_sec, ts.tv_nsec/1000 ); // show time in the microseconds
  #else
    sprintf( buff,"%d.%06d", (long)(now/freq), (long)(now*1000000/freq)%1000000 ); // show time in the microseconds
  #endif
    return buff;
  }
};

#endif //ndef _included_Timestamp_h_
