/*
 * LCD.cpp
 *
 *  Created on: Oct 3, 2012
 *      Author: todd
 */

#include "predef.h"
#include <stdio.h>
#include <ctype.h>
#include "LCD.h"
#include "HttpResponse.h"
#include "Connection.h"

LCD *g_lcd=NULL;

LCD::LCD()
{
	Clear();
	Message("No Connection");
}

void LCD::Clear()
{
	m_dirty = 1;
	for ( int i=0; i<ROWS*COLUMNS; ++i )
		m_contents[i] = ' ';
	for ( int i=0; i<ROWS*COLUMNS; ++i )
		m_attributes[i] = 0;
}

void LCD::Message( const char *msg, int row )
{
  for ( int i=0; i<COLUMNS && *msg; ++i, ++msg )
  {
    m_contents[i+row*COLUMNS] = *msg;
  }
  m_dirty = 1;
}

#define writestring(s,str) conn->Write(str,(int)strlen(str))

// this came from RemoteController/DefaultDevice.cs 
int ConvertChar(int extend_char)
{
  int pi_Winchar;

  if ( extend_char < 0 )
    extend_char += 256; // this is because a signed char of 0xa6 would show up negative, and we want to look at it as +

  switch ( extend_char)
  {
    case 128: // 0x80
      pi_Winchar = 199; break;
    case 129:
      pi_Winchar = 252; break;
    case 130:
      pi_Winchar = 233; break;
    case 131:
      pi_Winchar = 226; break;
    case 132:
      pi_Winchar = 228; break;
    case 133:
      pi_Winchar = 224; break;
    case 134:
      pi_Winchar = 229; break;
    case 135:
      pi_Winchar = 231; break;
    case 136: // 0x88
      pi_Winchar = 234; break;
    case 137:
      pi_Winchar = 235; break;
    case 138:
      pi_Winchar = 232; break;
    case 139:
      pi_Winchar = 239; break;
    case 140:
      pi_Winchar = 238; break;
    case 141:
      pi_Winchar = 236; break;
    case 142:
      pi_Winchar = 196; break;
    case 143:
      pi_Winchar = 197; break;
    case 144:
      pi_Winchar = 201; break;
    case 145:
      pi_Winchar = 230; break;
    case 146:
      pi_Winchar = 198; break;
    case 147:
      pi_Winchar = 244; break;
    case 148:
      pi_Winchar = 246; break;
    case 149:
      pi_Winchar = 242; break;
    case 150:
      pi_Winchar = 251; break;
    case 151:
      pi_Winchar = 249; break;
    case 152:
      pi_Winchar = 255; break;
    case 153:
      pi_Winchar = 214; break;
    case 154:
      pi_Winchar = 220; break;
    case 155:
      pi_Winchar = 162; break;
    case 156:
      pi_Winchar = 163; break;
    case 157:
      pi_Winchar = 165; break;
    case 158:
      pi_Winchar = 164; break;
    case 159:
      pi_Winchar = 136; break;
    case 160:
      pi_Winchar = 225; break;
    case 161:
      pi_Winchar = 237; break;
    case 162:
      pi_Winchar = 243; break;
    case 163:
      pi_Winchar = 250; break;
    case 164:
      pi_Winchar = 241; break;
    case 165:
      pi_Winchar = 209; break;
    case 166:
      pi_Winchar = 62; break;
    case 167:
      pi_Winchar = 186; break;
    case 168:
      pi_Winchar = 191; break;
    case 169:
      pi_Winchar = 147; break;
    case 176:
      pi_Winchar = 227; break;
    //case 0xA6:
    // pi_Winchar = '>'; break; //1-20-03 fix Foreign chars (prob. w/ AscB())
    default:
      return extend_char;
  }
  return pi_Winchar;
}

void LCD::SendContents( HttpResponse *resp )
{
	m_dirty = 0;
  resp->SetContentType( "text/html; charset=ISO-8859-1" );
	resp->Append( "<pre><span id=LCDUpperLeft class=ptr>" ); // sets pointer for whole area
	for ( int y=0; y<ROWS; ++y )
	{
    // put span at start of row to take care of mouse click->touch events
    char spanbuffer[100];
    sprintf( spanbuffer, "<span onClick=\"onTouch(%d,%d,event);\">", y, -1 );
    resp->Append( spanbuffer );
		unsigned char lastAttr = 0xff;
		for ( int x=0; x<COLUMNS; ++x )
		{
			char c = ConvertChar(m_contents[x+y*COLUMNS]);
			int attr = m_attributes[x+y*COLUMNS];
			if ( lastAttr != attr )
			{
				if ( x > 0 )
					resp->Append( "</span>" );
			  switch ( attr )
			  {
			  	case ATTR_NORMAL :
						resp->Append( "<span class=LcdTxt>" ); break;
			  	case ATTR_INVERSE :
						resp->Append( "<span class=rvsLcdTxt>" ); break;
			  	case ATTR_BLINK :
						resp->Append( "<span class=blnk0LcdTxt>" ); break;
			  	case ATTR_INVERSE|ATTR_BLINK :
						resp->Append( "<span class=blnk0rvsLcdTxt>" ); break;
			  }
				lastAttr = attr;
			}
			resp->AppendContentChar( c );
		} // end for x
      resp->Append( "</span>" );
		resp->Append( "</span>\n" ); // always end the line by ending the style, and a newline (we're inside <PRE>)
	} // end for y
	resp->Append( "</span></pre>" );
}
/*
void LCD::SendContents( Connection *conn )
{
	m_dirty = 0;
	writestring(sock, "<pre>" );
	for ( int y=0; y<ROWS; ++y )
	{
		unsigned char lastAttr = 0xff;
		for ( int x=0; x<COLUMNS; ++x )
		{
			char c = ConvertChar(m_contents[x+y*COLUMNS]);
			int attr = m_attributes[x+y*COLUMNS];
			if ( lastAttr != attr )
			{
				if ( x > 0 )
					writestring(sock, "</span>" );
			  switch ( attr )
			  {
			  	case ATTR_NORMAL :
						writestring(sock, "<span class='LcdTxt'>" ); break;
			  	case ATTR_INVERSE :
						writestring(sock, "<span class='reverseLcdTxt'>" ); break;
			  	case ATTR_BLINK :
						writestring(sock, "<span class='blink0LcdTxt'>" ); break;
			  	case ATTR_INVERSE|ATTR_BLINK :
						writestring(sock, "<span class='blink0reverseLcdTxt'>" ); break;
			  }
				lastAttr = attr;
			}
			if ( c == '<' )
				writestring( sock, "&lt;");
			else if ( c == '>' )
				writestring( sock, "&gt;");
			else
			{
				char str[2] = { c,0 };
				writestring( sock, str );
			}
		} // end for x
		writestring(sock, "</span>\n" ); // always end the line by ending the style
	} // end for y
	writestring(sock, "</pre>" );
}
*/
void LCD::OnLCDUpdate( int row, int col, const unsigned char *data, int len )
{
  if ( row > ROWS ) // some other code...
  {
    m_curRev = col+0x10;
    return;
  }

	int attr = LCD::ATTR_NORMAL;
	for ( int i=0; i<len; ++i )
	{
		char c = data[i];
		switch ( c )
		{
		case SO : // regular text
			attr = LCD::ATTR_NORMAL;
			break;
		case SI : // inverse text
			attr = LCD::ATTR_INVERSE;
			break;
		case HT : // set blinking on (does not affect normal vs. inverse)
			attr |= LCD::ATTR_BLINK;
			break;
		default : // an actual ASCII char
      if ( col > 23 ) 
        printf( "Col: %d\n", col );
			SetChar(row, col++, c, attr);
			break;
		}
	}
}

void LCD::SetChar( int row, int col, char c, unsigned char attr )
{
	if ( row >= 0 && row < ROWS && col >= 0 && col < COLUMNS )
	{
		int index = col+row*COLUMNS;
		m_contents[index] = c;
		m_attributes[index] = attr;
		++m_dirty;
	}
}

