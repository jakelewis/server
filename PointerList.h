#ifndef _included_PointerList_h_
#define _included_PointerList_h_

#include <string.h>

template <class T>
class PointerList
{
  T** m_pList;
  int *m_fList;
  unsigned int m_capacity,m_size;
  int grow()
  {
    if ( m_size+1 > m_capacity )
    {
      unsigned int newCap = 2*m_capacity+10;
      T** newList = (T**)malloc( sizeof(T*)*newCap ); // not using new [] because I don't want exceptions
      int *newfList = (int*)malloc( sizeof(int)*newCap );
      if ( newList && newfList )
      {
        if ( m_pList )
        {
          memcpy( newList, m_pList, m_size*sizeof(T*) );
          free(m_pList);
        }
        m_pList = newList;
        if ( m_fList )
        {
          memcpy( newfList, m_fList, m_size*sizeof(int) );
          free(m_fList);
        }
        m_fList = newfList;
        m_capacity = newCap;
      }
    }
    return m_size < m_capacity;
  }
public:
  PointerList()
  : m_pList(NULL),
    m_fList(NULL),
    m_capacity(0),
    m_size(0)
  {
  }
  ~PointerList()
  {
//    printf( "PointerList %p calling clear (starting with m_size=%d)\n", this, m_size );
    clear();
    if ( m_pList )
    {
//      printf( "PointerList %p calling free(m_pList)\n", this );
      free(m_pList);
    }
    if ( m_fList )
    {
//      printf( "PointerList %p calling free(m_fList)\n", this );
      free(m_fList);
    }
  }
  
  // once this pointer is in my list, I'm in charge of deleting it
  void add( T* p, int noDelete=0 )
  {
    if ( grow() )
    {
      m_pList[m_size] = p;
      m_fList[m_size++] = noDelete;
    }
  }


  
  // get object by its index in the list, returns null for invalid index
  T** getBase()
  {
    return &m_pList[0];
  }
  T* get( unsigned int index )
  {
    if ( index >= m_size )
    {
//      printf( "PointerList[%d] INV[%d] ", index, m_size );
      return (T*)0L;
    }
//    printf( "PointerList[%d] OK[%d] ", index, m_size );
    return m_pList[index];
  }
  
  // query how many objects are in the list
  unsigned int size()
  {
    return m_size;
  }
/*  
  // remove the object by its pointer, return nonzero if it worked
  // this requires a linear search!
  int remove( T* p )
  {
    for ( int i=0; i<m_size; ++i )
    {
      if ( m_pList[i] == p )
      {
        return remove(i);
      }
    }
    return 0;
  }
*/
  // remove the object by its index, return nonzero if it worked
  int remove( unsigned int index )
  {
    if ( index < m_size )
    {
//      printf( "PointerList %p removing object #%d/%d : %p\n", this, index, m_size, m_pList[index] );
      --m_size;
      if ( !m_fList[index] )
        delete m_pList[index]; // delete that object, if approved to do so
      for ( unsigned int i=index; i<m_size; ++i )
      {
        m_pList[i] = m_pList[i+1];
        m_fList[i] = m_fList[i+1];
      }
      return 1;
    }
    return 0;
  }
  void clear()
  {
    while ( m_size > 0 )
      remove(m_size-1); // do it from the end, we won't do a bunch of shuffling
  }
};

#endif //ndef _included_PointerList_h_
