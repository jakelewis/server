#include "predef.h"
#include "SignalList.h"
#include <stdio.h>


#if defined (_LINUX_) && defined( POLLIN ) && defined( POLLERR ) 

int SignalList::grow()
{
  if ( m_numFDs == m_capFDs || !m_pollFDs )
  {
    int newCap = 10 + 2*m_capFDs;
    struct pollfd *newP = (struct pollfd *)malloc( sizeof(struct pollfd)*newCap );
    if ( !newP )
      return 0;
    if ( m_pollFDs )
    {
      memcpy( newP, m_pollFDs, m_capFDs*sizeof(struct pollfd) );
      free(m_pollFDs);
    }
    m_pollFDs = newP;
    m_capFDs = newCap;
  }
  return 1;
}

SignalList::SignalList()
: m_pollFDs(NULL),
  m_numFDs(0),
  m_capFDs(0)
{
}
SignalList::~SignalList()
{
  if ( m_pollFDs )
    free( m_pollFDs );
}
void showlist( struct pollfd* pollFDs, int num )
{
  for ( int i=0; i<num; ++i )
  {
    printf( "SigLIST #%d: %d\n", i, pollFDs[i].fd );
  }
}

int SignalList::AddFD( int fd )
{
  if ( grow() )
  {
    m_pollFDs[m_numFDs].fd = fd;
    m_pollFDs[m_numFDs++].events = POLLIN|POLLPRI|POLLERR|POLLHUP|POLLNVAL|POLLRDHUP;
//    printf( "SignalList::AddFD(%d)\n", fd );
//    showlist( m_pollFDs,m_numFDs);
    return 1;
  }
  printf( "SignalList::AddFD failed to grow for %d\n", fd );
  return 0;
}
void SignalList::RemoveFD( int fd )
{
  int i;
  for ( i=0; i<m_numFDs; ++i )
  {
    if ( m_pollFDs[i].fd == fd )
    {
      --m_numFDs;
//      printf( "SignalList::RemoveFD(%d) at index %d\n", fd, i );
      for ( ; i<m_numFDs; ++i )
      {
//        printf( "Copying index %d with fd %d to index %d with fd %d\n", i+1, m_pollFDs[i+1].fd, i, m_pollFDs[i].fd );
        memcpy( &m_pollFDs[i], &m_pollFDs[i+1], sizeof(struct pollfd ) );
      }
//      showlist( m_pollFDs,m_numFDs);
      return;
    }
  }
  printf( "SignalList::RemoveFD failed to find %d\n", fd );
//  showlist( m_pollFDs,m_numFDs);
}
int SignalList::Poll( long timeoutMS )
{
  if ( !m_numFDs )
  {
    printf( "Poll called with no FDs!\n" );
    return -1;
  }
  for ( int i=0; i<m_numFDs; ++i )
  {
    m_pollFDs[i].revents = 0;
  }
//    printf( "Calling poll with %d fds\n", m_numFDs );
  int ret = poll( m_pollFDs, m_numFDs, timeoutMS );
//    printf( "Back from poll : %d\n", ret );
  return ret;
}
int SignalList::IsReadable( int index )
{
  return m_pollFDs[index].revents & (POLLPRI|POLLIN);
}
int SignalList::HasError( int index )
{
  if ( index >= m_numFDs ) return 0;

  return m_pollFDs[index].revents & (POLLERR|POLLHUP|POLLNVAL|POLLRDHUP);
}
#elif defined( FD_SET )

SignalList::SignalList()
: m_numFDs(0),
  m_maxFD(0)
{
  FD_ZERO( &m_masterFDS );
  FD_ZERO( &m_efds );
  FD_ZERO( &m_rfds );
}
int SignalList::AddSocket( Socket *p )
{
#ifndef _WIN32
  if ( p->m_sock >= FD_SETSIZE )
    return 0; // sock beyond limits!!!
#endif
  FD_SET( p->m_sock, &m_masterFDS );
  if ( (int)p->m_sock > m_maxFD )
    m_maxFD = (int)p->m_sock;
  
  ++m_numFDs;

  return 0;
}
void SignalList::RemoveSocket( Socket *p, int i ) // remove by pointer and/or index, known from other array
{
  --m_numFDs;
  FD_CLR( p->m_sock, &m_masterFDS );
  if ( m_maxFD == p->m_sock ) // removeing our max.... best we can do is reduce max by 1
    --m_maxFD;
}
int SignalList::Poll( long timeoutMS )
{
  if ( !m_numFDs )
    return -1;
  m_rfds = m_masterFDS;
  m_efds = m_rfds;
  timeval to = { timeoutMS/1000,(timeoutMS%1000)*1000 };
  return select( m_maxFD, &m_rfds, NULL, &m_efds, &to );
}
int SignalList::IsReadable( Socket *p, int index )
{
  return FD_ISSET(p->m_sock, &m_rfds );
}
int SignalList::HasError( Socket *p, int index )
{
  return 0;//FD_ISSET(p->m_sock, &m_efds );
}
#endif
