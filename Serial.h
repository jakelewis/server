#ifndef _included_Serial_h_
#define _included_Serial_h_

#ifdef _WIN32
#include <stdio.h>
#define _WINSOCKAPI_
#include <winsock2.h>
#else
#include <sys/types.h>
#include <string.h>
#endif
#include <vector>
#include "Stream.h"

// this puts all the socket stuff into one class, so it can 
// have a global list of sockets for a select statement
class Serial : public Stream
{
	int m_baud;
	
public:
  // to create serial connection
  Serial( const char *fn, int baud);
  
  virtual ~Serial();

	int Read( void *data, int numBytes );
	int Write( const void *data, int numBytes );
  int StreamType() { return STREAMTYPE_SERIAL; }
//  int WriteStr( const char *str );
  
  // how many bytes are available to be read
  int DataReadable();
  
  bool operator == (const Serial &other ) const
  {
    return other.GetFD() == GetFD();
  }
};

#endif //ndef _included_Socket_h_

