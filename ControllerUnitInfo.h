#ifndef _included_ControllerUnitInfo_h_
#define _included_ControllerUnitInfo_h_

#include <string>
#include "timehelp.h"

#ifdef _DO_SYNC
class SyncInfo
{
public:
  SyncInfo()
  : known(0),startGapDate(0), stopGapDate(0),
    startGapTime(0),stopGapTime(0),
    scansMissing(0),
    datalogAttempts(0)
  {
    GET_ZERO_TIME(lastDatalogAttempt);
    GET_CURRENT_TIME(connectionTime);
  }
  int known; // whether this sync info has been correctly loaded from the db
  TIMESTAMP_T connectionTime; // when this unit connected to the server
  // some stuff calculated when unit initially connects
  int startGapDate,stopGapDate; // the dates of the start and stop of the gap
  int startGapTime,stopGapTime; // the dates of the start and stop of the gap
  std::string lastScanInDB;
  int scansMissing;  // how many scans were missing since the last scan in the database, according to the scanfreq setting
  TIMESTAMP_T lastDatalogAttempt;
  int datalogAttempts; // how many times we've attempted to download datalog
  std::string tz; // what timezone the unit is in, according to the db
};
#endif
class ControllerUnitInfo
{
public:
  ControllerUnitInfo() : unitId(0),unitNum(0),scanfreq(0)
  {
  }

  std::string identifier; // MAC-addr type thing of the controller
  int unitId; // db id of the unit
  int unitNum; // user-friendly subunit # of the unit
  std::string name; // name of the controller
  std::string msidsn; // GSM id of the controller
  int scanfreq;

#ifdef _DO_SYNC
  SyncInfo syncInfo;
#endif
};


#endif //ndef _included_DBConn_h_
