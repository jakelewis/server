#! /bin/sh

### BEGIN INIT INFO
# Provides:          chemtrol_server
# Required-Start:    
# Required-Stop:
# Should-Start:
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: SBCS Controller Server Service
# Description:       Intended to start chemtrol_server on system startup.
### END INIT INFO

DESC="SBCS Controller Server Service"
DAEMON=/home/ControllerServer/chemtrol_server_d

PATH=/sbin:/bin:/usr/bin

#. /lib/lsb/init-functions


do_start () {
        echo "Starting chemtrol_server"
        cd /home/ControllerServer ; /home/ControllerServer/chemtrol_server_d
}

do_stop () {
	killall -u controller shell
	PIDFILE="/tmp/chemtrol.pid"
        PID=`cat $PIDFILE`
        [ -e "$PIDFILE" ] && echo "Stopping chemtrol_server at $PID"
        [ -e "$PIDFILE" ] && [ "$PID" != "" ] && [ -d "/proc/$PID" ] && kill -INT "$PID" 
	sleep 1
        [ -e "$PIDFILE" ] && [ "$PID" != "" ] && [ -d "/proc/$PID" ] && kill -INT "$PID" 
        [ -e "$PIDFILE" ] && [ "$PID" != "" ] && [ -d "/proc/$PID" ] && kill -9 "$PID" 
	[ -e "$PIDFILE" ] && echo "Process killed" && rm -f "$PIDFILE"
}

case "$1" in
  start)
        do_start
        ;;
  restart)
	do_stop
	do_start
	;;

  reload|force-reload)
        echo "Error: argument '$1' not supported" >&2
        exit 3
        ;;
  stop)
        # No-op
	do_stop
        ;;
  *)
        echo "Usage: $0 start|stop" >&2
        exit 3
        ;;
esac

