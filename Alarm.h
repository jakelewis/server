#ifndef _included_Alarm_h_
#define _included_Alarm_h_

/* This class is built to receive an alarm message and pass it back 
   to the main server process */

#include "Connection.h"
#include <time.h> // for clock() and clock_t

class Alarm : public Connection
{
  std::string m_msg;
#ifndef _WIN32
  struct timespec m_start;
#else
  clock_t m_start;
#endif
  signed long TimeElapsed();

public:
  class Callback
  {
  public:
    virtual void OnAlarm( const char *mac, const char *typ, int unitNum, const char *msg ) = 0;
  };

  Alarm( Callback *pCB, Socket *pSock );
  ~Alarm();

  void DumpState( void (*poutfcn)( void *vparam, const char *fmt ... ) = NULL, void *vparam=NULL );
  
  // hijacking the callbacks...
  virtual void OnException( Socket *pSock, int events );
	virtual void OnDataAvailable( Socket *pSock );

	// used to check the state of its connection and process any received data
	// returns >0 if it actually did something and wants to be called again
	// return <0 if it needs to be killed off
	// calls Controller::Service, and may process data 
	int Service();

private:
  Callback *m_pCallback;
  int m_done;
  
};

#endif//ndef _included_Alarm_h_
