/*
 * MessageCreator.h
 *
 *  Created on: Oct 3, 2012
 *      Author: todd
 */

#ifndef MESSAGECREATOR_H_
#define MESSAGECREATOR_H_

#include <string.h>

class MessageCreator
{
	unsigned char m_ba[80];
	int m_len;
	unsigned char m_cc;
	typedef unsigned char byte;
	typedef enum
	{
		STX = 0x02,
		ETX = 0x03,
		EOT = 0x04
	} MySpecialChars;
public:
	MessageCreator()
	: m_len(0),
	  m_cc(0)
	{
	}
	int makeMessage( int unitNum, const char *data )
	{
		m_cc = 0;
		m_len = 0;
		if ( unitNum < 0 || unitNum > 999 )
		{
			printf( "Bad unit number: %d", unitNum );
			return -1; // bad unit num
		}
		if ( strlen(data) < 2 ) // there's always at least two chars of data
			return -2; //throw new Exception( "Bad data, length too small: "+data.length() );
		add(STX);
		add((byte) (0x30+(unitNum/100)%10));  // set the unit number
		add((byte) (0x30+(unitNum/10)%10));
		add((byte) (0x30+unitNum%10));
		while ( data && *data )
			add( (byte)*data++ );
		add( EOT ); // this still needs to be part of CC
		add( (byte)(m_cc|0x80) ); // this should not change m_cc
		m_ba[m_len] = 0; // make it null-terminated just in case it's used that way
		return m_len;
	}
	byte *get()
	{
		if ( m_len <= 0 )
			return NULL; //throw new Exception( "MessageCreator::get has no data!");

		return m_ba;
	}
	int length()
	{
		return m_len;
	}

	void add( byte b )
	{
		if ( m_len == sizeof(m_ba) )
			return; //throw new Exception( "MessageCreator ran out of space!");
//iprintf( "%02X ",b );
		m_ba[m_len++] = b;
		m_cc += b;
	}
};

#endif /* MESSAGECREATOR_H_ */
