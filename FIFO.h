/*
 * FIFI.h
 *
 *  Created on: Oct 3, 2012
 *      Author: todd
 */

#ifndef FIFI_H_
#define FIFI_H_

#define _USE_DEQUE
#ifdef _USE_DEQUE
#include <deque>
#endif

//#include "Monitor.h"
#define ENTER // m_cs.enter();
#define LEAVE // m_cs.leave();

// this class had to be updated to be able to be used in the RFB connections, so that it had a much higher limit than the original 8192 bytes

class FIFO : public std::deque<unsigned char>
{
#ifndef _USE_DEQUE
  typedef enum
  {
    FIFOSIZE=5000
  } EValues;
  unsigned char m_buffer[FIFOSIZE];
  int m_readpos,m_writepos;
#endif
public:
	FIFO()
#ifndef _USE_DEQUE
        : m_readpos(0),m_writepos(0)
#endif
	{
		
	}
	int available()
	{
#ifdef _USE_DEQUE
          return size();
#else
          return (m_writepos+FIFOSIZE-m_readpos)%FIFOSIZE;
#endif
	}
	int read()
	{
#ifdef _USE_DEQUE
    if ( !size() ) return -1;
    int ret = front();
    pop_front();
#else
      if ( m_readpos == m_writepos ) return -1; // nothing here to return
    
      int ret = m_buffer[m_readpos];
      m_readpos = (m_readpos+1)%FIFOSIZE;
if ( m_readpos == 0 ) printf( "FIFO %p read index back to 0\n", this );
#endif

		return ret;
	}
	int write( unsigned char c )
	{
#ifdef _USE_DEQUE
    push_back(c);
#else
      int newwrite = (1+m_writepos)%FIFOSIZE;
if ( newwrite == m_readpos )
  printf( "FIFO %p will WRAPPED at %d!\n", this, m_writepos );
     m_buffer[m_writepos] = c;
     m_writepos = newwrite;
if ( newwrite == 0 ) printf( "FIFO %p write index back to 0\n", this );
#endif
		return 0;
	}
	int write( unsigned char *buf, int len )
	{
		int ret =0;
    while ( len-- > 0 )
    {
      write(*buf++);
    }
		return 0;
	}
};

#endif /* FIFI_H_ */
