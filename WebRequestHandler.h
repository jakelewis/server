#ifndef _included_WebRequestHandler_h_
#define _included_WebRequestHandler_h_


#include "HttpRequest.h"
#include "HttpResponse.h"

class RequestHandler
{
public:
  // returns nonzero if it handled the request
  virtual int HandleRequest( HttpRequest *req, HttpResponse *resp ) = 0;
};

class WebRequestHandler
{
  typedef enum
  {
    MAX_REQUEST_TIMEOUT_MS = 30000 // 30sec ought to be enough for me to satisfy ANY request... 
  } EValues;
  HttpRequest m_req;
  HttpResponse m_resp;
  Connection *m_conn; // I am in charge of this, to close it when I'm done
  TIMESTAMP_T m_timeStarted;
  
  RequestHandler **m_handlers;
  int m_nHandlers;
  
  ScriptHandler **m_scriptHandlers;
  int m_nScriptHandlers;

  int m_finished;
  int m_status,m_index;
  typedef enum
  {
    RETURNED_NOTFOUND_ERROR = -2,
    RETURNED_INVALID_ERROR = -1,
    NOT_STARTED=0,
    CHECKING_HANDLERS = 1,
    HANDLER_USED = 2,
    SENDING_FILE_CONTENTS = 3,
    SENT_FILE_CONTENTS = 4
    
  } EStatus;
public:
  WebRequestHandler( Connection *c, 
                      RequestHandler **handlers, int nHandlers, // per-file handlers
                      ScriptHandler **scriptHandler, int nScriptHandlers // insertion handlers
                       );
  ~WebRequestHandler();
  
  int IsReq( HttpRequest *req );

	// print out the current state of this request to stdout (which will be console or log)
	void DumpState( void (*poutfcn)( void *vparam, const char *fmt ... ) = NULL, void *vparam=NULL );
	
  int Service();
};

#endif //ndef _included_WebRequestHandler_h_
