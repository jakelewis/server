/*
 * LCD.h
 *
 *  Created on: Oct 3, 2012
 *      Author: todd
 */

#ifndef LCD_H_
#define LCD_H_

class Connection;
class HttpResponse;

class LCD
{
public:
	typedef enum
	{
		COLUMNS = 22,
		ROWS = 8,

		ATTR_NORMAL = 0,
		ATTR_INVERSE = 0x01,
		ATTR_BLINK = 0x02
	} EValues;
  typedef enum 
  {
  	SO=14,
  	SI=15,
  	HT=9
  } ELCDAttrChars;
private:
	int m_dirty;
  char m_contents[ROWS*COLUMNS]; // array of chars to display
  unsigned char m_attributes[ROWS*COLUMNS]; // flash/reverse, etc
  int m_curRev; // comes as the last row of data: fake row #, col# is the rev #
public:
	LCD();

	int IsDirty() { return m_dirty; }

	// send LCD contents to a connected client
	//void SendContents( Connection *conn );
	void SendContents( HttpResponse *resp );

	// parse LCD data that came from the controller
  void OnLCDUpdate( int row, int col, const unsigned char *data, int len );
	void SetChar( int row, int col, char c, unsigned char attr );

	void Clear();
	void Message( const char *msg, int row=0 );
};

#endif /* LCD_H_ */
