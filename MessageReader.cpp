#include "MessageReader.h"
#include <stdio.h>

// ---------------------------------- MessageReader functions -------------------------------------------
class NullLogging : public Logging
{
public:
  NullLogging() {}

  virtual void LogPreSTXChar( unsigned int c ) {}
  virtual void OnSTX() {}
  virtual void LogReceivedMsg( const unsigned char *msgContents, int len, const char *msgComment, int hl ) {}
  virtual void LogSentMsg( const unsigned char *msgContents, int len ) {}
};

MessageReader::MessageReader( Logging *myLog )
: m_status(STAT_WAITING_START),
  m_myLog(myLog),
  m_bufLen(0)
{
  if ( !myLog )
  {
    static NullLogging nullLog;
    m_myLog = &nullLog;
  }
}
int MessageReader::Read( IStream *pConn, int usesChecksum, Callback * pCallback )
{
	// read message into buffer
	unsigned char c;
	int res;
	while ( STAT_COMPLETE != m_status )
	{
		res = pConn->Read(&c,1);
		if ( 0 == res )
		{
			return MSG_NONE; // didn't receive a char, keep checking
		} else if ( -1 == res )
    {
      printf( "MessageReader got disconnected error back from Connection: %d\n", res );
			return ERR_DISCONNECTED;
    } else if ( res < 0 )
    {
      printf( "MessageReader got unknown error back from Connection: %d\n", res );
      return ERR_UNKNOWN;
    }
		// else, it has better have returned a '1'
		if ( STX == c ) // make it so this always restarts, regardless of status
		{
			//iprintf( "(STX=%X)",c);
      if ( m_bufLen > 0 )
      {
        // message truncated by new STX
        m_myLog->LogReceivedMsg( &(*m_rawbuf.begin()), m_rawbuf.size(), "message truncated by new STX", 0xff0000 );
        if ( pCallback )
          pCallback->OnProtocolError(ERR_RESTART);
      }
			m_cc = STX;
			m_bufLen = 0;
      m_rawbuf.clear();
      m_rawbuf.push_back(c);
			m_status = STAT_READING_MSG;
			continue;
		}
		switch ( m_status )
		{
			case STAT_WAITING_START :
			{
        m_myLog->LogPreSTXChar( (unsigned int)c );
				break;
			}
      case STAT_READING_MSG :
      {
        m_rawbuf.push_back(c);
				if ( ETX == c || EOT == c )
				{
					//iprintf( "(ETX=%X)",c);
          m_endc = c;
					m_cc += c;
					m_cc |= 0x80;
					if ( usesChecksum )
					  m_status = EOT == c ? STAT_AWAITING_CC_EOT : STAT_AWAITING_CC_ETX;
					else
					  m_status = STAT_COMPLETE;
				} else
				{
					if ( m_bufLen+1 >= sizeof(m_buf ) )
					{
            m_myLog->LogReceivedMsg( &(*m_rawbuf.begin()), m_rawbuf.size(), "Msg too long, buffer overrun", 0xff0000 );
//						    iprintf( "Buffer overrun reading message!\n");
            if ( pCallback )
              pCallback->OnProtocolError(ERR_BUFFEROVERRUN);
						m_status = STAT_WAITING_START;
						return ERR_BUFFEROVERRUN;
					}
					m_cc += c;
					//iprintf( "(%X)",c);
					m_buf[m_bufLen++] = c;
					m_buf[m_bufLen] = 0xff; // marker in the buffer for unused data
				}
				break;
			}
			case STAT_AWAITING_CC_ETX :
			case STAT_AWAITING_CC_EOT :
			{
        m_rawbuf.push_back(c);
				if ( !(c & 0x80) ) // not a valid checksum... I've seen this show up here...
				{
          char msg[50];
          sprintf( msg, "Got checksum missing 0x80: 0x%02X after 0x%02X", (int)c, m_status );
          if ( pCallback )
            pCallback->OnProtocolError(ERR_BADCRC);
          m_myLog->LogReceivedMsg( &(*m_rawbuf.begin()), m_rawbuf.size(), msg, 0xff0000 );
				  //printf( "[Rcv invalid checksum: %02X AFTER Rcv %02X\n", (int)c, m_status );
				  return ERR_BADCRC; // wait for another character
				}  
				if ( -1 == res )
				{
				  printf( "[msg left while waiting for checksum!]\n");
          if ( pCallback )
            pCallback->OnProtocolError(ERR_NOCRC);
				  m_status = STAT_WAITING_START;
					break; //return -3; // died waiting for checksum
				}
				if ( (unsigned char)c != (unsigned char)m_cc )
				{
          char msg[50];
				  sprintf( msg,"[Rcvd bad msg CC: rcvd %02X != calcd %02X]\n", c&0xff, m_cc&0xff );
          if ( pCallback )
            pCallback->OnProtocolError(ERR_BADCRC);
          m_myLog->LogReceivedMsg( &(*m_rawbuf.begin()), m_rawbuf.size(), msg, 0xff0000 );
				  int sum=STX+m_status;  // start and stop of the message
				  for ( unsigned int i=0; i<m_bufLen; ++i )
				    sum += (unsigned char)m_buf[i];
				  sum |= 0x80;
				  m_status = STAT_WAITING_START;
					return ERR_BADCRC; // bad CC, no message
			  }
			  m_status = STAT_COMPLETE;
			  break;
			}
		} // end switch
	}
	if ( STAT_COMPLETE == m_status ) 
	{
		m_buf[m_bufLen] = 0; // null-term the data
    m_myLog->LogReceivedMsg( &(*m_rawbuf.begin()), m_rawbuf.size(), "OK", 0x00ff00 );
		m_status = STAT_WAITING_START; // get it set to wait for the next message
		return m_bufLen ? m_bufLen : ( m_rawbuf.size() ? MSG_OK_BUT_EMPTY : 0);
	}
	return MSG_NONE;
}
int MessageReader::ended_with_eot()
{
  return EOT == m_endc;
}
unsigned char *MessageReader::get()
{
  return m_buf;
}
unsigned int MessageReader::size()
{
  return m_bufLen;
}
void MessageReader::reset()
{
  m_bufLen = 0;
}

