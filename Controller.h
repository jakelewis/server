#ifndef _included_Controller_h_
#define _included_Controller_h_

#include <stdio.h>
#include "Connection.h"
#include "LCD.h"
#include <string>
#include "Alarm.h" // for callback interface
#include "PointerList.h" // for subunit list
#include "ControllerUnitInfo.h"
#ifndef _NO_DBCONN
#include "DBConn.h"
#endif
#include "timehelp.h"
#include "Bus.h"
#include "Timestamp.h"

class IController // virtual interface to a controller -- might be talking to primary or to a daisy-chained subunit
{
public:
  virtual ~IController() {} // make it possible to delete these objects using their interface pointer

#ifndef _NO_DBCONN
  // read any extra info from the database about myself
  // read info about potential daisy-chained subunits also
  virtual void UpdateFromDatabase() = 0;
#endif	
  virtual int UpdateFromDBInfo( ControllerUnitInfo *info ) = 0;

  // so external creator of this object can set certain flags based on where the connection occurred.
  // ie, lowercase k for newer controllers that connect on 10004
//#define CONTROLLER_FLAG_LOWERCASE_K "lwrk"
//  virtual void SetFlag( const char *flag ) = 0;

#ifndef _NO_DBCONN
  virtual int IsUnknown() = 0;
#endif
  // if this is called, then the object has been living in memory
  // clongsign a duplicate, and should not make any database updates
  // after this
  virtual void TerminateAsDuplicate() = 0;

  virtual int IsDuplicateOf( IController *other ) = 0;

  // informational calls about this subunit
	virtual const char *GetID() = 0;
	virtual int GetUnitNum() = 0;
#ifndef _NO_DBCONN
	virtual int GetUnitDBID() = 0;
#endif
  virtual IController *GetSubunitById( int subunitnum ) { return NULL; }
  virtual IController *GetSubunitByIndex( int subunitindex ) { return NULL; }
  virtual void SetAllowedSubunits( Bus::UNIT_MASK_T mask ) { } // only implemented by primary 
  virtual Bus::UNIT_MASK_T GetAllowedSubunits() = 0;


	// used to check the state of its connection and process any received data
	// returns >0 if it actually did something and wants to be called again
	// return <0 if it needs to be killed off
	virtual int Service() = 0; 
	
  // send LCD contents of this controller out via the given HTTP response object
	virtual void SendLCDContents( int userNum, HttpResponse *resp ) = 0;

  // ping this subunit
  typedef enum
  {
    PING_DEFAULT = '0', // will respond with alarm or crash report or OK
    PING_CONNDIAG = 'c' // ask for connection diagnostic response (could still rcv default response if the controller doesn't support this)
  } EPingType;
  virtual int Ping( char pingType=PING_DEFAULT ) = 0;

  typedef enum
  {
    NO_ONE = -1,
    NO_UNIT = -1,
    AUTO_USER = 0
  } ESpecialUserNumbers;

  // availability/busy queries
  virtual int IsAvailableTo( int userNum ) = 0;
  virtual int IsAvailableForRemoteControl( int userNum ) = 0;
  virtual int IsAvailableForDataScan( int userNum ) = 0;
  virtual int IsAvailableForDataLog( int userNum ) = 0;
  virtual int IsReceivingLCDContents() = 0;
  virtual int CurrentUser() = 0; // user number, or -1 if no current user
  virtual int CurrentUnit() = 0;
#ifndef _NO_DBCONN
  virtual const char *GetName() = 0; // retrieve name of this controller, empty or NULL if unknown
#endif
  typedef enum
  {
    FREEREASON_UNKNOWN = 0,
    FREEREASON_LIST_REQUESTED = 1,
    FREEREASON_OP_FAILED = 2,
    FREEREASON_TIMEOUT = 3,
    FREEREASON_OP_COMPLETED = 4,
    FREEREASON_DISCONNECT=5

  } EFreeReason;
  virtual void FreeFromOwner( int userNum, int reason=FREEREASON_UNKNOWN ) = 0;

  // user input functions
	virtual int OnKeyHit( int userNum, const char *txt ) = 0; // txt is button name from HTML GUI
	virtual int OnTouch( int userNum, int row, int col ) = 0;

	virtual int CauseRefresh(int changesOnly=0 ) = 0;

	virtual int StartDataLog( int userNum, int dateStart, int dateEnd ) = 0;
  virtual int GetDataLog( HttpResponse *resp ) = 0;

	virtual int StartScan( int userNum, int show_header ) = 0;
  virtual const char *GetScanText( std::string &str ) = 0;
  virtual int GetScan( HttpResponse *resp ) = 0;
	
  // this is for received messages to be passed into the object; subcontrollers will get their data this way from the primary controller
  virtual void OnLCDData( int row, int col, const unsigned char *data, int len ) = 0;
  virtual void OnLogData( const char *log, int is_final ) = 0;
  virtual void OnScanData( const char *scan ) = 0;
  virtual void OnPingResponse( char type, const char *msg ) = 0;
  virtual int OnOperationTimeout( int operation ) = 0; 

	// print out the current state of this controller to stdout (which will be console or log)
	virtual void DumpState( void (*poutfcn)( void *vparam, const char *fmt ... ) = NULL, void *vparam=NULL ) = 0;

  typedef enum
  { 
    FLAG_TIMEOUTS = 1,   // timeouts more than 10% of the commands sent
    FLAG_COMMERR  = 2,   // protocol errors more than 10% of the commands sent
    FLAG_NORESPONSE = 4, // timeouts+1 >= commands sent

    FLAG_UNKNOWN  = 0x1000
  } EFlags;
  virtual int GetFlags() = 0; 

};

class ControllerLogging : public Logging // this case class takes care of logging and dumping for all types of controllers
{
protected:
  class MessageInfo
  {
  public:
    std::vector<unsigned char> msg;
    std::string comment;
    bool received;
    Timestamp ts;
    int highlight;
    MessageInfo( const unsigned char *in_msg, int len, const char *in_comment, bool rcvd, int hl=0 )
    : comment( in_comment ),
      received(rcvd),
      highlight(hl)
    {
      int i;
      for ( i=0; i<len; ++i )
        msg.push_back(in_msg[i]);
    }
    void Dump( std::string &out )
    {
      struct tm *t;
      char buff[200],ascii[200];
      unsigned int i;
//      size_t it;
      std::string tmp;

      if ( highlight )
      {
        sprintf(buff, "<FONT COLOR=\"#%06X\">", highlight );
        out += buff;
      }

      // want a fairly high-res time stamp to be displayed, to evaluate performance
      // to get this, we'll give up having some kind of actual hh:mm:ss time display
      out += ts.toString(buff);

      if ( received )
        out += " RCVD [";
      else
        out += " SENT [";

      for ( i=0; i<msg.size() && i+1 < sizeof(ascii); i++ )
      {
        sprintf( buff, "%s%02X", i > 0 ? " " : "", (int)(unsigned char)msg[i] );
        if ( isprint(msg[i]) )
          ascii[i] = msg[i];
        else
          ascii[i] = '.';
        out += buff;
      }
      ascii[i] = 0;
      out += " '";
      out += ascii;
      out += "'] ";
      out += comment;
      if ( highlight )
      {
        out += "</FONT>";
      }
    }
  };
  typedef std::vector<MessageInfo> MessageLog;
  MessageLog m_messageLog;
  std::vector<unsigned char> m_preSTXchars;

public:
  ControllerLogging() {}
  virtual ~ControllerLogging() {}

  // for the msg reader to log message contents
  void LogPreSTXChar( unsigned int c );
  void OnSTX(); // called when STX received, to show any non-STX chars
  void LogReceivedMsg( const unsigned char *msgContents, int len, const char *msgComment, int hl );
  void LogSentMsg( const unsigned char *msgContents, int len );

	

};

#define UNKNOWN_SUBUNIT -1

class BasicController : public IController, 
                        public ControllerLogging, 
                        public BusRequests // all types implement this: Subs fwd to primarys, primary fwd to bus
{
protected:
#ifdef _DO_SYNC
  SyncInfo m_syncInfo;
#endif
	typedef enum
	{
          MS_USER_INACTIVITY_TIMEOUT = 30000, // if we get no user requests for this long, remove the owner
	  MS_BETWEEN_AUTO_REFRESHES = 3000, // every two seconds
    MS_BETWEEN_KEEPALIVE_REFRESHES = 20000, // every 15 seconds, we'll ask for a screen refresh whether somebody is using the unit or not ... if no response, we know the unit went offline somehow
	    
    MS_BETWEEN_PINGS = 20000,         // every 20 seconds

	  MS_BEFORE_HANGUP = 20000,   // how long before I'll hang up without valid CONNECT string

    MS_BEFORE_HANGUP_AWAITING_REFRESH = 30000, // how long I'll wait an initial refresh response before disconnecting

    MS_BEFORE_GIVEUP_AWAITING_REFRESH = 4000, // how long I'll wait a refresh response before giving up and going to talking mode
    MAX_REFRESH_TIMEOUTS = 3, // if the above refresh timeout occurs 3x, we assume the controller is dead
	  
    MS_SCAN_TIMEOUT = 5000,
    
    MS_DATALOG_TIMEOUT = 60000,

    CURRENT_PING_VER = 0
  } EMiscValues;
  
  virtual int OnRemoteAction( int userNum, int unitNum, int resetTimeout, int doCauseRefresh) = 0;
  virtual void ResetIdleTimer() = 0;
//  virtual int SendKeyHit( int userNum, int unitNum, int keyNum ) = 0;
	virtual int Ping( char pingType ) = 0;

  int m_iUnitNum; // this needs to be set as part of connection negotiation
  int m_scanfreq; // how often I should automatically scan data
  int m_numScans; // count of automated scans since connected
  char m_controllerID[40];
  std::string m_strName; // controller name, if we can find it in the DB
  int m_iUnitDBID; // unit's ID in the core_unit table, if we can find it

	LCD m_lcd;
  int m_isUnknown;
  int m_isOnGSM; // if nonzero, the unit will timeout with inactivity
  int m_dirty;
  int m_bDisconnected;
  int m_is_duplicate;
  int m_flags;

  typedef enum
  {
    STATE_UNKNOWN =   0,    // don't know yet if it's even a controller
    STATE_CONNECTED = 1,    // got a connect string, need to send refresh request
    STATE_AWAITING_FIRST_REFRESH = 2, // sent first refresh request, but no refreshes received yet
    STATE_AWAITING_REFRESH = 3, // sent refresh request, awaiting response
    STATE_TALKING =   4,     // we've received valid pool controller messages

    STATE_SCANDATA = 5,       // we're awaiting a response to a scan data request (response does not start with STX)
    STATE_DATALOG = 6
  } EConnectionState; 
  int m_connState;  // from above

  Alarm::Callback *m_pAlarmCallback; // where to notify about incoming alarms

  std::string m_scanData,m_dataLog;

  int CauseRefreshImpl( int changesOnly );

  int m_refreshTimeouts,m_lTotalTimeouts,m_lPingTimeouts;
  int m_protocolErrors;
  int m_lCommandsSent;
  TIMESTAMP_T m_connStart;


  TIMESTAMP_T m_lastRefreshRequest,m_lastRefreshResponse,
              m_lastPing,m_lastPingResponse,
              m_lastScan,m_scanStart,m_holdoff,
              m_datalogStart,m_lastRemoteAction;

//  virtual int SendData( const unsigned char *msg, int size ) = 0;
public:
  BasicController(Alarm::Callback *pCB);
  virtual ~BasicController() {}

#ifndef _NO_DBCONN
  // read any extra info from the database about myself
  // read info about potential daisy-chained subunits also
  void UpdateFromDatabase();
#endif	
  int UpdateFromDBInfo( ControllerUnitInfo *info );


  // so external creator of this object can set certain flags based on where the connection occurred.
  // ie, lowercase k for newer controllers that connect on 10004
//#define CONTROLLER_FLAG_LOWERCASE_K "lwrk"
//  void SetFlag( const char *flag );

  int IsUnknown();
  int GetFlags();
  void UpdateFlags();

  void TerminateAsDuplicate();
  int IsDuplicateOf( IController *other );

  const char *GetName(); // retrieve name of this controller, empty or NULL if unknown

	int OnKeyHit( int userNum, const char *txt );
	int OnTouch( int userNum, int row, int col );
	int CauseRefresh(int changesOnly=0 );
	int StartDataLog( int userNum, int dateStart, int dateEnd );
  int GetDataLog( HttpResponse *resp );
	int StartScan( int userNum, int show_header );
  const char *GetScanText( std::string &str );
  int GetScan( HttpResponse *resp );
	
  // the unit ID from the CONNECT string
	const char *GetID() { return m_controllerID; } 
	int GetUnitNum() { return m_iUnitNum; }
	int GetUnitDBID() { return m_iUnitDBID; }

	void SendLCDContents( int userNum, HttpResponse *resp );

  void OnLCDData( int row, int col, const unsigned char *data, int len );
  void OnLogData( const char *log, int is_final );
  void OnScanData( const char *scan );
  void OnPingResponse( char type, const char *msg );
  int OnOperationTimeout( int op );

  void DumpState( void (*poutfcn)( void *vparam, const char *fmt ... ) = NULL, void *vparam=NULL );

	// used to check the state of its connection and process any received data
	// returns >0 if it actually did something and wants to be called again
	// return <0 if it needs to be killed off
	int Service();
	
};

// this class represents a daisy-chained controller that need to communicate via a PrimaryController (which owns the socket connection)
class PrimaryController; // forward declaration so sub controller can have a pointer to primary controller
class SubController : public BasicController
{
  PrimaryController *m_primary;
public: 
  SubController( ControllerUnitInfo &unitInfo, PrimaryController *primary, Alarm::Callback *pCP );
  virtual ~SubController();

  Bus::UNIT_MASK_T GetAllowedSubunits() { return BUS_UNIT_MASK(BasicController::m_iUnitNum); }

  // all of these functions need to forward the call to the primary 
  int IsAvailableTo( int userNum );
  int IsAvailableForRemoteControl( int userNum );
  int IsAvailableForDataScan( int userNum );
  int IsAvailableForDataLog( int userNum );
  int IsReceivingLCDContents();
  int CurrentUser();
  int CurrentUnit();
  void FreeFromOwner( int userNum, int reason );
  int OnRemoteAction( int userNum, int unitNum, int resetTimeout, int doCauseRefresh);
  void ResetIdleTimer();
  //int SendKeyHit( int userNum, int unitNum, int keyNum );
  //int SendData( const unsigned char *msg, int size );

  int Ping( char pingType );

  // BusRequest functions, all forwarded to the primary controller
  virtual int SendKeyPress( int unitNum, int keyNum );
  virtual int SendTouch( int unitNum, int row, int col );
  virtual int RequestRefresh( int unitNum, int changesOnly );
  virtual int RequestDatalog( int unitNum, int dateStart, int dateEnd );
  virtual int RequestScan( int unitNum, int show_header );
  virtual int Ping( int unitNum, char pingType );

};

class PrimaryController : public BasicController, public Bus::Callback
{
  Bus m_bus;
  PointerList<SubController> m_subUnits;

  std::string m_lastAlarmMsg; // the last alarm message we got
  TIMESTAMP_T m_lastAlarmTime; // the last time we got it
  unsigned long m_lastAlarmDuplicates; // count of duplicates

  TIMESTAMP_T m_lastUpdateFromDB; // the last time we got our info from database
//  void ParseFlags( char *txt );

  // this will return -1 if the client end does not send a correct connect string within the amount above
  int CheckForMessage();
  int CheckForScanData();
  int CheckForDataLog();

  std::string m_connString; // this will be appended-to up to a max size until a CR is received

  void UpdateOwner( int usernum, int unit );
  int m_controlledBy,  // which user is controlling the unit
      m_controlledUnit; // which subunit the user is controlling

public:
  void DumpState( void (*poutfcn)( void *vparam, const char *fmt ... ), void *vparam );

  const char *GetRemoteAddress( std::string &ap ) { return m_bus.GetRemoteAddress(ap); }

  int SendData( const unsigned char *msg, int size );
  void ResetIdleTimer();
  int OnRemoteAction( int userNum, int unitNum, int resetTimeout, int doCauseRefresh );

//	int SendKeyHit( int userNum, int unitNum, int keyNum );
  TIMESTAMP_T m_lastActive;
public:

	PrimaryController( int older, Socket *sock, Alarm::Callback *pCB );
	virtual ~PrimaryController();

#ifndef _NO_DBCONN
  // read any extra info from the database about myself
  // read info about potential daisy-chained subunits also?
  virtual void UpdateFromDatabase();
#endif	

  void SetAllowedSubunits( Bus::UNIT_MASK_T mask );
  Bus::UNIT_MASK_T GetAllowedSubunits();
  virtual IController *GetSubunitById( int subunitnum );
  virtual IController *GetSubunitByIndex( int subunitindex );

  int IsAvailableTo( int userNum );
  int IsAvailableForRemoteControl( int userNum );
  int IsAvailableForDataScan( int userNum );
  int IsAvailableForDataLog( int userNum );
  int IsReceivingLCDContents();
  int CurrentUser(); // user number, or -1 if no current user
  int CurrentUnit(); // which unit # is currently being controlled
  void FreeFromOwner( int userNum, int reason );

  int Ping( char pingType ) { return Ping(m_iUnitNum, pingType ); }

  int Service();

  // callbacks from bus object
  virtual Bus::UNIT_MASK_T OnUnknownData( const char *data, int len, const char *addrAndPort );
  virtual void OnPingResponse( int unitNum, char type, const char *msg );
  virtual void OnRefreshData( int unitNum, int row, int col, unsigned char *data, int len );
  virtual void OnLogData( int unitNum, const char *logData, int is_final );
  virtual void OnScanData( int unitNum, const char *scanData );
  virtual int OnOperationTimeout( int unitNum, int operation );
  void OnMessageError( int curUnit, int err );
  void OnProtocolError( int err ); // this happens at bus parsing level
  virtual void OnConnectionDropped();

  // BusRequest functions, all forwarded to the bus object
  virtual int SendKeyPress( int unitNum, int keyNum );
  virtual int SendTouch( int unitNum, int row, int col );
  virtual int RequestRefresh( int unitNum, int changesOnly );
  virtual int RequestDatalog( int unitNum, int dateStart, int dateEnd );
  virtual int RequestScan( int unitNum, int show_header );
  virtual int Ping( int unitNum, char pingType );

};

#endif //ndef _included_Controller_h_
