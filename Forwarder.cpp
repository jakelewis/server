#include "predef.h"
#include "Forwarder.h"

Forwarder::Forwarder( Socket *pClient, const char *fwdAddr, int fwdPort )
: //m_pClient(pClient),
  //m_pServer(NULL),
  m_cliConn(pClient),
  m_srvConn(new Socket(fwdAddr,fwdPort)),
  m_bClosed(1)
{
  GET_TIMEOUT_TIME(m_connTimeoutAt,30000); // make it so if we don't see data transferred within 30 seconds, we hang up
/*
  if ( m_pClient )
  {
    // create outbound connection to forward all traffic
    m_pServer = new Socket( fwdAddr, fwdPort ); // make an outbound connection
    if ( m_pServer && m_pServer->IsOpen() )
    {
      m_pClient->SetKeepalive(1);
      m_pClient->SetNonblocking(1);
      m_pClient->SetCallback(this);

      m_pServer->SetKeepalive(1);
      m_pServer->SetNonblocking(1);
      m_pServer->SetCallback(this);
      m_bClosed = 0;
    }
  }*/
}

Forwarder::~Forwarder()
{
//  if ( m_pClient )
//    delete m_pClient;
//  if ( m_pServer )
//    delete m_pServer;
}
/*
void Forwarder::OnDataAvailable( Socket *pSock )
{
  Socket *outSock = pSock == m_pClient ? m_pServer : m_pClient;

  int res;
  // read a byte into the FIFO
  if ( outSock )
  {
    unsigned char c;
    while ( 1 == (res=pSock->Read( &c, 1 ) ) ) // should be fine if non-blocking
    {
      if ( 1 != (res = outSock->Write( &c, 1 ) ) )
        break;
    }
  } else
    res = -1;
  if ( res <= 0 ) // graceful close or error
  {
    if ( WSAGetLastError() != WSAEWOULDBLOCK )
    {
      if ( m_pClient )
        m_pClient->Close();
      if ( m_pServer )
        m_pServer->Close();
      m_bClosed = 1;
    }
  }
}
void Forwarder::OnException( Socket *pSock )
{
  if ( m_pClient )
    m_pClient->Close();
  if ( m_pServer )
    m_pServer->Close();
  m_bClosed = 1;
}
*/
int Forwarder::Service( )
{
  char buf[4096];
  int bytesTransferred = 0;
  Connection *srcdst[2] = { &m_cliConn, &m_srvConn };
  for ( int loop=0; loop<2; ++loop )
  {
    int avail = srcdst[loop]->Available();
    if ( avail < 0 ) // the connection indicating it's been cut
    {
      return -1;
    }
    while ( avail )
    {
      GET_TIMEOUT_TIME(m_connTimeoutAt,30000); // make it so if we don't see data transferred within 30 seconds, we hang up
      int toRead = avail > sizeof(buf) ? sizeof(buf) : avail;
      int numRead = srcdst[loop]->Read( buf, toRead );
      if ( numRead < 0 )
        return -1;
      int nWrote = 0;
      do
      {
        int res = srcdst[1-loop]->Write( &buf[nWrote], numRead-nWrote );
        if ( res <= 0 )
          return -1;
        nWrote += res;
      } while ( nWrote < numRead );
      avail -= numRead;
      bytesTransferred += numRead;
    }
  }
  if ( HasPassed( &m_connTimeoutAt ) )
    return -1; // hang up: delete me
  return bytesTransferred ? 1 : 0;
}
