#include "predef.h"
#include "Bus.h"
#include "MessageCreator.h"
#include "LCD.h"
#include "ErrorLog.h"

//#define LCD_DEBUG printf
#define LCD_DEBUG (void)

//#define BUS_STATE_DEBUG printf
#define BUS_STATE_DEBUG (void)


// 485 bus speeds:
//   2100c runs at 115200
//   2100b runs at 9600 baud
//   older 3000 ones run at 2400 baud

//  port 10003 will not respond to ping (older units), use partial refresh periodically
//  port 10004 will respond to ping (do every 10 seconds)

// in general, only do refreshes during remote control

// commands whose responses do NOT include unit numbers must 
//    Own and Lock the bus until a response is received or timeout expired.

int Bus::Take( int unitNum, int operation, int msTimeout )
{
  if ( unitNum<=0 || unitNum > 30 ) 
  {
    ErrorLog::logf( "Invalid unit %d requested for op %d\n", unitNum, operation );
    return BUS_INVALIDUNIT;
  }

  if ( operation != OP_PING && !(m_available_units & BUS_UNIT_MASK(unitNum)) ) // only allow pings to units not in our current list
  {
BUS_STATE_DEBUG( "[DISA:%X %d] ", operation, unitNum );
    //printf( "Disabled unit %d requested for op %d\n", unitNum, operation );
    return BUS_DISABLEDUNIT; // subunit is not enabled in our current mask
  }

  if ( OP_NONE != m_operation && HasPassed( &m_when_becomes_idle ) ) // the pending operation has timed out
  {
    // notify about the timeout
    Cancel();
    // fall through to taking ownership
  }

  //if ( (operation & OPS_NEEDING_LOCK) )
    //return 1; // no ownership needed

  if ( OP_NONE != m_operation )
  {
BUS_STATE_DEBUG( "[BUSY:%X] ", m_operation );
    return BUS_BUSY;
  }
BUS_STATE_DEBUG( "[TAKE %X %d] ", operation, unitNum );
  m_current_owner = unitNum;
  m_operation = operation;
  GET_TIMEOUT_TIME( m_when_becomes_idle, msTimeout );
  return 1;
}

// called from whatever location has realized that an operation (with a locked bus) has timed out
void Bus::Cancel()
{
  if ( OP_NONE != m_operation )
  {
    // log that this operation timed out
    unsigned char empty[2] = { 0,0 };
    char buf[200];
    sprintf( buf, "TIMEOUT (op=%d)", m_operation );
    m_myLog->LogReceivedMsg( empty,0,buf,0xff0000 );

    // notify caller about an operation timing out
    int owner=m_current_owner,op=m_operation;
BUS_STATE_DEBUG( "[CANC %X %d] ", m_operation, m_current_owner );
    Release();
    if ( m_pCallback->OnOperationTimeout( owner, op ) )
    {
      printf( "Bus calling close due to too many timeouts\n" );
      Close();
    }
    if ( owner > 1 ) // for subunits, disable them in the enabled mask until they respond to ping again
    {
      m_available_units &= ~BUS_UNIT_MASK(owner);
    }
  }
}

void Bus::Release()
{
  if ( OP_NONE != m_operation )
  {
BUS_STATE_DEBUG( "[RELE %X %d] ", m_operation, m_current_owner );

    m_operation = OP_NONE;
    m_current_owner = 0;
    GET_ZERO_TIME(m_when_becomes_idle);
  }
}


Bus::Bus( int is_older, Socket *sock, Callback *pCallback, Logging *thelog )
: m_is_older(is_older),
  m_connection(sock),
  m_pCallback(pCallback),
  m_reader( thelog ),
  m_myLog(thelog),
  m_operation(OP_CONNECT),      // doing initial connection
  m_current_owner(1), // owned by unit #1 until connect is done
  m_ping_units(BUS_UNIT_MASK(1)), // no subunits known except for the default 1
  m_available_units(0), // wait until pings work
  m_supportsLowercaseK( is_older ? 0 : BUS_UNIT_MASK(1) ),
  m_requested_connstring(0)
{
  sock->SetNoDelay(1);
  m_lastSentMsg[0] = 0;
  GET_ZERO_TIME(m_when_becomes_idle); // irrelevant, it's already idle
  GET_CURRENT_TIME(m_connStart);
  GET_CURRENT_TIME(m_lastActive);
  GET_TIMEOUT_TIME(m_when_becomes_idle,TIMEOUT_CONNECT);
}

// needs at least 20 chars in the buffer
char *make_timestamp( char *buf )
{
  time_t ltime;
  struct tm *Tm;

  ltime=time(NULL);
  Tm=localtime(&ltime);

  sprintf(buf,"%04d/%02d/%02d %02d:%02d:%02d",
          Tm->tm_year+1900,
          Tm->tm_mon+1,
          Tm->tm_mday,
          Tm->tm_hour,
          Tm->tm_min,
          Tm->tm_sec);
  return buf;
}

void timestamp()
{
  char buf[30];
  printf("%s ", make_timestamp(buf) );
}

int Bus::ReadAvailableData( std::string &strData )
{
	// read message into buffer
	char c;
	int res,avail;
  int nRead = 0;
	while ( (avail=m_connection.Available()) > 0 )
	{
    while ( avail > 0 )
    {
      char buf[2048];
      int toRead = avail;
      if ( toRead+1 > sizeof(buf) )
        toRead = sizeof(buf)-1;
		  res = m_connection.Read( (unsigned char*)&buf[0],toRead );
		  if ( 0 == res ) // nothing available, would be strange after available said there is
		  {
		    break;
		  } else if ( res < 0 )
      {
        m_pCallback->OnConnectionDropped();
        m_connection.Close();
			  return res; // error on connection
      }
      buf[res] = 0; // null-term
      strData += buf;
      avail -= res;
      nRead += res;
    }
	}
  if ( avail < 0 )
  {
printf( "Bus::ReadAvailableData got %d avail\n", avail );
    return avail;
  }
  return nRead;
}

const char *Bus::GetRemoteAddress( std::string &ap )
{
  return m_connection.GetAddressAndPort(ap);
}

int Bus::Service()
{
  int avail = m_connection.Available();
  if ( avail < 0 ) // the connection indicating it's been cut
  {
    std::string ap;
  	iprintf("Connection %s dropped (avail<0, got %d)\n", m_connection.GetAddressAndPort(ap), avail );
		m_pCallback->OnConnectionDropped();
    m_connection.Close();
    return -1;
  }
  
  if ( OP_CONNECT == m_operation ) // we are in the initial connection stage
  {
    std::string addr;
    if ( HasPassed( &m_when_becomes_idle ) ) // connection timeout has expired
    {
      if ( !m_requested_connstring ) // we have not yet sent the request for the connection string
      {
        m_requested_connstring = XmitMessage( "CC", 999 ) > 0;
        if ( m_requested_connstring )
          printf( "Requested connect string.\n" );

        GET_TIMEOUT_TIME(m_when_becomes_idle,TIMEOUT_CONNECT);
        return 0;
      } else
      {
        ErrorLog::logf( "Hanging up on unresponsive client on controller port [%s]", m_connection.GetAddressAndPort(addr) );
		    m_pCallback->OnConnectionDropped();
        return -1; // we're done here
      }
    }
    std::string strData;
    int res = ReadAvailableData( strData );
    if ( res <= 0 ) return res;
    UNIT_MASK_T mask = m_pCallback->OnUnknownData( strData.c_str(), strData.length(), m_connection.GetAddressAndPort(addr) );
    if ( UNIT_MASK_INVALID == mask )
    {
      // complaint will be printed by the callee above
      printf( "Got bad connect data: %s\n==> ", strData.c_str() );
      for ( int i=0; i<strData.length(); ++i )
        printf( "%02X ", 0xff&(int)strData[i] );
      printf("\n");
		  m_pCallback->OnConnectionDropped();
      return -1; // we're done here
    }
    else if ( mask != 0 ) // we got a unit ID mask returned, meaning we're done with this "operation"
    {
      m_available_units |= BUS_UNIT_MASK(1); // assume it's unit #1 that initiated the connection (will be a requirement to always have a unit #1)
      m_ping_units |= mask; // additional sub units will need to be pinged first
BUS_STATE_DEBUG( "[CONNOK] ");
      Release(); // operation no longer holds the bus
    } else
      return 0;
  } 
  
  if ( OP_NONE != m_operation )
  {
    if ( HasPassed( &m_when_becomes_idle ) )
    {
      // TODO: track failures so we can hangup if unit is not responding
      int owner = m_current_owner;
      Cancel();
//printf( "Operation %d timed out for %d\n", m_operation, owner );
      if ( m_lastSentMsg[0] == 'k' ) // lowercase k command timed out, assume that one does not work!
      {
        m_supportsLowercaseK &= ~BUS_UNIT_MASK(owner);
        m_lastSentMsg[0] = 'r'; // don't track this problem again
      } else if ( 'p' == m_lastSentMsg[0] )
      {
        if ( owner != 1 ) // only subunits can become disabled
          m_available_units &= ~BUS_UNIT_MASK(owner); // make this unit no longer valid
      }
    }
  }
  
  if ( OP_DATALOG == m_operation ) // we are awaiting the response to a datalog request
  {
    int res = ReadAvailableData( m_dataLog );
    if ( res < 0 ) return res;
    else if ( res > 0 )
    {
      GET_TIMEOUT_TIME( m_when_becomes_idle, TIMEOUT_DATALOG ); // reset this timeout as long as we're receiving data
      m_myLog->LogReceivedMsg( (unsigned char*)&(m_dataLog[m_dataLog.length()-res]), res, "LOGDATA", 0x00ff00 );
      char c = m_dataLog[m_dataLog.length()-1];
      if ( c == MessageReader::EOT || c == MessageReader::ETX )
      {
        if ( strstr( m_dataLog.c_str(), "CONNECT") ) // problem!  we rebooted the device
        {
          m_dataLog.append( "\n*** This device appears to have rebooted! ***\n" );
        }
        // message is complete, there won't be any more
        if ( !strncmp( "dd", m_dataLog.c_str(), 2 ) )
        {
          m_dataLog = m_dataLog.substr(2,m_dataLog.length()-3); // also get rid of the EOT/ETX at the end
        }
        m_lastSentMsg[0] = 'r'; // after this scan response, subsequent messages will be LCD updates
        m_pCallback->OnLogData( m_current_owner, m_dataLog.c_str(), 1 );
printf( "Datalog download complete\n" );
BUS_STATE_DEBUG( "[LOGOK] ");
        Release();
      } else
        m_pCallback->OnLogData( m_current_owner, m_dataLog.c_str(), 0 );
      return 1;
    }
    return 0;
  } 
  else if ( OP_SCAN == m_operation ) // we are awaiting the response to a scan request
  {
    int res = ReadAvailableData( m_scanData );
    if ( res < 0 ) return res;
    else if ( res > 0 )
    {
      m_myLog->LogReceivedMsg( (unsigned char*)&(m_scanData[m_scanData.length()-res]), res, "SCANDATA", 0x00ff00 );
      char c = m_scanData[m_scanData.length()-1];
      if ( c == MessageReader::EOT || c == MessageReader::ETX )
      {
        if ( strstr( m_scanData.c_str(), "CONNECT") ) // problem!  we rebooted the device
        {
          m_scanData.append( "\n*** This device appears to have rebooted! ***\n" );
        }
        // message is complete, there won't be any more
        if ( !strncmp( "ss", m_scanData.c_str(), 2 ) )
        {
          m_scanData = m_scanData.substr(2);
        }
        // remove carriage returns and/or linefeeds from the scan data
        while ( m_scanData.length() &&
                ( *m_scanData.rbegin() == '\n' || *m_scanData.rbegin() == '\r' ||
                  *m_scanData.rbegin() == MessageReader::EOT || *m_scanData.rbegin() == MessageReader::ETX )
              )
        {
          m_scanData.erase( m_scanData.length()-1 );
        }
			  m_lastSentMsg[0] = 'r'; // after this scan response, subsequent messages will be LCD updates
        int owner = m_current_owner;
//printf( "Bus::OnScanData %d %s\n", owner, m_scanData.c_str() );
        m_pCallback->OnScanData( owner, m_scanData.c_str() );
        Release();
	m_scanData.clear();
BUS_STATE_DEBUG( "[SCANOK %d] ", owner );
      }
      return 1;
    }
    return 0;
  }

  // at this point, we know we have controller(s) connected, and need to parse a messsage if there is one
	avail = m_connection.Available();
  if ( avail < 0 )
  {
    std::string str;
    printf("Connection dropped (avail<0,avail=%d) %s\n",avail, GetRemoteAddress(str));
    m_pCallback->OnConnectionDropped();
    m_connection.Close();
		return -1;
  }
//printf( "Bus reading %d bytes\n", avail );
	int res = m_reader.Read( &m_connection, m_lastSentMsg[0] != 's', m_pCallback );
	if ( MessageReader::ERR_DISCONNECTED == res )
	{
		printf("Connection dropped (readres=%d,avail=%d)\n",res,avail);
		m_pCallback->OnConnectionDropped();
    m_connection.Close();
		return -1;
	}
	if ( MessageReader::MSG_NONE == res )
	{
		return 0; // no complete messsage at this time
	}
  if ( res < 0 ) // remaining error codes mean a CRC problem of some kind
  {
    int owner = m_current_owner;
BUS_STATE_DEBUG( "[ERR %d %d] ", m_operation, owner );
    Cancel();
    m_pCallback->OnMessageError( owner, res );
    return 0;
  }
  // assume that an alarm may come through unbidden, and it's format is good enough for us to parse without knowing context
	unsigned char *pl = m_reader.get();
  int plSize = m_reader.size();
  m_reader.reset(); // this is not destructive to payload string, just to a buffer size member
  if ( MessageReader::MSG_OK_BUT_EMPTY == res && m_reader.ended_with_eot() ) // could be STX EOT CC
  {
    int owner = m_current_owner;
BUS_STATE_DEBUG( "[EMPTY %d] ", owner );
    Release();
    m_pCallback->OnRefreshData( owner, 0, 0, NULL, 0 );
LCD_DEBUG( "[LCDEND1]\n" );
    return 1; // got valid empty message, which would end ownership of the bus
  }
  int unitId =  (pl[0] >= '0' && pl[0] <= '9' && pl[1] >= '0' && pl[1] <= '9' && pl[2] >= '0' && pl[2] <= '9') ?
                  ((int)(pl[0]-'0')*100 + (int)(pl[1]-'0')*10 + (int)(pl[2]-'0')) :
                  -1;
  // figure out what to do with this response
  // parse and use this message
	//iprintf( "We thought %d bytes were available, MessageReader returned %d, says length is %d\n", avail, res, mr.size() );
	switch( m_lastSentMsg[0] )
	{
    case 'p' : // we sent a ping message
    {
      if ( unitId > 0  
           && ('A' == pl[3] || 'C' == pl[3]) || 'm' == pl[3] )
      {
        // we got an alarm... deal with it
        // payload looks like this:
        //    nnnAalarm text
    BUS_STATE_DEBUG( "[PRESP] ");
        Release();
        m_available_units |= BUS_UNIT_MASK(unitId);
        m_pCallback->OnPingResponse( unitId, pl[3], (const char *)pl+4 );
        return 1;
      }
      break;
		}
    case 'k' : case 'K' : // keyboard message sent, expect LCD refresh coming back
    case 't' : case 'T' : // touch message sent, expect LCD refresh coming back
    case 'R' :
		case 'r' : // LCD refresh data coming back
		{
			// parse the payload of the LCD data, pass to LCD object
//LCD_DEBUG( "Got LCD update : %s\n", m_reader.get() );
			int row = pl[0] - 0x10;
			int col = pl[1] - 0x10;
      m_pCallback->OnRefreshData( m_current_owner, row, col, &pl[2], plSize-2 );
LCD_DEBUG( "[LCD(%d,%d):%d '%s']\n", row, col, m_reader.ended_with_eot(), m_reader.get() );
      if ( m_reader.ended_with_eot() )
      {
        int owner = m_current_owner;
        Release();
        m_pCallback->OnRefreshData( owner, 0, 0, NULL, 0 );
LCD_DEBUG( "[LCDEND2]\n" );
      }
      // TODO: if we get stuck waiting for EOT and it never comes, we need to give up
		  // This didn't work right out of the box... return m_reader.ended_with_eot();  // don't want to leave this state until we get EOT
      break;
		}
		default : // unknown stuff came back.  Read it, log it, ignore it
		{
    /* //TODO: check if this ever got hit... should never have executed in previous version
		  char buf[1501];
      int avail = m_connection.Available();
		  while ( avail > 0 )
		  {
				int toRead = avail;
				if ( toRead > (int)sizeof(buf) )
					toRead = sizeof(buf)-1;
				int nRead = m_connection.Read( buf, toRead );
				if ( nRead <= 0 ) // error or graceful close
				{
					iprintf("Connection dropped (nRead=%d)\n",nRead);
					OnConnectionDropped();
					return 0;
				}
				else
				{
					buf[nRead] = 0; // null-terminate the string we read in
					// see if we can parse what we've received so far....
					iprintf( "Received unknown response '%s' from Controller\n",buf);
				}
				avail -= nRead;
		  }*/
      break;
		}
	}
  return 1;
}

void Bus::Close()
{
  m_connection.Close();
}

int Bus::SendKeyPress( int unitNum, int keyNum )
{
  int take = Take( unitNum, OP_KEY, TIMEOUT_KEY );
  if ( take < 0 ) return take;

	char buf[40];
	sprintf( buf, "%c%c", BUS_UNIT_MASK(unitNum) & m_supportsLowercaseK ? 'k' : 'K', keyNum+16 );
	return XmitMessage( buf, unitNum );
}

int Bus::SendTouch( int unitNum, int row, int col )
{
  int take = Take( unitNum, OP_TOUCH, TIMEOUT_TOUCH );
  if ( take < 0 ) return take;

	char buf[40];
	sprintf( buf, "t%c", ((row%LCD::ROWS)*LCD::COLUMNS+(col%LCD::COLUMNS)) + 32 );
	return XmitMessage( buf, unitNum );
}

int Bus::RequestRefresh( int unitNum, int changesOnly )
{
  int take = Take( unitNum, OP_REFRESH, TIMEOUT_REFRESH );
  if ( take < 0 ) return take;

  return XmitMessage( changesOnly ? "rS" : "RS", unitNum );
}

int Bus::RequestDatalog( int unitNum, int dateStart, int dateEnd )
{
  int take = Take( unitNum, OP_DATALOG, TIMEOUT_DATALOG );
  if ( take < 0 ) return take;
printf( "Requesting datalog for unit #%d (%s)\n", unitNum, m_is_older ? "old" : "new" );
  m_dataLog.clear(); // clear our holding area
	char buf[40];
  if ( 1 ) //m_is_older )
	  sprintf( buf, "d%06x%06x", dateStart, dateEnd );
  else
	  sprintf( buf, "d%06d%06d", dateStart, dateEnd );
	return XmitMessage( buf, unitNum );
}

int Bus::RequestScan( int unitNum, int show_header )
{
  char cmd[3] = { 's', show_header ? 'h' : 's', 0 };
  int take = Take( unitNum, OP_SCAN, TIMEOUT_SCAN );
  if ( take < 0 ) return take;
  m_scanData.clear();
  return XmitMessage( cmd, unitNum );
}

int Bus::Ping( int unitNum, char pingType )
{
  int take = Take( unitNum, OP_PING, TIMEOUT_PING );
  if ( take < 0 ) 
  {
    //printf( "Ping(%d) returned %d\n", unitNum, take );
    return take;
  }

  char buf[4];
  sprintf( buf, "p%c", pingType );
//printf( "[PING %d] ", unitNum );
  return XmitMessage( buf, unitNum ); // ping version 0 -- nobody is looking at version yet

}

void Bus::UpdateSubunitMask( UNIT_MASK_T newMask )
{
  m_supportsLowercaseK &= newMask; // remove any that aren't in the new mask
  m_available_units &= newMask; // keep any that we already know about, get rid of any not in the mask
  m_ping_units = newMask; // set up to ping any units we think should be there
}

Bus::UNIT_MASK_T Bus::GetSubunitMask()
{
  return m_ping_units;
}

int Bus::IsConnected()
{
  return m_connection.IsOpen();
}

int Bus::XmitMessage( const char *msg, int unitNum )
{
  if ( unitNum != m_current_owner )
  {
    //printf( "XmitMessage called for %03d%s when owner is %d\n", unitNum, msg, m_current_owner );
  }
	// package up the message and send it out
	MessageCreator msgC;
	int result =msgC.makeMessage( unitNum, msg );
	if ( result >= 0 )
	{
		if ( m_connection.Write( msgC.get(), msgC.length() ) < msgC.length() )
		{
			printf("Connection dropped (write failed)\n");
			m_pCallback->OnConnectionDropped();
      return -1;
		}
		m_myLog->LogSentMsg( msgC.get(), msgC.length() ); //iprintf( "Sending message: '%s' (len=%d)\n", msgC.get(), msgC.length() );
		strcpy( m_lastSentMsg, msg );
    return 1;
	}
	else
	{
		printf( "makemessage returned %d\n", result );
    return 0;
	}
}


