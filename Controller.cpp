/*
 * Controller.cpp
 *
 *  Created on: Oct 3, 2012
 *      Author: todd
 */

#include "predef.h"
#include <stdio.h>
#include <ctype.h>
#include "Controller.h"
#include "Connection.h"
#include "Server.h" // for checking Controller info on database.  Server.cpp has db login info
#include "MessageCreator.h"
#include "LCD.h"
#include <time.h>
#include <algorithm>
#include "ErrorLog.h"

// if alarms are coming in a lot, this is the minimum # of ms between messages that will actually get put into the database for the dispatcher
#define MIN_TIME_BETWEEN_ALARMS_MS 3600000L

// how many messages will be kept in the history
// 10 is only enough to see a refresh request, 8 lines of LCD contents, and the "end" msg, so I set it higher
#define MAX_MESSAGE_LOG 20

//#define DBG_PRINTF printf
#define DBG_PRINTF (void)

//#define OWNER_DEBUG printf
#define OWNER_DEBUG (void)

BasicController::BasicController(Alarm::Callback *pCB)
: m_pAlarmCallback(pCB),
  m_connState(STATE_UNKNOWN),
  m_iUnitNum(UNKNOWN_SUBUNIT), // unknown until received
  m_iUnitDBID(-1), // unknown until we know who this is, and find them in the db
  m_isOnGSM(1),    // make believe this is a GSM unit to enforce a timeout, until we know better
  m_isUnknown(1),
  m_refreshTimeouts(0),
  m_lTotalTimeouts(0),
  m_lPingTimeouts(0),
  m_protocolErrors(0),
  m_lCommandsSent(0),
  m_bDisconnected(0),
  m_is_duplicate(0),
  m_numScans(0),
  m_flags(-1) // unknown flags
{
  GET_CURRENT_TIME(m_connStart);
  GET_ZERO_TIME(m_lastRefreshRequest);
  GET_ZERO_TIME(m_lastScan);
  GET_TIMEOUT_TIME(m_holdoff,60000);// no matter what, we will not scan for 60s
  GET_ZERO_TIME(m_lastRefreshResponse);
  GET_ZERO_TIME(m_lastPing);
  GET_ZERO_TIME(m_scanStart);
  GET_ZERO_TIME(m_lastRemoteAction);
  m_controllerID[0] = 0; // invalid controller name
  m_dirty = 1;
}

SubController::SubController( ControllerUnitInfo &info, PrimaryController *primary, Alarm::Callback *pCP )
: BasicController(pCP),
  m_primary(primary)
{
//printf( "%s_%d Created Sub\n", info.identifier.c_str(), info.unitNum );
  strcpy( m_controllerID, info.identifier.c_str() );
  m_iUnitNum = info.unitNum;
  m_iUnitDBID = info.unitId;
  m_strName = info.name;
  m_isOnGSM = info.msidsn.length() >= 5 ;
  m_isUnknown = 0;
  m_scanfreq = info.scanfreq;
  m_connState = STATE_TALKING; // we are assumed to be connected when created, because we talk via Primary controller
#ifndef _NO_DBCONN
  Server::DBConnection conn;
  std::string tmp;
  conn.UpdateConnectionStatus( m_controllerID, m_iUnitNum, 1, 0, primary->GetRemoteAddress(tmp) );
#endif
  CauseRefresh(0); // get a full refresh from this unit
}

SubController::~SubController()
{
//  printf( "Sub Controller deleted\n" );
#ifndef _NO_DBCONN
  if ( !m_is_duplicate )
  {
    Server::DBConnection conn;
    conn.UpdateConnectionStatus( m_controllerID, m_iUnitNum, 0, -1, NULL );
  }
#endif
//printf( "%s_%d Destroyed Sub\n", m_controllerID,m_iUnitNum );
}

PrimaryController::PrimaryController( int older, Socket *sock, Alarm::Callback *pCB )
: BasicController(pCB),
  m_bus( older, sock, this, this ),
  m_controlledBy(NO_ONE),
  m_controlledUnit(NO_UNIT)
{
  GET_ZERO_TIME(m_lastAlarmTime);
  GET_CURRENT_TIME( m_lastActive );
// no update to controller status until it's done getting connected
  //if ( !older )
    //SetFlag( CONTROLLER_FLAG_LOWERCASE_K );
}

PrimaryController::~PrimaryController()
{
  m_subUnits.clear();
  m_bus.Close();
#ifndef _NO_DBCONN
  if ( !m_is_duplicate )
  {
    Server::DBConnection conn;
    conn.UpdateConnectionStatus( m_controllerID, m_iUnitNum, 0, -1, NULL );
  }
#endif
//printf( "%s_%d Destroyed Pri\n", m_controllerID,m_iUnitNum );
}

// read any information I need from the database.  
int BasicController::UpdateFromDBInfo( ControllerUnitInfo *info )
{
  if ( !info )
  {
    m_isUnknown = 1;
    m_iUnitDBID = -1;
    m_isOnGSM = 1;
    m_strName = "";
    return 0;
  }
  if ( info->unitNum != m_iUnitNum )
    return 0;
  m_iUnitDBID = info->unitId;
  m_isOnGSM = info->msidsn.length() >= 5 ;
  m_strName = info->name;
  m_scanfreq = info->scanfreq;
  m_isUnknown = 0;
#ifdef _DO_SYNC
  m_syncInfo = info->syncInfo;
#endif
  // if there are missing scans, try to query the controller for minimum datalog

  // needed to fill in the gap
/*
  if ( m_syncInfo.scansMissing > 0 )
  {
    int ret;
    ret = StartDataLog( AUTO_USER, m_syncInfo.startGapDate, m_syncInfo.stopGapDate);
    if ( ret < 0 ) printf( "==> start datalog returned %d\n", ret );
  }*/
  return 1;
}

#ifndef _NO_DBCONN
void BasicController::UpdateFromDatabase()
{
//printf("BC::UpdateFromDatabase %X '%s'\n", this, m_controllerID );
#ifndef _NO_DBCONN
  Server::DBConnection conn;
  std::vector<ControllerUnitInfo> info;
  int found = 0;
  if ( m_controllerID[0] && conn.GetControllerInfo( m_controllerID, info ) )
  {
    std::vector<ControllerUnitInfo>::iterator it;
    for ( it=info.begin(); it != info.end(); it++ )
    {
      if ( UpdateFromDBInfo( &(*it) ) )
      {
        found = 1;
        break;
      }
    }
  }
  if ( !found )
  {
    m_isUnknown = 1;
    m_iUnitDBID = -1;
    m_isOnGSM = 1;
    m_strName = "";
  }
#endif
}
void PrimaryController::UpdateFromDatabase()
{
//printf("PC::UpdateFromDatabase %X\n", this );
  GET_CURRENT_TIME( m_lastUpdateFromDB );
  BasicController::UpdateFromDatabase();
  IController *c;
  for (int index = 0; (c = m_subUnits.get(index)); ++index )
  {
//    printf( "Clearing subunit %s #%d to unknown\n", m_controllerID, c->GetUnitNum() );
    c->UpdateFromDBInfo( NULL ); // clear all subunits info
  }
  if ( !m_isUnknown )
  {
    // try to find any subunits also
#ifndef _NO_DBCONN
    Server::DBConnection conn;
    std::vector<ControllerUnitInfo> info;
    int found = 0;
    if ( conn.GetControllerInfo( m_controllerID, info ) )
    {
      std::vector<ControllerUnitInfo>::iterator it;
      for (it=info.begin(); it != info.end(); it++ )
      {
        if ( it->unitNum == m_iUnitNum ) // this is me
        {
          //UpdateFromDBInfo( *it );
          continue;
        }
        int found = 0;
//        printf( "Found subunit %s #%d in database\n", m_controllerID, it->unitNum );
        // see if the unit is already in my list, and update from it
        for (int index = 0; (c = m_subUnits.get(index)); ++index )
        {
          if ( c->UpdateFromDBInfo( &(*it)) )
          {
//            printf( "Updated subunit %s #%d in database\n", m_controllerID, it->unitNum );
            found = 1;
            break;
          }
        }

        // the subunit was not found in my list, so add it
        if ( !found )
        {
//          printf( "Adding subunit %s #%d in database\n", m_controllerID, it->unitNum );
          m_subUnits.add(new SubController( *it, this, m_pAlarmCallback ) );
        }
        //printf( "Done adding subcontroller\n" );
      }
    }
#endif
  }

  // remove any units that are still marked unknown
  // go backwards so index is not messed up if we remove something
  for (int index = m_subUnits.size()-1; (c = m_subUnits.get(index)); --index )
  {
    if ( c->IsUnknown() )
    {
//      printf( "Removing subunit %s index %d in database\n", m_controllerID, index );
      m_subUnits.remove(index);
    }
  }
//  printf( "Leaving PrimaryController::UpdateFromDatabase\n" );
}
#endif

Bus::UNIT_MASK_T PrimaryController::GetAllowedSubunits()
{
  return m_bus.GetSubunitMask();
}

void PrimaryController::SetAllowedSubunits( Bus::UNIT_MASK_T mask )
{
  m_subUnits.clear();
  ControllerUnitInfo info;
  info.identifier = this->m_controllerID;
  info.unitId = -1; // unknown
  for ( int i=2; i<SUBUNIT_MAX_NUMBER; ++i )
  {
    if ( mask & BUS_UNIT_MASK(i) )
    {
      info.unitNum = i;
      m_subUnits.add(new SubController( info, this, m_pAlarmCallback ) );
    }
  }
  m_bus.UpdateSubunitMask( mask );
}

IController *PrimaryController::GetSubunitById( int subunitnum )
{
  IController *c;
  for (int index = 0; (c = m_subUnits.get(index)); ++index )
  {
    if ( c->GetUnitNum() == subunitnum ) // found unit of interest
      return c;
  }
  return NULL;
}

IController *PrimaryController::GetSubunitByIndex( int subunitindex )
{
  IController *c;
  for (int index = 0; (c = m_subUnits.get(index)); ++index )
  {
    if ( index == subunitindex )
      return c;
  }
  return NULL;
}

#ifndef _NO_DBCONN
const char *BasicController::GetName()
{
  return m_strName.c_str();
}

int BasicController::IsUnknown()
{
  return m_isUnknown;
}
#endif

void BasicController::UpdateFlags()
{
  int newFlags = GetFlags();
  if ( newFlags != m_flags )
  {
#ifndef _NO_DBCONN
//    Server::DBConnection conn;
//    conn.UpdateConnectionStatus( m_controllerID, m_iUnitNum, 1, newFlags, NULL );
#endif
    m_flags = newFlags;
//printf( "Updated flags to %d for %s\n", newFlags, m_controllerID );
  }
}
int BasicController::GetFlags()
{
  int flags = 0;
  if ( m_lCommandsSent >= 10 )
  {
    if ( m_lTotalTimeouts > m_lCommandsSent/10 ) // timeouts more than 10% of the time (probably pings being ignored)
      flags |= FLAG_TIMEOUTS;
    if ( m_protocolErrors > m_lCommandsSent/10 ) // more than 10% protocol errors
      flags |= FLAG_COMMERR;
  }
  if ( m_lCommandsSent > 1 && m_lTotalTimeouts+1 >= m_lCommandsSent ) // all or nearly all commands have timed out
    flags |= FLAG_NORESPONSE;
  if ( m_isUnknown )
    flags |= FLAG_UNKNOWN;
  return flags;
}

void BasicController::TerminateAsDuplicate()
{
//printf("Controller (0x%X) terminated as duplicate\n", this );
  m_is_duplicate = 1;
}

int BasicController::IsDuplicateOf( IController *other )
{
  // can't know this until both are past "CONNECTED stage" when we discover unit number and MAC
  if ( UNKNOWN_SUBUNIT == m_iUnitNum || UNKNOWN_SUBUNIT == other->GetUnitNum() ||
       !m_controllerID[0] || !other->GetID() || !other->GetID()[0] )
    return 0; // dunno yet

  // for a match, must have same unit num and MAC address
  return m_iUnitNum == other->GetUnitNum() && !strcmp( GetID(), other->GetID() );
}

void PrimaryController::ResetIdleTimer()
{
  GET_CURRENT_TIME( m_lastActive );
}

void PrimaryController::DumpState( void (*poutfcn)( void *vparam, const char *fmt ... ), void *vparam )
{
  std::string mystate;
  char tmstr[50];
  if ( !poutfcn ) { poutfcn = (void (*)( void *vparam, const char *fmt ... ))fprintf; vparam = (void*)stdout; }
  std::string remoteaddr;
  poutfcn( vparam, "[%s]\n", m_bus.GetRemoteAddress(remoteaddr) );
  BasicController::DumpState( poutfcn, vparam );
}
	// print out the current state of this controller to stdout (which will be console or log)
void BasicController::DumpState( void (*poutfcn)( void *vparam, const char *fmt ... ), void *vparam )
{
  std::string mystate;
  char tmstr[50];
  if ( !poutfcn ) { poutfcn = (void (*)( void *vparam, const char *fmt ... ))fprintf; vparam = (void*)stdout; }
  long elapsed = Elapsed( &m_connStart );
  sprintf( tmstr, "%ld:%02ld:%02ld.%03ld", (elapsed/1000/60/60), (elapsed/1000/60)%60, (elapsed/1000)%60, elapsed % 1000 );
  poutfcn( vparam, "Controller(%s,%d): connState=%d, cur_user=%d, cur_unit=%d, connected=%s, cmds=%d, timeouts=%d, missping=%d, missrefresh=%d, protocol errors=%d\n", 
                m_controllerID, m_iUnitNum, m_connState, CurrentUser(), CurrentUnit(), tmstr, m_lCommandsSent, m_lTotalTimeouts, m_lPingTimeouts, m_refreshTimeouts, m_protocolErrors );
  if ( m_scanfreq > 0 )
  {
    if ( CurrentUser() != NO_ONE )
      poutfcn( vparam, "  Scan for history suspended while in use\n" );
    else if ( IS_TIME_ZERO(m_lastScan) || (60L*m_scanfreq-Elapsed( &m_lastScan)/1000 <= 0 ) )
      poutfcn( vparam, "  Scan for history will scan ASAP\n" );
    else 
    {
      int seconds = 60L*m_scanfreq-Elapsed( &m_lastScan)/1000;
      if ( seconds > 60*60*24*365 )
        poutfcn( vparam, "  Scan for history has invalid frequency value: 0x%X\n", m_scanfreq );
      else
        poutfcn( vparam, "  Scan for history will scan in %d:%02d\n", seconds/60, seconds%60 ); // scan it every so many minutes, but only if no one is remote-controlling
    }
  } else
    poutfcn( vparam, "  Scan for history is NOT CONFIGURED\n" );

  for ( MessageLog::iterator it = m_messageLog.begin(); it != m_messageLog.end(); it++ )
  {
    std::string mystate;
    it->Dump( mystate );
    poutfcn( vparam, "    %s\n", mystate.c_str() );
  }
  if ( m_scanData.length() > 0 )
  {
    poutfcn( vparam, "  scanData=%s\n",m_scanData.c_str() );
  }
  // the following is too big, it's probably writing into a stack variable of limited size!
  if ( m_dataLog.length() > 0 )
  {
    poutfcn( vparam, "  DataLog=%.180s ...\n",m_dataLog.c_str() );
  }
  poutfcn( vparam, "\n" );
}

int PrimaryController::IsAvailableTo( int userNum )
{
  return m_controlledBy == NO_ONE || m_controlledBy == userNum;
  // && STATE_TALKING == m_connState; removed this, because it's sometimes desirable to join a data log download already in progress
}

int PrimaryController::IsAvailableForRemoteControl( int userNum )
{
  return IsAvailableTo(userNum) && (STATE_TALKING == m_connState || STATE_AWAITING_REFRESH == m_connState);
}	
int PrimaryController::IsAvailableForDataScan( int userNum )
{
  return IsAvailableTo(userNum) && STATE_TALKING == m_connState;
}	
int PrimaryController::IsAvailableForDataLog( int userNum )
{
  return IsAvailableTo(userNum) && (STATE_TALKING == m_connState);
}
int PrimaryController::CurrentUser() // which user is using the given unit
{
  return m_controlledBy;
}	
int PrimaryController::CurrentUnit()
{
  return m_controlledUnit;
}
void PrimaryController::UpdateOwner( int usernum, int unit )
{
  if ( usernum == m_controlledBy ) // nothing to change
  {
//    OWNER_DEBUG("Controller UpdateOwner NO CHange %d=>%d (%s_%d)\n", m_controlledBy, usernum, m_controllerID, m_iUnitNum );
    return;
  }
OWNER_DEBUG("Controller owner CHANGE %d=>%d (%s_%d)\n", m_controlledBy, usernum, m_controllerID, m_iUnitNum );

  m_controlledBy = usernum;
  m_controlledUnit = unit;
#ifndef _NO_DBCONN
  Server::DBConnection conn;
  conn.UpdateUnitOwner( m_controllerID, unit, usernum );
#endif
}
void PrimaryController::FreeFromOwner( int userNum, int reason )
{
  if ( reason == IController::FREEREASON_LIST_REQUESTED &&
       m_bus.GetCurrentOperation() == Bus::OP_DATALOG )
    return; // DON'T release this until operation completed
  if ( FREEREASON_DISCONNECT == reason || 
       FREEREASON_TIMEOUT == reason || 
       ( m_controlledBy == userNum &&  // this user is now browsing for controllers again, so free it up
         m_controlledBy != NO_ONE &&
         (STATE_DATALOG != m_connState && STATE_SCANDATA != m_connState) 
       )
     )  // only free it up if not doing one of these
  {
OWNER_DEBUG("Controller freed from %d (%s_%d)\n", userNum, m_controllerID, m_iUnitNum );
    UpdateOwner( NO_ONE, NO_UNIT );
  } else 
  {
    // TODO: see if I should time out
OWNER_DEBUG("Controller NOT freed from %d, owned by %d (%s_%d)\n", userNum, m_controlledBy, m_controllerID, m_iUnitNum );
  }
}

int PrimaryController::IsReceivingLCDContents()
{
  int op = m_bus.GetCurrentOperation();  return Bus::OP_REFRESH == op || Bus::OP_KEY == op || Bus::OP_TOUCH == op;
}
/*
void BasicController::SetFlag( const char *flag )
{
  if ( !_stricmp( flag, CONTROLLER_FLAG_LOWERCASE_K ) ) // short for "lower k"
    m_supportsLowercaseK = 1;
//    if ( !stricmp( flag, "xt" ) ) // xt encryption
//      m_supportsEncryption = 1;
}
*//*
void PrimaryController::ParseFlags( char *txt )
{
  char buf[100];
  strncpy(buf,txt,100);
  buf[99] = 0;
  char *tok = strtok( buf, " " );
  while ( tok )
  {
    SetFlag( tok );
    tok = strtok( NULL, " " );
  }
}
*/
// since the only place the list of subcontrollers exist is within each Primary Controller,
//   this function will only be called by the Primary Controller on its subcontrollers
int BasicController::Service()
{
  INSTRUMENT_THIS_FUNCTION
  if ( m_bDisconnected )
    return -1;

#ifdef _DO_SYNC
  // delay getting sync info from db for 5 minutes after connection time
  if ( !m_syncInfo.known && Elapsed( &m_syncInfo.connectionTime ) > 5*60*1000 )
  {
    Server::DBConnection conn;
    conn.GetControllerSyncInfo( m_controllerID, m_iUnitNum, m_scanfreq, m_syncInfo );
  } 
  else if ( m_syncInfo.known && m_syncInfo.scansMissing > 0 && m_syncInfo.datalogAttempts < 3
       && m_connState == STATE_TALKING
     )
  {
printf( "%s_%d: Need to ask for datalog for %d@%d to %d@%d to fill %d missing scans\n",
        GetID(), m_iUnitNum,
        m_syncInfo.startGapDate,  m_syncInfo.startGapTime,
        m_syncInfo.stopGapDate,  m_syncInfo.stopGapTime,
        m_syncInfo.scansMissing
	);
    if ( StartDataLog( AUTO_USER, m_syncInfo.startGapDate, m_syncInfo.stopGapDate ) )
    {
      m_syncInfo.datalogAttempts++;
      GET_CURRENT_TIME(m_syncInfo.lastDatalogAttempt);
      return 1;
    }
  }
#endif
  if ( m_dirty )
  {

    if ( CauseRefreshImpl(m_dirty-1) )
      return 1; // don't go further, it caused something to be sent
  }
	if ( /*CurrentUser() == NO_ONE &&*/ STATE_UNKNOWN != m_connState )
	{
    // do these periodic things when nothing else is going on
    if ( STATE_TALKING == m_connState )
    {
#if 0
      if ( Elapsed( &m_lastRefreshRequest) >= MS_BETWEEN_AUTO_REFRESHES &&
           m_controlledBy != NO_ONE && // only do automatic refreshes when it's under remote control
           Elapsed( &m_lastPing ) > 1000 ) // at least a second since last ping so we don't stomp on any possible response
      {
DBG_PRINTF("refresh controlled (%s_%d)\n", m_controllerID, m_iUnitNum );
        CauseRefresh(1); // auto updates ask for changes only
      }
      else if ( Elapsed( &m_lastPing ) >= MS_BETWEEN_PINGS )
      {
DBG_PRINTF("ping (%s_%d)\n", m_controllerID, m_iUnitNum );
        Ping( CURRENT_PING_VER );
      }
#else
      if ( Elapsed( &m_lastPing ) >= MS_BETWEEN_PINGS /*&& m_controlledBy == NO_ONE*/ ) 
        // only ping when not doing remote control... NOT: still want ping so if an alarm happens, it goes out!
      {
        // this should not stomp on refresh resuests, because when those happen, we are no longer in STATE_TALKING
DBG_PRINTF("ping owner=%d (%s:%d)\n", CurrentUser(), m_controllerID, m_iUnitNum );
        Ping( m_refreshTimeouts ? PING_CONNDIAG : PING_DEFAULT );
      } else if ( Elapsed( &m_lastPing ) > 1000 ) // at least a second since last ping so we don't stomp on any possible response
      {
        if ( Elapsed( &m_lastRefreshRequest) >= MS_BETWEEN_AUTO_REFRESHES && CurrentUser() != NO_ONE && m_iUnitNum == CurrentUnit() )  // only do automatic 2s refreshes when it's under remote control
        {
          if ( Elapsed( &m_lastRemoteAction ) >= MS_USER_INACTIVITY_TIMEOUT )
          {
OWNER_DEBUG("   TIMEOUT removing user %d from (%s_%d)\n", this->CurrentUser(), m_controllerID, m_iUnitNum );
            FreeFromOwner( -1, IController::FREEREASON_TIMEOUT );
          } else
          {
            DBG_PRINTF("refresh controller owner=%d (%s:%d)\n", CurrentUser(), m_controllerID, m_iUnitNum );
//printf("CREF (AUTO) (%s_%d)\n", m_controllerID, m_iUnitNum );
            CauseRefresh(1); // auto updates ask for changes only
          }
        } else if ( Elapsed( &m_lastRefreshRequest) >= MS_BETWEEN_KEEPALIVE_REFRESHES ) // 15s refreshes if not being controlled, used to tell if the unit is alive
        {
DBG_PRINTF("refresh keepalive owner=%d (%s:%d)\n", CurrentUser(), m_controllerID, m_iUnitNum );
          if ( CurrentUser() != NO_ONE ) // this person's browser is no longer asking for refreshes... so we assume they shut their browser
          {
OWNER_DEBUG("   removing user %d from (%s_%d)\n", this->CurrentUser(), m_controllerID, m_iUnitNum );
            FreeFromOwner( -1, IController::FREEREASON_TIMEOUT );
          }
          // TODO: skip requesting this, if pings are working
          //if ( Elapsed( &m_lastPingResponse) >= MS_BETWEEN_KEEPALIVE_REFRESHES )

          if ( CauseRefresh(1) ) // auto updates ask for changes only
          {
//printf("CREF (KA) (%s_%d)\n", m_controllerID, m_iUnitNum );
          }
        } else 
        {
          if ( m_scanfreq > 0 && 
#ifdef _DO_SYNC
               m_syncInfo.known &&  // don't want to do any scans until the scan info is known
#endif
               (IS_TIME_ZERO(m_holdoff) || HasPassed(&m_holdoff)) && 
               (IS_TIME_ZERO(m_lastScan) || Elapsed( &m_lastScan) >= 60000L*m_scanfreq) 
               && CurrentUser() == NO_ONE) // scan it every so many minutes, but only if no one is remote-controlling (NO_UNIT means give back user of ANY daisy-chained unit)
          {
            if ( StartScan(AUTO_USER,0 == m_numScans ) >= 0 )
            {
//printf( "Scan: %d %d %d %d\n", m_scanfreq, HasPassed(&m_holdoff), Elapsed( &m_lastScan), CurrentUser() );
              GET_TIMEOUT_TIME(m_holdoff,30000);// no matter what, we will not scan again for at least a minute
              GET_CURRENT_TIME(m_lastScan); // this will keep it from starting another scan right away, but only if the above succeeds
            } else
            {
//printf( "Scan failed to start: %d %d %d %d\n", m_scanfreq, HasPassed(&m_holdoff), Elapsed( &m_lastScan), CurrentUser() );
              GET_TIMEOUT_TIME(m_holdoff,1000);
            }
          } //else
            //printf( "Noscan: %d %d %d\n", m_scanfreq, Elapsed(&m_lastScan), CurrentUser() );
        }
      }
#endif
    }
    
		return 0;
	}

  return 0;
}

int PrimaryController::Service()
{
#ifndef _NO_DBCONN
  if ( Elapsed(&m_lastUpdateFromDB) > 300000 ) // update from db every 5 minutes
    UpdateFromDatabase();
#endif

  // bus takes care of connection timeout
  int res = m_bus.Service();
  if ( res < 0 )
    return res;

  // take care of myself via base class
  res = BasicController::Service();
  if ( res < 0 )
    return res;

  // take care of subcontrollers via that list
  IController *c;
  for (int index = 0; (c = m_subUnits.get(index)); ++index )
  {
    res = c->Service();
    if ( res < 0 )
      return res;
  }

  return 0; // should never get here
}

int BasicController::CauseRefresh( int changesOnly )
{
  if ( m_dirty < 1+changesOnly )
  {
    m_dirty = 1+changesOnly;
    return 1;
  }
  return 0;
//	iprintf( "Causing refresh...\n");
}
int BasicController::CauseRefreshImpl( int changesOnly )
{
  if ( STATE_TALKING < m_connState || STATE_UNKNOWN == m_connState )
    return 0; // don't do this if we're otherwise busy with scan or datalog
  if ( STATE_AWAITING_FIRST_REFRESH == m_connState || STATE_AWAITING_REFRESH == m_connState )
  {
    if ( Elapsed( &m_lastRefreshRequest ) < 15000 )
      return 1; // a refresh is already in progress, so just say it worked.
    m_connState = STATE_TALKING; // just let the prior thing timeout, try it again
    ++m_refreshTimeouts;
  }
  if ( RequestRefresh( m_iUnitNum, changesOnly ) >= 0 )
  {
    GET_CURRENT_TIME( m_lastRefreshRequest );

    m_connState = STATE_AWAITING_REFRESH; // stays in that state until we get EOT, not just ETX
//printf("REFI (%s_%d) %d\n", m_controllerID, m_iUnitNum, m_dirty-1 );
    m_dirty = 0;
    return 1;
  }
  return 0;
}
int BasicController::StartDataLog( int user, int dateStart, int dateEnd )
{
//	iprintf( "Requesting data log for %s to %s...\n", dateStart, dateEnd );
  if ( STATE_DATALOG == m_connState ) // already in progress
    return 0;
  if ( STATE_TALKING != m_connState && STATE_CONNECTED != m_connState )
    return -1;
  if ( !OnRemoteAction( user, m_iUnitNum, 0, 0 ) )
    return -2;
  int ret = RequestDatalog(m_iUnitNum, dateStart, dateEnd );
  if ( ret < 0 )
  {
    ErrorLog::logf( "Request datalog failed for %s_%d (%d), freeing from owner...\n", m_controllerID, m_iUnitNum, ret );
    FreeFromOwner(user, IController::FREEREASON_OP_FAILED);
  } else
  {
    GET_CURRENT_TIME( m_datalogStart );
    m_dataLog.clear();
    m_connState = STATE_DATALOG;
  }
	return ret;
}
int BasicController::StartScan( int user, int show_header )
{
//	iprintf( "Doing data scan...\n");
  if ( STATE_TALKING != m_connState )
    return -1;
  if ( !OnRemoteAction( user, m_iUnitNum, 0, 0 ) )
    return -2;
  int ret = RequestScan(m_iUnitNum, show_header);
  if ( ret < 0 )
  {
    ErrorLog::logf( "Request scan failed for %s_%d, freeing from owner...\n", m_controllerID, m_iUnitNum );
    FreeFromOwner(user, IController::FREEREASON_OP_FAILED);
  }
  else
  {
    GET_CURRENT_TIME( m_scanStart );

    m_scanData.clear();
    m_connState = STATE_SCANDATA;
//printf("SCAN (%s_%d) %d\n", m_controllerID, m_iUnitNum, m_dirty );
  }
	return ret;
}
const char *BasicController::GetScanText( std::string &str )
{
  str = m_scanData;
  return str.c_str();
}
int BasicController::GetScan( HttpResponse *resp )
{
  if ( STATE_SCANDATA == m_connState )
  {
    if ( Elapsed(&m_scanStart) >= MS_SCAN_TIMEOUT ) // a little less than the window shows on the web page
    {
      m_connState = STATE_TALKING;
      resp->Append("Timed out!" );
      // TODO: figure out if the controller is even there anymore!
    } else
      resp->Append( "In Progress..." );
  } else if ( m_scanData.length() )
  {
    resp->Append( m_scanData.c_str() );
    return 1;
  } else
  {
    if ( CurrentUser() != NO_ONE )
      resp->Append( "BUSY" );
    else
      resp->Append( "No scan available!" );
  }
  return 0;
}
int BasicController::GetDataLog( HttpResponse *resp )
{
  if ( STATE_DATALOG == m_connState )
  {
    if ( Elapsed(&m_datalogStart) >= MS_DATALOG_TIMEOUT ) // a little less than the window shows on the web page
    {
      m_connState = STATE_TALKING;
      resp->Append("Timed out!" );
    } else
    {
      resp->Append( "In Progress...\n" );
      resp->Append( m_dataLog.c_str() );
    }
  } else if ( m_dataLog.length() )
  {
    resp->Append( m_dataLog.c_str() );
    return 1;
  } else
  {
    if ( CurrentUser() != NO_ONE )
      resp->Append( "BUSY" );
    else
      resp->Append( "No datalog available!" );
  }
  return 0;
}

int PrimaryController::OnRemoteAction( int userNum, int unitNum, int resetTimeout, int doCauseRefresh )
{
  if ( !m_bus.IsConnected() )
  {
printf( "OnRemoteAction: not connected\n");
    return 0;
  }
  if ( Bus::OP_NONE != m_bus.GetCurrentOperation() )
  {
printf( "OnRemoteAction: op %d in progress\n", m_bus.GetCurrentOperation());
    return 0;
  }
  if ( NO_ONE == m_controlledBy && userNum >= AUTO_USER )
  {
    // this will probably happen when a person first goes to the remote control page,
    // and an LCD update is requested.  This is the only time that an LCD update should reset the timer
OWNER_DEBUG( "New User %d for %s_%d\n", userNum, m_controllerID, m_iUnitNum );
    if ( doCauseRefresh )
    {
//printf("CREF (ORA) (%s_%d)\n", m_controllerID, m_iUnitNum );
      CauseRefresh(0); // cause a near-future refresh of the entire screen, since somebody is newly connected
    }
    UpdateOwner( userNum, unitNum );
    resetTimeout = 1;
  }
  if ( m_controlledBy == userNum )
  {
    if ( resetTimeout ) //  log time for timeout purposes
    {
      GET_CURRENT_TIME( m_lastRemoteAction );
    }
    return 1;
  } else
  {
printf( "OnRemoteAction: user %d cannot steal from user %d\n", userNum, m_controlledBy );
    return 0;
  }
}

int PrimaryController::SendKeyPress( int unitNum, int keyNum )
{
  int ret = m_bus.SendKeyPress( unitNum, keyNum );
  if ( ret >= 0 && m_iUnitNum == unitNum )
  {
    ++m_lCommandsSent;
    UpdateFlags();
  }
  return ret;
}
int PrimaryController::SendTouch( int unitNum, int row, int col )
{
  int ret = m_bus.SendTouch( unitNum, row, col );
  if ( ret >= 0 && m_iUnitNum == unitNum )
  {
    ++m_lCommandsSent;
    UpdateFlags();
  }
  return ret;
}
int PrimaryController::RequestRefresh( int unitNum, int changesOnly )
{
  int ret = m_bus.RequestRefresh( unitNum, changesOnly );
  if ( ret >= 0 && m_iUnitNum == unitNum )
  {
    ++m_lCommandsSent;
    UpdateFlags();
  }
  return ret;
}
int PrimaryController::RequestDatalog( int unitNum, int dateStart, int dateEnd )
{
  int ret = m_bus.RequestDatalog( unitNum, dateStart, dateEnd );
  if ( ret >= 0 && m_iUnitNum == unitNum )
  {
    ++m_lCommandsSent;
    UpdateFlags();
  }
  return ret;
}
int PrimaryController::RequestScan( int unitNum, int show_header )
{
  int ret = m_bus.RequestScan( unitNum, show_header );
  if( ret >= 0 && m_iUnitNum == unitNum )
  {
    ++m_lCommandsSent;
    UpdateFlags();
  }
  return ret;
}
int PrimaryController::Ping( int unitNum, char pingType )
{
  int ret = m_bus.Ping( unitNum, pingType );
  if ( ret >= 0 && m_iUnitNum == unitNum )
  {
    GET_CURRENT_TIME( m_lastPing );
    ++m_lCommandsSent;
    UpdateFlags();
  }
  return ret;
}

int SubController::SendKeyPress( int unitNum, int keyNum )
{
  int ret = m_primary->SendKeyPress( unitNum, keyNum );
  if ( ret >= 0 )
  {
    ++m_lCommandsSent;
    UpdateFlags();
  }
  return ret;
}
int SubController::SendTouch( int unitNum, int row, int col )
{
  int ret = m_primary->SendTouch( unitNum, row, col );
  if ( ret >= 0 )
  {
    ++m_lCommandsSent;
    UpdateFlags();
  }
  return ret;
}
int SubController::RequestRefresh( int unitNum, int changesOnly )
{
  int ret = m_primary->RequestRefresh( unitNum, changesOnly );
  if ( ret >= 0 )
  {
    ++m_lCommandsSent;
    UpdateFlags();
  }
  return ret;
}
int SubController::RequestDatalog( int unitNum, int dateStart, int dateEnd )
{
  int ret = m_primary->RequestDatalog( unitNum, dateStart, dateEnd );
  if ( ret >= 0 )
  {
    ++m_lCommandsSent;
    UpdateFlags();
  }
  return ret;
}
int SubController::RequestScan( int unitNum, int show_header )
{
  int ret = m_primary->RequestScan( unitNum, show_header );
  if ( ret >= 0 )
  {
    ++m_lCommandsSent;
    UpdateFlags();
  }
  return ret;
}
int SubController::Ping( int unitNum, char pingType )
{
  int ret = m_primary->Ping( unitNum, pingType );
  if ( ret >= 0 && m_iUnitNum == unitNum )
  {
    GET_CURRENT_TIME( m_lastPing );
    ++m_lCommandsSent;
    UpdateFlags();
  }
  return ret;
}

/*
int PrimaryController::SendKeyHit( int userNum, int unitNum, int keyNum )
{
  if ( OnRemoteAction( userNum, 1, 0 ) )
  {
    if ( BUS_OK != SendKeyPress( unitNum, keyNum ) )
      FreeFromOwner(userNum);

    ResetIdleTimer();
DBG_PRINTF( "[Sending key %d]\n", keyNum );
    int res = m_bus.SendKeyPress( unitNum, keyNum );
    if ( res < 0 )
      FreeFromOwner( userNum );
    else
    {
      m_dirty = 0;
      m_connState = STATE_AWAITING_REFRESH; // stays in that state until we get EOT, not just ETX
      GET_CURRENT_TIME(m_lastRefreshRequest);
    }
    return res;
  }
  return 0;
}
*/
// Bus callback functions
Bus::UNIT_MASK_T PrimaryController::OnUnknownData( const char *data, int len, const char *addrAndPort )
{
  // read message into buffer
  char c;
  int gotCR = 0;
  int cc = 0;
//	int res;
  while ( len-- > 0 )
  {
    c = (char)(*data++);
    if ( !c ) 
      break; // got null char

    if ( 0x0d == c || 0x03 /*ETX*/ == c ) // string is complete!
    {
      gotCR = 1;
      break;
    } else if ( !isprint(c) )
      return Bus::UNIT_MASK_INVALID; // invalid character
    if ( 12 + sizeof(m_controllerID) == m_connString.length()+1 )
      return Bus::UNIT_MASK_INVALID; // connect string too long
    m_connString += c;
  }
  if ( !gotCR )
    return 0;

// first thing other end sends is CONNECT %d %s [flags], 
//    where first param is controller's unit #, 
//    second is its MAC address/serial number
//    third may include flags of what the unit supports (may never be used because we still wouldn't know what subunits support)
  const char *str = m_connString.c_str();
  char *end;
  if ( !strncmp( str, "CONNECT ", 8 )
      && (m_iUnitNum = strtoul( &str[8], &end, 10 )) > 0 
      && m_iUnitNum == 1  // require this to be unit #1 -- the only one that is allowed to connect to the server
      && *end == ' ' && strlen(end+1) > 10 )// arbitrarily require 10 or more chars in identifier
  {
    ++end;
    // allow no spaces in controller ID... the rest are flags telling us what the controller is capable of
    char *space = strchr(end, ' ' );
    if ( !space )
      space = end+strlen(end);
    int len =  (int)(space-(end));
    strncpy( m_controllerID, end, len );
    m_controllerID[len] = 0;
#ifndef _NO_DBCONN
    Server::DBConnection conn;
    // TODO: get connection gap from database
    conn.UpdateConnectionStatus( m_controllerID, m_iUnitNum, 1, 0, addrAndPort );
    // TODO: if there is a connection gap, request the datalog so we can 
    //        merge it into the database
#endif
    /*
    if ( *space )
    {
      ParseFlags( ++space );
    }*/
    ++m_connState; // now, we're "connected"
printf( "%s_%d CONNECTED Pri %s (%p) [%s]\n", m_controllerID, m_iUnitNum, space, this, addrAndPort );
#ifndef _NO_DBCONN
    UpdateFromDatabase(); // read any information I need from the database.  
#endif
//printf("CREF (NEW) (%s_%d)\n", m_controllerID, m_iUnitNum );
    CauseRefresh(0); // get full refresh when the units connects

    //printf( "OnUnknownData: back from db update (%p)\n", this );
    Bus::UNIT_MASK_T units = BUS_UNIT_MASK(m_iUnitNum); // had better be unit #1
    // look through list of subunits and update the mask
    IController *c;
    for (int index = 0; (c = m_subUnits.get(index)); ++index )
    {
      //printf( "(subunit %p ", c );
      //printf( "#%d : %d)\n", index, c->GetUnitNum() );
      units |= BUS_UNIT_MASK(c->GetUnitNum());
    }
    //printf( "OnUnknownData returning\n" );
    return units;
  } 

  // the only way to get here is with a bad connect string    if ( res < 0 )
  {
    std::string addr;
    std::replace( m_connString.begin(), m_connString.end(), '\n', '_' );
    std::replace( m_connString.begin(), m_connString.end(), '[', '_' );
    std::replace( m_connString.begin(), m_connString.end(), '\r', '_' );
    ErrorLog::logf( "Bad connect string: '%s' from [%s]", m_connString.c_str(), addrAndPort ); //m_connection.GetAddressAndPort(addr) );
  }
  return Bus::UNIT_MASK_INVALID;
}

void PrimaryController::OnPingResponse( int unitNum, char type, const char *msg )
{
//printf( "Got new alarm fmt: %s\n", pl );
#ifdef _LINUX_
#define _strnicmp strncasecmp
#endif
  if ( _strnicmp( msg, "ok", 2 ) && msg[0] && !isspace((char)msg[0]) && ( 'C' == type || 'A' == type) )
  {
    ;//printf( "Got alarm msg: '%s'\n", &pl[4] );
    int dup = !m_lastAlarmMsg.compare(msg);
    if ( dup ) 
      ++m_lastAlarmDuplicates;
    else
      m_lastAlarmDuplicates = 0;
    if ( !dup || Elapsed( &m_lastAlarmTime ) > MIN_TIME_BETWEEN_ALARMS_MS )
    {
      std::string cmsg(msg);
      char str[50];
      if ( dup )
      {
        sprintf( str, " (Dup %d)", m_lastAlarmDuplicates ); // tack something onto the message so user may know they machine had a lot of duplicates
        cmsg += str;
      }
      m_pAlarmCallback->OnAlarm( m_controllerID, 'C' == type ? "Crash" : "Alarm", unitNum, cmsg.c_str() );
      // save the time and content of the alarm, so we can skip passing it along if it's coming in constantly
      GET_CURRENT_TIME(m_lastAlarmTime);
      m_lastAlarmMsg = msg;
    }
  } else
    ;//printf( "Got ping OK response\n" );

    // the "ok" response to a ping, saying "I'm alive", or an actual alarm... means this unit is alive and talking
    // TODO: make unit disconnect timeout also look at how long it's been since a ping occurred
  IController *c;
  if ( unitNum != m_iUnitNum )
  {
    for (int index = 0; (c = m_subUnits.get(index)); ++index )
    {
      if ( c->GetUnitNum() == unitNum ) // found unit of interest
      {
        break;
      }
    }
    if ( !c ) return; // got a ping response for something we don't have!
  } else
    c = this;

  c->OnPingResponse( type,msg );
}
void BasicController::OnPingResponse( char type, const char *msg )
{
  GET_CURRENT_TIME(m_lastPingResponse );
}
int BasicController::OnOperationTimeout( int op )
{
  ++m_lTotalTimeouts;
  FreeFromOwner(CurrentUser(),IController::FREEREASON_TIMEOUT);
  m_connState = STATE_TALKING;
  UpdateFlags();
  if ( Bus::OP_REFRESH == op )
  {
    //ErrorLog::logf("Timed out awaiting refresh (%s:%d)!\n", m_controllerID, m_iUnitNum );
    if ( ++m_refreshTimeouts >= MAX_REFRESH_TIMEOUTS )
    {
      if ( !strncmp( "00:20:", m_controllerID, 6 ) ) // is it a Lantronix?
      {
        // it's a LANTRONIX getting hung up on for not responding
        // log an alarm that this device should be powered off, or its 
        // controller powered UP.
        Server::DBConnection conn;
        if ( conn.LogAlarm( m_controllerID, "Alarm", GetUnitNum(), "LANTRONIX is ON but controller is unresponsive or OFF", "3 MONTH" ) )
          ErrorLog::logf( "Sent bad LANTRONIX alarm for %s_%d\n", m_controllerID, GetUnitNum() );
        //else
          //ErrorLog::logf( "Skipped sending bad LANTRONIX alarm for %s_%d\n", m_controllerID, GetUnitNum() );

      }
      return 1; // assume the controller is not even there anymore, hang up on it
    }
  }
  else if ( Bus::OP_SCAN == op )
  {
    //ErrorLog::logf("Timed out awaiting scan (%s:%d)!\n", m_controllerID, m_iUnitNum );
  }
  else if ( Bus::OP_PING == op )
  {
    // this one fails a lot, and the main log will get too full with this one
    //ErrorLog::logf("Timed out awaiting op %d (%s:%d)!\n", op, m_controllerID, m_iUnitNum );
    ++m_lPingTimeouts;
  }
  else
  {
    //ErrorLog::logf("Timed out awaiting op %d (%s:%d)!\n", op, m_controllerID, m_iUnitNum );
  }
  return 0;
}
void PrimaryController::OnRefreshData( int unitNum, int row, int col, unsigned char *data, int plSize )
{
  // find unit of interest
  IController *c;
  if ( unitNum != m_iUnitNum )
  {
    for (int index = 0; (c = m_subUnits.get(index)); ++index )
    {
      if ( c->GetUnitNum() == unitNum ) // found unit of interest
      {
        break;
      }
    }
    if ( !c ) return; // got a refresh for something we don't have!
  } else
    c = this;
  c->OnLCDData( row, col, data, plSize );

}
void PrimaryController::OnLogData( int unitNum, const char *logData, int is_final )
{
  // find unit of interest
  IController *c;
  if ( unitNum != m_iUnitNum )
  {
    for (int index = 0; (c = m_subUnits.get(index)); ++index )
    {
      if ( c->GetUnitNum() == unitNum ) // found unit of interest
      {
        break;
      }
    }
    if ( !c ) return; // got a refresh for something we don't have!
  } else
    c = this;
  c->OnLogData( logData, is_final );
}
void PrimaryController::OnScanData( int unitNum, const char *scanData )
{
  // find unit of interest
  IController *c;
  if ( unitNum != m_iUnitNum )
  {
    for (int index = 0; (c = m_subUnits.get(index)); ++index )
    {
      if ( c->GetUnitNum() == unitNum ) // found unit of interest
      {
        break;
      }
    }
    if ( !c ) return; // got a refresh for something we don't have!
  } else
  {
    ++m_numScans;
    c = this;
  }
  c->OnScanData( scanData );
}
int PrimaryController::OnOperationTimeout( int unitNum, int operation )
{
  int ret = 0;
//  printf( "Operation %d timed out on %s_%d\n", operation, m_controllerID, unitNum );
  if ( 1 ) //operation == Bus::OP_REFRESH )
  {
    IController *c;
    if ( unitNum != m_iUnitNum )
    {
      for (int index = 0; (c = m_subUnits.get(index)); ++index )
      {
        if ( c->GetUnitNum() == unitNum ) // found unit of interest
        {
          break;
        }
      }
      if ( !c )
      {
        //printf( "==> Could not find subunit %d\n", unitNum );
        return 1; // got a refresh for something we don't have!
      }
    } else
      c = this;
    ret = c->OnOperationTimeout(operation);
    if ( operation == Bus::OP_REFRESH )
      c->OnLCDData( -1,-1,0,0 ); // send the "done refresh" even when it times out, neg values indicate it was a timeout, not a normal empty response
  }
//  m_connState = STATE_TALKING; // just go to talking mode, give up on response
  return ret;
}

void PrimaryController::OnMessageError( int curUnit, int err )
{
  if ( curUnit )
    RequestRefresh(curUnit,0); // full refresh TODO: figure out if this is really necessary
  else
  {
    // TODO... what?
  }
}

void PrimaryController::OnProtocolError( int err )
{
  ErrorLog::logf("Protocol error %d (%s:%d)!\n", err, m_controllerID, m_iUnitNum );
  ++m_protocolErrors;
  UpdateFlags();
}

void PrimaryController::OnConnectionDropped()
{
	m_lcd.Clear();
	m_lcd.Message("[No Connection]",0 );
  m_bDisconnected = 1;
  m_bus.Close();
}


/*
 * this function will be called by the web page, to notify about screen touches
 *  returns updated display contents
 */
int BasicController::OnTouch( int userNum, int row, int col )
{
  if ( STATE_TALKING == m_connState && OnRemoteAction( userNum, m_iUnitNum, 1, 1 ) )
  {
    ResetIdleTimer();
	
    int ret = SendTouch( m_iUnitNum, row, col );
    if ( ret < 0 )
    {
      ErrorLog::logf( "SendTouch failed for %s_%d, freeing from owner...\n", m_controllerID, m_iUnitNum );
      FreeFromOwner(userNum, IController::FREEREASON_OP_FAILED);
      return 0;
    }
    else
    {
      GET_CURRENT_TIME(m_lastRefreshRequest );

      m_connState = STATE_AWAITING_REFRESH; // stays in that state until we get EOT, not just ETX
      m_dirty = 0;
      return 1; // sent request OK
    }
  } else
    return 0; // could not yet send in this request
}

/*
 * this function will be called by the web page, to notify about button presses,
 *  returns updated display contents
 */
int BasicController::OnKeyHit( int userNum, const char *keyTxt )
{
  if ( STATE_TALKING != m_connState || !OnRemoteAction( userNum, m_iUnitNum, 1, 1 ) )
    return 0;
	int code = 0;
	switch ( keyTxt[0] )
	{
		case '0' :
			code=14; break;
		case '1' :
			code=3; break;
		case '2' :
			code=2; break;
		case '3' :
			code=1; break;
		case '4' :
			code=7; break;
		case '5' :
			code=6; break;
		case '6' :
			code=5; break;
		case '7' :
			code=11; break;
		case '8' :
			code=10; break;
		case '9' :
			code=9; break;
		case 'u' :
			code=0; break;
		case 'd' : // could be down or dot
			code=keyTxt[2] == 't' ? 8 : 4; break;
		case 'l' :
			code=15; break;
		case 'r' :
			code=13; break;
		case 'e' :
			code=12; break;
		default: return 1; // invalid key text
	}

  ResetIdleTimer();
	
  int ret = SendKeyPress( m_iUnitNum, code );
  return ret;
}

void BasicController::SendLCDContents( int userNum, HttpResponse *resp )
{
//if ( userNum < 1 ) printf( "SendLCDContents: invalid userNum %d!\n", userNum );
// TKS: I no longer care if this controller is available  or not, we really just want to send whatever contents we have  
  /*if ( !IsAvailableForRemoteControl(userNum) )
  {
    resp->Append( "busy" );
    resp->Finish();
  }
  else */
  // TKS: I am ignoring the return value of this, because it's only here so that when a user first goes to the remote control page, 
  //      that user will be understood to be the owner of this controller.
  if ( -1 != userNum )
    OnRemoteAction( userNum, m_iUnitNum, 0, 1 );

  // just go ahead and send the contents no matter what
  m_lcd.SendContents(resp);
//  else
//    resp->Append( "busy" );
}

void BasicController::OnLCDData( int row, int col, const unsigned char *data, int len )
{
//printf( "BC::OnLCDData\n" );
  if ( data && len )
  {
    m_refreshTimeouts = 0; // reset this so we won't hang up without getting several in a row
    m_lcd.OnLCDUpdate( row, col, data, len );
  } else if ( row >=0 && col >= 0 ) // was just a normal empty response, NOT a timeout!
    m_refreshTimeouts = 0; // reset this so we won't hang up without getting several in a row
  else
  {
//printf( "BasicController::OnLCDData got 0 for data, len and row/col < 0!\n" );
  }

  GET_CURRENT_TIME(m_lastRefreshResponse);
  if ( !data ) // end refresh mode
    m_connState = STATE_TALKING;
}
void BasicController::OnLogData( const char *log, int is_final )
{
  if ( !is_final )
    GET_CURRENT_TIME( m_datalogStart ); // reset this timeout if there's more to come
  m_dataLog = log;
  if ( is_final )
  {
// TODO: insert each line of the log data into the database if it fills a gap 
//       that is at least 2*scanfreq minutes wide
#ifdef _DO_SYNC
    if ( m_syncInfo.known && m_syncInfo.scansMissing )
    {
      m_syncInfo.scansMissing = 0;
      Server::DBConnection conn;
      conn.SyncDatalog( m_iUnitDBID, m_scanfreq, log, &m_syncInfo );
    }
#endif
    m_connState = STATE_TALKING;
    FreeFromOwner( CurrentUser(), IController::FREEREASON_OP_COMPLETED );
  }
}
void BasicController::OnScanData( const char *scan )
{
  GET_CURRENT_TIME(m_lastScan); // store the time that we received the last scan

  ++m_numScans;
  // attempt to log this data scan to the database
#ifndef _NO_DBCONN
  Server::DBConnection conn;
//printf( "BC::OnScanData %X: %s\n", this, scan );
  conn.LogScan( m_iUnitDBID, scan );
  int mon=0,day=0,yr=0;
  const char * line = scan;
  if ( !strncmp( "    Date", line, 8 ) )
  {
    const char *cr = strchr( line, '\r' );
    const char *lf = strchr( line, '\n' );
    if ( lf )
    {
      if ( cr && cr > lf )
        line = cr+1;
      else 
        line = lf+1;
    } else if ( cr )
      line = cr+1;
  } else if ( !isdigit(scan[0]) )
    line = NULL; // invalid if header not there, and not a number
  if ( !line || !*line )
  {
    // put an alert in the database to be sent
    std::string msg = "Bad header in scan: \n'";
    msg += scan;
    msg += "'";

    int interval = 60*60*24; // once a day
    conn.LogAlarm( m_controllerID, "Date", m_iUnitNum, msg.c_str(), "1 DAY"  );
  } else if ( 3 != sscanf( line, "%02d/%02d/%02d", &mon, &day, &yr ) ||
       mon < 1 || mon > 12 || day < 1 || day > 31 || yr < 18 || yr > 50 )
  {
    // put an alert in the database to be sent
    char buf[200];
    sprintf( buf, "Bad date in scan (%d/%d/%d): \n'", mon, day, yr );
    std::string msg = "Bad date in scan (%d/%d/%d): \n'";
    msg += scan;
    msg += "'";

    int interval = 60*60*24; // once a day
    conn.LogAlarm( m_controllerID, "Date", m_iUnitNum, msg.c_str(), "1 DAY"  );
  }

#endif

//  printf( "Received scan data for %d: %s\n", m_iUnitDBID, scan );

  m_scanData = scan;
  m_connState = STATE_TALKING;
  FreeFromOwner( CurrentUser(), IController::FREEREASON_OP_COMPLETED );
}

void ControllerLogging::LogPreSTXChar( unsigned int c )
{
  m_preSTXchars.push_back((unsigned char)c);
}

void ControllerLogging::OnSTX()
{
  if ( m_preSTXchars.size() > 0 )
  {
    MessageInfo msg( &(*m_preSTXchars.begin()), m_preSTXchars.size(), "chars rcvd before STX",true, 0xff0000 );
    m_messageLog.push_back( msg );
    m_preSTXchars.clear();
  }
}
void ControllerLogging::LogReceivedMsg( const unsigned char *msgContents, int len, const char *msgComment, int hl )
{
  MessageInfo msg( msgContents,len,msgComment,true,hl);
  m_messageLog.push_back( msg );
  // make sure we only keep 10 messages in the log
  while ( m_messageLog.size() > MAX_MESSAGE_LOG )
    m_messageLog.erase( m_messageLog.begin() );
}
void ControllerLogging::LogSentMsg( const unsigned char *msgContents, int len )
{
  MessageInfo msg( msgContents, len, "", false );
  m_messageLog.push_back( msg );
  // make sure we only keep 10 messages in the log
  while ( m_messageLog.size() > MAX_MESSAGE_LOG )
    m_messageLog.erase( m_messageLog.begin() );
}

int SubController::IsAvailableTo( int userNum )                  { return m_primary->IsAvailableTo(userNum); }
int SubController::IsAvailableForRemoteControl( int userNum )    { return m_primary->IsAvailableForRemoteControl(userNum); }
int SubController::IsAvailableForDataScan( int userNum )         { return m_primary->IsAvailableForDataScan(userNum); }
int SubController::IsAvailableForDataLog( int userNum )          { return m_primary->IsAvailableForDataLog(userNum); }
int SubController::IsReceivingLCDContents()                      { return m_primary->IsReceivingLCDContents(); }
int SubController::CurrentUser()                                 { return m_primary->CurrentUser(); }
int SubController::CurrentUnit()                                 { return m_primary->CurrentUnit(); }
void SubController::FreeFromOwner( int userNum, int reason )                 { m_primary->FreeFromOwner(userNum,reason); }
int SubController::OnRemoteAction( int userNum, int unitNum, int resetTimeout, int doCauseRefresh) { return m_primary->OnRemoteAction( userNum, unitNum, resetTimeout, doCauseRefresh ); }
void SubController::ResetIdleTimer()                                                  { return m_primary->ResetIdleTimer(); }
//int SubController::SendKeyHit( int userNum, int unitNum, int keyNum )                  { return m_primary->SendKeyHit( userNum, unitNum, keyNum ); }
int SubController::Ping( char pingType )                  { int ret = m_primary->Ping( m_iUnitNum, pingType ); if ( ret >= 0 ) GET_CURRENT_TIME( m_lastPing ); return ret; }
