#include "Connection.h"
#include "timehelp.h"

class Forwarder //: public Socket::Callback
{
  Connection m_cliConn,m_srvConn;
  //Socket *m_pClient,*m_pServer;
  int m_bClosed;
  TIMESTAMP_T m_connTimeoutAt;
public:
  Forwarder( Socket *pClient, const char *fwdAddr, int fwdPort );
  virtual ~Forwarder();
//  void OnDataAvailable( Socket *pSock );
//  void OnException( Socket *pSock );

  int Service(); // called periodically just to know the connection is still alive.  Returns <0 if it needs to be deleted
};