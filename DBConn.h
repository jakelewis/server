/*
   DBConn.h describes a class that allows things to be queried from an ODBC-capable database
   
   T.Sprague, 3/12/2013
 */
 
#ifndef _included_DBConn_h_
#define _included_DBConn_h_

#include "predef.h"
#ifdef _WIN32
#include <windows.h> 
#else
#endif

#include "ControllerUnitInfo.h"

#ifdef _NO_DBCONN 
#pragma message(" dbcomm not used in Linux version... ")
#else
extern "C" {
#include <sql.h>
#include <sqltypes.h>
#include <sqlext.h>
}
#include <stdio.h>
#include <string.h>
#include <vector>

#ifndef _WIN32
#define SQLDriverConnectA SQLDriverConnect
#define SQLGetDiagRecA SQLGetDiagRec
#define SQLExecDirectA SQLExecDirect
#endif

class IDBConn
{
public:
  // tell whether we have a working database connection
  virtual int IsConnectionOK()=0;
  
  // get the UserId of the user matching the given username and password
  virtual int GetUserId( const char *username, const char *password )=0;

  // ask if the particular user is an administrator
  virtual int IsAdminUser( int userId )=0;
  
  // ask if the given Controller is accessible by the given user ID
  virtual int ControllerIsAccessibleBy( int unitId, const char *controller, int userId )=0;
  
  // verify a user's current password
  virtual int VerifyUserPassword( int userId, const char *pw )=0;
  
  // change a user's password
  virtual int ChangeUserPassword( int userId, const char *pw )=0;
  
  // put an alarm entry into the database
  virtual int LogAlarm( const char *mac, const char *typ, int unitNum, const char *msg, const char *minInterval )=0;

  // put a datascan entry into the database
  virtual int LogScan( int unitId, const char *scandata )=0;

  // get a list of controllers accessible by the given userId
  virtual int FindControllersAccessibleBy( int userId, std::vector<ControllerUnitInfo> &controllers, std::string *outSQL=NULL )=0;  

  virtual int GetControllerInfo( const char *identifier, std::vector<ControllerUnitInfo> &info )=0;  

#ifdef _DO_SYNC
  int GetControllerSyncInfo( const char *identifier, int subunit, int scanfreq, SyncInfo &info );
#endif

  // this routine will return 1 if MSISDN is not null in this database entry.
  // returns 0 if database has MSIDSN as NULL
  // return -1 if this controller is not in the database at all.
  // this is here so Controller object does not talk directly to database.  Only this server object knows the database login info
  virtual int IsControllerOnGSM( const char *identifier )=0;

  // routine to be called to change the connection status of a controller
  // internally, this will figure out the server's IP address to put into the
  // database
  virtual void UpdateConnectionStatus( const char *identifier, int unitnum, int connected, int flags, const char *conn_from_ip )=0;

  // update the listed owner in the connection status table, so it can be known if the unit is busy.
  virtual void UpdateUnitOwner( const char *identifier, int unitnum, int owner )=0;

  // if this unit is connected via tunnel, this will kill that tunnel
  virtual void DisconnectTunnel( const char *identifier ) = 0;
#ifdef _DO_SYNC
  // parse the datalog and insert missing entries into the database
  virtual void SyncDatalog( int unit_dbid, int scanfreq, const char *log, SyncInfo *syncInfo ) = 0;
#endif
};

class DBConn : public IDBConn
{
  static int have_server_ip;
  static char server_ip[100];

  class SQLHandle
  {
  protected:  
    SQLHANDLE m_h;
    SQLRETURN m_result;
    SQLSMALLINT m_type;
    void Free()
    {
      if ( m_h && SQL_SUCCEEDED(m_result) )
      {
        //if ( SQL_HANDLE_STMT == m_type )
        //  SQLFreeStmt(m_h,SQL_DROP );
        //else
          SQLFreeHandle(m_type,m_h);
      }
      m_h = NULL;
      m_result = SQL_ERROR;
    }
    SQLHandle( SQLHandle & other ); // no copying allowed
  public:
    SQLHandle()
    : m_type(0),
      m_h(NULL),
      m_result(SQL_ERROR)
    {
    }
    SQLHandle( SQLSMALLINT type, SQLHANDLE env )
    : m_type(0),
      m_h(NULL),
      m_result(SQL_ERROR)
    {
      Alloc( type, env );
    }
    virtual ~SQLHandle()
    {
      Free();
    }
    int Alloc( SQLSMALLINT type, SQLHANDLE env )
    {
      Free();
      
      m_type = type;
      //if ( SQL_ALLOC_STMT == type )
      //  m_result = SQLAllocStmt( type, env, &m_h );
      //else
        m_result = SQLAllocHandle( type, env, &m_h );
      return SQL_SUCCEEDED(m_result);
    }
    SQLHandle & operator <<=( SQLHandle &other )
    {
      m_h = other.m_h;
      other.m_h = NULL;
      m_type = other.m_type;
      m_result = other.m_result;
      
      return *this;
    }
    
    SQLSMALLINT Type()
    {
      return m_type;
    }
    operator SQLHANDLE()
    {
      return m_h;
    }
  };

  class SQLEnv : public SQLHandle
  {
  public:
    SQLEnv()
    {
      SQLRETURN retCode ;
      do
      {
        //retCode = SQLSetEnvAttr( NULL, SQL_ATTR_CONNECTION_POOLING, (void*)SQL_CP_ONE_PER_HENV, 0 ) ;
        //if ( !SQL_SUCCEEDED(retCode) )
       // {
       //   printf( "Could not set Env connection pooling: %d\n", retCode );
       // }
        if ( !Alloc( SQL_HANDLE_ENV, SQL_NULL_HANDLE ) )
        {
          printf( "Could not allocate ODBC Env\n" );
          exit(-1);
        }
        // tell it we want ODBC v3
        retCode = SQLSetEnvAttr( hEnv, SQL_ATTR_ODBC_VERSION, (void*)SQL_OV_ODBC3, 0 ) ;
        if ( !SQL_SUCCEEDED(retCode) )
        {
          printf( "Could not set Env ODBC version: %d\n", retCode );
          exit(-2);
        }
      } while ( 0 );
    }
  };


  static SQLEnv hEnv ;
  SQLHandle m_hConn;
  int m_connected;
  
  class Select : public SQLHandle
  {
    Select( Select & other ); // no copying allowed
  public:
    Select( SQLHANDLE parent )
    : SQLHandle( SQL_HANDLE_STMT, parent )
    {
//printf( "SELECT created\n" );
    }

    virtual ~Select() {} // so it can be sure to free the handle

    // returns the number of rows or zero  
    int Execute( const char *query )
    {
//printf( "%s\n", query );
      SQLRETURN retCode = SQLExecDirectA( *this, (SQLCHAR*)query, SQL_NTS );
      if ( !SQL_SUCCEEDED(retCode) )
      {
printf( "Execute FAILED: '%s'\n", query );
        ShowStatus( *this );
        return 0;
      }

      // 8.  Read data results that are now in the hStmt.
      retCode = SQLFetch( *this ) ;
      if ( !SQL_SUCCEEDED(retCode) )
      {
printf( "Fetch FAILED: '%s'\n", query );
        ShowStatus( *this );
        return 0;
      }

      SQLLEN numRows ;
      retCode = SQLRowCount( *this, &numRows ) ;
      if ( !SQL_SUCCEEDED(retCode) )
      {
        ShowStatus( *this );
        return 0;
      }
      return numRows;
    }
    int GetLong( SQLUSMALLINT index, long *plong )
    {
      SQLLEN lenout = 0;
      SQLRETURN ret = SQLGetData( *this, index, SQL_C_LONG, plong, sizeof(long), &lenout );
      if ( !SQL_SUCCEEDED(ret) )
        return 0; // could not get this value
      if ( SQL_NULL_DATA == lenout )
        return 0;
      return lenout; // size of the returned data
    }
    SQLRETURN GetText( SQLUSMALLINT index, char *buf, int bufBytesAvail, SQLLEN *pnumBytes )
    {
      return SQLGetData(
          *this,
          index,           // COLUMN NUMBER of the data to get
          SQL_C_CHAR,  // the data type that you expect to receive
          buf,         // the place to put the data that you expect to receive
          bufBytesAvail,         // the size in bytes of buf (-1 for null terminator)
          pnumBytes    // size in bytes of data returned

        );
    }
  };

  class Update : public SQLHandle
  {
    Update( Update & other ); // no copying allowed
  public:
    Update( SQLHANDLE parent )
    : SQLHandle( SQL_HANDLE_STMT, parent )
    {
//printf( "UPDATE created\n" );
    }

    virtual ~Update() {} // so it can be sure to free the handle

    // returns the number of rows or zero  
    int Execute( const char *query )
    {
      SQLRETURN retCode = SQLExecDirectA( *this, (SQLCHAR*)query, SQL_NTS );
      if ( !SQL_SUCCEEDED(retCode) )
      {
printf( "Execute FAILED: '%s'\n", query );
        ShowStatus( *this );
        return 0;
      }

      return 1;
    }
  };

  static void ShowStatus( SQLHandle &conn );

  const char *MakeNewSalt( std::string &out );
  const char *MakePasswordString( std::string &out, const char *salt, const char *pw );
  int CheckPasswordMatch( const char *pw, const char *dbInfo );

public:
protected:
  int ScrapeControllerInfo( Select &hStmt, ControllerUnitInfo *info );

  void clearControllerList();

public:
  typedef enum
  {
    NO_CONNECTION = -1
  }Errors;

  // one-time initialization when daemon starts
  static int Initialize(const char *dsn, const char *un, const char *pw);

  DBConn( const char *dsn, const char *un, const char *pw );
  virtual ~DBConn();
  
  // tell whether we have a working database connection
  int IsConnectionOK();
  
  // get the UserId of the user matching the given username and password
  int GetUserId( const char *username, const char *password );

  // ask if the particular user is an administrator
  int IsAdminUser( int userId );
  
  // ask if the given Controller is accessible by the given user ID
  int ControllerIsAccessibleBy( int unitId, const char *controller, int userId );
  
  // verify a user's current password
  int VerifyUserPassword( int userId, const char *pw );
  
  // change a user's password
  int ChangeUserPassword( int userId, const char *pw );
  
  // put an alarm entry into the database
  int LogAlarm( const char *mac, const char *typ, int unitNum, const char *msg, const char *minInterval );

  // put a datascan entry into the database
  int LogScan( int unitId, const char *scandata );

  // get a list of controllers accessible by the given userId
  int FindControllersAccessibleBy( int userId, std::vector<ControllerUnitInfo> &controllers, std::string *outSQL=NULL );  

  int GetControllerInfo( const char *identifier, std::vector<ControllerUnitInfo> &info );  

#ifdef _DO_SYNC
  int GetControllerSyncInfo( const char *identifier, int subunit, int scanfreq, SyncInfo &info );
#endif

  // this routine will return 1 if MSISDN is not null in this database entry.
  // returns 0 if database has MSIDSN as NULL
  // return -1 if this controller is not in the database at all.
  // this is here so Controller object does not talk directly to database.  Only this server object knows the database login info
  int IsControllerOnGSM( const char *identifier );

  // routine to be called to change the connection status of a controller
  // internally, this will figure out the server's IP address to put into the
  // database
  void UpdateConnectionStatus( const char *identifier, int unitnum, int connected, int flags, const char *conn_from_ip );

  // update the listed owner in the connection status table, so it can be known if the unit is busy.
  void UpdateUnitOwner( const char *identifier, int unitnum, int owner );
  void DisconnectTunnel( const char *identifier );
#ifdef _DO_SYNC
  void SyncDatalog( int unit_dbid, int scanfreq, const char *log, SyncInfo *syncInfo );
#endif
};

#endif

#endif //ndef _included_DBConn_h_

