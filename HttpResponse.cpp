#include "predef.h"
#include <stdarg.h>
#include "HttpResponse.h"

void HttpResponse::ParseAndInsert( const char *str, const char *fn, ScriptHandler **handlers, int nHandlers )
{
  while ( *str )
  {
    const char *found = strchr( str, '<' ); 
    if ( !found ) break;
    // found beginning tag
    // copy the stuff before the match
    m_responseContents.insert( m_responseContents.end(), str, found );
    str = found;

    const char *end = strchr( found, '>' );
    if ( !end )
      break;

    const char *found2 = strchr( found+1, '<' );
    if ( found2 && found2 < end ) // found another introducer before end
    {
      m_responseContents.insert( m_responseContents.end(), found, found2 );
      str = found2;
      continue;
    }
      
    // has to be at least this long to be one of our insertions 
    if ( (int)(end-found) < 19 ) 
    {
      // copy the tag we found, it's not the one we need
      m_responseContents.insert( m_responseContents.end(), found, end+1 );
      str = end+1;
      continue;
    }

    if ( !strncmp( "<!--FUNCTIONCALL ", found, 17 ) &&
          !strncmp( " -->", end-3, 4 )
        ) // function call tag
    {
      // we found the tag of interest, try to process it
      std::string invokee( &found[17], end-3 );
      int i;
      for ( i=0; i<nHandlers; ++i )
      {
        if ( handlers[i]->MakeInsertion( invokee.c_str(), fn, m_req, this ) ) 
          break;
      }
      if ( i == nHandlers )
      {
        // show something that says we tried for a handler, but didn't find one
        Append( "<!-- missing handler '" );
        Append( invokee.c_str() );
        Append( "' -->" );
      }
        
    } else if ( !strncmp( "<!--PARAMVALUE ", found, 15 ) &&
          !strncmp( " -->", end-3, 4 )
        ) // function call tag
    {
      // we found the tag of interest, try to process it
      std::string paramname( &found[15], end-3 );
      std::string val;
      if ( m_req->GetParam(paramname.c_str(),val) )
        Append(val.c_str());
    } else
    {
      // wrong introducer or trailer
      m_responseContents.insert( m_responseContents.end(), found, end+1 );
    }
    str = end+1;
  }
  if ( str && *str )
    Append( str );
}

HttpResponse::HttpResponse( Connection *c, HttpRequest *req )
: m_conn(c),
  m_req(req),
  m_retCode(200),
  m_retString("OK"),
  m_noCache(0),
  m_completed(0),
  m_status(0)
{
}
HttpResponse::~HttpResponse()
{
  if ( 0 ) //!m_completed )
  {
    printf( "Response destroyed but not completed:\n\n" );
    m_req->Dump();
  }
}
  
const char *HttpResponse::GetMimeType( const char *fn )
{
  if ( !fn )
    return 0L;
  const char *ext = strrchr( fn, '.' );
  if ( !ext )
    return 0L;
  ++ext;
  if ( !_stricmp(ext,"html") || !_stricmp(ext,"htm") )
    return "text/html; charset=utf-8";
  if ( !_stricmp(ext,"jpg") || !_stricmp(ext,"jpeg") )
    return "image/jpeg";
  if ( !_stricmp(ext,"png") )
    return "image/png";
  if ( !_stricmp(ext,"gif") )
    return "image/gif";
  if ( !_stricmp(ext,"js") )
    return "application/javascript";
  if ( !_stricmp(ext,"css") )
    return "text/css";
  return 0L;    
}
  
void HttpResponse::SetStatus( int stat )
{
  m_status = stat;
}
int HttpResponse::GetStatus()
{
  return m_status;
}
  
void HttpResponse::SetCookie( const char *nm, const char *val )
{
  std::string hval(nm);
  hval += "=";
  hval += val;
  AddHeader( "Set-Cookie", hval.c_str() );
}
void HttpResponse::SetContentType( const char *ct )
{
  m_contentType = ct;
}

void HttpResponse::Append( const char *txt )
{
  m_responseContents.insert( m_responseContents.end(), txt, &txt[strlen(txt)] );
}

void HttpResponse::AppendBinary( const char *txt, int numBytes )
{
  m_responseContents.insert( m_responseContents.end(), (const char *)txt, &txt[numBytes] );
}

// this is a special function for non-tag HTML content, to ensure it is encoded the right way
void HttpResponse::AppendContentChar( const wchar_t uc )
{
  // these entities should work regardless of charset
  switch ( uc )
  {
    case (wchar_t)199 : // � = ALT-128 or ALT-0199 �
    {
      static std::string ent( "&Ccedil;" ); // make it quicker after first one
      m_responseContents.insert( m_responseContents.end(), ent.begin(), ent.end() );
      return;
    }
    case L'&' :
    {
      static std::string ent( "&amp;" ); // make it quicker after first one
      m_responseContents.insert( m_responseContents.end(), ent.begin(), ent.end() );
      return;
    }
    case L'<' :
    {
      static std::string ent( "&lt;" ); // make it quicker after first one
      m_responseContents.insert( m_responseContents.end(), ent.begin(), ent.end() );
      return;
    }
    case L'>' :
    {
      static std::string ent( "&gt;" ); // make it quicker after first one
      m_responseContents.insert( m_responseContents.end(), ent.begin(), ent.end() );
      return;
    }
  }
  if ( uc > 127 ) // non-ASCII... need to figure out how to fix this...
  {
    // TODO: do something differently here.  We could potentially use an entity designating exact char number
    char buf[20];
    int len = sprintf( buf, "[%02X]", (unsigned char)uc );
    m_responseContents.insert( m_responseContents.end(), buf, &buf[len] );
  } else
    m_responseContents.push_back( (char)uc ); //.insert( m_responseContents.end(), txt, &txt[strlen(txt)] );
}
  
void HttpResponse::AddHeader( const char *nm, const char *val )
{
  m_extraHeaders += nm;
  m_extraHeaders += ": ";
  m_extraHeaders += val;
  m_extraHeaders += "\r\n";
}
  
void HttpResponse::NoCaching() 
{
  // call this to avoid caching the returned data
  m_noCache = 1;
}

void HttpResponse::Finish()
{
  char buffer[300];
  int res;
  do
  {
    int len = sprintf( buffer, "HTTP/1.1 %d %s\r\n"
                                "Connection: close\r\n"
                                "Content-Type: %s\r\n"
                                "Content-Length: %d\r\n", 
                               
              m_retCode, m_retString.c_str(), m_contentType.c_str(),
              m_responseContents.size() );
    if ( (res = m_conn->Write( buffer, len ) ) < 0 )
    {
      printf( "Response header 1 write failed: %d\n", res );
      break;
    }
    if ( !strstr( m_extraHeaders.c_str(), "Cache-Control" ) )
    {
      if ( m_noCache )
      {
        m_noCache = 1;
        AddHeader( "Cache-Control","no-cache, no-store, must-revalidate" );
        AddHeader( "Pragma","no-cache");
        AddHeader( "Expires","0" );
      } else
      {
        //AddHeader( "Cache-Control","max-age=86400" );
      }
    }
    if ( m_extraHeaders.length() )
    {
      res = m_conn->Write( m_extraHeaders.c_str(), (int)m_extraHeaders.length() );
      if ( res < 0 )
      {
        printf( "Response header 2 write failed: %d\n", res );
        break;
      }
    }
    res = m_conn->Write( "\r\n", 2 );
    if ( res < 0 )
    {
      printf( "Response header EOL write failed: %d\n", res );
      break;
    }
    res = m_conn->Write( &(*m_responseContents.begin()), (int)m_responseContents.size() );
    if ( res < 0 )
    {
      printf( "Response contents write failed: %d\n", res );
      break;
    }
  } while ( 0 );
  m_completed = 1;
  //printf( "[SErr] %d %s\n", errNum, msg );
}

int HttpResponse::IsComplete()
{
  return m_completed;
}

void HttpResponse::SendRedirect( const char *uri )
{
  m_retCode = 307; // temporarly moved
  m_retString = "Temporarily Moved";
  m_contentType =  "text/plain";
  m_responseContents.clear();
  AddHeader("Location",uri );
  Append(uri);
  Finish();    
}
void HttpResponse::ReturnError( int errNum, const char *msg )
{
  m_retCode = errNum;
  m_retString = msg;
  m_contentType =  "text/plain";
    
  char buffer[200];
  int len = sprintf( buffer, "%d %s\r\n", 
            errNum, msg );
  m_responseContents.clear();
  Append( buffer );
  Finish();
}

void HttpResponse::SendFileContents( const char *fn, ScriptHandler **handlers, int nHandlers )
{
  const char *type = GetMimeType(fn);
  if ( !type )
  {
    ReturnError( 404, "Invalid type" );
    return;
  }
  FILE *pFile = fopen( fn, "rb" );
  if ( !pFile )
  {
    printf( "Could not open %s\n", fn );
    ReturnError( 600+GetLastError(), "File not accessible" );
    return;
  }

  m_contentType = type;
    
//    __int64 creat=0,acces=0,lastWrite=0;
//    GetFileTime( hFile, (LPFILETIME)&creat, (LPFILETIME)&acces, (LPFILETIME)&lastWrite );
    
  // TODO: create Last-Modified: header
    
  printf( "[FILE>] %s\n", fn );

  char buffer[4096];
    
  size_t nSent = 0;
  while ( !feof(pFile) )
  {
    size_t toRead = sizeof(buffer)-1;
    size_t nread=fread( buffer, 1, toRead, pFile );
    if ( !nread )
      break; // read error, or EOF
    if ( !strncmp( m_contentType.c_str(), "text/", 5 ) )
    {
      buffer[nread] = 0; // null-term the contents
      ParseAndInsert( buffer, fn, handlers, nHandlers ); // parse this for possible text insertions
    } else // just send it
      m_responseContents.insert(m_responseContents.end(), &buffer[0], &buffer[nread] );
    nSent += nread;
    if ( nread < toRead )
      break; // reached EOF
  }
  fclose(pFile);
    
  Finish();
}

void HttpResponse::vprintf( const char *fmt, va_list args )
{
  char buffer[4000];
  vsnprintf( buffer, sizeof(buffer)-1, fmt, args );
  buffer[sizeof(buffer)-1] = 0;
  Append(buffer);
}

void HttpResponse::printf( const char *fmt ... )
{
  va_list args;
  va_start( args, fmt );
  vprintf( fmt, args );
  va_end(args);
}

void HttpResponse::st_printf( void *resp, const char *fmt ... )
{
  va_list args;
  va_start( args, fmt );
  ((HttpResponse*)resp)->vprintf( fmt, args );
  va_end(args);
}

