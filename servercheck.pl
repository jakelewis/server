#!/usr/bin/perl

# figure out if database is not currently reflecting reality

use strict;
use Net::HTTP;

my @servers = ( "sa2.us", "sa3.us", "chemcom.sbcontrol.com" );
my $operation;
my $retriesRemaining = 5;

my $fix_it = ( lc $ARGV[0] eq "fix" );


my $server = "chemcom.sbcontrol.com";
foreach $server (@servers )
{
  # this asks the controller server daemon directly for what is connected to it
  my @svrlist = split /[\r\n]+/,getList($server);
  my %hsvrlist;
  my $line;
  foreach $line ( @svrlist )
  {
    next if ( $line =~ m|^ID,| );
    next unless ( $line =~ m|^([^\,]+)| );
    my $id=$1;
    next unless ( is_valid_dbid($id) );
    $hsvrlist{$id} = 1;
  }
  
  # now, query the database for controllers listed as connected to that server
  my %hdblist;
  my $query = "SELECT DISTINCT c.identifier,c.unitnum from ".
          "core_connection_status c,core_controller_servers s ".
          "WHERE s.name='$server' and s.ip=c.ip";
  
  my @results = split /[\r\n]+/,`/usr/bin/mysql 2>/dev/null -u se -pse -D se -B -e \"$query\"`;
  foreach $line ( @results )
  {
    my $dbid = $line;
    $dbid =~ s|\t|_|;
    $dbid =~ s|[\r\n]+||g;
    next unless ( is_valid_dbid($dbid) );

    if ( exists $hsvrlist{$dbid} )
    {
      delete $hsvrlist{$dbid};
    } else
    {
      $hdblist{$dbid} = $dbid;
    }
  }

if ( scalar(keys %hdblist) )
{
  print "$server only in db:\n";
  print join ",",keys %hdblist;
  print "\n";
  if ( $fix_it )
  {
    removeFromDB( $server, \%hdblist );
  }
}
if ( scalar(keys %hsvrlist) )
{
  print "$server only in daemon:\n";
  print join ",",keys %hsvrlist;
  print "\n";
  if ( $fix_it )
  {
    removeFromDaemon( $server, \%hsvrlist );
  }
}
  
}

# TODO: I could use an URL of this form to boot a unit from a daemon:
#  http://$server:8077/removeUnit.htm?Unit=$dbid
# TODO: I could remove a unit from the database if it was not really connected

sub removeFromDB # ( $server, \%hdblist )
{
  my $svr = shift;
  my $rhList = shift;
  print "Removing from $svr DB: \n";
  my $dbid;
  foreach $dbid ( keys %$rhList )
  {
    my $ident;
    my $unitnum;
    die "Cannot interpret $dbid" unless ( $dbid =~ m|^([^_]+)_(\d+)$| );
    $ident = $1;
    $unitnum = $2;
    print "  $dbid\n";
    my $query = "UPDATE core_connection_status SET ip=NULL,disconnected=NOW() WHERE identifier='$ident' AND unitnum=$unitnum";
    system("/usr/bin/mysql 2>/dev/null -u se -pse -D se -B -e \"$query\"" );
  }
}
sub removeFromDaemon # ( $server, \%hdblist )
{
  my $svr = shift;
  my $rhList = shift;
  print "Removing from $svr Daemon:\n";
  my $dbid;
  foreach $dbid ( keys %$rhList )
  {
    print "  $dbid\n";
    getURIContents( $svr, "/removeUnit.htm?Unit=$dbid" );
  }
}
sub is_valid_dbid
{
  my $t = shift;
  return $t =~ m|^\S\S\:\S\S\:\S\S\:\S\S\:\S\S\:\S\S_\d+|;
}

sub getURIContents
{
  my $server = shift;
  my $uri = shift;
  operation("Looking up $server");
  my $s;
  eval { $s = Net::HTTP->new( Host=>$server, PeerPort=>8077); 1; };
  if ( !$s )
  {
    on_error("Failed to connect to $server: $@");
    return undef;
  }

  operation("Writing request $uri");
  eval { $s->write_request( GET => $uri, 'User-Agent' => 'Monkey'); 1; };
  if ( $@ )
  {
    on_error("Failed to write req to $server: $@");
    return undef;
  }

  operation("Getting request headers $uri");
  my ($code, $mess, %h);
  eval { ($code, $mess, %h) = $s->read_response_headers; 1; };

  if ( 200 != $code )
  {
    on_error("Could not load $uri: $code $mess\n");
    return undef;
  }

  operation( "Reading response $uri" );
  my $contents;
  while ( 1 )
  {
    my $buf;
    my $n;
    eval { $n = $s->read_entity_body( $buf, 1024 ); 1; };
    last unless ( defined $n && $n );
    $contents .= $buf;
  }
  return $contents;
}

sub operation
{
  $operation = shift;
#  print "$operation\n";
}

sub on_error
{
  my $errtxt = shift;

  while ( !open( ERRLOG, ">>errlog.log" ) )
  {
    sleep 1;
  };
  print ERRLOG (scalar localtime())." ".
        #"Unit $uid User $user Op ".
        "'$operation' : $errtxt";
  close ERRLOG;

  print color 'white on_red';
  print "DEATH: $errtxt";
  print color 'reset';
  sleep 5;
  if ( $retriesRemaining-- == 0 )
  { exit(0); } # only exit after we've died 5 times, over 25 seconds
}

sub getList
{
  my $svr = shift;
  return getURIContents( $svr,"/ajax.htm?action=getControllerListCSV" );
}
