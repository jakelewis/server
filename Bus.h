// this class tracks the bus state
// The ethernet connection has to exactly match the state of the 485 bus...
// Since some responses do not include unit IDs, that means the following sequence implies that
//   The addressed unit implicitly owns the bus until it responds, or until a timeout
// (Remote Controller)    001RS     (requests fullscreen refresh)
// (Unit 001)             ....      (responds with screen contents, LACKING unit ID, owns bus implicitly)

#include "Connection.h"
#include "timehelp.h"
#include "MessageReader.h"
#include "Logging.h"

// generate the mask value for the given unit number
#define BUS_UNIT_MASK(n) ((Bus::UNIT_MASK_T)1<<(n))
#define SUBUNIT_MAX_NUMBER 30

class BusRequests
{
public:
  typedef enum
  {
    BUS_OK          = 0,
    BUS_BUSY        = -1,
    BUS_INVALIDUNIT = -2,
    BUS_DISABLEDUNIT = -3
  } BUSRESULT;
  virtual int SendKeyPress( int unitNum, int keyNum ) = 0;
  virtual int SendTouch( int unitNum, int row, int col ) = 0;
  virtual int RequestRefresh( int unitNum, int changesOnly ) = 0;
  virtual int RequestDatalog( int unitNum, int dateStart, int dateEnd ) = 0;
  virtual int RequestScan( int unitNum, int show_header ) = 0;
  virtual int Ping( int unitNum, char pingType ) = 0;

};

class Bus : public BusRequests
{
	typedef enum
  {
    TIMEOUT_KEY = 5000,
    TIMEOUT_TOUCH = 5000,
    TIMEOUT_REFRESH = 5000,
    TIMEOUT_DATALOG = 5000, // this timeout is refreshed each time some data is received
    TIMEOUT_SCAN = 4000,
    TIMEOUT_PING = 3000,

    TIMEOUT_CONNECT = 5000 // how long client connection has to give appropriate connection string
  } ETimeoutTimes_in_ms;
public:
  typedef unsigned long UNIT_MASK_T;
  typedef enum
  {
    UNIT_MASK_INVALID = 0x80000000U // allows for up to 30 units
  } ESpecialUnitMask;

  typedef enum
  {
    OP_NONE =     0,
    OP_REFRESH =  1,
    OP_DATALOG =  2,
    OP_SCAN =     4,
    OP_PING =     8,
    OP_KEY =    0x10,
    OP_TOUCH =  0x20,

    OPS_NEEDING_LOCK = (OP_REFRESH|OP_DATALOG|OP_SCAN),

    OP_CONNECT = 0x40000000 // special initial operation that waits for callback to give the "OK" on the new connection
  } EOperation;

  class Callback : public MessageReader::Callback
  {
  public:
    // this is called for any initial data, to initiate or terminate connection
    // return >0 to say "this is a valid controller" (returned mask indicates which subunits supposedly exist)
    //        0 to say "I don't know yet"  (may eventually timeout)
    //        UNIT_MASK_INVALID to say "kick this connection off" (Close and OnConnectionDropped will be called next)
    virtual UNIT_MASK_T OnUnknownData( const char *data, int len, const char *addrAndPort ) = 0;
    virtual void OnRefreshData( int unitNum, int row, int col, unsigned char *data, int length ) = 0;
    virtual void OnLogData( int unitNum, const char *logData, int is_final ) = 0;
    virtual void OnScanData( int unitNum, const char *scanData ) = 0;
    virtual void OnPingResponse( int unitNum, char type, const char *msg ) = 0;
    // if this routine returns 1, it means the connection should be dropped
    virtual int OnOperationTimeout( int unitNum, int operation ) = 0;
    virtual void OnMessageError( int curUnit, int err ) = 0;
    virtual void OnConnectionDropped() = 0;
  };
private:
  MessageReader m_reader;

  // bus state includes :
  //   State (Idle / some operation)
  int m_operation;
  
  //   Timeout to Idle
  TIMESTAMP_T m_when_becomes_idle;

  // whether or not we have requested a connection string with the C command
  int m_requested_connstring;

  //   Current owner (unit num)
  int m_current_owner;

  UNIT_MASK_T m_ping_units; // mask saying which units need to be pinged to see if they're there
  UNIT_MASK_T m_available_units; // mask saying which units we know to be there (units that have recently responded to pings)
  UNIT_MASK_T m_supportsLowercaseK; // mask of which units support lowercase k command

  Callback *m_pCallback;

  int Take( int unitNum, int operation, int timeoutMS );
  void Cancel(); // called from whatever location has realized that an operation (with a locked bus) has timed out
  void Release(); 

  int ReadAvailableData( std::string &strData );

  std::string m_scanData,m_dataLog; // data coming in before being dispatched to callback

  int m_is_older; // it's an older unit, connected on 10003 instead of 10004 ( should mean not supporting OK response to ping, doesn't support lowercase k)
  Logging *m_myLog;
  char m_lastSentMsg[20];
  Connection m_connection;
  TIMESTAMP_T m_connStart, // when the connection was opened
                  m_lastActive; // when we last received data on the connection

public:
  typedef enum
  {
    DATE_MIN=10101,
    DATE_MAX=123189
  } ESpecialDates;
  Bus( int is_older, Socket *sock, Callback *pCallback, Logging *thelog );

  const char *GetRemoteAddress( std::string &ap );


  // to be called periodically to maintain bus state, timeouts, etc
  // will return 0 if nothing really happened, and no error
  //             >0 if it did some work without error
  //             <0 for an error (ie, needs disconnect)
  int Service();

  int GetCurrentOperation() { return m_operation;}
  int IsConnected();
  void Close(); // close our connection

  int SendKeyPress( int unitNum, int keyNum );
  int SendTouch( int unitNum, int row, int col );
  int RequestRefresh( int unitNum, int changesOnly );
  int RequestDatalog( int unitNum, int dateStart, int dateEnd );
  int RequestScan( int unitNum, int show_header );
  int Ping( int unitNum, char pingType );

  // this mask will replace the available mask currently in memory
  // this is so the Controller Server can be told about subunits via HTTP call
  void UpdateSubunitMask( UNIT_MASK_T newMask );
  UNIT_MASK_T GetSubunitMask();

private:
  int XmitMessage( const char *msg, int unitNum );
};
