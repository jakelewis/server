/*
 * Connection.h
 *
 *  Created on: Oct 3, 2012
 *      Author: todd
 */

#ifndef CONNECTION_H_
#define CONNECTION_H_

#include "Socket.h"
#include "FIFO.h"
//#include "OSThread.h"
#include <string>
#include "timehelp.h"

class Connection : public IStream, public Stream::Callback
{
	Socket *m_sock;
  FIFO m_fifo;
  FILE *m_pLogFile;
  static int m_bCommLogging;
  TIMESTAMP_T m_start;
  void OnDataTransferred( int received, const void *data, int length ); // for verbose logging 
public:
  // create outbound connection
	//Connection( IPADDR ip, int port, int *piConnected );
	// take over already-connected socket (eg server accepted socket)
	Connection( Socket *pSock );
  virtual ~Connection();
  int IsOpen();
  static void EnableCommLogging( int enable ) { m_bCommLogging = enable; }

  Stream *GetStream() { return m_sock; }
	
	const char *GetAddressAndPort( std::string &out );

  // how many available in my FIFO
	int Available();
	
	// callbacks from my socket
	virtual void OnDataAvailable( Stream *pSock );
  virtual void OnException( Stream *pSock, int events );

	int Read( void *data, int numBytes );
	int ReadStr( std::string &out, int numBytes );
	int Write( const void *data, int numBytes );
  int WriteStr( const char *str );
  int DataReadable() { return Available(); }

 
  void Close();

};

#endif /* CONNECTION_H_ */
