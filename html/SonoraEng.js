var blinkIntervalId=-1;
var lcdIntervalId=-1;

function CreateRequestObject()
{
  if(window.XMLHttpRequest)
  {
    // pretty much all browswer except IE support this
    try {return new XMLHttpRequest();} catch(e) {}
  }
  else if(window.ActiveXObject)
  {
    // Internet Explorer 5+
    try {return new ActiveXObject('MSXML2.XMLHTTP');} catch(e) {}//IE5-6
    try {return new ActiveXObject('Microsoft.XMLHTTP');} catch(e) {}
  }
  else if (window.createRequest)
  {
    try { return window.createRequest();} catch(e) {}
  }
  else
  {
    // There is an error creating the object,
    // just as an old browser is being used.
    alert('Problem creating the request object. Dynamic udpating will not work with this browser.');
  }
  return undefined;
}

function AbortReq(obj) 
{
 if(obj != undefined)
 {
  if((obj.readyState>0)&&(obj.readyState<4)){obj.abort();}
 }
}
function DoReq(obj,str,success_func,fail_func)
{
 AbortReq(obj);
 if(obj!=undefined)
 {
  var dt = new Date();
  obj.open('get','ajax.htm?'+str+"&t="+dt.getTime(),true);
  obj.onreadystatechange=function()
      {
       if(obj.readyState==4)
       {
         if(obj.status==200)
         { if ( success_func != undefined ) success_func(obj); }
         else if ( fail_func != undefined )
         { fail_func(obj); } 
       }
      }
  obj.send();
 }
}
var requestObjBtns = CreateRequestObject();
var requestObjTouch = CreateRequestObject();
var requestObjLCD = CreateRequestObject();
var lcdDiv = 'lcdDiv';
var lcdUpdateRate=2000;
var skippedUpdates=0;

// only gets called with a successful & completed return
function ProcessLCDResponse( reqObj )
{
  var answer = reqObj.responseText;
  if ( answer == "disconnected" )
  {
    window.location.replace( "/select.htm?msg=Controller%20was%20disconnected." );
    return;
  }
  if ( answer != ""  )
  {
    //ajaxText is the id of a DIV tag in index.htm
    document.getElementById(lcdDiv).innerHTML = answer;
  }
  if ( -1 != lcdIntervalId )
    clearTimeout(lcdIntervalId);
  lcdIntervalId = setTimeout(  "StartLCDUpdate()", lcdUpdateRate );
  
}

function ProcessLCDResponseFailure( reqObj )
{
  if ( -1 != lcdIntervalId )
    clearTimeout(lcdIntervalId);
  lcdIntervalId = setTimeout(  "StartLCDUpdate()", lcdUpdateRate );
}

function StartLCDUpdate()
{
  DoReq( requestObjLCD, "action=refresh", ProcessLCDResponse, ProcessLCDResponseFailure ); 
}

function setLCDUpdate( rate )
{
  // request LCD updates every 2 seconds
  lcdUpdateRate = rate;
  if ( rate > 0 )
    lcdIntervalId = setTimeout(  "StartLCDUpdate()", lcdUpdateRate );
}

function ProcessBtnResponse( reqObj )
{
  ProcessLCDResponse( reqObj );
/*
  // do nothing with the return
  if ( -1 != lcdIntervalId )
    clearTimeout(lcdIntervalId);
  lcdIntervalId = setTimeout(  "StartLCDUpdate()", 800 );
*/
}

function ProcessTouchResponse( reqObj )
{
  ProcessLCDResponse( reqObj );
/*
  // do nothing with the return
  if ( -1 != lcdIntervalId )
    clearTimeout(lcdIntervalId);
  lcdIntervalId = setTimeout(  "StartLCDUpdate()", 800 );
*/
}

//==============================================================================
// General purpose way to add events to objects without overwriting any
// existing events that might be on that object.
//==============================================================================
function addEvent(obj, eventType, functionToRun)
{
  if (obj.addEventListener)
  {
    obj.addEventListener(eventType, functionToRun, true);
    return true;
  } else if (obj.attachEvent)
  {
    var r = obj.attachEvent("on"+eventType, functionToRun);
    return r;
  } else 
  {
    return false;
  }
}

function doLoad()
{
  // do an immediate update
  StartLCDUpdate(); 

  // setup a timeout to request ajax updates every so often
  setLCDUpdate( lcdUpdateRate );
}

function insertLCD()
{
  document.write( "<DIV CLASS=\"lcd\" ID=\"lcdDiv\">" );
	document.write( "1:_Waiting_for_update..<BR>" );
	document.write( "<BR>" );
	document.write( "<BR>" );
	document.write( "<BR>" );
	document.write( "<BR>" );
	document.write( "<BR>" );
	document.write( "<BR>" );
	document.write( "<BR>" );
	document.write( "</DIV>" );

  // blink rate = 1Hz
  blinkIntervalId = setInterval( updateBlink, 500, "JavaScript" );

  addEvent(window, 'load', doLoad);

}

function onButtonHot( btn,id )
{
  //btn.className="btnHot";
}

function onButtonCold( btn,id )
{
  //btn.className="btnCold";
}

function onButtonDown( btn,id )
{
  btn.className="btnHot";
}

function onButtonClicked( btn,id )
{
  if ( btn != 'null' )
    btn.className="btnCold";
  DoReq(requestObjBtns,"btn="+id,ProcessBtnResponse );
//  if ( id == "Refresh" )
//  {
//    StartAjaxUpdate( "refresh=1" );
//  } else
//    StartAjaxUpdate( "btn="+id );
  //document.getElementById('lcdDiv').innerHTML = "Clicked: "+id;
}

function onTouch( row,col )
{
  DoReq(requestObjTouch,"touch=1&row="+row+"&col="+col,ProcessTouchResponse );
//  if ( id == "Refresh" )
//  {
//    StartAjaxUpdate( "refresh=1" );
//  } else
//    StartAjaxUpdate( "btn="+id );
  //document.getElementById('lcdDiv').innerHTML = "Clicked: "+id;
}

function onButtonUp( btn,id )
{
  onButtonClicked(btn,id);
}


function insertButton( txt, id, tdstyle )
{
  if ( !id ) id = txt;
  document.write( "<TD class=btn "+tdstyle+" ><DIV class=\"btnCold\" "+
                  "onMouseOver=\"onButtonHot(this,'"+id+"')\" "+
                  "onMouseOut=\"onButtonCold(this,'"+id+"')\" "+
                  //"onMouseDown=\"onButtonDown(this,'"+id+"')\" "+
                  //"onMouseUp=\"onButtonUp(this,'"+id+"')\" "+
                  //"onDblClick=\"onButtonClicked(this,'"+id+"')\" "+
                  "onClick=\"onButtonClicked(this,'"+id+"')\" "+
                  ">"+txt+"</DIV></TD>" );
}

var blinkState=0;
function updateBlink()
{
  blinkState=1-blinkState;
  // find all span objects whose stylename starts with blink
  var spans = document.getElementsByTagName('span');
  var patt=new RegExp("^blink([0-1])(.*)","i");
  for ( i=0; i<spans.length; i++ )
  {
    // check classnames of all spans
    var mtch = ( patt.exec(spans[i].className ) );
    if ( mtch != null )
    {
      var newClass = "blink"+blinkState+mtch[2];
      spans[i].className = newClass;
    } 
  }
}

function checkKey(e) {

    e = e || window.event;


    if (e.keyCode == '13') 
        onButtonClicked('null','enter');
    else if (e.keyCode == '37') 
        onButtonClicked('null','left');
    else if (e.keyCode == '38') 
        onButtonClicked('null','up');
    else if (e.keyCode == '39') 
        onButtonClicked('null','right');
    else if (e.keyCode == '40')
        onButtonClicked('null','down');
    else if (e.keyCode == '48' || e.keyCode == '96' ) // 40 on reg keyboard, 96 on keypad
        onButtonClicked('null','0');
    else if (e.keyCode == '49' || e.keyCode == '97' )
        onButtonClicked('null','1');
    else if (e.keyCode == '50' || e.keyCode == '98')
        onButtonClicked('null','2');
    else if (e.keyCode == '51' || e.keyCode == '99')
        onButtonClicked('null','3');
    else if (e.keyCode == '52' || e.keyCode == '100')
        onButtonClicked('null','4');
    else if (e.keyCode == '53' || e.keyCode == '101')
        onButtonClicked('null','5');
    else if (e.keyCode == '54' || e.keyCode == '102')
        onButtonClicked('null','6');
    else if (e.keyCode == '55' || e.keyCode == '103')
        onButtonClicked('null','7');
    else if (e.keyCode == '56' || e.keyCode == '104')
        onButtonClicked('null','8');
    else if (e.keyCode == '57' || e.keyCode == '105')
        onButtonClicked('null','9');
    
    else if (e.keyCode == '190' || e.keyCode == '110' ) // reg and numeric keypad
        onButtonClicked('null','dot');
}
  
function insertKeypad()
{
  document.write("<TABLE WIDTH=\"100%\" HEIGHT=\"100%\" CELLSPACING=1><TR>" );
  insertButton("1"); insertButton("2"); insertButton("3"); insertButton("&#9650;","up");
  document.write( "</TR><TR>" );
  insertButton("4"); insertButton("5"); insertButton("6"); insertButton(".","dot");
  document.write( "</TR><TR>" );
  insertButton("7"); insertButton("8"); insertButton("9"); insertButton("&#9660;","down");
  document.write( "</TR><TR>" );
  insertButton("&#9668;","left"); insertButton("0"); insertButton("&#9658;","right"); insertButton("&#10003;","enter");
  //document.write( "</TR><TR>" );
  //insertButton("Refresh","Refresh","COLSPAN=4");
  document.write( "</TR></TABLE>" );

  document.onkeydown = checkKey;

}

function getObj(name)
{
  var ths = new Object();
  if (document.getElementById)
  {
    ths.obj = document.getElementById(name);
        if ( ths.obj.style )
          ths.style= ths.obj.style;
        else
    ths.style = new Object();
  }
  else if (document.all)
  {
  ths.obj = document.all[name];
  ths.style = document.all[name].style;
  }
  else if (document.layers)
  {
    ths.obj = document.layers[name];
    ths.style = document.layers[name];
  }
  return ths;
}

function ScanData(dest, id) 
{
  if (-1 == dest) return;
  var drop = document.getElementById("dropbox" + id);

  var div = document.getElementById("scandata" + id);
  div.style.visibility = "visible";
  div.innerHTML = "";
  var requestObjScan = CreateRequestObject();
  var processScanFailure = function (reqObj) {
    div.innerHTML = "Scan Failed!";
    delete reqObj;
  };
  var processScanResponse = function (reqObj) {
    var answer = reqObj.responseText;
    if (answer == "disconnected") {
      window.location.replace("/select.htm?msg=Controller%20was%20disconnected.");
      return;
    }
    if (answer != "") 
    {
      answer.trim();
      answer = answer.replace( "\n", "<BR>" );
      div.innerHTML = answer;
      if (answer == "In Progress..." || answer == "Scan started...") 
      {
        DoReq(reqObj, "action=getscan&Unit=" + id, processScanResponse, processScanFailure);

      } else {
        delete reqObj;
        if (1 == dest) // want to download this scanned data
          window.location = "/ajax.htm?action=downloadscan&Unit=" + id;
      }
    }
  };
  DoReq(requestObjLCD, "action=startscan&Unit=" + id, processScanResponse, processScanFailure ); 
  setTimeout("document.getElementById(\"scandata"+id+"\").style.visibility=\"hidden\";", 6000);

  drop.value = -1;
}

function UpdateControllerList() {
  var spn = document.getElementById("ControllerList");
  if (spn != undefined) {
    var req = CreateRequestObject();
    var onControllerList = function (req) {
      spn.innerHTML = req.responseText;
      delete req;
    };
    var onControllerListFailure = function (req) {
      spn.innerHTML = "Cannot get controller list!";
      delete req;
    };
    DoReq(req, "action=getControllerList", onControllerList, onControllerListFailure);
  }
  var spng = document.getElementById("GSMControllerList");
  if (spng != undefined) {
    var reqg = CreateRequestObject();
    var onGSMControllerList = function (reqg) {
      spng.innerHTML = reqg.responseText;
      delete reqg;
    };
    var onGSMControllerListFailure = function (reqg) {
      spng.innerHTML = "Cannot get controller list!";
      delete reqg;
    };
    DoReq(reqg, "action=getGSMControllerList", onGSMControllerList, onGSMControllerListFailure);
  }
}

function RequestDataLog( id )
{
  var spn = document.getElementById("DataLog");
  if (spn != undefined) 
  {
    var req = CreateRequestObject();
    var onDataLogStart = function (req) 
    {
      spn.innerHTML = req.responseText;
      delete req;
      if (req.responseText.substring(0,14) == "In Progress..." || req.responseText == "Datalog retrieval started...")
        setTimeout( "CheckDataLog('"+id+"')", 1000 );
    };
    var onDataLogStartFailure = function (req) {
      spn.innerHTML = "Cannot get data log!";
      delete req;
    };
    DoReq(req, "action=startdatalog&Unit="+id, onDataLogStart, onDataLogStartFailure);
  }
}

function CheckDataLog(id) 
{
  // needs to re-up the interval to continue checking on the datalog...
  var spn = document.getElementById("DataLog");
  if (spn != undefined) 
  {
    var req = CreateRequestObject();
    var onDataLogCheck = function (req) 
    {
      var extra = "";
      if (req.responseText.substring(0, 14) != "In Progress...")
        extra = "<A HREF=\"ajax.htm?action=downloaddatalog&Unit="+id+"\">Download this Datalog</A><BR>";

      spn.innerHTML = extra+"<PRE>"+req.responseText+"</PRE><BR>"+extra;

      delete req;
      
      if (extra == "")
        setTimeout("CheckDataLog('" + id + "')", 1000);

    };
    var onDataLogCheckFailure = function (req) {
      spn.innerHTML = "Cannot get data log!";
      delete req;
    };
    DoReq(req, "action=getdatalog&Unit=" + id, onDataLogCheck, onDataLogCheckFailure);
  }
}