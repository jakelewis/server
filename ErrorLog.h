// this defines a class that can be used globally via statics for logging errors.  
// it will keep a list that can be accessed thru the web pages, so we can see problems that are occurring

#include "timehelp.h"
#include <vector>
#include <string>

class ErrorLog
{
  static std::vector< std::string > sm_vErrors;
  static int dup_count;
  static std::string sm_prev;
public:
  static void logf( const char *fmt ... );

  static const char *make_list( std::string &outstr, const char *linesep );
};