#include "predef.h"
#include "Socket.h"
#include <string.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <ctype.h>

void Keepalive( SOCKET sock )
{
  int on = 0xffffffff;
  setsockopt( sock, SOL_SOCKET, SO_KEEPALIVE, (char*)&on, sizeof(on) );
}

static int64_t GetInterfaceMAC( const char *ifacename, sockaddr_in *addr=NULL, sockaddr_in *mask=NULL )
{
  int64_t MAC=0;
  struct ifreq ifr;
  struct ifconf ifc;
  char buf[1024];

  int sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP);
  if (sock == -1) 
  {
    fprintf( stderr, "Could not create UDP socket to find MAC address: %s\n", strerror(errno) );
    return 0;
  }
  ifc.ifc_len = sizeof(buf);
  ifc.ifc_buf = buf;
  if (ioctl(sock, SIOCGIFCONF, &ifc) == -1) 
  { 
    fprintf( stderr, "Could not query UDP socket to find ifc: %s\n", strerror(errno) );
    close(sock);
    return 0;
  }

  struct ifreq* it = ifc.ifc_req;
  int numif = ifc.ifc_len / sizeof(struct ifreq);
  const struct ifreq* const end = it + numif;
  printf( "Getting %s MAC, found %d interfaces\n", ifacename, numif );

  for ( int pass=0; pass<2 && !MAC; ++pass )
  {
    for (; it != end; ++it) 
    {
      if ( strcmp( ifacename, it->ifr_name ) )
      {
        if ( pass )
          fprintf( stderr, "Rejecting interface %s\n", it->ifr_name );
        continue;
      }
      strcpy(ifr.ifr_name, it->ifr_name);
      if( pass )
        fprintf( stderr, "Interface name: %s\n", ifr.ifr_name );
      if (ioctl(sock, SIOCGIFFLAGS, &ifr) == 0) 
      {
        if (! (ifr.ifr_flags & IFF_LOOPBACK)) // don't count loopback
        { 
          if (ioctl(sock, SIOCGIFHWADDR, &ifr) == 0) 
          {
            memcpy( &MAC, ifr.ifr_hwaddr.sa_data, 6);
            if ( MAC != 0 )
            {
              if ( addr && !ioctl(sock, SIOCGIFADDR, &ifr) ) 
              {
                addr->sin_addr = *(in_addr*)&ifr.ifr_addr.sa_data[2];
                printf( "Found ip address: %s\n", inet_ntoa( addr->sin_addr ) );
              }
              if ( mask && !ioctl(sock, SIOCGIFNETMASK, &ifr) ) 
              {
                mask->sin_addr = *(in_addr*)&ifr.ifr_netmask.sa_data[2];
                printf( "Found netmask: %s\n", inet_ntoa( mask->sin_addr ) );
              }
              break;
            }
          }else 
          { 
            fprintf( stderr, "Could not query SIOCGIFHWADDR: %s\n", strerror(errno) );
          }
        }
      }
      else 
      { 
        fprintf( stderr, "Could not query SIOCGIFFLAGS: %s\n", strerror(errno) );
      }
    } // for it
  } // for pass
  close(sock);
  if ( !MAC )
  {
    system("/sbin/ifconfig >/tmp/ifc.noeth" );
    fprintf( stderr, "No interface found for MAC address!\n" );
  }
  return MAC;
}

int64_t Socket::GetEth0MAC()
{
  return GetInterfaceMAC("eth0");
}
int64_t Socket::GetWlan0MAC()
{
  return GetInterfaceMAC("wlan0");
}

int Socket::GetCurrentIPInfo( sockaddr_in *addr, sockaddr_in *mask, sockaddr_in *gw, sockaddr_in *dns )
{
  // look at contents of /proc/net/route to get gateway and know which interface is current :
  /*
Iface   Destination     Gateway         Flags   RefCnt  Use     Metric  Mask        MTU     Window  IRTT
wlan0   00000000        010BA8C0        0003    0       0       0       00000000        0       0       0
wlan0   000BA8C0        00000000        0001    0       0       0       00FFFFFF        0       0       0  
*/
  FILE *fp = fopen( "/proc/net/route", "r" );
  if ( !fp )
  {
    printf( "could not get access to /proc/net/route: %s\n", strerror(errno) );
    return 0;
  }
  char headers[200],line1[200];
  char iface[20]="";
  int gotthem = fgets( headers, 200, fp ) &&
       fgets( line1, 200, fp ) &&
       !strncmp("Iface",headers,5);
  fclose(fp);

  if ( gotthem )
  {
    char *pos = &line1[0];
    while ( *pos && !isspace(*pos) )
      ++pos;
    int len = (int)(pos - &line1[0]);
    while ( *pos && isspace(*pos) )
      ++pos;
    if ( strncmp( "00000000", pos, 8 ) )
    {
      printf( "route info does not start with default gateway!\n" );
      return 0;
    }

    while ( *pos && !isspace(*pos) ) // skip the default route address
      ++pos;
    while ( *pos && isspace(*pos) ) // skip the spaces
      ++pos;
    char *pchgw = pos;
    while ( *pos && !isspace(*pos) ) // skip the gateway address
      ++pos;
    if ( gw )
    {
      gw->sin_addr.s_addr = (uint32_t)strtoul( pchgw, 0, 16 );
      printf( "Found gateway address: %s\n", inet_ntoa( gw->sin_addr ) );
    }

    strncpy( iface, &line1[0], len );
    iface[len] = 0;
    printf( "Found interface '%s'\n", iface );
    
  } else
  {
    printf( "Could not interpret route info!\n" );
    return 0;
  }
  if ( !iface[0] )
    return 0;
  return GetInterfaceMAC(iface, addr, mask);
}

// this is an accepted inbound connection  	
Socket::Socket( SOCKET sock, SOCKADDR_IN *addr )
: m_listening(0),
  m_addr(*addr)
{
  SetFD(sock);
//    printf( "Accepted socket %d\n", sock );
    Keepalive( sock );
}
  
Socket::Socket( int port, int maxQueue, unsigned long listenaddr )
: m_listening(0)
{
  SOCKET sock = INVALID_SOCKET;
  do
  {
    sock = socket( AF_INET, SOCK_STREAM, 0/*IPPROTO_TCP*/ );
    if ( IS_INVALID_SOCKET( sock ) )
    {
      printf( "Failed to create socket for port %d\n", port );
      break;
    }
      
    unsigned long on = 1;
    // make it reusable^M
    if ( setsockopt(sock, SOL_SOCKET,  SO_REUSEADDR,
                   (char *)&on, sizeof(on)) < 0 )
    {
      perror("setsockopt() failed");
    }
                       
    // set it to non-blocking
    if ( ioctlsocket(sock,FIONBIO,&on ) < 0 ) 
    {
      printf( "Failed to set nonblocking mode on listen socket for port %d\n", port );
      perror("ioctl() failed");
    }
    memset(&m_addr,0,sizeof(m_addr));
    m_addr.sin_port = htons(port);
    m_addr.sin_family = AF_INET; 
    ADDR_FROM_SOCKADDR(m_addr) = htonl(listenaddr);
    if ( bind(sock, (sockaddr*)&m_addr, sizeof(m_addr) ) )
    {
      printf( "Could not bind to port %d (%d)\n", port, GetLastError() );
      break;
    }
    if ( listen( sock, maxQueue ) )
    {
      printf( "Could not listen on port %d (%d)\n", port, GetLastError() );
      break;
    }
    SetFD(sock);  
    sock = INVALID_SOCKET;
    m_listening = 1;
    printf( "socket %d listening on port %d\n", GetFD(), port );
  } while ( 0 );
  if ( IS_INVALID_SOCKET( sock ) )
    closesocket(sock);

}

Socket::Socket( const char *addr, int port )
: m_listening(0)
{
  SOCKET sock = INVALID_SOCKET;
  do
  {
    sock = socket( AF_INET, SOCK_STREAM, 0/*IPPROTO_TCP*/ );
    if ( IS_INVALID_SOCKET( sock ) )
    {
      printf( "Failed to create socket for port %d\n", port );
      break;
    }
      
    unsigned long on = 1;
                       
    struct hostent *he = gethostbyname( addr );
    memset(&m_addr,0,sizeof(m_addr));
    m_addr.sin_port = htons(port);
    m_addr.sin_family = he->h_addrtype; 
    ADDR_FROM_SOCKADDR(m_addr) = *(u_long*)he->h_addr_list[0];
//printf( "Connecting... " );
    if ( connect( sock, (const sockaddr*)&m_addr, sizeof(m_addr) ) )
    {
      printf( "Failed to connect to %s:%d\n", addr, port );
      perror("connect() failed");
      break;
    }
//printf( "Connected socket %d to %s:%d\n", sock, addr, port );
    SetFD(sock); // have to set this before the stuff below will work  
    sock = INVALID_SOCKET;
    SetNonblocking( 1 );
    SetKeepalive( 1 );
    SetNoDelay( 1 );
//    printf( "socket %d connected to %s:%d\n", m_sock, addr, port );
  } while ( 0 );
  if ( !IS_INVALID_SOCKET( sock ) )
    closesocket(sock);
}
  
Socket::~Socket()
{
  Close();
}
  
SOCKADDR_IN *Socket::GetAddress()
{
  return &m_addr;
}

void Socket::Close()
{
  int fd = GetFD();
  if ( IsOpen() )
  {
    SetFD(-1);
    closesocket( fd );
  }
}

// not valid for a listening socket
int Socket::Read( void *data, int numBytes )
{
  if ( m_listening )
    return -1; // don't want to read from this socket
  if ( !IsOpen() )
    return -1;
  char *pb = (char*)data;
  int nread = 0;
  while ( nread < numBytes )
  {
//printf( "recv ");
    int n = recv( GetFD(), &pb[nread], numBytes-nread, MSG_NOSIGNAL );
//printf( "%d\n", n );
    if ( n < 0 )
    {
      if ( WSAEWOULDBLOCK == WSAGetLastError() )
        return nread; // not an error
printf( "Socket::Read: recv got %d back, err=%d!\n", n, WSAGetLastError() );
      Close();
      return -WSAGetLastError(); // this is an error
    } else if ( n == 0 ) // graceful close
    {
printf( "Socket::Read: recv got %d back (graceful close)!\n", n );
      Close();
      return -1;
    }
    nread += n;
  }
  return nread;
}

int Socket::Write( const void *data, int numBytes )
{
  if ( m_listening )
  {
    fprintf( stderr, "Can't write to listening socket!\n" );
    return -1; // don't want to write to this socket
  }
  if ( !IsOpen() )
  {
    fprintf( stderr, "Can't write to closed socket!\n" );
    return -1;
  }
  char *pb = (char*)data;
  int written = 0;
  while ( written < numBytes )
  {
    int n = send( GetFD(), &pb[written], numBytes-written, MSG_NOSIGNAL );
    if ( n <= 0 )
    {
      if ( WSAGetLastError() != WSAEWOULDBLOCK )
      {
printf( "Socket::Write: send got %d back, err=%d!\n", n, WSAGetLastError() );
        Close();
        return -WSAGetLastError();
      }
      continue; // try again
    }
    written += n;
  }
  return written;
}

// only valid for a listening socket
Socket *Socket::Accept()
{
  if ( !m_listening )
  {
    printf( "Socket::Accept called on non-listening socket!\n" );
    return NULL;
  }

  SOCKADDR_IN addr;
  socklen_t addrlen = sizeof(addr);
  SOCKET sock = accept( GetFD(), (sockaddr*)&addr, &addrlen );

  if ( IS_INVALID_SOCKET( sock ) )
  {
printf( "Accept returned invalid socket %d\n", sock );
    return NULL;
  }
    
  return new Socket( sock, &addr );
}
  
int Socket::DataReadable()
{
  unsigned long n=0;
  if ( !IsOpen() )
    return -1;
  int res = ioctlsocket( GetFD(), FIONREAD, &n );
  if ( res < 0 )
  {
printf( "Socket::DataReadble: ioctlsocket returned %d, err=%d\n", res, WSAGetLastError() );
    Close();
    return -1; // return res?
  }
  return n;
}

