/*
    This object listens for inbound Controller connections on 10001, and 
    tracks all of the controller data.
    
    It also listens for web requests, to serve out pages and graphics 
    representing an interface to one of its connected Controllers
 */

#include "predef.h"
#include "Server.h"
#include <string>
#include "ErrorLog.h"
#include "git_version.h"
#include <stdarg.h>
#include "Timestamp.h"
  
#define PORT_CONTROLLERS      10003
#define PORT_NEWERCONTROLLERS 10004
#define PORT_HTTP             8077
#define PORT_ALARMS           10001

Server *g_server = 0L;

// define this to make it possible to login without using a database
// FOR TESTING ONLY!  VERY INSECURE!
//#define _USERNAME_WITHOUT_DATABASE "admin"

#ifndef _NO_DBCONN
// these settings control what database is connected-to, and username/password for it
#define DB_UNAME  "se"
#define DB_PASSWD "se"
#ifndef _WIN32
#define DB_DSN    "DRIVER={my-connector};SERVER=127.0.0.1;DATABASE=se;USER=se;PASSWORD=se;"
//#define DB_DSN    "DRIVER={MySQL ODBC 3.51 Driver};SERVER=70.85.16.221;DATABASE=se;USER=se;PASSWORD=se;"
#else

#define DB_DSN    "DRIVER={MySQL ODBC 3.51 Driver};SERVER=192.168.11.175;DATABASE=Sonora;USER=root;PASSWORD=Secret;"
#endif
//"DRIVER={MySQL ODBC 3.51 Driver};SERVER=127.0.0.1;DATABASE=se;USER=" DB_UNAME ";PASSWORD=" DB_PASSWD ";"
//"DSN=Sonora;Uid=" DB_UNAME ";Pwd=" DB_PASSWD ";"

// a printf with a high-res time printed out beforehand
void ts_printf( const char *fmt ... )
{
  Timestamp ts;
  char buf[100];
  printf( "[%s]: ",ts.toString(buf));

  va_list args;
  va_start( args, fmt );
  vprintf( fmt, args );
  va_end(args);
}

const char *GetDBDSN()
{
  // attempt to read in a file that contains the DSN information
  do
  {
    FILE *f = fopen( "DBDSN.conf", "r" );
    if ( !f )
      break;
    static char line[200];
    fgets( line, 200, f );
    char *pos = strchr( line, '\n' );
    if (pos) *pos = 0;
    pos = strchr( line, '\r' );
    if (pos) *pos = 0;
    fclose(f);
    if ( !line[0] ) break;
    return line;
  } while ( 0 );
  FILE *f = fopen( "DBDSN.conf", "w" ); // create the file if it was not there
  if ( f )
  {
    fprintf( f, "%s", DB_DSN );
    fclose(f);
  }
  return DB_DSN; // default from within this file
}
#define USE_CUSTOM_POOL
void Server::GetConnection( DBConn **ppOut )
{
#ifdef USE_CUSTOM_POOL
  *ppOut = NULL;
//: DBConn( GetDBDSN(), DB_UNAME,DB_PASSWD )
  /// look for an available connection and return it
  while ( m_dbpoolAvailable.size() > 0 )
  {
    *ppOut = *m_dbpoolAvailable.begin();
//printf( "Removed %p from avail list\n", *ppOut );
    m_dbpoolAvailable.erase(m_dbpoolAvailable.begin());
    if ( !(*ppOut)->IsConnectionOK() )
    {
printf( "Server tossing bad db connection: %p\n", *ppOut );
      delete *ppOut; //something is wrong with this connection, let it go
      *ppOut = NULL;
    } else
      break;
  }
  if ( !(*ppOut) )
  {
    if ( m_dbpoolInUse.size() > 100 )
    {
//printf( "DB connection pool reached limit!\n" );
      return; // prevent runaway memory leak*/
    } else
    {
      *ppOut = new DBConn( GetDBDSN(), DB_UNAME,DB_PASSWD );
      if ( !(*ppOut)->IsConnectionOK() )
      {
// TODO: might want to keep a count of this, and kill the process at some point
printf( "Attempt to create new DB connection failed!\n" );
        delete *ppOut;
        *ppOut = NULL;
        return;
      }
//printf( "Created new db conn %p \n", *ppOut );
    }
  }
//printf( "Added %p to in-use list\n", *ppOut );
  m_dbpoolInUse.push_back(*ppOut);
#else
  *ppOut = new DBConn( GetDBDSN(), DB_UNAME,DB_PASSWD );
#endif
}


void Server::ReturnConnection( DBConn *dbc )
{
#ifdef USE_CUSTOM_POOL
  // return this connection to the available list
  DBPoolList::iterator it;
  for ( it=m_dbpoolInUse.begin(); it != m_dbpoolInUse.end(); it++ )
  {
    if ( *it == dbc )
    {
//printf( "Removed %p from in-use list\n", dbc );
      m_dbpoolInUse.erase(it);
      break;
    }
  }
//printf( "Added %p to avail list\n", dbc );
  m_dbpoolAvailable.push_back( dbc );
#else
  delete dbc;
#endif
}

Server::DBConnection::DBConnection()
: m_dbconn(NULL)
{
  g_server->GetConnection( &m_dbconn );
}
Server::DBConnection::~DBConnection()
{
  g_server->ReturnConnection( m_dbconn );
}
int Server::DBConnection::IsConnectionOK()
{
  return m_dbconn && m_dbconn->IsConnectionOK();
}

    // get the UserId of the user matching the given username and password
int Server::DBConnection::GetUserId( const char *username, const char *password )
{
  if ( m_dbconn )
    return m_dbconn->GetUserId( username,password );
  else
    return DBConn::NO_CONNECTION;
}

    // ask if the particular user is an administrator
int Server::DBConnection::IsAdminUser( int userId )
{
  if ( m_dbconn )
    return m_dbconn->IsAdminUser( userId );
  else
    return DBConn::NO_CONNECTION;
}

    // ask if the given Controller is accessible by the given user ID
int Server::DBConnection::ControllerIsAccessibleBy( int unitId, const char *controller, int userId )
{
  if ( m_dbconn )
    return m_dbconn->ControllerIsAccessibleBy( unitId,controller,userId );
  else
    return DBConn::NO_CONNECTION;
}

    // verify a user's current password
int Server::DBConnection::VerifyUserPassword( int userId, const char *pw )
{
  if ( m_dbconn )
    return m_dbconn->VerifyUserPassword( userId,pw );
  else
    return DBConn::NO_CONNECTION;
}

    // change a user's password
int Server::DBConnection::ChangeUserPassword( int userId, const char *pw )
{
  if ( m_dbconn )
    return m_dbconn->ChangeUserPassword( userId,pw );
  else
    return DBConn::NO_CONNECTION;
}

    // put an alarm entry into the database
int Server::DBConnection::LogAlarm( const char *mac, const char *typ, int unitNum, const char *msg, const char *minInterval )
{
  if ( m_dbconn )
    return m_dbconn->LogAlarm( mac,typ,unitNum,msg,minInterval);
  else
    return DBConn::NO_CONNECTION;
}

int Server::DBConnection::LogScan( int unitId, const char *scandata )
{
  if ( m_dbconn )
    return m_dbconn->LogScan( unitId, scandata);
  else
    return DBConn::NO_CONNECTION;
}

    // get a list of controllers accessible by the given userId
int Server::DBConnection::FindControllersAccessibleBy( int userId, std::vector<ControllerUnitInfo> &controllers, std::string *outSQL )
{
  if ( m_dbconn )
    return m_dbconn->FindControllersAccessibleBy( userId, controllers, outSQL);
  else
    return DBConn::NO_CONNECTION;
}

int Server::DBConnection::GetControllerInfo( const char *identifier, std::vector<ControllerUnitInfo> &info )
{
  if ( m_dbconn )
    return m_dbconn->GetControllerInfo( identifier, info );
  else
    return DBConn::NO_CONNECTION;
}

#ifdef _DO_SYNC
int Server::DBConnection::GetControllerSyncInfo( const char *identifier, int subunit, int scanfreq, SyncInfo &info )
{
  if ( m_dbconn )
    return m_dbconn->GetControllerSyncInfo( identifier, subunit, scanfreq, info );
  else
    return DBConn::NO_CONNECTION;
}
#endif

    // this routine will return 1 if MSISDN is not null in this database entry.
    // returns 0 if database has MSIDSN as NULL
    // return -1 if this controller is not in the database at all.
    // this is here so Controller object does not talk directly to database.  Only this server object knows the database login info
int Server::DBConnection::IsControllerOnGSM( const char *identifier )
{
  if ( m_dbconn )
    return m_dbconn->IsControllerOnGSM( identifier );
  else
    return DBConn::NO_CONNECTION;
}

    // routine to be called to change the connection status of a controller
    // internally, this will figure out the server's IP address to put into the
    // database
void Server::DBConnection::UpdateConnectionStatus( const char *identifier, int unitnum, int connected, int flags, const char *conn_from_ip )
{
  if ( m_dbconn )
    m_dbconn->UpdateConnectionStatus( identifier, unitnum, connected, flags, conn_from_ip );
}

void Server::DBConnection::DisconnectTunnel( const char *identifier )
{
  if ( m_dbconn )
    m_dbconn->DisconnectTunnel( identifier );
}
#ifdef _DO_SYNC
void Server::DBConnection::SyncDatalog( int unit_dbid, int scanfreq, const char *log, SyncInfo *syncInfo )
{
  if ( m_dbconn )
    m_dbconn->SyncDatalog( unit_dbid, scanfreq, log, syncInfo );
}
#endif

void Server::DBConnection::UpdateUnitOwner( const char *identifier, int unitnum, int owner )
{
  if ( m_dbconn )
    m_dbconn->UpdateUnitOwner( identifier, unitnum, owner );
}
#endif

IController *Server::FindController( const char *unitId )
{
  IController *c;
  const char *under = strchr( unitId, '_' );
  if ( !under ) under = &unitId[strlen(unitId)];
  std::string mac( unitId, under );
  int unitNum = 0;
  if ( *under ) // found underscore, so we have a subunit number
    unitNum = strtoul( under+1, 0, 10 );
  for ( int i=0; c = m_controllers.get(i); ++i )
  {
    if ( c->GetUnitNum() <= 0 ) continue;
    if ( !mac.compare( c->GetID() ) )
    {
      if ( (c->GetUnitNum() == unitNum || !unitNum) )
        return c;
      return c->GetSubunitById( unitNum );
    }
  }
  return NULL;
}

int GetRequestUserId( HttpRequest *req )
{
  std::string val;
  if ( req->GetCookie("UserID",val) )
    return atoi( val.c_str() );
  if ( req->GetParam( "User", val ) )
    return atoi( val.c_str() );
  return 0; 
}

IController *Server::FindController( HttpRequest *req )
{
  std::string unitId;
  if ( !req->GetCookie("UnitID", unitId ) && !req->GetParam("Unit",unitId) )
    return NULL;
  return FindController( unitId.c_str() );
}

void Server::RefreshController( HttpRequest *req, HttpResponse *resp )
{
  // the unit ID here may have a * at the end, meaning that the subunit ID is not known
  // so we need a different search here that can match any connected subunits
  std::string unitId;
  if ( !req->GetCookie("UnitID", unitId ) && !req->GetParam("Unit",unitId) )
    return;
  IController *c;
  const char *under = strchr( unitId.c_str(), '_' );
  if ( !under ) under = &unitId[unitId.length()];
  std::string mac( unitId.c_str(), under );
  int unitNum = 0;
  if ( *under && under[1] != '*' ) // found underscore, so we have a subunit number
    unitNum = strtoul( under+1, 0, 10 );
  int found = 0;
  for ( int i=0; c = m_controllers.get(i); ++i )
  {
    if ( c->GetUnitNum() <= 0 ) continue;
    if ( !mac.compare( c->GetID() ) && (c->GetUnitNum() == unitNum || unitNum != 1 ) )
    {
#ifndef _NO_DBCONN
      c->UpdateFromDatabase();
#endif
      resp->Append( mac.c_str() );
      resp->Append( "_" );
      char sub[20];
      sprintf( sub, "%d", c->GetUnitNum() );
      resp->Append(sub);
      resp->Append( ":OK " );
      ++found;
    }
  }
  if ( !found )
  {
    resp->Append( unitId.c_str() );
    resp->Append(":Not found");
    printf( "RefreshController failed to find %s to refresh it!\n", unitId.c_str() );
  }
}

Server::Server( const char *fwdAddr )
: m_sockControllers( PORT_CONTROLLERS, 50 ),  // create listen socket for Controllers to connect to
  m_sockNewerControllers( PORT_NEWERCONTROLLERS, 50 ),  // create listen socket for Controllers to connect to
  m_sockWeb( PORT_HTTP,100/*, INADDR_LOOPBACK*/), // create listen socket for Web Clients to connect to, now supporting only localhost, PHP front-end
  m_sockAlarms(PORT_ALARMS,50) // where alarms are sent
{
  g_server = this;

#ifndef _NO_DBCONN
  printf( "Server initializing MySQL connection..." );
  DBConn::Initialize( GetDBDSN(), DB_UNAME,DB_PASSWD );
  printf( "Done.\n" );
#endif
//  DBConnection conn;
  if ( fwdAddr )
    m_fwdAddr = fwdAddr;

  // 1's mean don't try to delete this object at shutdown
  AddRequestHandler( this, 1 ); // so I get first shot to handle any request I want
  AddScriptHandler( this, 1 );
  
  m_sockControllers.SetNonblocking(1);
  m_sockNewerControllers.SetNonblocking(1);
  m_sockWeb.SetNonblocking(1);
  m_sockAlarms.SetNonblocking(1);
  
  m_sockNewerControllers.SetCallback(this);
  m_sockControllers.SetCallback(this);
  m_sockWeb.SetCallback(this);
  m_sockAlarms.SetCallback(this);
}

Server::~Server()
{
  // kill all my controller connections
  printf( "~Server %p started\n", this );

  printf( "Removing %d controllers:\n", m_controllers.size() );
//  fflush(stdout);
  while ( m_controllers.size() )
  {
    m_controllers.get(0)->DumpState();
    m_controllers.remove(0);
  }

  printf( "Removing %d web req handlers\n", m_webHandlers.size() );
//  fflush(stdout);
  while ( m_webHandlers.size() )
  {
    m_webHandlers.get(0)->DumpState();
    m_webHandlers.remove(0);
  }

  printf( "Removing %d alarm handlers\n", m_alarms.size() );
//  fflush(stdout);
  while ( m_alarms.size() )
  {
    m_alarms.get(0)->DumpState();
    m_alarms.remove(0);
  }

  printf( "Removing forwarders...\n" );
  m_forwarders.clear();

  printf( "Removing req handlers...\n" );
  m_reqHandlers.clear();

  printf( "Removing script handlers...\n" );
  m_scriptHandlers.clear();

#ifndef _NO_DBCONN
  // make sure the database connections are released
  printf( "Removing db available pool: %d\n", m_dbpoolAvailable.size() );
  DBPoolList::iterator it;
  for ( it=m_dbpoolAvailable.begin(); it != m_dbpoolAvailable.end(); it++ )
  {
    DBConn *dbc = *it;
    delete dbc;
  }
  printf( "Removing db in use pool: %d\n", m_dbpoolInUse.size() );
  for ( it=m_dbpoolInUse.begin(); it != m_dbpoolInUse.end(); it++ )
  {
    DBConn *dbc = *it;
    delete dbc;
  }
#endif

  printf( "~Server complete \n" );
  // my sockets close automatically
}

void Server::OnDataAvailable( Stream *pSock )
{
  // figure out which socket it is, and do appropriate thing
  Socket *pNew = reinterpret_cast<Socket*>(pSock)->Accept();
  if ( !pNew )
  {
    printf( "Server::OnDataAvailable Accept returned null!\n" );
    return;
  }
  if ( pSock == &m_sockWeb )
  {
//printf( "Server::OnDataAvailable Accepted web connection\n" );
    WebRequestHandler *handler = new WebRequestHandler( new Connection(pNew), 
                            m_reqHandlers.getBase(), m_reqHandlers.size(),
                            m_scriptHandlers.getBase(), m_scriptHandlers.size() );
    m_webHandlers.add(handler);
  } 
  else if ( m_fwdAddr.length() ) // if we have a forwarding address, just create a forwarder
  {
    int port;
    if ( pSock == &m_sockControllers )
      port = PORT_CONTROLLERS;
    else if ( pSock == &m_sockNewerControllers )
      port = PORT_NEWERCONTROLLERS;
    else if ( pSock == &m_sockAlarms )
      port = PORT_ALARMS;
    else
      port = 0;
    if ( port )
    {
      Forwarder *fwd = new Forwarder( pNew, m_fwdAddr.c_str(), port );
      m_forwarders.add(fwd);
    }
  } else if ( pSock == &m_sockControllers || pSock == &m_sockNewerControllers )
  {
    // create a new controller object and add it to my list
#ifdef _WIN32
    MessageBeep( 0xffffffff );
#endif
//    printf( "Controller connected from %s\n", inet_ntoa(addr.sin_addr) );
    pNew->SetNoDelay(1); // make it send/receive things immediately, don't wait
    pNew->SetNonblocking(1);
    IController *c = new PrimaryController( &m_sockNewerControllers != pSock, pNew, this );
//    printf( "Controller %p connected on %s port (%s:%d)\n", c, &m_sockNewerControllers == pSock ? "new" : "old", inet_ntoa(pNew->GetAddress()->sin_addr), ntohs(pNew->GetAddress()->sin_port) );

    m_controllers.add(c);
//    ++workDone;
  } else if (pSock == &m_sockAlarms )
  {
    // got a new alarm connection
    printf( "Alarm connected\n" );
    Alarm *handler = new Alarm( this, pNew);
    m_alarms.add(handler);
  } else 
  {
    printf( "Server::OnDataAvailable could not figure out which socket became signalled!\n" );
    Connection conn(pNew); // this will ensure socket is closed
  }
}

void Server::OnException( Stream *pSock, int events )
{
  if ( events & POLLHUP )
  {
    printf( "Server::OnException closing fd %d\n", pSock->GetFD() );
    pSock->Close();  // will probably make the whole program quit, from Service() below
  }
}

void Server::AddRequestHandler( RequestHandler *handler, int noDelete )
{
  m_reqHandlers.add(handler,noDelete);
}
  
void Server::AddScriptHandler( ScriptHandler *handler, int noDelete )
{
  m_scriptHandlers.add(handler,noDelete);
}
  

void Server::OnAlarm( const char *mac, const char *typ, int unitNum, const char *msg )
{
  // parse this alarm and put it somewhere for another process to deal with it
  // format is {MAC}<CR>Alarm<CR>{UnitNum:3},{Msg}<CR>
  printf( "Logging ALARM: MAC='%s', Type='%s', #%d, Msg='%s'\n", mac, typ, unitNum, msg );
#ifdef _NO_DBCONN
  m_vReceivedAlarms.push_back( ReceivedAlarm( mac, typ, unitNum, msg ) );
#else
  DBConnection conn;
  if ( conn.LogAlarm( mac, typ, unitNum, msg, !strcmp("Alarm",typ) ? "1 HOUR" : NULL ) <= 0 )
  {
    
    // for now, print it out
    printf( "==> Failed to log ALARM!!\n" );
  }
#endif
}

int Server::Service()
{
  INSTRUMENT_THIS_FUNCTION

  int workDone = 0;
  
  if ( !m_sockWeb.IsOpen() || !m_sockControllers.IsOpen() )
    return -1; // need to quit

  // this takes care of all accepts and reads via callbacks
  Stream::DoSelect(10,10);

  // got through all forwarding connections and service them (all this does is prune them, no work is done in Service() )
  Forwarder *f;
  int i;
  for ( i=0; f=m_forwarders.get(i); ++i )
  {
    if ( f->Service() < 0 )
    {
      m_forwarders.remove(i);
      --i; // go back one so we do this slot again
    }
  } 
  
  // go through all controller connections and service them
//printf( "Servicing controllers\n");
  IController *c;
  // this loop checks for duplicates, and kicks older ones off
  // later ones in the list were connected later
  for ( i=0; c = m_controllers.get(i); ++i )
  {
    IController *c2;
    for ( int j=m_controllers.size()-1; j > i && (c2 = m_controllers.get(j)); --j )
    {
      if ( c->IsDuplicateOf( c2 ) )
      {
        c->TerminateAsDuplicate();
        m_controllers.remove( i );
        --i; // start with same index next time through i loop
        break;
      }
    }
  }
  // this loop goes and services all the controllers (which should no longer have duplicates)
  for ( i=0; c = m_controllers.get(i); ++i )
  {
    int result = c->Service();
    if ( result < 0 )
    {
//      printf( "Dropped controller at %s\n",  c->InetAddr() );
      m_controllers.remove(i);
      --i; // do this slot again
    } else
      workDone += result;
  }

  // go through all alarm connections and service them
  Alarm *a;
  for ( i=0; a = m_alarms.get(i); ++i )
  {
    int result = a->Service();
    if ( result < 0 )
    {
      printf( "Dropped alarm\n" );
      m_alarms.remove(i);
      --i; // do this slot again
    } else
      workDone += result;
  }
  
  // go through all web request connections and service them
//printf( "Servicing web handlers\n");
  WebRequestHandler *wrh;
  for ( i=0; wrh = m_webHandlers.get(i); ++i )
  {
    int result = wrh->Service();
    if ( result < 0 ) // done
    {
//      printf( "Dropped web handler\n" );
      m_webHandlers.remove(i);
      --i; // do this slot again
    }// else
      //workDone += result;
  }
  return workDone;
}

int Server::IsRequestByAdmin( HttpRequest *req )
{
  // if this connection came from localhost, then it is always authorized
  if ( req->IsConnectionLocal() )
    return 1;

  // look up user credentials, and do appropriate thing
  int userId = GetRequestUserId( req );
  if ( !userId )
    return 0; 
#ifdef _USERNAME_WITHOUT_DATABASE 
  return 1;
#endif
#ifndef _NO_DBCONN
  DBConnection conn;
  if ( !conn.IsConnectionOK() )
    return 0;
  return conn.IsAdminUser( userId ) > 0 ;
#else
  return 0;
#endif
}

int Server::IsRequestAuthorized( HttpRequest *req )
{
  // look up user credentials, and do appropriate thing
  int uid = GetRequestUserId( req );
  if ( !uid )
    return req->IsConnectionLocal(); // if this connection came from localhost, then it is always authorized
  return uid;
}

int Server::HandleUnauthorizedRequest( HttpRequest *req, HttpResponse *resp )
{
  resp->SendRedirect( "/index.htm?msg=Not%20Authorized" );
  return 1;
}

int Server::FindAuthorizedUser( const char *un, const char *pw )
{
  // look this up in a database
#ifdef _USERNAME_WITHOUT_DATABASE 
  return !strcmp(un,_USERNAME_WITHOUT_DATABASE) && !strcmp(pw,_USERNAME_WITHOUT_DATABASE);
#endif
#ifndef _NO_DBCONN
  DBConnection conn;
  if ( !conn.IsConnectionOK() )
    return 0;
  return conn.GetUserId( un,pw );
#else
return -1;
#endif
//  // TODO: return "site" ID that associates this user with particular Controllers registered to that site ID
// TODO: add special login so ALL controllers are visible
//  return !strcmp( un, "admin") && !strcmp( pw, "admin");
}

#ifdef _NO_DBCONN
int Server::GetAlarmList( HttpRequest *req, HttpResponse *resp )
{
  std::string Clear;
  if ( !req->GetParam("Clear", Clear ) )
    Clear = "0";
  m_vReceivedAlarms.List( resp, Clear.c_str() );
  return 1;
}
#endif

int Server::HandleSelectUnitRequest( HttpRequest *req, HttpResponse *resp )
{
  std::string unitID;
  if ( !req->GetParam("Unit", unitID ) )
    return 0;
  IController *c;
  for ( int i=0; c = m_controllers.get(i); ++i )
  {
    if ( c->GetUnitNum() <= 0 ) continue;
    if ( !unitID.compare( c->GetID() ) )
    {
      c->CauseRefresh(0);
      break;
    }
  }
  resp->SetCookie("UnitID",unitID.c_str() );
  resp->SendRedirect("/display.htm");
  return 1;
}

int Server::HandleRemoveUnitRequest( HttpRequest *req, HttpResponse *resp )
{
  std::string unitId;
  if ( !req->GetParam("Unit", unitId ) )
    return 0;
  IController *c;
  const char *under = strchr( unitId.c_str(), '_' );
  if ( !under ) under = &unitId[unitId.length()];
  std::string mac( unitId.c_str(), under );
  int unitNum = 0;
  if ( *under && under[1] != '*' ) // found underscore, so we have a subunit number
    unitNum = strtoul( under+1, 0, 10 );
  int found = 0;
  int count = 0;
  for ( int i=0; c = m_controllers.get(i); ++i )
  {
    if ( c->GetUnitNum() <= 0 ||  // invalid numbers
         c->GetUnitNum() > 1 )    // can't directly disconnect subunits
      continue;
    if ( !mac.compare( c->GetID() ) && (c->GetUnitNum() == unitNum || !unitNum) )
    {
      ++count;
      c->FreeFromOwner(-1,IController::FREEREASON_DISCONNECT);
#ifndef _NO_DBCONN
      DBConnection dbconn;
      dbconn.DisconnectTunnel(c->GetID());
#endif
      m_controllers.remove(i);
      --i; // in case it's a wildcard and there's more to come
    }
  }
  resp->SetContentType("text/plain" );
  char buf[100];
  sprintf( buf, "Disconnected %d\r\n", count );
  resp->Append(buf);
  resp->Finish();
  return 1;
}

#ifndef _NO_DBCONN

int Server::HandleChangePasswordRequest( HttpRequest *req, HttpResponse *resp )
{
  // check credentials and set cookie representing the user
  resp->NoCaching();
  
  int userId = IsRequestAuthorized( req );
  if ( !userId )
    return HandleUnauthorizedRequest(req,resp);
  
  std::string op,p,pc;
  int got = req->GetParam("OP",op) +
             req->GetParam("P",p) +
             req->GetParam("PC",pc);
  if ( !got )
    return 0; // default handling
    
  if ( got > 0 && got < 3 )
  {
    m_msg = "Inadequate information supplied";
    return 0;
  }
  
  if ( p.compare( pc ) )
  {
    m_msg = "New Password was not confirmed";
    return 0;
  }

  DBConnection conn;
  if ( !conn.IsConnectionOK() )
  {
    m_msg = "Bad Database Connection";
    return 0;
  }
  if ( conn.VerifyUserPassword( userId, op.c_str() ) <= 0 )
  {
    m_msg = "Old password did not match";
    return 0;
  }
  
  if ( conn.ChangeUserPassword( userId, p.c_str() ) <= 0 )
  {
    m_msg = "Password change failed";
    return 0;
  }

  m_msg = "Password change succeeded";
  return 0;
}

int Server::HandleLoginRequest( HttpRequest *req, HttpResponse *resp )
{
  // check credentials and set cookie representing the user
#ifdef _WIN32
  MessageBeep( 0xffffffff );
#endif
  resp->NoCaching();
  
  std::string un,pw;
  int token = 0;
  if ( !req->GetParam("UN",un) ||
       !req->GetParam("PW",pw) ||
       (token = FindAuthorizedUser(un.c_str(),pw.c_str())) <= 0 )
  {
    resp->SendRedirect("/index.htm?msg=Unrecognized%20Username%20or%20Password"); // go back to login page
    return 1;
  }
  
  char strToken[10];
  sprintf( strToken, "%d", token );
  resp->SetCookie( "UserID", strToken ); // TODO: encode/obscure this somehow
  resp->SendRedirect( "/select.htm" );
  return 1;
}

#endif

int Server::HandleAjaxRequest( IController *controller, HttpRequest *req, HttpResponse *resp )
{
  resp->NoCaching();

  int site = GetRequestUserId( req );
  if ( !site && req->IsConnectionLocal() )
  {
    //printf( "HandleAjaxRequest: no user given in request:\n" );
    //req->Dump();
    site = -1;
  }
  std::string btn,action,row,col;
  if ( req->GetParam("btn", btn ) )
  {
//printf( "[Ajax btn %s]\n", btn.c_str() );
    if ( !controller )
    {
	    resp->SetContentType("text/plain" );
	    resp->Append("disconnected");
	    resp->Finish();
      return 1;
    }

	  if ( !req->WasProcessed() )
    {
//ts_printf("[KeyHit %s]\n", btn.c_str() );
      if ( controller->OnKeyHit( site, btn.c_str() ) > 0 )
      {
//ts_printf( "KeyHitReq processed\n" );
        req->SetProcessed(); // don't mark this as processed until the key hit was sent to controller
      }
    }
    else if ( !resp->IsComplete() && !controller->IsReceivingLCDContents() )
    {
//printf("[LCD %X]\n", req );
//ts_printf( "KeyHitReq sending LCD\n" );
	    resp->SetContentType("text/plain" );
	    controller->SendLCDContents(site, resp);
	    resp->Finish();
    } else
    {
//ts_printf( "KeyHitReq awaiting LCD\n" );
    }

    return 1;
  } else if ( req->GetParam("touch", btn ) && 
              req->GetParam("row", row ) && 
              req->GetParam("col", col ) )
  {
    if ( !controller )
    {
	    resp->SetContentType("text/plain" );
	    resp->Append("disconnected");
	    resp->Finish();
      return 1;
    }

	  if ( !req->WasProcessed() )
    {
//printf("[Touch %X]\n", req );
      int r=atoi(row.c_str()),c=atoi(col.c_str());
	    if ( controller->OnTouch( site, r,c ) > 0 )
        req->SetProcessed();
    } 
    else if ( !resp->IsComplete() && !controller->IsReceivingLCDContents() )
    {
//printf("[LCD %X]\n", req );
	    resp->SetContentType("text/plain" );
	    controller->SendLCDContents(site, resp);
	    resp->Finish();
    }
    return 1;

  } else if ( req->GetParam("action",action) )
  {
    if ( !action.compare("updateSubunits") )
    {
      // Unit=MAC_#, mask=#
      // look at identifier and subunit mask, and pass to that controller
      std::string mask;
      if ( req->GetParam( "mask", mask ) )
      {
        unsigned long m = strtoul( mask.c_str(), 0, 0 );
        controller->SetAllowedSubunits( m );
        resp->Append("OK");
      } else
        resp->Append("mask missing");
      resp->Finish();
      return 1;
    }
    else if ( !action.compare("AdminPage") )
    {
      AdminPageBody(req,resp);
      resp->Finish();
      return 1;
    } else if ( !action.compare("refresh") )
    {
      if ( !controller )
      {
	      resp->SetContentType("text/plain" );
	      resp->Append("disconnected");
	      resp->Finish();
        return 1;
      }

	    resp->SetContentType("text/plain" );
	    controller->SendLCDContents(site, resp);
	    resp->Finish();
    } else if ( !action.compare("startscan") )
    {
      if ( !controller )
      {
	      resp->SetContentType("text/plain" );
	      resp->Append("disconnected");
	      resp->Finish();
        return 1;
      }

	    if ( !req->WasProcessed() )
      {
	      if ( controller->IsAvailableTo(site) )
        {
          std::string header;
          // make it possible for the PHP to control whether the scan includes a header (if the device supports it)
          if ( controller->StartScan( site, req->GetParam("header",header) && header[0] == '1' ) > 0 )  
          {
	          resp->SetContentType("text/plain" );
            resp->Append( "Scan started..." );
            req->SetProcessed();
	          resp->Finish();
            return 1;
          } //else
            //resp->Append( "Could not start scan!" );
        } else
        {
	        resp->SetContentType("text/plain" );
          resp->Append( "BUSY" );
	        resp->Finish();
          return 1;
        }
      }
    } else if ( !action.compare("downloadscan") )
    {
      if ( !controller )
      {
	      resp->SetContentType("text/plain" );
	      resp->Append("disconnected");
	      resp->Finish();
        return 1;
      }

	    resp->SetContentType("application/octet-stream;charset=us-ascii" );
      resp->AddHeader( "Content-Disposition", "attachment; filename=scandata.txt;" );
	    controller->GetScan(resp);
	    resp->Finish();
    } else if ( !action.compare("getscan") )
    {
      if ( !controller )
      {
	      resp->SetContentType("text/plain" );
	      resp->Append("disconnected");
	      resp->Finish();
        return 1;
      }

	    resp->SetContentType("text/plain" );
	    controller->GetScan(resp);
	    resp->Finish();
    } else if ( !action.compare("startdatalog") )
    {
      if ( !controller )
      {
	      resp->SetContentType("text/plain" );
	      resp->Append("disconnected");
	      resp->Finish();
        return 1;
      }

	    if ( !req->WasProcessed() )
      {
        if ( controller->IsAvailableTo(site) )
        {
	        if ( controller->StartDataLog(site,Bus::DATE_MIN,Bus::DATE_MAX) ) // originally had 123199, but the code on the systems considers that to be 1999 not 2099
          {
  	        resp->SetContentType("text/plain" );
            resp->Append( "Datalog retrieval started..." );
  	        resp->Finish();
            return 1;
          } else
          {
  	        resp->SetContentType("text/plain" );
            resp->Append( "BUSY" );
  	        resp->Finish();
            return 1;
          }
        } else
        {
  	      resp->SetContentType("text/plain" );
          resp->Append( "BUSY" );
  	      resp->Finish();
          return 1;
        }
      }
    } else if ( !action.compare("downloaddatalog") )
    {
      if ( !controller )
      {
	      resp->SetContentType("text/plain" );
	      resp->Append("disconnected");
	      resp->Finish();
        return 1;
      }

	    resp->SetContentType("application/octet-stream;charset=us-ascii" );
      resp->AddHeader( "Content-Disposition", "attachment; filename=datalog.txt;" );
	    controller->GetDataLog(resp);
	    resp->Finish();
    } else if ( !action.compare("getdatalog") )
    {
      if ( !controller )
      {
	      resp->SetContentType("text/plain" );
	      resp->Append("disconnected");
	      resp->Finish();
        return 1;
      }

	    resp->SetContentType("text/plain" );
	    int done = controller->GetDataLog(resp);
	    resp->Finish();
    } else if ( !action.compare("refreshController") )
    {
      // find controller with this mac and subunit, and refresh it from the database
	    resp->SetContentType("text/html" );
      RefreshController( req, resp );
      resp->Finish();
    } else if ( !action.compare("getControllerList") )
    {
      // return controller list table
	    resp->SetContentType("text/html" );
      resp->Append("<TABLE>");
      GetControllerList(req, resp);
      resp->Append("</TABLE>");
      resp->Finish();
    } else if ( !action.compare("clearUserFromControllers") )
    {
      // remove this user from ownership of any controllers
	    resp->SetContentType("text/html" );
      ClearUserFromControllers(req, resp);
      resp->Append("OK.");
      resp->Finish();
    } else if ( !action.compare("getControllerListCSV") )
    {
      // return controller list table
	    resp->SetContentType("text/plain" );
      GetControllerListCSV(req, resp);
      resp->Finish();
#ifndef _NO_DBCONN
    } else if ( !action.compare("getGSMControllerList") )
    {
      // return GSM controller list table
	    resp->SetContentType("text/html" );
      resp->Append("<TABLE>");
      GetGSMControllerList( req, resp);
      resp->Append("</TABLE>");
      resp->Finish();
#endif
    } else 
    {
	    resp->SetContentType("text/plain" );
	    resp->Append( "Invalid request" );
      resp->ReturnError(404,"Invalid request" );
	    resp->Finish();
      printf( "Unknown action to ajax: %s\n", action.c_str() );
req->Dump();
    }
	} else
  {
printf( "Ajax request not matched:\n" );
req->Dump();
  }

#if 0
  resp->SetContentType( "text/plain" );
  resp->Append( "AjaxHandler" );
  resp->Finish();
#endif
    
  return 1;
}

int Server::HandleRequest( HttpRequest *req, HttpResponse *resp )
{
  if ( !strncmp( "/index.htm", req->Uri(), 10 ) ||
       !strncmp( "/logout.htm", req->Uri(), 11 )
     ) 
  {
    resp->NoCaching();
    return 0; // always want default handling for this file
  }
  const char *mime = HttpResponse::GetMimeType( req->Uri() );
  if ( !mime
       || !strncmp( mime, "image/", 6 ) 
       || !strcmp( mime, "text/css") 
       || !strcmp( mime, "application/javascript" ) )
    return 0; // all images are authorized, and may be cached for up to a day (set in HttpResponse::Finish() )
    
  resp->NoCaching();

#ifndef _NO_DBCONN
  if ( !strncmp( "/login.htm", req->Uri(), 10 ) )
    return HandleLoginRequest(req,resp);
#endif
//  if ( !IsRequestAuthorized( req ) ) // if not authorized, always redirect to login page
//    return HandleUnauthorizedRequest(req,resp);

#ifndef _NO_DBCONN
  if ( !strncmp( "/changepw.htm", req->Uri(), 10 ) )
    return HandleChangePasswordRequest(req,resp);
#endif
#ifdef _NO_DBCONN
  if ( !strncmp( "/getAlarmList.htm", req->Uri(), 17 ) )
    return GetAlarmList(req,resp);
#endif
  if ( !strncmp( "/selectUnit.htm", req->Uri(), 15 ) )
    return HandleSelectUnitRequest(req,resp);

  if ( !strncmp( "/removeUnit.htm", req->Uri(), 15 ) ) //&& IsRequestByAdmin(req) )
    return HandleRemoveUnitRequest(req,resp);

  if ( !strcmp( "/removeWebRequests.htm", req->Uri() ) && IsRequestByAdmin(req) )
  {
    // go through and boot off all the web requests except my own
    WebRequestHandler *wrh;
    printf( "Booting off all web requests:\n" );
    for ( int i=0; wrh = m_webHandlers.get(i); ++i )
    {
      if ( wrh->IsReq(req) )
        continue;
      m_webHandlers.get(i)->DumpState();
      m_webHandlers.remove(i);
      --i;
    }
    resp->SendRedirect( "/admin.htm" );
    return 1;
  }
  if ( !strcmp( "/removeAlarms.htm", req->Uri() ) && IsRequestByAdmin(req) )
  {
    // go through and boot off all the web requests except my own
    Alarm *a;
    printf( "Booting off all alarm connections:\n" );
    for ( int i=0; a = m_alarms.get(i); ++i )
    {
      m_alarms.get(i)->DumpState();
      m_alarms.remove(i);
      --i;
    }
    resp->SendRedirect( "/admin.htm" );
    return 1;
  }
    
  if ( !strcmp( "/wakeController.htm", req->Uri() ) )
  {
    std::string strID,strName,strNum;
    if ( !req->GetParam( "ID", strID ) )
      return 0;
    if ( !req->GetParam( "Name", strName ) )
      return 0;
    if ( !req->GetParam( "Num", strNum ) )
      return 0;

    // TODO: send the wakeup GSM msg to this controller
    // Controller.Wake(strID);
    
    // redirect to page that will wait for that controller to connect
    std::string url( "/waitForController.htm?Timeout=60&ID=" );
    char buffer[200];
    sprintf( buffer, "%s&Name=%s&Num=%s", strID.c_str(), strName.c_str(), strNum.c_str() );
    url += buffer;
    resp->SendRedirect( url.c_str() );
    return 1;
  }

  if ( !strcmp( "/waitForController.htm", req->Uri() ) )
  {
    std::string strID,strTimeout;
    if ( !req->GetParam( "ID", strID ) )
      return 0;
    if ( !req->GetParam( "Timeout", strTimeout ) )
      return 0;
    int timeout = strtol(strTimeout.c_str(),0,0);

    // if the controller is now connected, redirect to select that controller
    if ( FindController( strID.c_str() ) )
    {
      resp->SetCookie("UnitID",strID.c_str() );
      resp->SendRedirect("/display.htm");
      return 1;
    }
    // if not connected, reduce the timeout and check that it's not expired
    if ( !timeout )
    {
      resp->SendRedirect( "/select.htm?msg=Timeout%20waiting%20for%20controller%20to%20connect" );
      return 1;
    }
    else
    {
      // somehow need to make a nice-looking page with a refresh URL whose timeout is lower each time
      resp->SendFileContents( "html" OS_SLASH_STR "waitForController.htm", m_scriptHandlers.getBase(), m_scriptHandlers.size() );
    }
    return 1;
  }
  
  // user is authorized, but has not selected a controller
  IController *controller = FindController( req ); 

  // TODO: verify that this controller is under control of this user

  // It's possible controller has been dropped... in that case, if it's an ajax request, return something to mean "disconnected" and to go to select page
  if ( !strcmp( "/ajax.htm", req->Uri() ) || !strcmp( "/ajax.php", req->Uri() ) )
    return HandleAjaxRequest(controller, req,resp);

  if ( !strcmp( "/admin.htm", req->Uri() ) )
  {
    resp->SendFileContents( "html" OS_SLASH_STR "admin.htm", m_scriptHandlers.getBase(), m_scriptHandlers.size() );
    return 1;
  }

  // any additional pages must be specifically added here, so this isn't a gaping security hole

  if ( !controller )
  {
    resp->SendFileContents( "html" OS_SLASH_STR "select.htm", m_scriptHandlers.getBase(), m_scriptHandlers.size() );
    return 1;
  }

  return 0;  // go to default handling of this request
}

#ifndef _NO_DBCONN
int Server::GetGSMControllerList( HttpRequest *req, HttpResponse *resp )
{
  int site = IsRequestAuthorized( req );
  if ( !site )
    return 0;

  int is_admin = IsRequestByAdmin( req );
        
  char buffer[3000];
  DBConnection conn;
  std::vector<ControllerUnitInfo> list;
  std::string sql;
  if ( !conn.FindControllersAccessibleBy( site, list, &sql ) )
  {
    //sprintf( buffer, "<!-- userId was %d\nSQL:\n%s\n-->\n", site,sql.c_str() );
    //resp->Append( buffer );
    resp->Append( "<TR><TD>No controllers found that you are authorized to wakeup via GSM.</TD></TR>" );
    return 0;
  }
  for ( unsigned int i=0; i<list.size(); ++i )
  {
    ControllerUnitInfo *unit = &(*(list.begin()+i));
    if (unit->msidsn.length() < 5 )
      continue; // not a GSM device
    if ( FindController( unit->identifier.c_str() ) )
      continue; // don't list those that are already connected
    int len = sprintf( buffer, "<TR><TD><A HREF=\"/wakeController.htm?ID=%s&Name=%s&Num=%03d\">Controller %s #%d</A></TD></TR>", 
                                unit->identifier.c_str(), unit->name.c_str(), unit->unitNum,
                                unit->name.c_str(), unit->unitNum ); //siteCount );
    if ( len+1 >= sizeof(buffer) )
      printf( "%s (%d): buffer overrun!\n", __FILE__, __LINE__ );
    resp->Append(buffer);
  }
  return 1;
}
#endif
int Server::AddControllerToHTMLList( HttpResponse *resp, IController *c, int user )
{
  char buffer[3000];
  char controller_name[500];

#ifdef _NO_DBCONN
    sprintf( controller_name, "Controller #%d (%s)", c->GetUnitNum(), c->GetID() ); //siteCount );
#else
    sprintf( controller_name, "Controller %s #%d (%s)", c->GetName(), c->GetUnitNum(), c->GetID() ); //siteCount );
#endif
  // TODO: make these not be links if a controller is busy (ie, "owned by another")
    
  if ( !c->IsAvailableForRemoteControl( user ) )
  {
    int len = sprintf( buffer, "<TR><TD>%s</TD></TR>", controller_name );
    if ( len+1 >= sizeof(buffer) )
      printf( "%s (%d): buffer overrun!\n", __FILE__, __LINE__ );
    resp->Append(buffer);
  } else
  {
    int len = 0;
    resp->Append("<TR>");
    if ( !c->IsAvailableForRemoteControl(user) )
    {
      len = sprintf( buffer, "<TD>%s [BUSY]</TD>", controller_name );
      if ( len+1 >= sizeof(buffer) )
        printf( "%s (%d): buffer overrun!\n", __FILE__, __LINE__ );
      resp->Append(buffer);
      resp->Append( "</TD>" );
    } else 
    {
      len = sprintf( buffer, "<TD><A HREF=\"/selectUnit.htm?Unit=%s\">%s</A></TD>", c->GetID(), controller_name ); //siteCount );
      if ( len+1 >= sizeof(buffer) )
        printf( "%s (%d): buffer overrun!\n", __FILE__, __LINE__ );
      resp->Append(buffer);
    }

    if ( c->IsAvailableForDataScan(user) ) // want all users to be able to remove their controllers is_admin ) // give a way to boot a controller off the server
    {
      len = sprintf( buffer, "<TD>&nbsp;</TD><TD><A HREF=\"/removeUnit.htm?Unit=%s\">[disconnect]</A></TD>", c->GetID() );
      if ( len+1 >= sizeof(buffer) )
        printf( "%s (%d): buffer overrun!\n", __FILE__, __LINE__ );
      resp->Append(buffer);
    }

    len = sprintf( buffer, "<TD>&nbsp;</TD><TD><SPAN CLASS=\"scanfloater\" ID=\"scandata%s\">TestScanContents</SPAN>", c->GetID() );
    if ( len+1 >= sizeof(buffer) )
      printf( "%s (%d): buffer overrun!\n", __FILE__, __LINE__ );
    resp->Append(buffer);
    if ( c->IsAvailableForDataScan(user) )
    {
      len = sprintf( buffer, "<SELECT ID=\"dropbox%s\" onChange=\"ScanData(this.value,'%s');\">"
                              "<Option VALUE=-1 SELECTED>Scan Data..."
                              "<OPTION VALUE=0>To Popup"
                              "<OPTION VALUE=1>And Download</SELECT></TD>", c->GetID(), c->GetID() );
      if ( len+1 >= sizeof(buffer) )
        printf( "%s (%d): buffer overrun!\n", __FILE__, __LINE__ );
      resp->Append(buffer);
    } else
    {
      resp->Append("[Scan unavailable]</TD>" );
    }
    if ( c->IsAvailableForDataLog( user ) )
    {
      len = sprintf( buffer, "<TD>&nbsp;</TD><TD><A TARGET=\"_blank\" HREF=\"datalog.htm?Unit=%s\">[Data Log]</A></TD>", c->GetID() );
      if ( len+1 >= sizeof(buffer) )
        printf( "%s (%d): buffer overrun!\n", __FILE__, __LINE__ );
      resp->Append(buffer);
    }

    resp->Append("</TR>");
  }
  return 1;
}
int Server::ClearUserFromControllers( HttpRequest *req, HttpResponse *resp )
{
  int site = IsRequestAuthorized( req );
  if ( !site )
    return 0;

  int is_admin = IsRequestByAdmin( req );
        
#ifndef _NO_DBCONN
  DBConnection conn;
#endif
  IController *c;
  int siteCount = 0;
  for ( int i=0; c = m_controllers.get(i); ++i )
  {
    c->FreeFromOwner( site, IController::FREEREASON_LIST_REQUESTED ); 
  }
  return 1;
}

int Server::GetControllerList( HttpRequest *req, HttpResponse *resp )
{
  int site = IsRequestAuthorized( req );
  if ( !site )
    return 0;

  int is_admin = IsRequestByAdmin( req );
        
#ifndef _NO_DBCONN
  DBConnection conn;
#endif
  IController *c;
  int siteCount = 0;
  for ( int i=0; c = m_controllers.get(i); ++i )
  {
    if ( c->GetUnitNum() <= 0 ) continue;
#ifndef _NO_DBCONN
#ifndef _USERNAME_WITHOUT_DATABASE 
    if ( conn.ControllerIsAccessibleBy( c->GetUnitNum(), c->GetID(), site ) <= 0 )
      continue;
#endif
#endif
    // if this controller thought it was under control of this user, the fact that the user
    // is requesting a controller list means they are no longer using any controller remotely
    // the controller only becomes free if that controller is currently marked under control of the given user
    // this is also where timeouts become possible
  //DBG_PRINTF( "Freeing unit %s from user %d\n", c->GetID(), site );
    c->FreeFromOwner( site, IController::FREEREASON_LIST_REQUESTED ); 

    if ( AddControllerToHTMLList( resp, c, site ) )
    {
      ++siteCount;
      IController *sub;
      for ( int j=0; (sub = c->GetSubunitByIndex(j)); ++j )
      {
        if ( AddControllerToHTMLList( resp, sub, site ) )
          ++siteCount;
      }
    }
  }
  if ( !siteCount )
  {
    resp->Append( "<TR><TD>No controllers found that you are authorized to access.</TD></TR>" );
  }
  return 1;
}

int Server::AddControllerToCSVList( HttpResponse *resp, IController *c )
{
  char buffer[3000];
  std::string str;    
#ifdef _NO_DBCONN
  // ID,MAC,UNIT,IN_USE_BY,SUBS,LASTSCAN
  sprintf( buffer, "%s_%d,%s,%d,%d,%d,%s\r\n",
            c->GetID(), c->GetUnitNum(), c->GetID(), c->GetUnitNum(), c->CurrentUser(), c->GetAllowedSubunits(), 
            c->GetScanText(str) );
#else
  // ID,UNITDBID,NAME,MAC,UNIT,IN_USE_BY,UNKNOWN,SUBS,FLAGS,LASTSCAN
  sprintf( buffer, "%s_%d,%d,%s,%s,%d,%d,%d,%d,%d,%s\r\n",
            c->GetID(), c->GetUnitNum(), c->GetUnitDBID(), c->GetName(), c->GetID(), c->GetUnitNum(), c->CurrentUser(), 
            c->IsUnknown(),(int)c->GetAllowedSubunits(), 
            c->GetFlags(),
            c->GetScanText(str) );
#endif
  resp->Append(buffer);
  return 1;
}

int Server::GetControllerListCSV( HttpRequest *req, HttpResponse *resp )
{
  int site = IsRequestAuthorized( req );
#ifdef _NO_DBCONN
  resp->Append( "ID,MAC,UNIT,IN_USE_BY,SUBS,LASTSCAN\r\n" ); // output some CSV headers
#else
  resp->Append( "ID,UNITDBID,NAME,MAC,UNIT,IN_USE_BY,UNKNOWN,SUBS,FLAGS,LASTSCAN\r\n" ); // output some CSV headers
#endif
  IController *c;
  int siteCount = 0;
  for ( int i=0; c = m_controllers.get(i); ++i )
  {
    if ( c->GetUnitNum() <= 0 ) continue;

//DBG_PRINTF( "Freeing unit %s from user %d (cur user = %d)\n", c->GetID(), site, c->CurrentUser() );
    c->FreeFromOwner( site, IController::FREEREASON_LIST_REQUESTED ); 
    if ( AddControllerToCSVList( resp, c ) )
    {
      ++siteCount;
      IController *sub;
      for ( int j=0; (sub = c->GetSubunitByIndex(j)); ++j )
      {
        if ( AddControllerToCSVList( resp, sub ) )
          ++siteCount;
      }
    }
  }
  return 1;
}

void Server::AdminPageBody( HttpRequest *req, HttpResponse *resp )
{
  char buffer[4000];
  std::string cont;
  req->GetParam("cont", cont );
  IController *c;
  int i,count=0;
  std::string ref;
  if ( req->GetParam("refresh",ref ) )
  {
    int nref = strtoul( ref.c_str(), 0, 0 );
    if ( nref > 0 )
    {
      sprintf( buffer, "<HTML><HEAD><META http-equiv=\"refresh\" content=\"%d\"></HEAD><BODY>", nref );
      resp->Append( buffer );
    }
  }
  resp->Append( "Controller Server built " __DATE__ " at " __TIME__ " (v"
                GIT_VERSION "/" GIT_DATE ")<BR>" );
#ifndef _NO_DBCONN
  sprintf( buffer, "DB Connection Pool : %d/%d used/total<BR>", 
                m_dbpoolInUse.size(),
                m_dbpoolInUse.size()+m_dbpoolAvailable.size() );
  resp->Append(buffer);
#else
  resp->Append( "DB support not included<BR>\n" );
#endif
  resp->Append("<H3>Controllers:</H3><DIV ALIGN=left><PRE>" );
  for ( i=0; c = m_controllers.get(i); ++i )
  {
    if ( cont.length() && cont.compare( c->GetID() ) )
      continue; // caller wants only a specific controller (for debugger)
    c->DumpState( HttpResponse::st_printf, resp );
    ++count;
    IController *sub;
    for ( int j=0; sub=c->GetSubunitByIndex(j); ++j )
    {
      resp->Append( "\nSUBUNIT: " );
      sub->DumpState( HttpResponse::st_printf, resp );
      ++count;
    }
  }
  if ( !count ) resp->Append( "[None]\n" );
  resp->Append("</PRE>" );
  if ( count )
    resp->Append("<A HREF=\"/removeUnit.htm?Unit=*\">[Remove All]</A>" );

#if 0
  resp->Append("</PRE></DIV><H3>Web requests:<H3><PRE>" );
  // go through all web request connections and service them
//printf( "Servicing web handlers\n");
  count = 0;
  WebRequestHandler *wrh;
  for ( i=0; wrh = m_webHandlers.get(i); ++i )
  {
    if ( wrh->IsReq(req) )
      continue; // don't show the request that caused the page to be served out
    ++count;    
    wrh->DumpState( HttpResponse::st_printf, resp );
  }
  if ( !count ) resp->Append( "[None]\n" );
  resp->Append("</PRE>" );
  if ( count )
    resp->Append("<A HREF=\"/removeWebRequests.htm\">[Remove All]</A>" );
#endif

  resp->Append("</PRE><H3>Alarm Connections:<H3><PRE>" );
  // go through all web request connections and service them
//printf( "Servicing web handlers\n");
  count = 0;
  Alarm *a;
  for ( i=0; a = m_alarms.get(i); ++i )
  {
    ++count;    
    a->DumpState( HttpResponse::st_printf, resp );
  }
  if ( !count ) resp->Append( "[None]\n" );
  resp->Append("</PRE>" );
  if ( count )
    resp->Append("<A HREF=\"/removeAlarms.htm\">[Remove All]</A>" );

  if ( cont.length() == 0 ) // only show this when on the main all-controller status page
  {
    std::string errlist;
    resp->Append( "<H3>Error List</H3>\n" );
    resp->Append( ErrorLog::make_list( errlist, "<BR>\n" ) );
  }
}

int Server::MakeInsertion( const char *invokee, const char *fn, HttpRequest *req, HttpResponse *resp )
{
  if ( !strcmp( invokee, "msg" ) )
  {
#ifdef _USERNAME_WITHOUT_DATABASE 
    resp->Append("<B><FONT COLOR=\"#ff0000\">SERVER RUNNING WITHOUT DATABASE!</FONT></B>");
#endif
    if ( m_msg.length() )
    {
      resp->Append( m_msg.c_str() );
      m_msg.clear();
    }
    return 1;
  }
  else if ( !strcmp( invokee, "AdminPageLink" ) )
  {
    char buffer[300];
    int site = IsRequestByAdmin( req );
    if ( !site )
      return 0;
    sprintf( buffer, "<A HREF=\"admin.htm\">Admin Page</A>" );
    resp->Append(buffer);
    return 1;
  }
  else if ( !strcmp( invokee, "AdminPageBody" ) )
  {
    int site = IsRequestByAdmin( req );
    if ( !site )
      return 0;
    AdminPageBody( req,resp );
    return 1;
  }
  else if ( !strcmp( invokee, "ControllerList" ) )
  {
    return GetControllerList( req, resp );
#ifndef _NO_DBCONN
  }
  else if ( !strcmp( invokee, "GSMControllerList" ) )
  {
    return GetGSMControllerList( req, resp );
#endif
  } else if ( !strcmp( invokee, "Logout" ) ) 
  {
    resp->SetCookie("UserID", "" );
    resp->SetCookie("UnitID", "" );
  } else if ( !strcmp( invokee, "ConfigGetREFRESH" ) )
  {
    char buf[20];
    sprintf( buf, "%d", 5000 );
    resp->Append( buf );
    return 1;
  } else if ( !strcmp( invokee, "AjaxRequest" ) )
  {
    IController *controller = FindController( req ); 

    HandleAjaxRequest(controller, req,resp);
    return 1;
  }
  return 0;
}

