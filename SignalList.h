#include <sys/types.h>
#include <sys/poll.h>
#include <stdlib.h>
#include <string.h>

class SignalList
{
#if defined (_LINUX_) && defined( POLLIN ) && defined( POLLERR ) 
//  #pragma message( "Socket is using poll() instead of select()" )
#define _USE_POLL 1
  struct pollfd* m_pollFDs;
  int m_numFDs,m_capFDs;
  int grow();
public:
  SignalList();
  ~SignalList();
  int AddFD( int fd );
  void RemoveFD( int fd );
  int Poll( long timeoutMS );
  int IsReadable( int index );
  int HasError( int index );
#elif defined( FD_SET )
//  #pragma message( "Socket is using select() instead of poll()" )
  fd_set m_masterFDS,m_rfds,m_efds;
  int m_maxFD,m_numFDs;
public:
  SignalList();
  int AddSocket( Socket *p );
  void RemoveSocket( Socket *p, int i );
  int Poll( long timeoutMS );
  int IsReadable( int index );
  int HasError( int index );
#else
#error could not figure out what kind of select/poll function to use!
#endif
};
