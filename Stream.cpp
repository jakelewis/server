#include "predef.h"
#include "Stream.h"
#include "SignalList.h"
#include <errno.h>

// abstract class describing a generic data stream, which could be a TCP socket or a serial port
Stream::StreamList Stream::sm_vStreams;
static SignalList g_streamSignals;

#ifndef MSG_NOSIGNAL
#define MSG_NOSIGNAL 0 
#endif

void Stream::AddStream( Stream *stm )
{
//printf( "Adding socket %p to list\n", sock );
  for ( StreamList::iterator it=sm_vStreams.begin(); it != sm_vStreams.end(); it++ )
  {
    if ( (*it) == stm )
      return; // already there
  }
  sm_vStreams.push_back( stm );
}
void Stream::RemoveStream( Stream *stm )
{
//printf( "Removing socket %p from list\n", sock );
  int index = 0;
  for ( StreamList::iterator it=sm_vStreams.begin(); it != sm_vStreams.end(); it++, ++index )
  {
    if ( (*it) == stm )
    {
      sm_vStreams.erase(it);
      return;
    }
  }
}

Stream::Stream()
: m_fd(-1)
{
  AddStream(this);
//  printf( "Stream %p created\n", this );
}
Stream::~Stream()
{
  RemoveStream(this);
  Close();
//  printf( "Stream %p destroyed\n", this );
}

int Stream::DoSelect( int loops, int ms_per_loop )
{
  INSTRUMENT_THIS_FUNCTION
  int maxLoops = loops;
  int didSomething=0;
  do
  {
    int res = g_streamSignals.Poll( ms_per_loop ); // 10ms polling
    if ( res < 0 )
    {
      int res2=errno;
      if ( errno )
        perror("poll/select failed");
      return -res2;
    }
    if ( res > 0 ) // something is ready
    {
      int found = 0;
      for ( unsigned int i=0; i<sm_vStreams.size(); ++i )
      {
        StreamList::iterator it=sm_vStreams.begin()+i;
        Stream *stm = *it;
        
        if ( stm && stm->IsOpen() && stm->m_pCallback )
        {
          if ( g_streamSignals.IsReadable(i) )
          {
//printf( "[R%d]", stm->GetFD() );
            stm->m_pCallback->OnDataAvailable( stm );
            ++didSomething;
          }
          int errmask;
          if ( errmask = g_streamSignals.HasError(i) )
          {
// just clear the error and don't say we did anything
            int error = 0;
            socklen_t errlen = sizeof(error);
            getsockopt(stm->GetFD(), SOL_SOCKET, SO_ERROR, (void *)&error, &errlen);

//            stm->m_pCallback->OnDataAvailable( stm );
//            ++didSomething;
//printf( "[E%d:%d]", stm->GetFD(), errmask );
            //stm->m_pCallback->OnException( stm, errmask );
            //++didSomething;
          }
          ++found;
        }
      }
      if ( !found ) 
        printf( "Poll returned 0x%X, but I found no signalled sockets in the set with callbacks!\n", res );
      if ( didSomething )
        return 1;
    }
  } while ( --maxLoops ); //&& didSomething );  
  return 0;
}

void Stream::SetCallback( Callback *cb ) 
{ 
  m_pCallback = cb; 
}
  
void Stream::SetFD( int fd )
{
  if ( m_fd != fd )
  {
//    printf( "Stream %p changing FD %d => %d\n", this, m_fd, fd );
    if ( -1 != m_fd )
      g_streamSignals.RemoveFD(m_fd);
    m_fd = fd;
    if ( -1 != m_fd )
      g_streamSignals.AddFD(m_fd);
  }
}

int Stream::GetFD( ) const
{
  return m_fd;
}

int Stream::IsOpen() 
{ 
  return m_fd >= 0; 
}

void Stream::Close() 
{ 
  int fd = m_fd;
  if ( fd > 0 ) 
  {
    SetFD(-1);
    close(fd); 
  }
}

#include <string.h>
int Stream::WriteStr( const char *str )
{
  return Write( str, (int)strlen(str) );
}

