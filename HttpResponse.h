#ifndef _included_HttpResponse_h_
#define _included_HttpResponse_h_

#include "Connection.h"
#include <string.h>
#include <string>
#include <vector>

// virtual class for having ways to insert data into HTML source files
// when particular tags are encountered

#include "HttpRequest.h"

class HttpResponse;
class ScriptHandler
{
public:
  // returns non-zero if the insertion was made
  virtual int MakeInsertion( const char *invokee, const char *fn, HttpRequest *req, HttpResponse *resp ) = 0;
};

class HttpResponse
{
  Connection *m_conn;
  HttpRequest *m_req;
  std::vector<char> m_responseContents;
  int m_retCode;
  std::string m_retString;
  std::string m_contentType;
  std::string m_extraHeaders;
  int m_noCache,m_completed;
  int m_status; // a place to get/store the status of serving this response
  // looks for insertion like <!--FUNCTIONCALL ConfigGetREFRESH -->
  void ParseAndInsert( const char *str, const char *fn, ScriptHandler **handlers, int nHandlers );

  void vprintf( const char *fmt, va_list args );
public:
  HttpResponse( Connection *c, HttpRequest *req );
  virtual ~HttpResponse();
  
  static const char *GetMimeType( const char *fn );

  void SetStatus( int stat );
  int GetStatus();
  
  void SetCookie( const char *nm, const char *val );
  void SetContentType( const char *ct );

  void Append( const char *txt );
  void AppendBinary( const char *txt, int numBytes );

  // this is a special function for non-tag HTML content, to ensure it is encoded the right way
  void AppendContentChar( const wchar_t uc );
  void AddHeader( const char *nm, const char *val );
  
  void NoCaching() ;

  void Finish();
  int IsComplete();
  void SendRedirect( const char *uri );
  void ReturnError( int errNum, const char *msg );

  void SendFileContents( const char *fn, ScriptHandler **handlers, int nHandlers );
  void printf( const char *fmt ... );
  static void st_printf( void *resp, const char *fmt ... );
};

#endif
