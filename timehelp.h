/* timehelp.h
 *
 * This file supplies types, macros, and inline functions to deal with elapsed time and timeouts on multiple platforms
 *
 * It tries to do it with the best possible resolution (which we hope is at least as good as milliseconds)
 */

#ifndef _included_timehelp_h_
#define _included_timehelp_h_

#include <time.h>

extern void timestamp(); // print a timestamp compatible with fail2ban's parsing
char *make_timestamp( char *buf ); // create a timestamp formatted string in the buffer, need at least 20 chars, returns same buf

#ifndef _WIN32

typedef struct timespec TIMESTAMP_T;
#define GET_CURRENT_TIME(x) clock_gettime( CLOCK_REALTIME, &(x) )
#define GET_TIMEOUT_TIME(x,toMS) { GET_CURRENT_TIME((x)); (x).tv_nsec += (long)(((long)toMS%1000)*1000000L); (x).tv_sec += (time_t)((toMS/1000)+(x).tv_nsec/1000000000L); (x).tv_nsec %= (long)1000000000L; }
#define GET_ZERO_TIME(x) (x).tv_sec = (x).tv_nsec = 0
#define IS_TIME_ZERO(x) ((x).tv_sec == 0 && (x).tv_nsec == 0)

__inline int HasPassed( struct timespec *t) 
{ 
  TIMESTAMP_T now; 
  GET_CURRENT_TIME( now );
  if ( now.tv_sec > t->tv_sec ) // definitely passed
    return 1;
  else if ( now.tv_sec < t->tv_sec ) // definitely not passed
    return 0;
  else 
    return ( now.tv_nsec >= t->tv_nsec ); // seconds equal, it's up to the nsec field
}
__inline signed long Elapsed( struct timespec *start )
{
  signed long elapsed_ms;
  struct timespec now;
  clock_gettime(CLOCK_REALTIME,&now);
  elapsed_ms = (now.tv_sec*1000+now.tv_nsec/1000000) 
         - (start->tv_sec*1000 + start->tv_nsec/1000000);
//printf( "Cntrlr elapsed %d\n", elapsed_ms);
  if ( elapsed_ms < 0 ) // something overflowed.... say it's really big!
    elapsed_ms = 0x7fffffff;
  return elapsed_ms;
}
#else

typedef clock_t TIMESTAMP_T;
#define GET_CURRENT_TIME(x)  x = clock()
#define GET_TIMEOUT_TIME(x,toMS) { GET_CURRENT_TIME(x); x += (toMS)*CLOCKS_PER_SEC/1000; }
#define GET_ZERO_TIME(x) x = 0
#define IS_TIME_ZERO(x) ((x) == 0)

int __inline HasPassed( clock_t *start ) 
{ 
  TIMESTAMP_T now; 
  GET_CURRENT_TIME( now );
  return now >= *start;
}
signed long __inline Elapsed( clock_t *start )
{
  signed long elapsed_ms;
  clock_t now = clock();
  elapsed_ms = (now-*start)*1000/CLOCKS_PER_SEC;
  return elapsed_ms;
}

#endif

#endif //ndef _included_timehelp_h_
