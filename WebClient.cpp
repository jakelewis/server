/*
 * WebClient.cpp
 *
 *  Created on: Oct 3, 2012
 *      Author: todd
 */

#include "predef.h"
#include <stdio.h>
#include <ctype.h>
#include "WebClient.h"
#include <string.h>
#include "LCD.h"
#include "Controller.h"
//#include "Config.h"
#if 0
extern "C" void AjaxRequest(int sock, const char* url)
{
	// url will look like ajax.htm?btn=XXXX
	//iprintf( "Processing AjaxRequest '%s'\n", url );

	if ( !strncmp( &url[9], "btn=",4 ))
	{
		g_controller->OnKeyHit( &url[9+4] );
		// wait long enough to receive an LCD update
		writestring(sock,"refresh");
	} else
		g_lcd->SendContents(sock);
	//iprintf( "Completed AjaxRequest\n" );

/*	char java_script[100];
	sprintf(java_script, " System Tick count:%u<BR>", TimeTick);
	writestring(sock, java_script );
	writestring(sock, url );
*/
}

extern "C" int OnWebClientPost( int sock, char *url, char *pData, char *rxBuffer )
{
//  iprintf("----- Processing Post -----\r\n");
//  iprintf("Post URL: %s\r\n", url);
//  iprintf("Post Data: %s\r\n", pData);
  if ( !strncmp( url+1,"updateConfig",12) )
  {
  	char errBuf[200];

  	if ( g_config->ScrapeFormParams(pData, errBuf) ) // got an error
  	{
  		writestring(sock,"<HTML>"
  				"<HEAD>"
  				"<Title>Config Update Error</title>"
  				"<LINK REL=stylesheet TYPE=\"text/css\" HREF=\"styles.css\">"
  				"</head><BODY><H2>Problem updating Configuration</H2>");
  		writestring(sock,errBuf);
  		writestring( sock,"<BR><BR><A HREF=config.htm>Click here to go back and try again</A></BODY></HTML>");
  	} else
  		RedirectResponse( sock, "config.htm" );
  } else
  	iprintf("Unknown post:%s, data=%s\n",url+1,pData );

	return 1; // presumably this means we handled it
}

WebClient *g_webClient = NULL;
#endif
WebClient::WebClient()
{
	//SetNewPostHandler( OnWebClientPost );
}
