#!/bin/bash

mysql -u se -pse -D se -e "START TRANSACTION; SET @N := NOW(); insert into core_unit_scans_archive select * from core_unit_scans where ts < DATE_SUB(@N,INTERVAL 30 DAY); DELETE FROM core_unit_scans where ts < DATE_SUB(@N,INTERVAL 30 DAY); COMMIT;" >/tmp/archive_scans.log 2>&1

