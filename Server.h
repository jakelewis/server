#ifndef _included_Server_h_
#define _included_Server_h_

// main server class, which listens on two ports, and when it gets a connection
// creates a handler object to handle that port

#include "PointerList.h"
#include "Socket.h"
#ifndef _NO_DBCONN
#include "DBConn.h"
#endif
#include "Controller.h"
#include "WebRequestHandler.h"
#include "Alarm.h"
#include "Forwarder.h"
#include <vector>

class Server : public RequestHandler, 
               public ScriptHandler, 
               public Stream::Callback,
               public Alarm::Callback
{
  Socket m_sockWeb,m_sockControllers,m_sockAlarms,m_sockNewerControllers; // listen sockets
  
  PointerList<Forwarder> m_forwarders;          // forwarders if we're forwarding all connections somewhere else
  PointerList<IController> m_controllers;        // currently-connected controllers
  PointerList<WebRequestHandler> m_webHandlers; // currently-running handlers
  PointerList<RequestHandler>    m_reqHandlers; // registered handlers for particular requests
  PointerList<ScriptHandler>     m_scriptHandlers; // for server-side fill-ins
  PointerList<Alarm> m_alarms;        // currently-connected alarms

#ifdef _NO_DBCONN
  class ReceivedAlarm
  {
  public:
    std::string mac,type,msg;
    int unit;
    ReceivedAlarm( const char *mc, const char *typ, int unitNum, const char *mg )
    : mac(mc), type(typ), msg(mg), unit(unitNum)
    {
    }
  };
  class AlarmList : public std::vector<ReceivedAlarm> 
  {
  public:
    void List( HttpResponse *resp, const char *clearVal )
    {
      resp->Append( "MAC,TYPE,UNIT,MSG\n" );
      for ( iterator it=begin(); it != end(); it++ )
      {
        char buf[500];
        sprintf( buf, "%s,%s,%d,%s\n", it->mac.c_str(), it->type.c_str(), it->unit, it->msg.c_str() );
        resp->Append( buf );
      }
      if ( clearVal[0]-'0' ) // if nonzero...
        clear(); // presumably this means that whomever made this request is going to dispatch and/or log the alarms
    }
  };
  AlarmList m_vReceivedAlarms;
#endif
  
  void ServiceWebConnection( Connection *c );
  int IsRequestAuthorized( HttpRequest *req );
  
  IController *FindController( const char *unitId );
  IController *FindController( HttpRequest *req );
  void RefreshController( HttpRequest *req, HttpResponse *resp );

  int FindAuthorizedUser( const char *un, const char *pw );
  
  int IsRequestByAdmin( HttpRequest *req );
  
  // goes here if client is not currently authorized -- force login
  int HandleUnauthorizedRequest( HttpRequest *req, HttpResponse *resp ); 
  
  // goes here when client hits login button
#ifndef _NO_DBCONN
  int HandleLoginRequest( HttpRequest *req, HttpResponse *resp ); 
#endif
  // selecting a new unit
  int HandleSelectUnitRequest( HttpRequest *req, HttpResponse *resp );
  
  int GetAlarmList( HttpRequest *req, HttpResponse *resp );

  int HandleRemoveUnitRequest( HttpRequest *req, HttpResponse *resp );
  
  // button presses and display refreshes come here
  int HandleAjaxRequest( IController *controller, HttpRequest *req, HttpResponse *resp );

#ifndef _NO_DBCONN
  int HandleChangePasswordRequest( HttpRequest *req, HttpResponse *resp );
#endif
  int ClearUserFromControllers( HttpRequest *req, HttpResponse *resp );
  int GetControllerList( HttpRequest *req, HttpResponse *resp );
  int GetControllerListCSV( HttpRequest *req, HttpResponse *resp );
#ifndef _NO_DBCONN
  int GetGSMControllerList( HttpRequest *req, HttpResponse *resp );
#endif
  void AdminPageBody( HttpRequest *req, HttpResponse *resp ); 

  int AddControllerToHTMLList( HttpResponse *resp, IController *c, int user );
  int AddControllerToCSVList( HttpResponse *resp, IController *c );

	std::string m_msg;
  std::string m_fwdAddr;

public:
  Server( const char *fwdAddr );// address to forward to, or NULL to handle everything locally
  ~Server();
  
  // callback from my listen sockets
  void OnDataAvailable( Stream *pSock );
  void OnException( Stream *pSock, int events );

  void AddRequestHandler( RequestHandler *handler, int noDelete=0 );
  
  void AddScriptHandler( ScriptHandler *handler, int noDelete=0 );
  
  void OnAlarm( const char *mac, const char *typ, int unitNum, const char *msg );
  
  int Service();

  // request handler
  int HandleRequest( HttpRequest *req, HttpResponse *resp );
  
  // insertion handler
  int MakeInsertion( const char *invokee, const char *fn, HttpRequest *req, HttpResponse *resp );
  
#ifndef _NO_DBCONN
  void GetConnection( DBConn **ppOut );

  void ReturnConnection( DBConn *dbc );

  // function to get a database connection
  class DBConnection : public IDBConn
  {
    DBConn *m_dbconn;
  public:
    DBConnection(); // this will get a connection from the pool
    virtual ~DBConnection(); // this will do a release

    // tell whether we have a working database connection
    int IsConnectionOK();

    // get the UserId of the user matching the given username and password
    int GetUserId( const char *username, const char *password );

    // ask if the particular user is an administrator
    int IsAdminUser( int userId );

    // ask if the given Controller is accessible by the given user ID
    int ControllerIsAccessibleBy( int unitId, const char *controller, int userId );

    // verify a user's current password
    int VerifyUserPassword( int userId, const char *pw );

    // change a user's password
    int ChangeUserPassword( int userId, const char *pw );

    // put an alarm entry into the database
    int LogAlarm( const char *mac, const char *typ, int unitNum, const char *msg, const char *minInterval );

    // put a datascan entry into the database
    int LogScan( int unitId, const char *scandata );

    // get a list of controllers accessible by the given userId
    int FindControllersAccessibleBy( int userId, std::vector<ControllerUnitInfo> &controllers, std::string *outSQL=NULL );

    int GetControllerInfo( const char *identifier, std::vector<ControllerUnitInfo> &info );

#ifdef _DO_SYNC
    int GetControllerSyncInfo( const char *identifier, int subunit, int scanfreq, SyncInfo &info );
#endif

    // this routine will return 1 if MSISDN is not null in this database entry.
    // returns 0 if database has MSIDSN as NULL
    // return -1 if this controller is not in the database at all.
    // this is here so Controller object does not talk directly to database.  Only this server object knows the database login info
    int IsControllerOnGSM( const char *identifier );

    // routine to be called to change the connection status of a controller
    // internally, this will figure out the server's IP address to put into the
    // database
    void UpdateConnectionStatus( const char *identifier, int unitnum, int connected, int flags, const char *conn_from_ip );

    // update the listed owner in the connection status table, so it can be known if the unit is busy.
    void UpdateUnitOwner( const char *identifier, int unitnum, int owner );

    virtual void DisconnectTunnel( const char *identifier );

#ifdef _DO_SYNC
    void SyncDatalog( int unit_dbid, int scanfreq, const char *log, SyncInfo *syncInfo );
#endif

  };
  typedef std::vector<DBConn*> DBPoolList;
  DBPoolList m_dbpoolAvailable,m_dbpoolInUse;
#endif
};

//extern Server *g_server;


#endif //ndef _included_Server_h_
