// see header for class API explanations

#include "predef.h"
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include "Alarm.h"

Alarm::Alarm( Callback *pCB, Socket *pSock )
: Connection(pSock),
  m_pCallback(pCB),
  m_done(0)
{
#ifndef _WIN32
  clock_gettime(GTOD_CLOCK, &m_start);
#else
  m_start = clock();
#endif
}

Alarm::~Alarm()
{
}

signed long Alarm::TimeElapsed()
{
  signed long elapsed_ms;
#ifndef _WIN32    
  struct timespec now;
  clock_gettime(GTOD_CLOCK, &now);
  elapsed_ms = now.tv_sec*1000 + now.tv_nsec/1000000 
              - (m_start.tv_sec*1000 + m_start.tv_nsec/1000000);
#else
  clock_t now = clock();
  elapsed_ms = (now-m_start)*1000/CLOCKS_PER_SEC;
#endif
  return elapsed_ms;
}
void Alarm::OnException( Socket *pSock, int events )
{
  Connection::OnException(pSock,events);
  
  m_done = 1;
}

void Alarm::OnDataAvailable( Socket *pSock )
{
  Connection::OnDataAvailable(pSock);
  
  // do something with the alarm message
  ReadStr( m_msg, Available() );
    
}

// used to check the state of its connection and process any received data
// returns >0 if it actually did something and wants to be called again
// return <0 if it needs to be killed off
// calls Controller::Service, and may process data 
int Alarm::Service()
{
  INSTRUMENT_THIS_FUNCTION
  if ( m_done )
    return -1;

  // add something here to timeout the connection to avoid using up sockets if we're under attack
  if ( TimeElapsed() > 60000 )
  {
    printf( "Alarm time limit exceeded: '%s'\n", m_msg.c_str() );
    m_done = 1;
    return -1;
  }
  
  // read in the message until we get a CR, and then parse it
  if ( m_msg.length() > 100 ) // don't allow this
  {
    printf( "Alarm exceeded max length: '%s'\n", m_msg.c_str() );
    m_done = 1;
  }
  else if ( m_msg.length() > 0 )
  {
    const char *pos1 = strchr( m_msg.c_str(), '\n' ),*pos2,*pos3,*pos4;
    if ( pos1 && !strncmp( pos1+1, "Alarm", 5 ) // appears to be a valid message
         && (pos2=strchr(pos1+1,'\n') ) 
         && (pos3=strchr(pos2+1,',')) 
         && (pos4=strchr(pos3+1,'\r')) 
        ) // message is done
    {
      std::string mac( m_msg.c_str(), pos1 );
      std::string typ( pos1+1, pos2 );
      int unitNum = strtol( pos2+1, 0, 10 );
      std::string msg( pos3+1, pos4 );
      m_pCallback->OnAlarm( mac.c_str(), typ.c_str(), unitNum, msg.c_str() );
      m_done = 1;
    } else
    {
      int a = 0;
    }
  }

  return m_done ? -1 : 0;
}

void Alarm::DumpState( void (*poutfcn)( void *vparam, const char *fmt ... ), void *vparam )
{
	std::string str;
	if ( !poutfcn ) { poutfcn = (void (*)( void *vparam, const char *fmt ... ))fprintf; vparam = (void*)stdout; }
  poutfcn( vparam, "Alarm(%s): Received '%s' (Elapsed %d)\n", GetAddressAndPort(str), m_msg.c_str(), TimeElapsed() );
  const char *pos1 = strchr( m_msg.c_str(), '\n' ),*pos2,*pos3,*pos4;
  if ( pos1 )
  { 
    if ( strncmp( pos1+1, "Alarm", 5 ) )
      poutfcn( vparam, "  Does not have 'Alarm' after first LF\n" );
    else if ( !((pos2=strchr(pos1+1,'\n') ) ) )
      poutfcn( vparam, "  Does not have second LF\n" );
    else if ( !(pos3=strchr(pos2+1,',')) )
      poutfcn( vparam, "  Does not have comma after second LF\n" );
    else if ( !(pos4=strchr(pos3+1,'\r'))  )
      poutfcn( vparam, "  Does not have any CR after second LF\n" );
   } else
    poutfcn( vparam, "  Does not have LF\n" );
}
