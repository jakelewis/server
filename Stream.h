#ifndef _included_Stream_h_
#define _included_Stream_h_

#include <vector>

class IStream
{
public:
  virtual int IsOpen()=0;
  virtual void Close()=0;

  virtual int Read( void *data, int numBytes ) = 0;
  virtual int Write( const void *data, int numBytes ) = 0;
  virtual int WriteStr( const char *str ) = 0;

  // how many bytes are available to be read
  virtual int DataReadable() = 0;
};

// abstract class describing a generic data stream, which could be a TCP socket or a serial port
class Stream : public IStream
{
	static void AddStream( Stream *sock );
	static void RemoveStream( Stream *sock );
	typedef std::vector<Stream*> StreamList;
  static StreamList sm_vStreams;
  int m_fd; // my file descriptor

public:
  class Callback
  {
  public:
    virtual void OnDataAvailable( Stream *pSock ) = 0;
    virtual void OnException( Stream *pSock, int polleventsreceived ) = 0;
  };
protected:
  Callback *m_pCallback;

public:
  Stream();
  virtual ~Stream();

  // wait for activity on any of the sockets in the list
  // and call the callback's OnDataAvailable function with 
  // appropriate flags
  static int DoSelect(int loops, int ms_per_loop);

  void SetCallback( Callback *cb );
  void SetFD( int fd );
  int GetFD() const;
  
	virtual int IsOpen();
	virtual void Close();

  typedef enum
  {
    STREAMTYPE_UNKNOWN=0,
    STREAMTYPE_TCP=1,
    STREAMTYPE_SERIAL=2
  } EStreamTypes;
  virtual int StreamType() { return STREAMTYPE_UNKNOWN; }
  int WriteStr( const char *str );
  
  
  bool operator == (const Stream &other ) const
  {
    return other.m_fd == m_fd;
  }
  
};

#endif //ndef _included_Stream_h_
