<?php
$debug = 0;
if ( $debug )
{
error_reporting(E_ALL);
ini_set( 'display_errors','1' );
}

  include "session.php";
  global $dbh;
function refuse($reason, $extra)
{
  http_response_code(400); // bad request
  // log this bad request
  global $dbh;
  $stmt = $dbh->prepare( "INSERT INTO PiPackageRefusals (reason,extra,ip) VALUES (:reason,:extra,:ip)" );
  $stmt->execute( array( "reason" => $reason, 
                         "extra" => isset($extra) ? $extra : "[empty]",
			"ip" => $_SERVER['REMOTE_ADDR'] ) );
  die($reason);
}
  if ( !isset( $_REQUEST['cpu'] ) || !isset( $_REQUEST['sd'] ) || !isset( $_REQUEST['mac'] ) || !isset( $_REQUEST['shadow']) )
  {
    refuse("Bad request","");
  }

  $allowed_ips = array( 
                         "50.173.216.211" => 1,
                         "207.154.78.1" => 1
                      );

  $cpu = $_REQUEST['cpu'];
  $sd  = $_REQUEST['sd'];
  $mac = $_REQUEST['mac'];
  $shad = $_REQUEST['shadow'];
  $type = isset( $_REQUEST['type'] ) ? $_REQUEST['type']:"";


  // TODO: see if this unit is in the database.  
  $stmt = $dbh->prepare( "SELECT * FROM PiPackageAuthorizations WHERE ".
             "mac=:mac AND sd=:sd AND cpu=:cpu" );
  $params = array( "mac"=>$mac, "sd"=>$sd, "cpu" => $cpu );
  $stmt->execute( $params );
  $result=$stmt->fetch();
//  var_dump($result);
  if ( $result == FALSE || $result[0] == 0 )
  {
    // if this is NOT happening from a known SBCS IP address, REFUSE
    if ( !isset( $allowed_ips[$_SERVER['REMOTE_ADDR']] ) )
      refuse( "New from unauth ip", "" );

    // add it as a disabled device and return an error
    $stmt = $dbh->prepare( "INSERT INTO PiPackageAuthorizations (registered,mac,sd,cpu,systype,last_check,shadow) ".
             " VALUES (NOW(),:mac,:sd,:cpu,:type,NOW(),:shad)" );
    $params['type'] = $type;
    $params['shad'] = $shad;
    $stmt->execute($params);
    
    http_response_code(400); // bad request
    //var_dump($result);
    die("Unregistered"); // this is not logged, it will happen normally
  }
// TODO: put last_check_ip in here too
  $stmt = $dbh->prepare( "UPDATE PiPackageAuthorizations SET check_count=check_count+1,last_check=NOW(),last_check_ip=:ip where mac=:mac AND sd=:sd AND cpu=:cpu " );
  $params['ip']=$_SERVER['REMOTE_ADDR'];
  $stmt->execute($params);
  // if it's there and disabled, return an error
  if ( !$result['enabled'] )
  {
    refuse("Disabled",$result['id'] );
  }
  // if it's there and enabled, return package type it's signed up for
  $fn = $result['systype']; // DB tells what kind of device this will be
  $file = "dl/$fn";
  if ( !file_exists( $file.".tgz" ) || !file_exists( $file.".sig" ) )
  {
    refuse("Error 2","systype $file not found" ); // 2 = file not found
  }

  if ( strlen( $cpu) > 16 )
    $cpu = substr( $cpu,-16 );
$encrypt = "/usr/bin/openssl enc -e -des-cfb -S $cpu -k \"$mac$sd$shad\" -in ${file}.tgz";
if ( $debug )
{
  echo "$encrypt\n";
  echo 'Last-Modified: '.gmdate('D, d M Y H:i:s', filemtime($file.".tgz"));
} else
{
  header( "Content-Disposition: attachment; filename=\".sbcspkg.bin\"" );
  header( "Content-Type: application/octet-stream");
  header('Last-Modified: '.gmdate('D, d M Y H:i:s', filemtime($file.".tgz")));
  header('Cache-Control: must-revalidate');
  header('Pragma: public');
  header('Expires: 0');
//  $encrypted = `openssl enc -e -des-cfb -S $cpu -k \"$mac$sd\" -in ${file}.tgz`;
//  $content_len = filesize($file.".sig")+strlen($encrypted);
//  header('Content-Length: ' . $content_len );

  // NOW we do an encryption of this file as we read it in, sending out the parts
  readfile($file.".sig"); // output the signature 
  passthru( $encrypt );
}
?>
