<?php
$logging_out = 1;

$page_title = "Chemtrol remote monitoring";
$force_logout = 1;

  // get the current controller list, so it will clear up any controllers
  // that it thinks we're in control of
  $usr = isset($_SESSION['UserId']) ? 0+$_SESSION['UserId'] : 0;
  $url = "http://127.0.0.1:8077/ajax.htm?User=$usr&action=clearUserFromControllers";
?><?php @include "header.php"?>
<?php 
  if ( $usr > 0 )
    file_get_contents( $url );
  $_SESSION['UserId'] = 0; // make this be empty, so there is no user here anymore
  echo "<H2>You are now logged out.</H2>";
?>

<A HREF="index.php">Click here to go back to login page</A>
<?php @include "footer.php"?>
