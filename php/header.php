<?php //session_start();

  // check that we're using https, and if not, redirect there
  if(!isset($logging_out) && 
      ( !isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "") 
      && isset( $_SERVER['HTTP_HOST']) && isset( $_SERVER['REQUEST_URI']) )
  {
    $redirect = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    header("HTTP/1.1 301 Moved Permanently");
    header("Location: $redirect");
    die;
  }
  include_once "session.php";

  global $logged_in_as;
  global $user_name;
$menulinks = array();
if ( $logged_in_as > 0 && !isset($logging_out) && !isset($logging_in) )
{
  if ( $page_fn != "select.php" )
  {
    array_push( $menulinks, "<A HREF=\"select.php\">Select Controller</A>");
  }
  if ( $page_fn != "display.php" && $page_fn != "admin.php" && ( $is_superuser || $user_level > 1 ) )
  {
    array_push( $menulinks, "<A HREF=\"admin.php\">Admin Page</A>");
  }
  if ( $page_fn != "display.php" && $page_fn != "changepw.php" )
  {
    array_push( $menulinks, "<A HREF=\"changepw.php\">Change Password</A>");
  }
  if ( $page_fn != "logout.php" )
  {
    array_push( $menulinks, "<A HREF=\"logout.php\">Logout $user_name</A>");
  }
}
if ( isset( $no_menus ) && $no_menus )
{
  $menulinks = array(); // just trash the array
}

if ( !isset($page_title) )
{
  $page_title = "Chemtrol Remote Monitoring";
}
?><!DOCTYPE html><html lang="en">
<head>
<meta charset="utf-8"/>
<Title><?= $page_title ?></title>
<script LANGUAGE="JavaScript" src="SonoraEng.js"></script>
<LINK REL=stylesheet TYPE="text/css" HREF="styles.css">
<?= isset($head_script) ? $head_script : "" ?>
</head>
<body onload="<?= isset($page_onload) ? $page_onload : "" ?>">

<CENTER><TABLE>
<TR><TD ALIGN=CENTER><IMG SRC="toplogo.gif"></TD></TR>
<TR>
<TD><?php 
if ( count($menulinks) > 0 )
{
  ?><TABLE WIDTH="100%" BORDER=0 CELLSPACING=0 CELLPADDING=0><TR>
  <TD WIDTH="<?= 100/count($menulinks) ?>%" ALIGN=CENTER>
  <?= join( "</TD><TD WIDTH=\"". 100/count($menulinks) ."%\" ALIGN=CENTER>", $menulinks ) ?>
  </TD></TR></TABLE><?php
}
?>
</TD>
</TR>
</TABLE>
<BR>
<br>
<?= $msg  ?><br>
<noscript>
<H1 STYLE="color: red">In order to use this site effectively, you must enable JavaScript.</H1>
</noscript>
<SPAN ID=MainContentSpan STYLE="visibility: hidden;">
