
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/><script src="./js/jquery-1.7.js"></script>
<script src="./js/moment.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="./js/graph.js"></script>
<script src="./js/graph-temp.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<style>
.centerAlign{
width:800px;
margin: 0 auto;
}
.input{
  background-color: rgb(0,150,200);
  color: rgb(254,242,0);
}
.col_80{
width:80%;margin:auto;
}
.boxBorder{
border:2.5px solid lightblue;
}
#progress {
    display: block;
    width: 100%;
    height: 100%;
    min-height: 100%;
    position: fixed;
    z-index: 10000;
    top: 0;
    left: 0;
    background: white;
    opacity: 0.5;
}
#progress div {
    position: absolute;
    top: 50%;
    left: 50%;
}
</style>
</head>
<body>
</body>
</html>