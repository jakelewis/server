<?php 

  $require_login = 1;
  include "session.php";

  $cid = $_REQUEST["user"];
  if ( !isset($cid) ) die( "Invalid request" );
  $custid = $_REQUEST["cust"];
  if ( !$is_superuser && !isset( $custid) )
    $custid = $user_customer;

  global $dbh;
  // TODO: make it so a customer admin can edit their own info
  if ( !$is_superuser && $user_level < 2 ) die( "Superuser or admin rights required!" );

  $fields_req = array( "username"=>1, "email"=>1 );
  $fields = array( "username"=>"Username","first_name"=>"First Name",
                   "last_name"=>"Last Name",
			"email"=>"Email",
			"user_level"=>"User Level", "is_enabled"=>"Enabled", 
			"is_superuser"=>"Superuser",
//			"smsnumber" => "SMS Number",
			"last_login" => "Last Login",
			"date_joined" => "Date Joined"
                  );
  $ua_fields = array( "smsnumber"=>"SMS Number" );
  $fields_disabled = array( "user_level" => $user_level < 2 && !$is_superuser,
                            "is_superuser" => !$is_superuser,
                            "is_enabled" => $user_level < 2 && !$is_superuser );
  $fields_ro = array( "last_login"=>1, "date_joined"=>1 );
  $fields_enum = array( "user_level" => array( 1 => "Regular", 2 => "Admin" ) );
  $fields_bool = array( "is_enabled"=>1, "is_superuser"=>1 );

  // see if a form has been submitted, and if so, do the appropriate thing
  if ( isset( $_REQUEST["SUBMIT"] ) )
  {
     $errmsg;
     if ( $_REQUEST["SUBMIT"] == "ADD NEW" )
     {
       // this does NOT receive read-only fields, 
       //      and DOES receive p1 & p2 for password
       // needs to check that password and confirm match, 
       if ( !isset( $_REQUEST['cust'] ) )
         $errmsg = "Cannot add user without known customer.";
       else if ( !isset( $_REQUEST['p1']) || $_REQUEST['p1'] == "" || 
             $_REQUEST['p1'] != $_REQUEST['p2'] )
       {
         $errmsg = "Passwords did not match!";
       }
       // and that username is unique
       else
       {
         $stmt = $dbh->prepare( "SELECT id FROM auth_user where username=:un" );
         $stmt->execute( array( "un" => $_REQUEST['username'] ) );
         if ( $stmt->fetch() )
           $errmsg = "Username already exists.";
         else
         {
           echo "Performing ADD NEW<BR>" ;
           $pwstr = makePasswordString( "".rand(0,99999), $_REQUEST['p1'] );
           $vallist = "NOW(),'$pwstr'";
           $fldlist = "date_joined,password";
           $params = array( );
           foreach ( $fields as $nm=>$val )
           {
             if ( $fields_ro[$nm] || $fields_disabled[$nm] ) continue;
             if ( !$fields_bool[$nm] && !isset( $_REQUEST[$nm] ) ) die( "Cannot find all parameter values ($nm)!" );
             if ( $fldlist != "" )
               $fldlist .= ", ";
             $fldlist .= $nm;
             if ( $vallist != "" )
               $vallist .= ", ";
             if ( "" == $_REQUEST[$nm] && !$fields_bool[$nm] )
               $vallist .= "NULL";
             else
             {
               $vallist .= ":".$nm;
               if ( $fields_bool[$nm] )
                 $params[$nm] = $_REQUEST[$nm] == "on" ? 1 : 0;
//               else if ( isset($fields_enum[$nm]) )
//                 $params[$nm] = $_REQUEST[$nm];
               else
                 $params[$nm] = $_REQUEST[$nm];
             }
           }
//echo $vallist;
//var_dump( $params );
           $stmt = $dbh->prepare( "INSERT INTO auth_user (".$fldlist.") ".
	    	"VALUES (".$vallist.")" );
           $res = $stmt->execute( $params );
           $stmt = $dbh->query( "SELECT @@IDENTITY" );
           $ary = $stmt->fetch();
           $cid = $ary[0];
           $stmt = $dbh->prepare( "INSERT INTO core_useraccount (user_ptr_id,customer_id,smsnumber) VALUES (:uid,:cid,:sms)" );
           $stmt->execute( array( "uid"=>$cid, "cid"=>$_REQUEST['cust'],
                                  "sms" => $_REQUEST['smsnumber'] ) );
         }
       }
     }
     else if ( $_REQUEST["SUBMIT"] == "UPDATE" )
     {
       echo "Performing UPDATE<BR>" ;
       $vallist = "";
       $params = array( "id" => $_REQUEST['user'] );
       foreach ( $fields as $nm=>$val )
       {
         if ( $fields_ro[$nm] || $fields_disabled[$nm] ) continue;
         if ( !isset( $_REQUEST[$nm] ) && !$fields_bool[$nm] ) 
           die( "Cannot find all parameter values ($nm)!" );
         if ( $vallist != "" )
           $vallist .= ", ";
         if ( "" == $_REQUEST[$nm] )
           $vallist .= $nm.="=NULL";
         else
         {
           $vallist .= $nm."=:".$nm;
           if ( $fields_bool[$nm] )
             $params[$nm] = $_REQUEST[$nm] == "on" ? 1 : 0;
           else
             $params[$nm] = $_REQUEST[$nm];
         }
       }
//echo $vallist;
//var_dump( $params );
       $stmt = $dbh->prepare( "UPDATE auth_user SET ".$vallist." ".
		"WHERE id=:id" );
       $res = $stmt->execute( $params );
       $stmt = $dbh->prepare( "UPDATE core_useraccount SET smsnumber=:sms ".
		"WHERE user_ptr_id=:id" );
       $params["sms"] = $_REQUEST["smsnumber"];
       $res = $stmt->execute( array( "sms"=>$_REQUEST["smsnumber"], 
                                     "id"=>$_REQUEST["user"] ) );
     } else if ( $_REQUEST["SUBMIT"] == "DELETE" && isset( $_REQUEST['user']) )
     {
       echo "Performing DELETE " ;
       $params = array( "id" => $_REQUEST['user'] );
       // needs to delete the user and anything that points to it:
       //    core_unit_users, auth_user_groups, core_useraccount, 
       //    auth_user_user_permissions
       $stmt = $dbh->prepare( "DELETE from core_useraccount WHERE user_ptr_id=:id" );
       $stmt->execute( $params );
       $stmt = $dbh->prepare( "DELETE from auth_user_groups WHERE user_id=:id" );
       $stmt->execute( $params );
       $stmt = $dbh->prepare( "DELETE from auth_user_user_permissions WHERE user_id=:id" );
       $stmt->execute( $params );
       $stmt = $dbh->prepare( "DELETE from core_unit_users WHERE user_id=:id" );
       $stmt->execute( $params );
       $stmt = $dbh->prepare( "DELETE from auth_user WHERE id=:id" );
       $stmt->execute( $params );

     } else
       echo "Unknown operation!\n";
 //    echo "<A HREF=\"admin.php?tab=user&user=$cid".(isset($_REQUEST['cust']) ? "&cust=".$_REQUEST['cust']:"")."\">go here</A>";
     echo "<SCRIPT LANGUAGE=JavaScript>window.location.replace( \"admin.php?tab=user&user=$cid".
         (isset($_REQUEST['cust']) ? "&cust=".$_REQUEST['cust']:"").
         (isset($errmsg) ? "&err=".$errmsg : "").
          "\");</SCRIPT>";
     exit();
  }
  $params = array( 'id' => $cid );
  $stmt = "";
  if ( isset( $custid) )
  {
    $params['custid'] = $custid;
    $stmt = $dbh->prepare( "SELECT ".join(",",array_keys($fields))." ".
		"FROM auth_user u, core_useraccount ua ".
		"WHERE u.id=:id AND u.id=ua.user_ptr_id ".
                "AND ua.customer_id=:custid" );
  } else 
  {
    $stmt = $dbh->prepare( "SELECT ".join(",",array_keys($fields))." ".
		"FROM auth_user u ".
		"WHERE u.id=:id" );
  }
  if ( !$stmt || !$stmt->execute( $params ) )
      return 0;

  $entry = $stmt->fetch();
  if ( $cid > 0 )
    echo "<A HREF=\"changepw.php?UID=$cid&returnToAdmin=1\">Change Password...</A>";
  if ( $is_superuser && $cid > 0 )
    echo " <A HREF=\"impersonate.php?UID=$cid\">Impersonate</A>";

  echo "<BR><FORM METHOD=GET ACTION=\"get_user.php\" ID=\"userform\" AUTOCOMPLETE=off>";
  echo "<INPUT TYPE=HIDDEN NAME=user VALUE=$cid>";
  if ( isset( $_REQUEST['cust'] ) )
    echo "<INPUT TYPE=HIDDEN NAME=cust VALUE=".$_REQUEST['cust'].">";
  echo "<TABLE>";
  foreach ( $fields as $nm=>$desc )
  {
    if ( -99 == $cid && $fields_ro[$nm] ) 
      continue;
    if ( $fields_disabled[$nm] )
      continue;
    echo "<TR><TD ALIGN=RIGHT>$desc:</TD><TD ALIGN=LEFT>";
    if ( $fields_bool[$nm] )
      echo "<INPUT TYPE=CHECKBOX NAME=$nm ".($entry[$nm]?"CHECKED":"").($fields_ro[$nm]?" readonly":"")."></TD></TR>";
    else if ( isset($fields_enum[$nm]) )
    {
      echo "<SELECT NAME=$nm>\n";
      if ( !isset( $fields_enum[$nm][$entry[$nm]] ) )
        $entry[$nm]++;// will turn 0 into 1
      foreach ( $fields_enum[$nm] as $eid => $edesc )
        echo "<OPTION VALUE=$eid".($entry[$nm] == $eid ? " SELECTED" : "").">$edesc\n";
      echo "</SELECT>";
    } else
      echo "<INPUT TYPE=TEXT SIZE=50 NAME=$nm VALUE='$entry[$nm]'".($fields_ro[$nm]?" readonly":"").">";
    echo (isset($fields_req[$nm]) ? " *":"")."</TD></TR>";
  }
  if ( -99 != $cid )
  {
    $stmt = $dbh->query( "SELECT smsnumber from core_useraccount WHERE user_ptr_id=$cid");
    foreach ( $stmt as $entry )
      echo "<TR><TD ALIGN=RIGHT>SMS Number:</TD><TD><INPUT TYPE=TEXT NAME=smsnumber VALUE=$entry[0]></TD></TR>";
  }
  if ( -99 == $cid )
  {
    echo "<TR><TD ALIGN=RIGHT>SMS Number:</TD><TD><INPUT TYPE=TEXT NAME=smsnumber></TD></TR>";
    echo "<TR><TD ALIGN=RIGHT>New Password:</TD><TD><INPUT TYPE=PASSWORD NAME=p1 AUTOCOMPLETE=off> *</TD></TR>";
    echo "<TR><TD ALIGN=RIGHT>Confirm New Password:</TD><TD><INPUT TYPE=PASSWORD NAME=p2 AUTOCOMPLETE=off> *</TD></TR>";
  }
  echo "<TR><TD ALIGN=CENTER COLSPAN=2>";
  if ( -99 == $cid ) // we're in the "Add new user" page
  {
    echo "<INPUT TYPE=SUBMIT NAME=SUBMIT ".
         "onClick=\"return passwordsMatch('userform','p1','p2');\" ".
         "VALUE=\"ADD NEW\">";
  } else
  {
    echo "<INPUT TYPE=SUBMIT NAME=SUBMIT VALUE=UPDATE>";
    if ( $cid != $logged_in_as ) // don't allow user to delete themselves
      echo "<INPUT TYPE=SUBMIT NAME=SUBMIT VALUE=DELETE>";
  }
  echo "</TD></TR>";

  echo "</TABLE>";
  echo "</FORM>";
?>
