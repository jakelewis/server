<?php 
  // make the session be stored in a database, so it's shared between clustered
  // servers.  This should make it possible to move from one server to another
  // transparently without requiring a new login.
  session_set_save_handler('sess_open','sess_close','sess_read','sess_write',
                           'sess_destroy','sess_clean');
  
  if ( !session_id() && isset( $_REQUEST["PHPSESSID"]) )
    session_id( $_REQUEST["PHPSESSID"] );

  session_start();
  if ( isset($logging_in) )
    session_regenerate_id(true);

  function dbconn_create()
  {
    $h = null;
    try {
//      $h = new PDO('mysql:host=70.85.16.221;dbname=se','se','se' ); // use SA3
      $h = new PDO('mysql:host=localhost;dbname=se','se','se' );
    } catch ( PDOException $exc )
    {
      // TODO: send an alert to somebody, so they know there's a database problem
//      die( "Could not connect to database: ".$exc->getMessage() );
    }
    // TBD: should I try to connect to a different database server via internet?
    //      OR redirect to the other system?
    if ( !$h )
    {
      $svr = "";
      if ( $_SERVER['SERVER_NAME'] == "sa2.us" )
        $svr = "sa3.us";
      if ( $_SERVER['SERVER_NAME'] == "sa3.us" )
        $svr = "sa2.us";
      if ( $svr == "" || (isset( $_REQUEST['redirected_from'] ) 
           && $_REQUEST['redirected_from'] == $svr ) )
      {
        die( "It appears that all servers' databases are down.  Please contact support." );
      }
      $url = $_SERVER['REQUEST_SCHEME']."://".$svr.$_SERVER['REQUEST_URI'];
      if ( FALSE == strstr( $url, "?" ) )
        $url .= "?";
      else
        $url .= "&";
      // make it so it won't bounce back and forth
      $url .= "redirected_from=".$_SERVER['SERVER_NAME'];
      // this will preserve the session, so users will switch servers without 
      // having to login again
      $url .= "&PHPSESSID=".session_id();
      header( "Location: $url" );
      exit;
    }
    $h->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $h->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $h;
  }

  include_once "dbconn.php";

global $base_url,$base_url_secure;
$base_url = getenv("SCRIPT_URI");
$msg = isset($_GET['msg']) ? $_GET['msg'] : "";
$page_fn = substr( $_SERVER["SCRIPT_NAME"], strrpos( $_SERVER["SCRIPT_NAME"], '/' )+1 );
$prefix = $_SERVER["SCRIPT_NAME"];
$prefix = substr( $_SERVER["SCRIPT_NAME"], 0, strrpos($prefix,'/')+1 );
$is_https = isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on";
$base_url = ($is_https ? "https://": "http://" ). $_SERVER["SERVER_NAME"] . $prefix;
$base_url_secure = "https://" . $_SERVER["SERVER_NAME"] . $prefix;

global $is_superuser;
global $user_level;
global $user_customer;
global $user_name;
$user_level=0;
$user_customer=0;
$is_superuser = 0;
$user_name=0;
global $logged_in_as;
$logged_in_as = isset( $_SESSION['UserId'] ) ? $_SESSION['UserId']+0 : 0;
if ( !$logged_in_as && !isset($_REQUEST['action']) )
{
  if ( isset( $_REQUEST['UN']) && isset( $_REQUEST['PW'] ) )
  {
    // check if this is a real user, and if so, set their user ID in session
    if ( user_login( $_REQUEST['UN'] , $_REQUEST['PW'] ) )
    {
      // and redirect to select.php
      $params['test'] = 1;
//      http_redirect( "${base_url}select.php", $params, true, HTTP_REDIRECT_FOUND  );
//      http_response_code(302);
//      header( "Location: ${base_url}select.php" );
      $logged_in_as = $_SESSION['UserId'];
      $dest = isset( $_REQUEST['dest'] ) ? $_REQUEST['dest'] : "select.php";
      echo "<HTML><BODY>Redirect to <A HREF=\"${base_url}${dest}\">${base_url}${dest}</A><SCRIPT LANGUAGE=\"JavaScript\">window.location.replace(\"${dest}\");</SCRIPT></BODY></HTML>";
      die();
    } else
    {
      $msg .= " Incorrect Username or Password.";
    }
  }
  else if ( isset($require_login) && $require_login )
  {
/// this did not work for ajax pages, we need to affect the main window
    header( "Location: ${base_url}index.php" );
    echo "<HTML><BODY onLoad=\"location.replace('${base_url}index.php');\">This page requires you to be <A HREF=\"${base_url}index.php\">logged in</A>!".
         "<SCRIPT LANGUAGE=JavaScript>location.replace('${base_url}index.php');</SCRIPT></BODY></HTML>";
    die();
  }
} else 
{
  if ( isset($force_logout) && $force_logout )
  {
    $logged_in_as = 0;
    unset( $_SESSION["UserId"]);
    unset( $_SESSION["UserIsSuperuser"]);
    unset( $_SESSION["UserCustomer"]);
    unset( $_SESSION["UserLevel"]);
    unset( $_SESSION["UserName"]);
    session_write_close();
  } else 
  {
    if ( isset($_SESSION["UserCustomer"] ) )
    {
      $user_customer = $_SESSION["UserCustomer"];
    }
    if ( isset($_SESSION["UserName"] ) )
    {
      $user_name= $_SESSION["UserName"];
    }
    if ( isset($_SESSION["UserLevel"] ) )
    {
      $user_level = $_SESSION["UserLevel"];
    }
    if ( isset($_SESSION["UserIsSuperuser"]) && $_SESSION["UserIsSuperuser"] )
    {
      $is_superuser = 1;
    }
  }
}
function sess_open()
{
  global $_sess_dbh;
  $_sess_dbh = dbconn_create();
  return $_sess_dbh != null;
}
function sess_close()
{
  global $_sess_dbh;
  $_sess_dbh = null; // release the last ref to it
  return TRUE;
}
function sess_read($id)
{
  global $_sess_dbh;
  if ( $_sess_dbh == null ) return FALSE;
  $stmt = $_sess_dbh->prepare("SELECT data from sessions WHERE id=:id");
  if ( !$stmt->execute( array( "id"=>$id) ) )
    return '';
  $ary = $stmt->fetch();
  if ( !$ary ) return '';
  return $ary['data'];
}
function sess_write($id,$data)
{
  global $_sess_dbh;
  if ( $_sess_dbh == null ) return FALSE;
  $stmt = $_sess_dbh->prepare("REPLACE INTO sessions VALUES (:id,:access,:data)");
  return $stmt->execute( array( "id"=>$id,"access"=>time(),"data"=>$data ) );
}
function sess_destroy($id)
{
  global $_sess_dbh;
  if ( $_sess_dbh == null ) return FALSE;
  $stmt = $_sess_dbh->prepare("DELETE FROM sessions WHERE id=:id");
  return $stmt->execute( array( "id"=>$id) );
}
function sess_clean($max)
{
  global $_sess_dbh;
  if ( $_sess_dbh == null ) return FALSE;
  $stmt = $_sess_dbh->prepare("DELETE FROM sessions WHERE access<:old");
  return $stmt->execute( array( "old"=>(time()-$max) ) ) ;
}

?>
