<?php
$debug = 1;
if ( $debug )
{
error_reporting(E_ALL);
ini_set( 'display_errors','1' );
}

  include_once "session.php";

  include "timezone.php";

  $add_to_existing_company = 0;
  if ( $is_superuser )
  {
    $add_to_existing_company = intval($_REQUEST['AddToCustomer']);
    if ( !$add_to_existing_company )
      $add_to_existing_company = -99; // need to select it
    else if ( -1 == $add_to_existing_company )
      $add_to_existing_company = 0;
  }
  else if ( $logged_in_as > 0  )
    $add_to_existing_company = 0+$_SESSION['UserCustomer'];

  $page_title = "Chemtrol Remote Monitoring: Register Your Controller";

  $req_fields = array( 
     //                   "cust_name"=>"Name",
                        "cont_name" => "Controller Name",
                        "unit_name" => "Subunit Name",
                        "loc_tz" => "Time Zone" );
  if ( !$add_to_existing_company )
  {
     $req_fields["cust_name"]="Customer Name";
     $req_fields["user_name"] = "User Name";
     $req_fields["user_fname"] = "First Name";
     $req_fields["user_lname"] = "Last Name";
     $req_fields["user_pw"] = "Password";
     $req_fields["user_pw2"] = "Password Confirmation";
     $req_fields["user_email"] = "Email Address";
   } else
   {
     //$req_fields["cust_name"]="Location Name";
   }
   $head_script = "<SCRIPT LANGUAGE=JavaScript>function validateForm(f) {";
//$head_script .= "alert('Validating...'); ";
   foreach ( $req_fields as $nm=>$val )
   {

     $head_script .= "
   if ( null == f[\"$nm\"].value || \"\" == f[\"$nm\"].value )
   {
     alert( \"$val must be filled out\" );
     return false;
   }";

   }
   if ( !$add_to_existing_company )
   {
     $head_script .= "  if ( f[\"user_pw\"].value != f[\"user_pw2\"].value )
     { alert(\"Passwords do not match!\"); return false; }";
     $head_script .= "  if ( null != f[\"user_email\"].value && 
       \"\" != f[\"user_email\"].value ) {
       var em = f[\"user_email\"].value;
       var atpos = em.indexOf(\"@\");
       var dotpos = em.lastIndexOf(\".\");
       if ( atpos < 1 || dotpos<atpos+2 || dotpos >= em.length )
       {
         alert(\"Email address does not look valid!\"); 
         return false; } } ";
   }
  $head_script .= "return true; }</SCRIPT>" ;
  include "header.php";

//   if ( $is_superuser ) // we don't currently support this
//     die "This page is not yet built to allow SuperUser to add a customer's controller!";

if ( $is_superuser )
{
  echo "This page does not yet support the super-user!";
  exit(0);
}
  $mac = $_REQUEST["MAC"];
  $num = $_REQUEST["NUM"];
  $uname = $_REQUEST["user_name"];
  if ( $add_to_existing_company ) $uname = $_SESSION['UserName'];
  $found_unit = 0;

  global $dbh;
  // check on whether these values describe a unit that is connected but not yet registered
  while ( isset( $mac ) && isset( $num ) )
  {
    // check that this username is unique in the database
    if ( isset( $uname) && !$add_to_existing_company )
    {
      $stmt = $dbh->prepare( "SELECT id from auth_user WHERE username=:un" );
      if ( $stmt->execute( array( 'un' => $uname ) ) && $stmt->fetch() )
      {
        echo "<H2>That username is already in use.</H2>";
        break;
      }
    }

    // make a call to the ControllerServer to get the info about connected units
    $connstat = array();
    $params = array('mac'=>$mac,'unum'=>$num);
#var_dump($params);
    $stmt = $dbh->prepare( "SELECT * from core_connection_status where identifier=:mac AND unitnum=:unum and connected is not null and disconnected is null" );
    if ( $stmt->execute( $params ) )
    {
      $connstat = $stmt->fetch();
#var_dump($connstat);
    }
    $ctrlinfo = array();
    $stmt = $dbh->prepare( "SELECT c.* from core_networkcontroller c, core_unit u ".
    "where c.identifier=:mac AND u.subunitid=:unum and u.controller_id=c.id" );
    if ( $stmt->execute( $params ) )
    {
      $ctrlinfo = $stmt->fetch();
#var_dump($ctrlinfo);
    }
    /*
    $url = "http://127.0.0.1:8077/ajax.htm?action=getControllerListCSV";
    $ctx = stream_context_create(array(
      'http' => array(
        'timeout' => 10
        )
        )
      );
    $csv = file_get_contents( $url,0,$ctx );
    // first, check for unit being connected, simple text search:

    // CAREFUL: this depends on the ordering of the columns, since it does not
    //          parse the CSV and use the column names
    $patt = "/$mac,$num,[\d-]+,(\d+)/";
// special condition here for testing, so others can see all the fields
$matches=array();
*/
if ( $mac == "xx:xx:xx:xx:xx:xx" )
{
  $found_unit =1;
} else
    if ( !$is_superuser && !isset($connstat['identifier']) )
    {
      echo "<H2>The unit you describe is not currently connected</H2>";
    } else if ( !$is_superuser && isset($ctrlinfo['identifier']) )
    {
    // then test for next to last field to be a 1 (UNKNOWN)
      echo "<H2>The unit you describe is already registered</H2>";
    } else
    {
      $found_unit = -99 != $add_to_existing_company; // unit is connected and not registered
    }
    break;
  }
  if ( !$found_unit )
  {
echo "<!-- add_to_existing=".$add_to_existing_company." -->";
    ?><H2>Enter the information below to register your controller</H2>
      Please ensure that the controller is turned on and able to access the internet.<BR>You will not be able to re-register a controller using this web page.<BR>
      If you already have login info, you should <A HREF="index.php?dest=admin.php%3Ftab%3Dcont%26cont%3D-99">use that to add this controller</A>.
<BR>

      <TABLE>
      <FORM>
      <TR><TD>Enter Controller MAC address:</TD><TD><INPUT TYPE=TEXT MAXLENGTH=18 NAME=MAC VALUE=<?= $mac ?>></TD></TR>
      <TR><TD>Enter Subunit ID:</TD><TD><INPUT TYPE=TEXT MAXLENGTH=3 NAME=NUM VALUE=<?= $num ?>></TD></TR>
<?php
      if ( !$add_to_existing_company || -1 == $add_to_existing_company )
        echo "<TR><TD>Your desired username:</TD><TD><INPUT TYPE=TEXT MAXLENGTH=30 NAME=user_name VALUE=". $uname ."></TD></TR>";
      else if ( -99 == $add_to_existing_company )
      {
        // this means we're the superuser, and we don't know what company to
        // add the controller to...
        echo "<TR><TD>Company whose controller you're registering:</TD><TD><SELECT NAME=AddToCustomer><OPTION VALUE=-1>[Add to New Company]\n";
// show options for which company to add to
        
        echo "</SELECT></TD></TR>";
      }
?>
      <TR><TD COLSPAN=2 ALIGN=CENTER><INPUT TYPE=SUBMIT NAME=SUBMIT VALUE=Submit></TD></TR>
     </FORM>
     </TABLE><BR>
     <?php
  } 
  else if ( !isset( $_REQUEST['cont_name'] ) 
            || ( !$add_to_existing_company &&
                 ( !isset( $_REQUEST['user_pw']  )
                   || $_REQUEST['user_pw'] != $_REQUEST['user_pw2'] ) )
            || $mac == "xx:xx:xx:xx:xx:xx" ) 
  { // we have MAC and subunit # of a unit that is connected and not registered, but the form is not yet submitted
$ip = $_SERVER['REMOTE_ADDR'];
$csv = file_get_contents( "http://freegeoip.net/csv/".$ip );
$fields = split("\",\"",$csv );
$city = isset( $_REQUEST['city']) ? $_REQUEST['city'] : $fields[5];
$state = isset( $_REQUEST['state']) ? $_REQUEST['state'] : $fields[3];
$zip = isset( $_REQUEST['zip']) ? $_REQUEST['zip'] : $fields[6];
$tz =  isset( $_REQUEST['loc_tz']) ? $_REQUEST['loc_tz'] : $fields[7];
$lat = isset( $_REQUEST['loc_lat']) ? $_REQUEST['loc_lat'] : $fields[8];
$lon =  isset( $_REQUEST['loc_long']) ? $_REQUEST['loc_long'] : $fields[9];

    if ( (isset( $_REQUEST['user_pw'] ) || isset( $_REQUEST['user_pw2'] ) ) &&
         $_REQUEST['user_pw'] != $_REQUEST['user_pw2'] )
      echo "ERROR: Password was not confirmed!<BR>";
?>
    <h2>Registering <?= $mac ?> subunit #<?= $num ?></H2>
    <TABLE>
    <FORM NAME="setupNewCustomer" onsubmit="return validateForm(this);">
    <INPUT TYPE=HIDDEN NAME="MAC" VALUE="<?= $mac ?>">
    <INPUT TYPE=HIDDEN NAME="NUM" VALUE="<?= $num ?>">
<?php
    if ( !$add_to_existing_company )
    { ?>
    <TR><TH ALIGN=CENTER BGCOLOR="#000080" COLSPAN=2>Company Info</TH></TR>
    <TR><TD ALIGN=RIGHT>Name:</TD><TD><INPUT MAXLENGTH=150 NAME=cust_name VALUE="<?= $_REQUEST['cust_name'] ?>"> *</TD></TR>
    <TR><TD ALIGN=RIGHT>Address 1:</TD><TD><INPUT MAXLENGTH=150 NAME=addr1 VALUE="<?= $_REQUEST['addr1'] ?>"></TD></TR>
    <TR><TD ALIGN=RIGHT>Address 2:</TD><TD><INPUT MAXLENGTH=150 NAME=addr2 VALUE="<?= $_REQUEST['addr2'] ?>"></TD></TR>
    <TR><TD ALIGN=RIGHT>City:</TD><TD><INPUT MAXLENGTH=150 NAME=city VALUE="<?= $city ?>"></TD></TR>
    <TR><TD ALIGN=RIGHT>State:</TD><TD><INPUT MAXLENGTH=2 NAME=state VALUE="<?= $state ?>"></TD></TR>
    <TR><TD ALIGN=RIGHT>Zip:</TD><TD><INPUT MAXLENGTH=10 NAME=zip VALUE="<?= $zip ?>"></TD></TR>
    <TR><TD ALIGN=RIGHT>Phone:</TD><TD><INPUT MAXLENGTH=20 NAME=cust_phone VALUE="<?= $_REQUEST['cust_phone'] ?>"></TD></TR>
    <TR><TH ALIGN=CENTER BGCOLOR="#000080" COLSPAN=2>Your User Info</TH></TR>
    <TR><TD ALIGN=RIGHT>Username:</TD><TD><INPUT MAXLENGTH=30 NAME=user_name VALUE="<?= $uname ?>" readonly></TD></TR>
    <TR><TD ALIGN=RIGHT>First Name:</TD><TD><INPUT MAXLENGTH=30 NAME=user_fname VALUE="<?= $_REQUEST['user_fname'] ?>"> *</TD></TR>
    <TR><TD ALIGN=RIGHT>Last Name:</TD><TD><INPUT MAXLENGTH=30 NAME=user_lname VALUE="<?= $_REQUEST['user_lname'] ?>"> *</TD></TR>
    <TR><TD ALIGN=RIGHT>Email:</TD><TD><INPUT MAXLENGTH=75 NAME=user_email VALUE="<?= $_REQUEST['user_email'] ?>"> *</TD></TR>
    <TR><TD ALIGN=RIGHT>Password:</TD><TD><INPUT TYPE=PASSWORD MAXLENGTH=128 NAME=user_pw> *</TD></TR>
    <TR><TD ALIGN=RIGHT>Re-enter Password:</TD><TD><INPUT TYPE=PASSWORD MAXLENGTH=128 NAME=user_pw2> *</TD></TR>
    <TR><TD ALIGN=RIGHT>Eve Phone:</TD><TD><INPUT MAXLENGTH=20 NAME=user_ephone VALUE="<?= $_REQUEST['user_ephone'] ?>"></TD></TR>
    <TR><TD ALIGN=RIGHT>SMS Phone:</TD><TD><INPUT MAXLENGTH=20 NAME=user_sms VALUE="<?= $_REQUEST['user_sms'] ?>"></TD></TR>
<?php } ?>
    <TR><TH ALIGN=CENTER BGCOLOR="#000080" COLSPAN=2>Controller Info</TH></TR>
    <TR><TD ALIGN=RIGHT>Name:</TD><TD><INPUT MAXLENGTH=150 NAME=cont_name VALUE="<?= $_REQUEST['cont_name'] ?>"> *</TD></TR>
    <TR><TD VALIGN=TOP ALIGN=RIGHT>Notes:</TD><TD><TEXTAREA COLS=37 ROWS=6 NAME=cont_notes><?= $_REQUEST['cont_notes'] ?></TEXTAREA></TD></TR>
    <TR><TD ALIGN=RIGHT>GSM ID:</TD><TD><INPUT MAXLENGTH=150 NAME=cont_msisdn VALUE="<?= $_REQUEST['cont_msisdn'] ?>"></TD></TR>
    <TR><TD ALIGN=RIGHT>Name this subunit:</TD><TD><INPUT MAXLENGTH=100 NAME=unit_name VALUE="<?= 
       isset($_REQUEST['unit_name']) ? $_REQUEST['unit_name'] : "main" ?>"> *</TD></TR>
    <TR><TD VALIGN=TOP ALIGN=RIGHT>Notes for this subunit:</TD><TD><TEXTAREA COLS=37 ROWS=6 NAME=unit_notes><?= $_REQUEST['unit_notes'] ?></TEXTAREA></TD></TR>
    <TR><TH ALIGN=CENTER BGCOLOR="#000080" COLSPAN=2>Where is this Controller?</TH></TR>
    <TR><TD ALIGN=RIGHT>Name of this Location:</TD><TD><INPUT MAXLENGTH=150 NAME=loc_name VALUE="<?= $_REQUEST['loc_name'] ?>"> *</TD></TR>
    <TR><TD ALIGN=RIGHT>Latitude:</TD><TD><INPUT TYPE=TEXT MAXLENGTH=20 NAME=loc_lat VALUE="<?= $lat ?>"></TD></TR>
    <TR><TD ALIGN=RIGHT>Longitude:</TD><TD><INPUT TYPE=TEXT MAXLENGTH=20 NAME=loc_long VALUE="<?= $lon ?>"></TD></TR>
    <TR><TD ALIGN=RIGHT>Address 1:</TD><TD><INPUT MAXLENGTH=150 NAME=addr1 VALUE="<?= $_REQUEST['addr1'] ?>"></TD></TR>
    <TR><TD ALIGN=RIGHT>Address 2:</TD><TD><INPUT MAXLENGTH=150 NAME=addr2 VALUE="<?= $_REQUEST['addr2'] ?>"></TD></TR>
    <TR><TD ALIGN=RIGHT>City:</TD><TD><INPUT MAXLENGTH=150 NAME=city VALUE="<?= $city ?>"></TD></TR>
    <TR><TD ALIGN=RIGHT>State:</TD><TD><INPUT MAXLENGTH=2 NAME=state VALUE="<?= $state ?>"></TD></TR>
    <TR><TD ALIGN=RIGHT>Zip:</TD><TD><INPUT MAXLENGTH=10 NAME=zip VALUE="<?= $zip ?>"></TD></TR>
    <TR><TD ALIGN=RIGHT>Time Zone:</TD><TD><?php emit_timezone_select("loc_tz",$tz); ?> *</TD></TR>
    <TR><TD ALIGN=CENTER BGCOLOR="#000080" COLSPAN=2>
       <INPUT TYPE=SUBMIT NAME=SUBMIT VALUE=Register></TD></TR>
    </FORM></TABLE>
    <?php
  } 
  else
  {
    // the form has been filled-in, submitted.  
    //     Now put the stuff into the database
    // first: core_customer (company) info
    $dbh->beginTransaction(); // only want this to be committed if it all works
    if ( !$add_to_existing_company ) // don't know current user, must create cust/user
    {
      $stmt = $dbh->prepare( "
  INSERT INTO core_customer
              (name,address1,address2,city,state,zip,phone,contactname,
               contactphone,contactemail)
        VALUES (:cust_name,:cust_addr1,:cust_addr2,:cust_city,:cust_state,
                :cust_zip,:cust_phone,:cust_ctcname,:cust_ctcphone,
                :cust_ctcemail )" );
      $cust_info = array( 
          'cust_name'=>$_REQUEST['cust_name'],
          'cust_addr1'=>$_REQUEST['addr1'],
          'cust_addr2'=>$_REQUEST['addr2'],
          'cust_city'=>$_REQUEST['city'],
          'cust_state'=>$_REQUEST['state'],
          'cust_zip'=>$_REQUEST['zip'],
          'cust_phone'=>$_REQUEST['cust_phone'],
//        'cust_fax'=>$_REQUEST['cust_fax'],
          'cust_ctcname'=>($_REQUEST['user_fname']." ".$_REQUEST['user_lname']),
          'cust_ctcphone'=>$_REQUEST['user_sms'],
          'cust_ctcemail'=>$_REQUEST['user_email'] );
      if ( !$stmt->execute( $cust_info) ) // this array should already have what we need in it
      {
        $dbh->rollback();
        die( "customer insert failed" );
      }
      $stmt = $dbh->query( "SELECT @@IDENTITY" );
      $ary = $stmt->fetch();
      $cust_id = $ary[0];

      // second: auth_user
      $stmt = $dbh->prepare( "
  INSERT INTO auth_user
             (username,first_name,last_name,email,password,date_joined,user_level)
     VALUES (:user_name,:user_fname,:user_lname,:user_email,:user_pw,NOW(),2 )" );
      $user_info = array( 'user_name'=>$_REQUEST['user_name'],
                        'user_fname'=>$_REQUEST['user_fname'],
                        'user_lname'=>$_REQUEST['user_lname'],
                        'user_email'=>$_REQUEST['user_email'],
                        'user_pw'=> makePasswordString( "".rand(0,99999), $_REQUEST['user_pw'] ) );
      if ( !$stmt->execute( $user_info) ) // this array should already have what we need in it
      {
        $dbh->rollback();
        die( "user insert failed" );
      }
      $stmt = $dbh->query( "SELECT @@IDENTITY" );
      $ary = $stmt->fetch();
      $user_id = $ary[0];

      // third: core_useraccount info (linked to above)
      $stmt = $dbh->prepare( "
  INSERT INTO core_useraccount
              (user_ptr_id,home_address,home_city,home_state,home_zip,day_phone,evening_phone,customer_id,smsnumber)
        VALUES ($user_id,:user_addr,:user_city,:user_state,:user_zip,:user_dphone,
                :user_ephone,$cust_id,:user_sms )" );
      $ua_info = array( 'user_addr' => $_REQUEST['addr1']." ".$_REQUEST['addr2'],
                        'user_city' => $_REQUEST['city'],
                        'user_state' => $_REQUEST['state'],
                        'user_zip' => $_REQUEST['zip'],
                        'user_dphone' => $_REQUEST['cust_phone'],
                        'user_ephone' => $_REQUEST['user_ephone'],
                        'user_sms' => $_REQUEST['user_sms'] );
      //    TODO: this user must be set as admin for this customer
      if ( !$stmt->execute( $ua_info) ) // this array should already have what we need in it
      {
        $dbh->rollback();
        die( "useraccount insert failed" );
      }
    } else
    {
      // need to get user's customer id, user_id
      $user_id = $_SESSION['UserId'];
      $cust_id = $_SESSION['UserCustomer'];
    }
    // fourth: core_location info
    $stmt = $dbh->prepare( "
INSERT into core_location
            (name,latitude,longitude,address1,address2,city,state,zip,tz)
     VALUES (:loc_name,:loc_lat,:loc_long,:loc_addr1,
             :loc_addr2,:loc_city,:loc_state,:loc_zip,:loc_tz )" );
    $loc_info = array( 'loc_name'=>$_REQUEST['loc_name'],
                       //'loc_notes'=>$_REQUEST['loc_notes'],
                       'loc_lat'=>$_REQUEST['loc_lat'],
                       'loc_long'=>$_REQUEST['loc_long'],
                       'loc_addr1'=>$_REQUEST['addr1'],
                       'loc_addr2'=>$_REQUEST['addr2'],
                       'loc_city'=>$_REQUEST['city'],
                       'loc_state'=>$_REQUEST['state'],
                       'loc_zip'=>$_REQUEST['zip'],
                       'loc_tz'=>$_REQUEST['loc_tz'] );
    if ( !$stmt->execute( $loc_info) ) 
    {
      $dbh->rollback();
      die( "location insert failed" );
    }
    $stmt = $dbh->query( "SELECT @@IDENTITY" );
    $ary = $stmt->fetch();
    $loc_id = $ary[0];

    // fifth: core_controller info linkt to core_customer and core_location
    $stmt = $dbh->prepare( "
INSERT INTO core_networkcontroller
            (identifier,customer_id,name,email,notes,location_id,msisdn)
     VALUES (:mac,$cust_id,:cont_name,:cont_email,:cont_notes,$loc_id,:cont_msisdn ) " );
    $cont_info = array( 'mac'=>$mac, 
                  'cont_name'=>$_REQUEST['cont_name'],         
                  'cont_email'=>$_REQUEST['user_email'],         
                  'cont_notes'=>$_REQUEST['cont_notes'],         
                  'cont_msisdn'=>$_REQUEST['cont_msidsn']  );
    if ( !$stmt->execute( $cont_info) ) 
    {
      $dbh->rollback();
      die( "location insert failed" );
    }
    $stmt = $dbh->query( "SELECT @@IDENTITY" );
    $ary = $stmt->fetch();
    $cont_id = $ary[0];

    // sixth: core_unit linked to core_controller
    $stmt = $dbh->prepare( "
INSERT INTO core_unit
            (controller_id,subunitid,name,notes)
     VALUES ($cont_id,:subunitid,:unit_name,:unit_notes ) ");
    $unit_info = array( 'subunitid' => $num,
                        'unit_name' => $_REQUEST['unit_name'],
                        'unit_notes' => $_REQUEST['unit_notes'] );
    if ( !$stmt->execute( $unit_info) ) 
    {
      $dbh->rollback();
      die( "location insert failed" );
    }
    $stmt = $dbh->query( "SELECT @@IDENTITY" );
    $ary = $stmt->fetch();
    $unit_id = $ary[0];

    // seventh: core_unit_users linked to auth_user and core_unit
    $dbh->query( "
INSERT into core_unit_users 
       (unit_id,user_id)
VALUES ($unit_id,$user_id)" );
    $dbh->commit();

    // make a call to the controller server to tell it to refresh
    //   this controller's info from the database

    echo "<H2>Controller is now set up!</H2>";
    if ( !$add_to_existing_company )
      echo "You may now <A HREF=index.php>login here</A>";
    else
      echo "You may now <A HREF=select.php>go to the unit selection list</A>";
    $res = file_get_contents( "http://127.0.0.1:8077/ajax.htm?action=refreshController&Unit=".$mac."_".$num );
    echo "<BR>Controller DB update: $res<BR>";
  }
  
?>

<?php include "footer.php"; ?>
