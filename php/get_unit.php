<?php 

  $require_login = 1;
  include "session.php";

  $uid = $_REQUEST["unit"];
  $cid = $_REQUEST["cont"];
  if ( isset($_REQUEST['cust']) )
    $custid = $_REQUEST["cust"];

  // make it so a customer admin can edit their own info
  if ( !$is_superuser && $user_level < 2 ) die( "Superuser or admin rights required!" );

  $not_null = array( "subunitid" => 1, "name" => 1 );
  $fields = array( "subunitid"=>"Subunit #",
                   "name"=>"Name",
                   "info"=>"Info",
                   "notes"=>"Alarm Notes",
                   "scanfreq"=>"Scan Every X minutes",
                  );
// see if a form has been submitted, and if so, do the appropriate thing
  if ( isset( $_REQUEST["remove_notif"] ) 
       || isset( $_REQUEST["add_notif"] ) 
       || isset( $_REQUEST["add_user"] ) 
       || isset( $_REQUEST["SUBMIT"] ) )
  {
     $errmsg;
     $retryparams = "";
     if ( isset( $_REQUEST["remove_notif"] )  )
     {
       echo "Performing removal of notification ".$_REQUEST["notif"]."<BR>" ;
       $stmt = $dbh->prepare( "DELETE FROM core_unit_notifications ".
                              "WHERE id=:nid"
                              );
       $stmt->execute( array( "nid" => $_REQUEST["notif"] ) );
     }
     elseif ( isset( $_REQUEST["add_notif"] )  )
     {
       echo "Performing addition of notification <BR>" ;
       $stmt = $dbh->prepare( "INSERT INTO core_unit_notifications ".
                              "(unit_id,dest_addr,type) ".
                              "VALUES (:unit,:addr,:type)" );
       $stmt->execute( array( "unit" => $_REQUEST["unit"],
                              "addr" => $_REQUEST["notif_addr"],
  				"type"=> $_REQUEST["notif_type"] ) );
     }
     elseif ( isset($_REQUEST["add_user"]) && isset($_REQUEST["user"]) )
     {
       echo "Performing addition of access<BR>" ;
       $stmt = $dbh->prepare( "INSERT INTO core_unit_users ".
                              "(user_id,unit_id) ".
                              "VALUES (:user,:unit)" );
       $stmt->execute( array( "unit" => $_REQUEST["unit"],
                              "user" => $_REQUEST["user"] ) );
     } elseif ( $_REQUEST["SUBMIT"] == "remove_user_access" && isset($_REQUEST["uuid"]) )
     {
       echo "Performing removal of access<BR>" ;
       $stmt = $dbh->prepare( "DELETE from core_unit_users WHERE id=:uuid" );
       $stmt->execute( array( "uuid" => $_REQUEST["uuid"] ) );
     } elseif ( $_REQUEST["SUBMIT"] == "ADD NEW" && isset($cid) )
     {
       
       echo "Performing ADD NEW<BR>" ;
       $fldlist = "controller_id";
       $vallist = ":cid";
       $params = array( "cid" => $cid );
       foreach ( $fields as $nm=>$val )
       {
         if ( $fields_ro[$nm] ) continue;
         if ( !isset( $_REQUEST[$nm] ) ||
              ("" == $_REQUEST[$nm] && $not_null[$nm]) ) 
         {
           $errmsg .= "Need value for '".$fields[$nm]."'<BR>";
           continue;
         }
         if ( isset( $_REQUEST[$nm] ) && "" !== $_REQUEST[$nm])
           $retryparams .= "&__".$nm."=".urlencode($_REQUEST[$nm]);
         if ( $fldlist != "" )
           $fldlist .= ", ";
         $fldlist .= $nm;
         if ( $vallist != "" )
           $vallist .= ", ";
         if ( "" == $_REQUEST[$nm] )
           $vallist .= "NULL";
         else
         {
           $vallist .= ":".$nm;
           if ( $fields_bool[$nm] )
             $params[$nm] = $_REQUEST[$nm] == "on" ? 1 : 0;
           else
             $params[$nm] = $_REQUEST[$nm];
         }
       }
//echo $vallist;
//var_dump( $params );
       if ( !isset( $errmsg ) )
       {
         $stmt = $dbh->prepare( "INSERT INTO core_unit (".$fldlist.") ".
  		"VALUES (".$vallist.")" );
         $res = $stmt->execute( $params );
         // need to get ID of the unit we just added, and make it the current selection
         $stmt = $dbh->query( "SELECT @@IDENTITY" );
         $ary = $stmt->fetch();
         $uid = $ary[0];

         $stmt = $dbh->query( "SELECT identifier from core_networkcontroller ".
                              "WHERE id=".$_REQUEST["cont"] );
         $ary = $stmt->fetch();
         $res = file_get_contents( "http://127.0.0.1:8077/ajax.htm?action=refreshController&Unit=".$ary[0]."_".$_REQUEST["subunitid"] );

       }
     }
     elseif ( $_REQUEST["SUBMIT"] == "UPDATE" )
     {
       echo "Performing UPDATE<BR>" ;
       $vallist = "";
       $params = array( "id" => $_REQUEST['unit'] );
       foreach ( $fields as $nm=>$val )
       {
         if ( $fields_ro[$nm] ) continue;
         if ( !isset( $_REQUEST[$nm] ) ) die( "Cannot find all parameter values!" );
         if ( $vallist != "" )
           $vallist .= ", ";
         if ( "" == $_REQUEST[$nm] )
           $vallist .= $nm.="=NULL";
         else
         {
           $vallist .= $nm."=:".$nm;
           if ( $fields_bool[$nm] )
             $params[$nm] = $_REQUEST[$nm] == "on" ? 1 : 0;
           else
             $params[$nm] = $_REQUEST[$nm];
         }
       }
//echo $vallist;
//var_dump( $params );
       $stmt = $dbh->prepare( "UPDATE core_unit SET ".$vallist." ".
		"WHERE id=:id" );
       $res = $stmt->execute( $params );
       $stmt = $dbh->query( "SELECT identifier from core_networkcontroller ".
                              "WHERE id=".$_REQUEST["cont"] );
       $ary = $stmt->fetch();

       $res = file_get_contents( "http://127.0.0.1:8077/ajax.htm?action=refreshController&Unit=".$ary[0]."_".$_REQUEST["subunitid"] );
     } elseif ( $_REQUEST["SUBMIT"] == "DELETE" && isset( $_REQUEST['unit']) )
     {
       echo "Performing DELETE " ;
       $params = array( "id" => $_REQUEST['unit'] );
       $stmt = $dbh->prepare( "DELETE uu.* ".
                              " FROM core_unit_users uu ".
                              "WHERE uu.unit_id=:id "
                               );
       $stmt->execute( $params );
       // delete any alertee entries for this unit
       $stmt = $dbh->prepare( "DELETE un.* from core_unit_notifications un ".
                              "WHERE un.unit_id=:id" );
       $stmt->execute( $params );
       $stmt = $dbh->prepare( "DELETE u.* ".
                              " FROM core_unit u ".
                              "WHERE u.id=:id " );
       $stmt->execute( $params );
       unset( $uid ); // so it doesn't try to display the deleted unit
     } else
     {
       echo "Unknown operation!\n";
print_r($_REQUEST);
die("!!!");
     }
//     echo "<A HREF=\"admin.php?tab=cust&cust=$cid\">go here</A>";
     echo "<SCRIPT LANGUAGE=JavaScript>window.location.replace( \"admin.php?tab=cont&cont=$cid".
     (isset($uid)?"&unit=$uid":"").
     (isset($custid)?"&cust=$custid":"").
     (isset($errmsg) ? $retryparams."&err=".urlencode($errmsg) : "")."\");</SCRIPT>";
     exit();
  }
  global $dbh;
  $stmt = $dbh->prepare( "SELECT COUNT(u.id) FROM core_networkcontroller c,core_unit u WHERE c.id=:cid AND u.controller_id=c.id");
  $unit_count = 0;
  if ( $stmt && $stmt->execute( array( 'cid' => $cid ) ) )
  {
    $ary = $stmt->fetch();
    $unit_count = $ary[0];
  }
  $stmt = $dbh->prepare( "SELECT ".join(",",array_keys($fields))." FROM core_unit WHERE id=:id" );
  if ( !$stmt || !$stmt->execute( array( 'id' => $uid ) ) )
      return 0;

  $entry = $stmt->fetch();
  echo "<FORM METHOD=GET ACTION=\"get_unit.php\" NAME=\"unitform\">".
       "<INPUT TYPE=HIDDEN NAME=cont VALUE=$cid>".
       "<INPUT TYPE=HIDDEN NAME=unit VALUE=$uid>";
  if ( isset( $custid ) )
    echo "<INPUT TYPE=HIDDEN NAME=cust VALUE=$custid>";

  echo "<TABLE>";
  foreach ( $fields as $nm=>$desc )
  {
    $prevvalue = "";
    if ( -99 == $uid )
    {
      if ( isset( $_REQUEST[$nm] ) )
        $prevvalue = $_REQUEST[$nm];
      else if ( 'subunitid' == $nm && $unit_count == 0 )
        $prevvalue = 1;
      else if ( 'scanfreq' == $nm )
        $prevvalue = 60;
    } else
      $prevvalue = stripslashes($entry[$nm]);
    if ( $nm == "notes" )
    {
      echo "<TR><TD VALIGN=TOP ALIGN=RIGHT>$desc:</TD><TD ALIGN=LEFT><TEXTAREA ROWS=6 COLS=37 NAME=$nm>$prevvalue</TEXTAREA>";
      echo (isset($not_null[$nm]) ? " *":"")."</TD></TR>";
    }
    else if ( $nm =='subunitid' && 1 == $prevvalue )
      echo "<TR><TD ALIGN=RIGHT>$desc:</TD><TD ALIGN=LEFT>$prevvalue<INPUT TYPE=HIDDEN SIZE=50 NAME='$nm' VALUE='$prevvalue'>";
    else
    {
      echo "<TR><TD ALIGN=RIGHT>$desc:</TD><TD ALIGN=LEFT><INPUT TYPE=TEXT SIZE=50 NAME=$nm VALUE='$prevvalue'>";
      echo (isset($not_null[$nm]) ? " *":"")."</TD></TR>";
    }
  }
  echo "<TR><TD ALIGN=CENTER COLSPAN=2>";
  if ( -99 == $uid )
    echo "<INPUT TYPE=SUBMIT NAME=SUBMIT VALUE=\"ADD NEW\">";
  else
  {
    echo "<INPUT TYPE=SUBMIT NAME=SUBMIT VALUE=UPDATE>";
    if ( 1 < $entry['subunitid'] )
      echo "<INPUT TYPE=SUBMIT NAME=SUBMIT VALUE=DELETE>";
  }
  echo "</FORM>";
  echo "</TD></TR>";
  
  if ( -99 != $uid )
  {
    $stmt = $dbh->prepare( "select COUNT(s.scandata) from core_networkcontroller c, core_unit u, core_unit_scans s WHERE u.id=:uid AND c.id=u.controller_id AND u.id=s.unit_id" );
    if ( $stmt && $stmt->execute( array('uid'=>$uid) ) && ($ary=$stmt->fetch()) )
    {
      echo "<TR><TD ALIGN=CENTER COLSPAN=0><A TARGET=_blank HREF=\"datascanhist.php?UID=$uid\">View data scan history ($ary[0] entries)</A></TD></TR>";
    }
   // show who has permission, allow them to be removed and added
   $stmt = $dbh->prepare( "SELECT uu.id,u.first_name,u.last_name,u.id,u.username ".
 			 "FROM core_unit_users uu,auth_user u ".
                          "WHERE uu.unit_id=:uid AND uu.user_id=u.id ".
                          "ORDER BY u.last_name,u.first_name,u.username ");
   $stmt->execute( array( "uid"=>$uid ) );
   echo "<TR><TH ALIGN=LEFT COLSPAN=2>Users with Access to this Subunit:</TH></TR>";
   $users_with_access = array();
   foreach ( $stmt as $entry )
   {
     $users_with_access[$entry[3]] = 1;
     echo "<TR><TD ALIGN=RIGHT><INPUT TYPE=BUTTON TITLE=\"Remove $entry[1]'s access\" VALUE=\"X\" onClick=\"window.location.href='get_unit.php?SUBMIT=remove_user_access&uuid=$entry[0]&unit=$uid&cont=$cid';\"> ";
     echo "</TD><TD><A HREF=\"admin.php?tab=user&user=$entry[3]\">$entry[2], $entry[1] ($entry[4] $entry[3])</A></TD></TR>";
   } 
   $params = array();
   $sortby = isset( $_REQUEST['sortuser']) ? $_REQUEST['sortuser'] : "last_name";
   if ( !$is_superuser )
   {
     $stmt = $dbh->query( "SELECT u.id,u.last_name,u.first_name,u.username ".
                          "FROM auth_user u, core_useraccount ua ".
                          "WHERE u.id=ua.user_ptr_id AND ".
                          "   ua.customer_id=:usercust ".
                          " ORDER BY u.$sortby" );
     $params["usercust"] = $user_customer;
   }
   else
     $stmt = $dbh->query( "SELECT u.id,u.last_name,u.first_name,u.username ".
                          "FROM auth_user u ".
                          "ORDER BY u.$sortby" );
   $stmt->execute( $params );
   $options = "";
   foreach ($stmt as $entry )
   {
     if ( !isset( $users_with_access[$entry[0]] ) )
       $options .= "<OPTION VALUE=$entry[0]>$entry[1], $entry[2] ($entry[3] $entry[0])\n";
   }
   if ( $options != "" )
   {
     echo "<TR><TD ALIGN=CENTER COLSPAN=2><FORM ACTION=\"get_unit.php\"><INPUT TYPE=HIDDEN NAME=unit VALUE=$uid>";
     echo "<INPUT TYPE=HIDDEN NAME=cont VALUE=$cid>";
     echo "<INPUT TYPE=SUBMIT NAME=add_user VALUE=\"+\" TITLE=\"Add selected user for access\"><SELECT NAME=user> ";
     echo "$options</SELECT></FORM>";
    echo " (Sort by ";
    if ( $sortby == "last_name" )
      echo "<B>Last name</B> ";
    else
      echo "<A HREF=\"admin.php?tab=cont&_unit=$uid&cont=$cid&__sortuser=last_name\">Last name</A> ";
    if ( $sortby == "username" )
      echo "<B>Username</B>)<BR>";
    else
      echo "<A HREF=\"admin.php?tab=cont&_unit=$uid&cont=$cid&__sortuser=username\">Username</A>)<BR>";

     echo "</TD></TR>";
   } else 
   {
     echo "<TR><TD></TD><TD><FONT SIZE=-1>Found no other customer users that can be added for this unit</FONT></TD></TR>";
   }
   // show current list of extra notifications (with removal [X] buttons)
   $stmt = $dbh->prepare( "SELECT id,dest_addr,type from core_unit_notifications WHERE unit_id=:uid" );
   $stmt->execute( array( "uid"=>$uid ) );
   echo "<TR><TH ALIGN=LEFT COLSPAN=2>Notifications for this Subunit:</TH></TR>";
   foreach ( $stmt as $entry )
   {
     echo "<TR><TD ALIGN=RIGHT><FORM ACTION=get_unit.php>".
          "<INPUT TYPE=HIDDEN NAME=unit VALUE=$uid>".
          "<INPUT TYPE=HIDDEN NAME=cont VALUE=$cid>".
          "<INPUT TYPE=HIDDEN NAME=notif VALUE=$entry[0]>".
          "<INPUT TYPE=SUBMIT NAME=remove_notif TITLE=\"Remove this notification\" VALUE=\"X\"></TD><TD>".($entry[2] ? "SMS" : "Email")." to $entry[1]</FORM></TD></TR>";
   }
   // show fields for adding another notification
   echo "<TR><TD ALIGN=CENTER COLSPAN=2><FORM ACTION=get_unit.php>".
          "<INPUT TYPE=HIDDEN NAME=unit VALUE=$uid>".
          "<INPUT TYPE=HIDDEN NAME=cont VALUE=$cid>".
          "<INPUT TYPE=SUBMIT NAME=add_notif TITLE=\"Add this notification\" VALUE=\"+\"> ".
          "<SELECT NAME=notif_type><OPTION VALUE=0>Email to<OPTION VALUE=1>SMS to</SELECT>".
          "<INPUT TYPE=TEXT NAME=notif_addr MAXLENGTH=50 SIZE=50>".
          "</FORM></TD></TR>";
   echo "</TABLE>";
  } // end of if ( -99 != $uid )
?>
