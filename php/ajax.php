<?php // AjaxRequest 
//error_reporting(E_ALL);
//ini_set( 'display_errors','1' );

  include_once "session.php";

  if ( $logged_in_as <= 0 )
    die( "Not logged in!" );

// prevent caching of any of the responses from this file
  header("Cache-Control: no-cache,no-store,must-revalidate");
  header("Pragma: no-cache");
  header("Expires: 0");

function parse_csv( $csv )
{
    $lines = preg_split( "/[\r\n]+/", $csv ); // now it's split into lines, header is first
    $hdrs = explode( ",",$lines[0] );
    $data = array();
//    echo "CSV raw:\n$csv\n\n";
    $index = 0;
    for ( $i=1; $i<count($lines); $i++ )
    {
//      echo "RAW LINE: $lines[$i]\n";
      if ( $lines[$i] == "" )
      { continue; }
      $fields = explode(",",$lines[$i] );
//      echo count($fields).":".join("|",$fields )."\n";
      if ( count($hdrs) == count($fields) )
      {
        $row = array_combine($hdrs, $fields );
        $data[$index++] = $row;
      }
    }
    return $data;
}
  $ctx = stream_context_create(array(
    'http' => array(
        'timeout' => 10
        )
    )
  ); 
function file_contents( $path )
{
  global $ctx;
  try
  {
    $str = @file_get_contents( $path, 0, $ctx );
    if ( $str === FALSE )
    {
      error_log( print_r( "Failed to get URL contents '".$path."' :".get_headers($path), TRUE ) );
      return "Cannot get URL contents (1).";
    }
    return $str;
  } catch ( Exception $e )
  {
    error_log( print_r( "Exception getting URL contents '".$path."' ".$e->getMessage(), TRUE ) );
    return "Cannot get URL contents (2).";
  }
}
function get_url_base($unit)
{
  global $dbh;
  $stmt = $dbh->prepare( "Select dns.name FROM ".
                             "core_connection_status s,core_controller_servers dns WHERE ".
                             "s.connected is NOT NULL AND s.disconnected IS NULL AND ".
                             "s.ip=dns.ip AND CONCAT(s.identifier,'_',s.unitnum)=:u" );
  if ( !$stmt->execute( array("u"=>$unit) ) )
    return "http://127.0.0.1:8077/";

  if ( !($ary = $stmt->fetch() ) )
    return "http://127.0.0.1:8077/";
  
  if ( $ary[0] == $_SERVER['SERVER_NAME'] )
    return "http://127.0.0.1:8077/";

  return "http://".$ary[0].":8077/";
}
  $url_base = "http://127.0.0.1:8077/";
  $params = "";
  if ( $logged_in_as > 0 )
  {
    $params.= "User=".$logged_in_as."&";
  }
  if ( isset($_SESSION["SelectedUnit"]) )
  {
    $params .= "Unit=".$_SESSION["SelectedUnit"]."&";
    $url_base = get_url_base( $_SESSION["SelectedUnit"] );
  } else if ( isset($_REQUEST["Unit"]) )
  {
    $params .= "Unit=".$_REQUEST["Unit"]."&";
    $url_base = get_url_base( $_REQUEST["Unit"] );
  }
  $ajax_base = $url_base."ajax.htm?".$params;
//echo $ajax_base;
  if ( !isset( $_REQUEST["action"] ) )
  {
    if ( isset( $_REQUEST["btn"]  ) )
    {
      header( "Content-Type: text/html; charset=ISO-8859-1" );
//error_log(print_r("Btn=".$_REQUEST['btn'],TRUE));
      $lcd_contents = file_contents( $ajax_base."btn=".$_REQUEST['btn'] );
      if ( !(substr($lcd_contents,0,5) === "<pre>" ) )
        error_log(print_r("Btn=".$_REQUEST['btn']." got bad LCD=".$lcd_contents,TRUE));
      echo $lcd_contents;
    } 
    else if ( isset($_REQUEST["touch"]) && $_REQUEST["touch"] )
    {
      header( "Content-Type: text/html; charset=ISO-8859-1" );
//error_log(print_r("touch=".$_REQUEST['touch'],TRUE));
      $lcd_contents = file_contents( $ajax_base."touch=".$_REQUEST['touch']."&row=".$_REQUEST['row']."&col=".$_REQUEST['col'] );
      echo $lcd_contents;
//error_log(print_r("LCD=".$lcd_contents,TRUE));
    } 
  } else
  {
    if ( $_REQUEST["action"] == "downloadscan" )
    {
      header( "Content-Type: application/octet-stream;charset=ISO-8859-1" );
      header( "Content-Disposition: attachment; filename=snapshot.txt" );
      echo file_contents( $ajax_base . "action=".$_REQUEST["action"] );
    } 
    else if ( $_REQUEST["action"] == "getscan" )
    {
      echo file_contents( $ajax_base . "action=".$_REQUEST["action"] );
    } 
    else if ( $_REQUEST["action"] == "startscan" )
    {
      echo file_contents( $ajax_base . "action=startscan".
            (isset($_REQUEST['header']) ? "&header=".$_REQUEST['header'] : "") );
    } 
    else if ( $_REQUEST["action"] == "refresh" )
    {
      header( "Content-Type: text/html; charset=ISO-8859-1" );
//header( "X-Used-Url: ".$ajax_base."action=refresh" );
//      echo "URL='".$ajax_base."action=refresh'<BR>";
      echo file_contents( $ajax_base."action=refresh" );
    } 
    else if ( $_REQUEST["action"] == "getdatalog" )
    {
      echo file_contents( $ajax_base . "action=".$_REQUEST["action"] );
    } 
    else if ( $_REQUEST["action"] == "startdatalog" )
    {
      echo file_contents( $ajax_base . "action=".$_REQUEST["action"] );
    } 
    else if ( $_REQUEST["action"] == "removeUnit" )
    {
//    echo $url_base . "removeUnit.htm?".$params."<BR>\n";
      echo file_contents( $url_base . "removeUnit.htm?".$params );
    
//    http_redirect( "${base_url}select.php", "", true, HTTP_REDIRECT_FOUND  );
    } 
    else if ( $_REQUEST["action"] == "downloaddatalog" )
    {
      header( "Content-Type: application/octet-stream;charset=us-ascii" );
      header( "Content-Disposition: attachment; filename=history.txt" );
      echo file_contents( $ajax_base . "action=".$_REQUEST["action"] );
    } 
    else if ( $_REQUEST["action"] == "clearUserFromControllers" )
      $dummy = file_contents( $ajax_base."action=clearUserFromControllers" );
    else if ( $_REQUEST["action"] == "getControllerList" )
    {
      $dummy = file_contents( $ajax_base."action=clearUserFromControllers" );
      // get list from database now,instead of HTTP request to ControllerServer
      global $dbh;
// first, clear this user from ownership of any units
      $stmt = $dbh->prepare( "UPDATE core_connection_status SET owner=0 WHERE owner=:owner" );
      $stmt->execute( array("owner"=>$logged_in_as ) );
      $stmt = $dbh->prepare( 
"Select DISTINCT s.identifier,s.unitnum,s.connected,s.disconnected,s.owner,dns.name AS dnsname,s.flags,c.name ".
" FROM core_connection_status s,core_controller_servers dns,".
"(SELECT c.identifier,c.name ".
"FROM core_networkcontroller c,core_connection_status s ".
" WHERE c.identifier=s.identifier ".
"UNION ALL ".
"select DISTINCT s.identifier,'UNKNOWN' as name ".
"FROM core_networkcontroller c,core_connection_status s ".
" WHERE ".
"NOT EXISTS (Select * from core_networkcontroller c where c.identifier=s.identifier)) c ".
"WHERE ".
"s.connected IS NOT NULL AND s.disconnected IS NULL AND ".
"s.ip=dns.ip ".
"AND s.identifier=c.identifier ORDER BY c.name" );
      $stmt->execute( array() );
      
    // clear the selected unit from this session
      unset( $_SESSION["SelectedUnit"] );

      echo "<TABLE>";
    // go through the list and decide which ones should be visible to this user
      $unitCount = 0;
      $rows = $stmt->fetchAll();
      foreach ( $rows as $data )
      {
//      echo "<PRE>"; var_dump( $data[$i] ); echo "</PRE>";
        $mac = $data["identifier"];
        $unitnum = $data["unitnum"];
        $id = $mac."_".$unitnum;
        if ( (!isset( $_SESSION['UserIsSuperuser']) || !$_SESSION['UserIsSuperuser']) &&
             !user_can_access_unit( $logged_in_as, $id ) 
           )
        {
echo "<!-- skipped unit $id because it's not accessible -->\n";
          continue;
        }
        $cur_user = 0+$data["owner"];
        $unitnumstr = $unitnum > 1 ? " #$unitnum" : "";
        $ip = $data["dnsname"];
        
        if ( $ip == $_SERVER['SERVER_NAME'] )
        {
          $uri = $_SERVER['PHP_SELF'];
          $matches = array();
          if ( preg_match( "/(\/.*)\/[^i\/]+/", $uri, $matches ) )
          {
            $ip .= $matches[1];
          }
        }

//        $unit_id = $data[$i]["UNITDBID"];
        $name = $data["name"];
        $flags = isset($data['flags']) ? $data['flags'] : -1;//isset( $data[$i]['FLAGS'] ) ? 0+$data[$i]["FLAGS"] : -1;
        $unknown = 0;//+$data[$i]["UNKNOWN"];
        // check access to this controller/unit by currently logged-in user
        echo "<TR><TD>";
echo "<!-- "; var_dump($data); echo " -->\n";
        // see if we can find a name for this controller -- if not, it's unknown
        if ( $name === "UNKNOWN" ) $unknown = 1;

      // show some unit status, but only to the superuser
        if ( isset( $_SESSION["UserIsSuperuser"]) && $_SESSION['UserIsSuperuser'] )
        {
/*
        // show a green/yellow/red indication for this unit based on flags
// &#x2717; = &cross;
// &#x2713; = &check;
          if ( -1 != $flags && ($flags & 4) ) // no response, probably lantronix with unit off 
          {
            echo "<a href=\"#\" class=\"tooltip\"><FONT COLOR=\"ff0000\">&nbsp;&#x2717;&nbsp;</FONT><span>Not responding</span></a>";
          }
          else if ( -1 != $flags && ($flags & 2) ) // high protocol errors, probably misconfigured hardware
          {
            echo "<a href=\"#\" class=\"tooltip\"><FONT COLOR=\"ff0000\">&nbsp;&#x2717;&nbsp;</FONT><span>Comm Errors</span></a>";
          } else if ( -1 != $flags && ($flags & 1) ) // non-zero timeouts
          {
          // orange exclamation point
            echo "<a href=\"#\" class=\"tooltip\"><FONT COLOR=\"ffa000\">&nbsp;&nbsp;!&nbsp;&nbsp;</FONT><span>Needs upgrade</span></a>";
          } else if ( (0x3ff & $flags) == 0 )
          {
            echo "<a href=\"#\" class=\"tooltip\"><FONT SIZE=\"+1\" COLOR=\"00ff00\"><B>&nbsp;&#x2713;&nbsp;</B></FONT><span>On and working OK</span></a>";
          }
          global $dbh;
          $stmt = $dbh->prepare( "Select UNIX_TIMESTAMP(NOW())-UNIX_TIMESTAMP(a.timestamp) as age,a.timestamp,a.message FROM ".
                               "alarm_log a, core_unit u, core_networkcontroller c WHERE ".
                               "a.dispatched>0 AND ".
                               "u.subunitid=:unitnum AND u.controller_id=c.id AND ".
                               "c.identifier=a.identifier AND c.identifier=:mac ".
                               "AND UNIX_TIMESTAMP(NOW())-UNIX_TIMESTAMP(a.timestamp)<24*60*60 ".
                               "ORDER BY a.timestamp DESC ".
                               "LIMIT 1" );
          $stmt->execute( array("unitnum"=>$unitnum,"mac"=>$mac ) );
          $ary = $stmt->fetch();
// &#x260e; = &phone;
          if ( isset( $ary[0] ) && isset( $ary[1] ) )
          {
          // last alarm was within the last 24 hours
            echo "<a href=\"#\" class=\"bigtooltip\"><FONT COLOR=\"ff0000\">&nbsp;&#x260e;&nbsp;</FONT><span>Last alarm ".$ary[1].": ".$ary[2]."</span></a>";
          } else
            echo "<a href=\"#\" class=\"tooltip\"><FONT COLOR=\"00ff00\">&nbsp;&#x260e;&nbsp;</FONT><span>No alarms in 24 hours</span></a>";
*/
        } else
        {
          // NOT the superuser
          if ( $unknown )
            continue; // don't show this to non superusers
        }
        if ( $cur_user > 0 ) // somebody using this device
        {
          if ( isset( $_SESSION["UserIsSuperuser"]) && $_SESSION['UserIsSuperuser'] )
            echo "$name$unitnumstr ($mac)</TD><TD></TD><TD>(IN USE BY $cur_user)";
          else
            echo "$name$unitnumstr ($mac)</TD><TD></TD><TD>(IN USE)";
        } else if ( $unknown ) // this device is not in the database
        {
          echo "<A HREF=\"https://$ip/display.php?Unit=$id&PHPSESSID=".session_id()."\">";
          echo "UNKNOWN$unitnumstr ($mac)";
          echo "</A>";
          echo "</TD><TD></TD><TD>";
//          echo "<A HREF=\"setupController.php?AddToCustomer=-99&MAC=$mac&NUM=$unitnum\">Setup now</A>";
        }
        else
        {
          $unitCount++;
          echo "<A HREF=\"https://$ip/display.php?Unit=$id&PHPSESSID=".session_id()."\">$name$unitnumstr ($mac)</A></TD>";
          echo "<TD>&nbsp;</TD><TD><SPAN CLASS=\"scanfloater\" ID=\"scandata$id\">TestScanContents</SPAN>";
          echo "<SELECT ID=\"dropbox$id\" onChange=\"TakeReadings(this.value,'$id');\">".
               "<Option VALUE=-1 SELECTED>Readings...\n".
               "<OPTION VALUE=0>View Snapshot\n".
               "<OPTION VALUE=1>Download Snapshot\n".
//               "<OPTION VALUE=2>View/Download History from Unit\n".
               "<OPTION VALUE=3>View/Download History from Server\n".
               "</SELECT></TD>";
          echo "<TD>&nbsp;</TD><TD>&nbsp;";
        }
        if ( $unitnum ==1 &&  // only primary unit can be disconnected
             ( (isset($_SESSION["UserIsSuperuser"]) && $_SESSION["UserIsSuperuser"]) || $user_level > 1  ) ) // superuser and admin can disconnect a unit
          echo " <BUTTON onClick=\"UpdateSpan('ControllerList','ajax.php?action=removeUnit&Unit=".$id."'); setTimeout(UpdateControllerList,1000);\">Disconnect</BUTTON>";
//        echo " <A HREF=\"/ajax.php?action=removeUnit&Unit=$id\">[Disconnect]</A>";
        if ( isset( $_SESSION["UserIsSuperuser"]) && $_SESSION["UserIsSuperuser"] )
          echo "<BUTTON onClick=\"window.location.href='admin.php?tab=cont&cont=".$data['identifier']."';\">&#9881;</BUTTON>";
        echo "</TD>";
        echo "</TR>\n";
      }
      echo "</TABLE>";
      if ( $unitCount == 0 )
        echo "<P>No connected units found</P>";
      $stmt = $dbh->prepare( 
"Select DISTINCT s.identifier,s.unitnum,s.connected,s.disconnected,c.name,c.id ".
" FROM core_connection_status s,core_networkcontroller c ".
"WHERE ".
"(s.connected IS NULL OR s.disconnected IS NOT NULL) ".
"AND s.identifier=c.identifier ORDER BY c.name" );
      $stmt->execute( array() );
      
      if ( $rows = $stmt->fetchAll() )
      {
        echo "<H2>Disconnected units:</H2>\n";
        echo "<TABLE>";
        echo "<TR><TH>Unit</TH><TH>Last connection</TH><TH>Operations</TH></TR>\n";
        foreach ( $rows as $data )
        {
          $mac = $data["identifier"];
          $unitnum = $data["unitnum"];
          $id = $mac."_".$unitnum;
          if ( (!isset( $_SESSION['UserIsSuperuser']) || !$_SESSION['UserIsSuperuser']) &&
             !user_can_access_unit( $logged_in_as, $id ) 
           )
          {
  echo "<!-- skipped unit $id because it's not accessible -->\n";
            continue;
          }
          echo "<TR><TD>".$data['name']." (".$data['identifier'].")</TD>\n";
          echo "<TD>".$data['connected']." - ".$data['disconnected']."</TD>\n";
          echo "<TD><BUTTON onClick=\"TakeReadings(3,'$id');\">View Data History</BUTTON>";
// <SELECT ID=\"dropbox$id\" onChange=\"TakeReadings(this.value,'$id');\">".
//               "<Option VALUE=-1 SELECTED>Readings...\n".
//               "<OPTION VALUE=0>View Snapshot\n".
//               "<OPTION VALUE=1>Download Snapshot\n".
//               "<OPTION VALUE=2>View/Download History from Unit\n".
//               "<OPTION VALUE=3>View/Download History from Server\n".
//               "</SELECT>";
          if ( isset( $_SESSION["UserIsSuperuser"]) && $_SESSION["UserIsSuperuser"] )
            echo "<BUTTON onClick=\"window.location.href='admin.php?tab=cont&cont=".$data['id']."';\">&#9881;</BUTTON>";

          echo "</TD></TR>";
        }
        echo "</TABLE>";
      }
    } 
    else if ( $_REQUEST["action"] == "getGSMControllerList" )
    {
    // this one is purely a database exercise, doesn't need to talk to
    // ControllerServer except perhaps to NOT list GSM units that are
    // currently online (returned by getControllerList)
      global $dbh;
      $ary = $dbh->query( "SELECT DISTINCT _cont.identifier,_unit.id,_cont.msisdn,_cont.name,_unit.subunitid ".
                  "FROM auth_user _user, ".
                  "     core_unit_users _unit_users,".
                  "     core_unit _unit,".
                  "     core_networkcontroller _cont ".
                  "WHERE _user.id=".$logged_in_as." AND ".
                  "      ( _user.is_superuser OR ".
                  "        ( _user.id=_unit_users.user_id AND ".
                  "          _unit_users.unit_id=_unit.id )) ".
                  "        AND  _cont.id=_unit.controller_id  ".
                  "      " );
      if ( !$ary )
      {
        die( "Found no controllers that you are authorized to awaken via GSM." );
      }
      echo "<TABLE>";
      foreach ( $ary as $item ) //= mysql_fetch_array($result,MYSQL_NUM)) 
      {
         if ( strlen($item[2]) >= 5 ) 
         {
           echo "<TR><TD>";
//       var_dump( $item );
           echo "<A HREF=\"/wakeController.php?ID=".$item[0]."&Name=".$item[3]."&Num=".$item[4]."\">Controller ".$item[3]." #".$item[4]."</A>";
           echo "</TD></TR>";
         }
      }
      echo "</TABLE>";
    } 
    else if ( isset( $_REQUEST["action"] ) )
    {
      die("Invalid request for '".$_REQUEST["action"]."' by ".$logged_in_as );
    }
  } // end if set action
?>
