<?php
// this file is made to set up access to the MySQL database for the PHP scripts

//error_reporting(E_ALL);
//ini_set( 'display_errors','1' );
  global $dbh;
  $dbh = dbconn_create();

  function makePasswordString( $salt, $pw )
  {
    $out = "sha1$";
    $out .= $salt . "$";
    // run salt.string through SHA
    $out .= sha1( $salt . $pw );
    return $out;
  }
  function user_change_password( $uid, $pw )
  {
    // since this is run through a hash, it does not need to be sanitized for SQL
    $pwstr = makePasswordString( "".rand(0,99999), $pw );
    // however $uid should be made safer....
    global $dbh;
    $stmt = $dbh->prepare( "UPDATE auth_user SET password=:pwstr WHERE id=:uid" );
    if ( !$stmt || !$stmt->execute( array( 'uid' => $uid, 'pwstr' => $pwstr ) ) )
      return 0;
    return 1;
  }
  function user_check_password( $uid, $pw )
  {
    global $dbh;
    $stmt = $dbh->prepare( "SELECT password from auth_user WHERE id=:uid" );
    if ( !$stmt->execute( array(uid => $uid ) ) || 
         !($ary = $stmt->fetch() ) )
    {
echo "<HTML><BODY>user_check_password got no SQL results.</BODY></HTML>";
      return 0;
    }

    if ( substr( $ary[0], 0, 5 ) != "sha1$" )
    {
echo "<HTML><BODY>user_check_password got unknown type of password hash: ".$ary[0]." </BODY></HTML>";
      return 0; // unknown type of password string
    }
    // make the password string from SHA
    $salt = substr( $ary[0], 5, 5 ); // the salt used to create the password
    $pw_str = makePasswordString( $salt, $pw );

//echo "<HTML><BODY>pwstr=$pw_str</BODY></HTML>";
    // compare the password strings
    if ( $pw_str != $ary[0] )
    {
      return 0;
    }
    return 1; // this was the correct password for this user

  }
  function user_impersonate( $id )
  {
    global $dbh;
    if ( !$dbh ) die("No database connection for login!" );
    $stmt = $dbh->prepare( "SELECT u.id,u.password,u.is_superuser,u.user_level,u.username ".
                      "from auth_user u ".
                      "WHERE u.id=:uid ");
    if ( !$stmt->execute( array('uid' => $id) ) )
    {
      echo "<HTML><BODY>SQL exec error<BR>";
      return 0;
    }
    $ary = $stmt->fetch();
    if ( !$ary )
    {
//echo "<HTML><BODY>Found NOTHING<BR>";
      return 0;
    }

    if ( $ary[2]+0 > 0 )
    {
      $_SESSION["UserIsSuperuser"] = 1;
    } else
      $_SESSION["UserIsSuperuser"] = 0;

    $_SESSION["UserLevel"] = 0+$ary[3];
    $_SESSION["UserName"] = $ary[4];
    $stmt = $dbh->query( "SELECT customer_id FROM core_useraccount WHERE user_ptr_id=$id" );
    if ( $ary = $stmt->fetch() )
      $_SESSION["UserCustomer"] = 0+$ary[0];
    else
      unset( $_SESSION["UserCustomer"] );

    $_SESSION["UserId"] = $id;

    session_write_close();
    return $id;
  }
  function user_login( $un, $pw )
  {
    global $dbh;
    if ( !$dbh ) die("No database connection for login!" );
    $stmt = $dbh->prepare( "SELECT u.id,u.password,u.is_superuser,u.user_level,u.username ".
                      "from auth_user u ".
                      "WHERE u.username=:un AND u.is_enabled > 0");
    if ( !$stmt->execute( array('un' => $un) ) )
    {
      echo "<HTML><BODY>SQL exec error<BR>";
      return 0;
    }
    $ary = $stmt->fetch();
    if ( !$ary )
    {
//echo "<HTML><BODY>Found NOTHING<BR>";
      return 0;
    }

    if ( substr( $ary[1], 0, 5 ) != "sha1$" )
    {
echo "<HTML><BODY>Wrong PW type: <?= $ary ?><BR>";
      return 0; // unknown type of password string
    }
    // make the password string from SHA
    $salt = substr( $ary[1], 5, 5 ); // the salt used to create the password
    $pw_str = makePasswordString( $salt, $pw );

//echo "<HTML><BODY>pwstr=$pw_str</BODY></HTML>";
    // compare the password strings
    if ( $pw_str != $ary[1] )
    {
      return 0;
    }

    $id = 0+$ary[0];
    if ( $ary[2]+0 > 0 )
    {
      $_SESSION["UserIsSuperuser"] = 1;
    }
    $_SESSION["UserLevel"] = 0+$ary[3];
    $_SESSION["UserName"] = $ary[4];
    $stmt = $dbh->query( "SELECT customer_id FROM core_useraccount WHERE user_ptr_id=$id" );
    if ( $ary = $stmt->fetch() )
      $_SESSION["UserCustomer"] = 0+$ary[0];
    else
      unset( $_SESSION["UserCustomer"] );

    // update last login time
    $stmt = $dbh->prepare( "UPDATE auth_user SET last_login=NOW() WHERE id=:id" );
    $stmt->execute( array( 'id' => $id ) );
    $_SESSION["UserId"] = $id;

    session_write_close();
    return $id;
  }
  function user_permissions( $uid )
  {
    global $dbh;
    echo "<HTML><BODY>uid=$uid<BR>";
    $stmt = $dbh->prepare( 
"SELECT DISTINCT p.name 
  FROM auth_permission p,auth_user u
 WHERE u.id=:id AND 
       u.is_superuser > 0
UNION
SELECT DISTINCT p.name 
  FROM auth_permission p,auth_user u,auth_user_user_permissions up 
 WHERE u.id=:id2 AND 
       up.user_id=u.id AND up.permission_id=p.id
UNION
SELECT DISTINCT p.name 
  FROM auth_permission p,auth_user u,auth_user_groups ug,auth_group_permissions gp  
 WHERE u.id=:id3 AND 
       ug.user_id=u.id AND ug.group_id=gp.group_id AND gp.permission_id=p.id"
                         );
    if ( !$stmt->execute( array( 'id' => $uid, 'id2' => $uid, 'id3'=> $uid) ) )
    {
      echo "SQL execution returned empty!";
      return null;
    }
    $perms = array();
    foreach ( $stmt as $entry )
    {
//      var_dump( $entry ); echo "<BR>";
      array_push( $perms, $entry[0] );
    }
    return $perms;
  }
  function user_has_permission( $uid, $str )
  {
    global $dbh;
    $stmt = $dbh->prepare(  "
SELECT DISTINCT p.name 
  FROM auth_permission p,auth_user u,auth_user_user_permissions up 
 WHERE u.id=:id AND p.name=:pname AND
       up.user_id=u.id AND up.permission_id=p.id
UNION
SELECT DISTINCT p.name 
  FROM auth_permission p,auth_user u
 WHERE u.id=:id2 AND p.name=:pname2 AND
       u.is_superuser > 0
UNION
SELECT DISTINCT p.name 
  FROM auth_permission p,auth_user u,auth_user_groups ug,auth_group_permissions gp  
 WHERE u.id=:id3 AND p.name=:pname3 AND
       ug.user_id=u.id AND ug.group_id=gp.group_id AND gp.permission_id=p.id"
                          );

    if ( !$stmt->execute( array( 'id' => $uid, 'id2' => $uid, 'id3'=> $uid,
                            'pname' => $str, 'pname2' => $str, 'pname3' => $str ) ) )
      return 0;
    foreach ( $stmt as $entry )
    {
      if ( $entry[0] == $str ) return 1;
    }
    return 0;
  }
  function user_can_access_unit( $userId, $unitIdStr )
  {
    if ( isset($_SESSION['UserIsSuperuser']) && $_SESSION["UserIsSuperuser"] ) return 1;

    global $dbh;
    $parts = preg_split( "/_/", $unitIdStr );
    $identifier = $parts[0];
    $subunitid = $parts[1];
    return ($stmt=$dbh->query( "
SELECT cuu.user_id 
  FROM core_unit_users cuu, core_unit u, core_networkcontroller c
 WHERE cuu.user_id=$userId AND cuu.unit_id=u.id
       AND u.subunitid=$subunitid AND u.controller_id=c.id 
       AND c.identifier='$identifier'" )) && ($stmt->fetch());

  }
?>
