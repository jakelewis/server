<?php

// show status of various parts of our server setup:
//    Cluster health (should have 4 nodes)
//    Controller servers alive?
//    dispatcher alitve?
//    Apache alive at all nodes?

$int_ext_ips = array( 
                      "13.57.147.187" => "172.31.9.251",
                      "172.31.9.251" => "13.57.147.187" ,
                      "13.55.235.168" => "172.31.4.203",
                      "172.31.4.203" => "13.55.235.168"
                    );
function check_local( $ipa )
{
return 1; // stuff below doesn't seem to work
echo "checking local... ";
  $contents = `/sbin/ifconfig | grep $ipa`;
echo $contents;
  if ( isset( $contents ) ) return 1;
  if ( isset( $int_ext_ips[$ipa] ) )
  {
    $cmd = "/sbin/ifconfig | grep ".$int_ext_ips[$ipa];
    $contents = `$cmd`;
echo $contents;
    if ( isset( $contents ) ) return 1;
  }
echo "not local\n";
  return 0;
}

if ( isset( $_REQUEST['cmd'] ) && isset( $_REQUEST['ip'] ) )
{
  $fileDir = dirname(__FILE__);
  if ( $_REQUEST['cmd'] == 'start' )
  {
    echo "Attempting start... <BR><PRE>";
    $cmd = "/usr/bin/sudo $fileDir/svr_cmd.sh";
//    echo "$cmd\n";
    echo `$cmd start`;
    echo "</PRE>";
  } else if ( $_REQUEST['cmd'] == 'restart' )
  {
    echo "Attempting restart... <BR><PRE>";
    $cmd = "/usr/bin/sudo $fileDir/svr_cmd.sh";
//    echo "$cmd\n";
    echo `$cmd restart`;
    echo "</PRE>";
  } else if ( $_REQUEST['cmd'] == 'stop' )
  {
    echo "Attempting to stop... <BR><PRE>";
    $cmd = "/usr/bin/sudo $fileDir/svr_cmd.sh";
//    echo "$cmd\n";
    echo `$cmd stop`;
    echo "</PRE>";
  } else if ( $_REQUEST['cmd'] == 'log' )
  {
    echo "getting chemtrol_server log.. <BR><PRE>";
    $cmd = "/usr/bin/sudo $fileDir/svr_cmd.sh";
//    echo "$cmd\n";
    echo `$cmd log`;
    echo "</PRE>";
  } else if ( $_REQUEST['cmd'] == 'test' )
  {
    echo "testing server script... <BR><PRE>";
    $cmd = "/usr/bin/sudo $fileDir/svr_cmd.sh";
//    echo "$cmd\n";
    echo `$cmd test`;
    echo "</PRE>";
  } else
  {
    echo "Invalid request\n";
  }
  echo "<A HREF=\"javascript:location.replace('status.php');\">Click here to return to status page</A>";
  die;
}

$controller_servers = array( //"69.164.206.74" => 'chemcom.sbcontrol.com',
                             //"45.33.94.233" => 'old.sa2.us',
                             //"45.33.125.42" => 'old.sa3.us',
                             "13.57.147.187" => 'aws1.sa2.us'
                             //"13.55.235.168" => 'au.sa2.us'
                           );
$nonlocal_db = array( //"13.57.147.187" => 1,
                      "13.55.235.168" => 1 
                    );

date_default_timezone_set("America/Los_Angeles");
echo "<!DOCTYPE html><HTML lang=\"en\"><head><meta charset=\"utf-8\"/></head><BODY>";
if ( isset($_REQUEST['d']) && isset( $_REQUEST['a']) )
{
  echo "Removing unit ".$_REQUEST['d']."<BR>";
  $dummy = file_get_contents( "http://".$_REQUEST['a'].":8077/removeUnit.htm?Unit=".$_REQUEST['d'] );
  echo $dummy;
}


//echo "<H2>Cluster health status as of ".date("H:i:s")." (Pacific Time)</H2>";
$good_wsrep_values = array( 'wsrep_cluster_size'=>4,
                            'wsrep_cluster_status'=>'Primary',
                            'wsrep_ready'=>'ON',
                            'wsrep_connected'=>'ON',

                             );
$wsrep_notes = array ( 'wsrep_local_send_queue_avg' => 
                        "if much greater than 0, indicates network throughput issues" ,
                        'wsrep_local_recv_queue_avg' =>
         "if much greater than 0, this node cannot apply write sets as fast as it receives them"
                     );

# this sets up a shorter 15 second timeout for the calls to file_get_contents
# that are used to make HTTP retuests to other processes or servers
$get_context = stream_context_create( array( 'http'=>
                        array( 
                               'timeout' => 15
                             )
               ));
$h;
    try {
      $h = new PDO('mysql:host=localhost;dbname=se','se','se' );
      if ( !$h ) die( "Could not connect to database using PDO" );
    } catch ( PDOException $exc )
    {
      die( "Could not connect to database: ".$exc->getMessage() );
    }
    $h->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $h->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
/*
    $stmt = $h->prepare( 'SHOW GLOBAL STATUS LIKE \'wsrep_%\'' );
    if ( $stmt->execute( array() ) )
    {
      $entries=$stmt->fetchAll();
      foreach ( $entries as $entry )
      {
        if ( isset( $good_wsrep_values[$entry['Variable_name']] ) )
        {
          if ( $good_wsrep_values[$entry['Variable_name']] == $entry['Value'] )
          {
            echo "<FONT COLOR=\"#00ff00\">".$entry['Variable_name']."=".
                    $entry['Value']."</FONT><BR>\n";
          } else
            echo "<FONT COLOR=\"#ff0000\">".$entry['Variable_name']."=".
                    $entry['Value'].", should be ".
                    $good_wsrep_values[$entry['Variable_name']]."</FONT><BR>\n";
        } else if ( isset( $wsrep_notes[$entry['Variable_name']] ) )
        {
          echo "".$entry['Variable_name']."=".
                      $entry['Value']." (".
                      $wsrep_notes[$entry['Variable_name']].")<BR>\n";
        } else
        {
          echo "<!-- ".$entry['Variable_name']."=".
                    $entry['Value']."-->\n";

        }
      }
    } else
      echo "Could not execute query<BR>\n";
*/
foreach ( $controller_servers as $ip => $name )
{
  $is_me = check_local( $ip );

  echo "<BR><B>$name ($ip)</B> <A TARGET=_blank HREF=\"http://$name/bannedips.txt\">ban list</A><BR>\n";
  $stmt = 0;
  $getip = $is_me ? $int_ext_ips[$ip] : $ip;
  if ( !isset( $nonlocal_db[$ip] ) )
  {
   $stmt = $h->prepare( "SELECT c.identifier,c.name,cs.connected ".
                        "from core_networkcontroller c,core_connection_status cs ".
       "WHERE cs.ip='$getip' and cs.disconnected is null AND cs.identifier=c.identifier" );
   $stmt->execute( array() );
   $entries = $stmt->fetchAll();
   $dbcount = count($entries);
//var_dump($entries);
   echo count($entries). " controllers listed as connected in database<BR>\n";
  } else
  {
    echo "[Nonlocal database, cannot compare connected to database status]<BR>";
  }
  $dbc = array();
  foreach ( $entries as $entry )
  {
    $id = strtolower($entry[0]);
    $id = str_replace( " ", "", $id );
    if ( 1 != preg_match( "/\S\S:\S\S:\S\S:\S\S:\S\S:\S\S/",$id ) )
      next;
    $dbc[$id] = $entry;
    //echo "<SPAN STYLE=\"white-space:nowrap;\">".$entry[1]." ($id)</SPAN> ";
  }
  $resp = file_get_contents( "http://$getip:8077/ajax.htm?action=getControllerListCSV",false,$get_context );
  $lines = preg_split( "/[\r\n]+/",$resp,-1,PREG_SPLIT_NO_EMPTY );
  $svrcount = count($lines)-1;
  if ( -1 == $svrcount )
  {
    echo "<FONT COLOR=\"#ff0000\">Controller Server daemon appears to be offline!</FONT> <BUTTON onClick=\"location.replace('status.php?cmd=start&ip=$ip');\">Attempt to start</BUTTON><BR>";
  }
  else
  {
    $warn = "";
    if ( !isset($nonlocal_db[$ip]) )
    {
     $svrc = array();
     $bad = 0;
     for ( $i=1; $i<=$svrcount; $i++ )
     {
       $id = strtolower(substr($lines[$i],0,17 ));
       $id = str_replace( " ", "", $id );
       if ( 17 != strlen($id) || 1 != preg_match( '/^\S\S:\S\S:\S\S:\S\S:\S\S:\S\S$/',$id ) )
       {
         $bad++;
       } else
       {
         $svrc[$id] = $lines[$i];
         $cols = preg_split( "/,/",$lines[$i] );
         $unknown = "1" == $cols[6] ? " (UNREGISTERED)":"";
         if ( !isset( $dbc[$id] ) )
           $warn .= "<FONT COLOR=\"#ff0000\">'".$id."' not in database$unknown</FONT><BR>\n";
       }
     }
     $svrcount -= $bad;
     foreach ( $dbc as $id => $entry )
     {
       if ( !isset( $svrc[strtolower($id)] ) )
         $warn .= "<FONT COLOR=\"#ff0000\">'".$id."', '".$entry['name']."' in database since ".$entry['connected']." but not connected</FONT><BR>\n";
     }
     if ( $warn == "" ) $warn = "<FONT COLOR=\"#00ff00\">All match OK</FONT><BR>";
    }
   echo "$svrcount are seen as connected by controller server<BR>$warn\n";
   
   echo "<Font COLOR=\"#00ff00\">Server is online</FONT>";
   if ( $is_me )
   {
     echo " <BUTTON onClick=\"location.replace('status.php?cmd=restart&ip=$ip');\">Restart</BUTTON>";
     echo " <BUTTON onClick=\"location.replace('status.php?cmd=stop&ip=$ip');\">Stop</BUTTON>";
   }
   else
     echo "To restart this controller server, <A HREF=\"https://$ip/status.php\">go to its status page</A><BR>";
   echo "<BR>";
   }
}

echo "<h3>Alarms</h3>\n";
$stmt = $h->prepare( "SELECT COUNT(id) from alarm_log WHERE TO_DAYS(NOW())-TO_DAYS(timestamp) < 1" );
$stmt->execute( array() );
$entry = $stmt->fetch();
echo "Received ".$entry[0]." alarms in the last 24 hours, ";

$stmt = $h->prepare( "SELECT COUNT(id) from alarm_log WHERE dispatched=1 AND TO_DAYS(NOW())-TO_DAYS(timestamp) < 1" );
$stmt->execute( array() );
$entry = $stmt->fetch();
echo "dispatched ".$entry[0]."\n";

$stmt = $h->prepare( "SELECT COUNT(id) from alarm_log WHERE dispatched=2 AND TO_DAYS(NOW())-TO_DAYS(timestamp) < 1" );
$stmt->execute( array() );
$entry = $stmt->fetch();
echo "( ".$entry[0]." were duplicates)\n";

$stmt = $h->prepare( "SELECT COUNT(id) from alarm_log WHERE dispatched=4 AND TO_DAYS(NOW())-TO_DAYS(timestamp) < 1" );
$stmt->execute( array() );
$entry = $stmt->fetch();
echo "( ".$entry[0]." were marked old)";
echo " <A HREF=\"alarmstatus.php\">Alarm dispatch log</A><BR>\n";

$stmt = $h->prepare( "SELECT DISTINCT a.identifier,a.unit_num ".
          "from alarm_log a ".
          "WHERE a.dispatched=3 AND TO_DAYS(NOW())-TO_DAYS(a.timestamp) < 1" );
$stmt->execute( array() );
$results = $stmt->fetchAll();
if ( count($results) )
{
  echo "The following systems had alarms in the last 24 hours had no recipient and could not be dispatched:<BR>";
  echo "<TABLE BORDER=1 CELLPADDING=3 CELLSPACING=0><TR><TH>Name</TH><TH>MAC / unit#</TH><TH>Count</TH></TR>\n";
  $undispatchable=0;
  foreach ( $results as $row )
  {
    echo "<TR>";
    $stmt2 = $h->query("SELECT c.name as cname,u.name as uname from core_networkcontroller c, ".
                            "core_unit u where c.id=u.controller_id AND ".
                            "c.identifier='".$row['identifier']."' AND ".
                            "u.subunitid=".$row['unit_num']);
    if ( $stmt2 && $nameres=$stmt2->fetch() )
    {
      echo "<TD>".$nameres['cname'];
      if ( $nameres['cname'] != $nameres['uname'] )
        echo " (".$nameres['uname'].")";
      echo "</TD>";
    } else
      echo "<TD><I>[not configured]</I></TD>";

    echo "<TD>".$row['identifier']." #".$row['unit_num']."</TD>";
    $stmt2= $h->query( "SELECT COUNT(a.id) ".
          "from alarm_log a ".
          "WHERE a.dispatched=3 AND a.identifier='".$row['identifier']."'".
          " AND a.unit_num=".$row['unit_num'].
          " AND TO_DAYS(NOW())-TO_DAYS(a.timestamp) < 1" );

    $cres = $stmt2->fetch();
    if ( $cres )
    {
      $undispatchable += $cres[0];
      echo "<TD>".$cres[0]."</TD>";
    }
    echo "</TR>\n";
  }
  echo "<TR><TD COLSPAN=2 ALIGN=RIGHT>TOTAL:</TD><TD ALIGN=LEFT>$undispatchable</TD></TR>";
  echo "</TABLE>\n";
}

echo "<H3>Connection history</H3>\n";
$cols = array( "identifier" => "MAC", 
               "connected"=>"Last Connect Time", 
               "disconnected"=>"Last Disconnect Time", 
               "ip"=>"Connected To",
               "from_ip"=>"Last Connected From" );
$stmt = $h->prepare( "SELECT ".join(",",array_keys($cols)).
               " FROM core_connection_status ORDER BY ip,connected" );
$stmt->execute(array());
echo "<TABLE WIDTH=\"100%\"><TR>";
foreach ( $cols as $dbn => $desc )
{
  echo "<TH>$desc</TH>";
}
echo "</TR>";
while ( $entry = $stmt->fetch() )
{
  echo "<TR>";
  foreach ( $cols as $dbn => $desc )
  {
    if ( $dbn == "ip")
    {
      $svrname = isset( $controller_servers[$entry[$dbn]] ) ?
                   $controller_servers[$entry[$dbn]] :
                   $controller_servers[$int_ext_ips[$entry[$dbn]]];
      if ( $svrname=="" )
        $svrname = $entry[$dbn];
      echo "<TD>".$svrname;
      if ( $entry[$dbn] != "" )
        echo "<BUTTON TITLE=\"Disconnect this unit\" onClick='window.location.replace(\"status.php?d=".$entry['identifier']."&a=".$entry[$dbn]."\");'>-</BUTTON>\n";
      echo "</TD>";
    }
    else
      echo "<TD>".$entry[$dbn]."</TD>";
  }
  echo "</TR>";
}
echo "</TABLE>";


echo "</BODY></HTML>";

?>
