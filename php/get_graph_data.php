<?php
/* 24 April 2018, Task 107 - Enigmo Software Solutions - Version 1.07 - Live */
$debug = isset($_REQUEST['debug']) && $_REQUEST['debug']='1';
if ( $debug )
{ 
error_reporting(E_ALL);
ini_set( 'display_errors','1' );
}
  $require_login = 1;
  $no_menus = 1;
 
include_once "datalogutils.php";
  

  $start = isset( $_REQUEST['BEGIN'] ) ? $_REQUEST['BEGIN']:0;
  global $dbh;
  $end = isset( $_REQUEST['END'] ) ? $_REQUEST['END']:0;
  $def_start = $start;
  $def_end = $end;
  $db_start = 0;
  $db_end = 0;
  $matches = array();
  $ordering = isset($_REQUEST['order']) && $_REQUEST['order'] == '1' ? "DESC" : "ASC";
  if ( $start && preg_match( "/(\d+)\/(\d+)\/(\d+)/", $start, $matches ) )
    $db_start = sprintf( "%04d-%02d-%02d", 2000+$matches[3], $matches[1], $matches[2] );
  if ( $end && preg_match( "/(\d+)\/(\d+)\/(\d+)/", $end, $matches ) )
    $db_end = sprintf( "%04d-%02d-%02d", 2000+$matches[3], $matches[1], $matches[2] );
if ( $debug ) echo "Date range is $db_start to $db_end ($start - $end)<BR>\n";
  if ( isset( $_REQUEST["UID"] ) )
    $unit_id = $_REQUEST["UID"];
  if ( isset( $_REQUEST["Unit"] ) )
    $unit_code = $_REQUEST["Unit"];
  $paramsa = array();
  $awhere = "";
  $params = array();
  $where = "";
  if ( isset( $unit_id ) )
  {
    $params['uid'] = $unit_id;
    $where = "u.id=:uid";
    $paramsa['auid'] = $unit_id;
    $awhere = "u.id=:auid";
  } else if ( isset( $unit_code ) )
  {
    $parts = preg_split( "/_/", $unit_code );
    $where = " c.identifier=:mac AND u.subunitid=:unitnum ";
    $params['mac'] = $parts[0];
    $params['unitnum'] = $parts[1];
    $awhere = " c.identifier=:amac AND u.subunitid=:aunitnum ";
    $paramsa['amac'] = $parts[0];
    $paramsa['aunitnum'] = $parts[1];
  } else
    die( "Invalid parameters supplied!" );
    include_once "session.php";
    $tz = date_default_timezone_get();
    $stmt = $dbh->prepare( "select distinct u.id,l.tz from core_networkcontroller c, core_unit u, core_location l WHERE $where AND c.id=u.controller_id AND c.location_id=l.id" );
    if ( $stmt && $stmt->execute( $params ) )
    {
      $ary = $stmt->fetch();
      $unit_id = $ary[0];
      $tz = $ary[1];
      if ( isset( $params['mac']) )
      {
        // this will make select statements happen a lot faster if we don't have
        // to access controller and unit tables
        unset( $params['mac'] );
        unset( $paramsa['amac'] );
        unset( $params['unitnum'] );
        unset( $paramsa['aunitnum'] );
        $params['uid'] = $unit_id;
        $paramsa['auid'] = $unit_id;
        $where = "u.id=:uid";
        $awhere = "u.id=:auid";
      }
    }

    $stmt = $dbh->prepare( "select c.name,cust.name ".
                           "FROM core_networkcontroller c, core_unit u, core_customer cust ".
                           " WHERE $where AND cust.id=c.customer_id AND u.controller_id=c.id" );

    $cont_name=$unit_code;
    if ( $stmt->execute( $params ) && ($results=$stmt->fetch()) )
    {
      $cont_name=$results[0];
    }
  if ( $start && $end )
  {
  // read all of this unit's datascans from the database and display them
    $hdr = array();
    $all_data = get_unit_scans( $db_start, $db_end, $unit_id, $ordering, $tz, $hdr );
    {
  
      $finalArray= [];

    
      $indData= [];
      $indData0= [];
      $indData1= [];
      $indData2= [];
      $indData3= [];
      $indData4= [];
      $indData5= [];
      $indData6= [];
      $indData7= [];
      $indData8= [];
      $indData9=[];
      $indData10= [];
      $indData11= [];
      $indData12= [];
      $indData13= [];
      $indData14=[];

      $countHeader= count($hdr);
	  $tempIndex=0;
	  //check which header index is temp 
	  for($i=2;$i<$countHeader;$i++){
		  if($hdr[$i]=='Tmp'){
			  $tempIndex=$i;
		  }
	  }
      foreach($all_data as $data){
        if($data[2]=="750" || count($data)!=$countHeader){
          continue;
        } 
        // for($i=1;$i<$countHeader-2;$i++){
        //    $data1= array($data[0] .' '. $data[1], $data[$i+1]);
           
        //    array_push($indData,[
        //     $i=>$data1 ,
        //     'data1'=>$i+1
        //   ]);
        // }
   
       if($data[0]!='Date' && date('Y-m-d',strtotime($data[0]))>=$db_start &&  date('Y-m-d',strtotime($data[0]))<=$db_end && $data[$tempIndex]>=0 ) //&& $data[$tempIndex]<=42)
       {
       array_push($indData0,array($data[0] .' '. $data[1], (float)$data[2]));
       array_push($indData1,array($data[0] .' '. $data[1], (float)$data[3]));
       array_push($indData2,array($data[0] .' '. $data[1], (float)$data[4]));
       array_push($indData3,array($data[0] .' '. $data[1], (float)$data[5]));
       array_push($indData4,array($data[0] .' '. $data[1], (float)$data[6]));
       array_push($indData5,array($data[0] .' '. $data[1], (float)$data[7]));
       array_push($indData6,array($data[0] .' '. $data[1], (float)$data[8]));
       array_push($indData7,array($data[0] .' '. $data[1], (float)(isset($data[9])?$data[9]:0)));
       array_push($indData8,array($data[0] .' '. $data[1], (float)(isset($data[10])?$data[10]:0)));
       array_push($indData9,array($data[0] .' '. $data[1], (float)(isset($data[11])?$data[11]:0)));
       array_push($indData10,array($data[0].' '. $data[1], (float)(isset($data[12])?$data[12]:0)));
       array_push($indData11,array($data[0] .' '. $data[1], (float)(isset($data[13])?$data[13]:0)));
       array_push($indData12,array($data[0] .' '. $data[1], (float)(isset($data[14])?$data[14]:0)));
       array_push($indData13,array($data[0] .' '. $data[1], (float)(isset($data[15])?$data[15]:0)));
       array_push($indData14,array($data[0] .' '. $data[1], (float)(isset($data[16])?$data[16]:0)));
       }
      }
 
	for($i=2;$i<$countHeader;$i++){
		switch($i){
			case 2:
				array_push($finalArray,[
				'name'=>$hdr[$i] ,
				'data'=>$indData0
			  ]);
			  break;
			  case 3:
				array_push($finalArray,[
				'name'=>$hdr[$i] ,
				'data'=>$indData1
			  ]);
			  break;
			  case 4:
				array_push($finalArray,[
				'name'=>$hdr[$i] ,
				'data'=>$indData2
			  ]);
			  break;
			  case 5:
				array_push($finalArray,[
				'name'=>$hdr[$i] ,
				'data'=>$indData3
			  ]);
			  break;
			  case 6:
				array_push($finalArray,[
				'name'=>$hdr[$i] ,
				'data'=>$indData4
			  ]);
			  break;
			  case 7:
				array_push($finalArray,[
				'name'=>$hdr[$i] ,
				'data'=>$indData5
			  ]);
			  break;
			  case 8:
				array_push($finalArray,[
				'name'=>$hdr[$i] ,
				'data'=>$indData6
			  ]);
			  break;
			    case 9:
				array_push($finalArray,[
				'name'=>$hdr[$i] ,
				'data'=>$indData7
			  ]);
			  break;
			  case 10:
				array_push($finalArray,[
				'name'=>$hdr[$i] ,
				'data'=>$indData8
			  ]);
			  break;
			   case 11:
				array_push($finalArray,[
				'name'=>$hdr[$i] ,
				'data'=>$indData9
			  ]);
			  break; 
			  case 12:
				array_push($finalArray,[
				'name'=>$hdr[$i] ,
				'data'=>$indData10
			  ]);
			  break;
			  case 13:
				array_push($finalArray,[
				'name'=>$hdr[$i] ,
				'data'=>$indData11
			  ]);
			  break;
			  case 14:
				array_push($finalArray,[
				'name'=>$hdr[$i] ,
				'data'=>$indData12
			  ]);
			  break;
			  case 15:
				array_push($finalArray,[
				'name'=>$hdr[$i] ,
				'data'=>$indData13
			  ]);
			  break;
			  case 16:
				array_push($finalArray,[
				'name'=>$hdr[$i] ,
				'data'=>$indData14
			  ]);
			 
		}
     
	}
      
      $jsonData= array($cont_name,$finalArray);
     echo json_encode($jsonData);
  
    }
  } else
  {
    echo "Found no scan history in the database for this controller.<BR>";
  }
  //}



