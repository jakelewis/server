<?php

include "session.php";

$visible = array( "mac" => 1,
                  "registered" => 1,
                  "systype" => 1,
                  "enabled" => 1,
                  "customer_id" => 1,
                  "last_check" => 1,
                  "last_check_ip" => 1,
                  "check_count" => 1
                );
echo "<HTML><HEAD>";

$show_all_columns=isset($_REQUEST['show_all']) && (0+$_REQUEST['show_all'])>0;

global $dbh;
if ( isset( $_REQUEST['del'] ) )
{
  $stmt = $dbh->prepare( "DELETE FROM PiPackageAuthorizations WHERE id=:id" );
  $stmt->execute( array( "id" => 0+$_REQUEST['del'] ) );
  echo "<HEAD><SCRIPT>window.location.replace('pi_auth.php');</SCRIPT></HEAD>";
  echo "<BODY>Deleted</BODY></HTML>";
  die();
}
if ( isset( $_REQUEST['upd'] ) )
{
  $stmt = $dbh->prepare( "UPDATE PiPackageAuthorizations SET systype=:ty,enabled=:en,customer_id=:cu WHERE id=:id" );
  $stmt->execute( array( "id" => 0+$_REQUEST['upd'],
      "en" => $_REQUEST['en'],
      "cu" => $_REQUEST['cu'],
      "ty" => $_REQUEST['ty'] ) );
  echo "<HEAD><SCRIPT>window.location.replace('pi_auth.php');</SCRIPT></HEAD>";
  echo "<BODY>Deleted</BODY></HTML>";
  die();
}
echo "<SCRIPT>function updateentry(id)
{
  var ty = document.getElementById('systype'+id).value;
  var en = document.getElementById('enabled'+id).checked ? 1 : 0;
  var cu = document.getElementById('cust'+id).value;

  var url = '?upd='+id+'&ty='+ty+'&en='+en+'&cu='+cu;
  //alert(url);
  window.location.replace( url );
  
}
function deleteentry(id)
{
  window.location.replace( '?del='+id );
}
</SCRIPT>
";
echo "</HEAD><BODY>";
$stmt = $dbh->query( "SELECT id,name from core_customer ORDER BY name");
$customers = $stmt->fetchAll(PDO::FETCH_ASSOC);
$stmt = $dbh->query( "SELECT * from PiPackageAuthorizations" );
$count = 0;
echo "<TABLE BORDER=1 CELLPADDING=5 CELLSPACING=0>";
while ( $row = $stmt->fetch(PDO::FETCH_ASSOC) )
{
  if ( !$count++ )
  {
    echo "<TR>";
    foreach ( $row as $nm => $val )
    {
      if ( !$show_all_columns && !isset( $visible[$nm]) ) continue;
      echo "<TH>$nm</TH>";
    }
    echo "</TR>\n";
  }
  echo "<TR>";
  $col = 0;
  foreach ( $row as $nm => $val )
  {
    if ( !$show_all_columns && !isset( $visible[$nm]) ) continue;
    echo "<TD VALIGN=TOP>";
    if ( $nm == "systype" )
    {
      echo "<SELECT ID=\"systype".$row['id']."\">";
      if ( $row['systype'] == "" )
        echo "<OPTION VALUE=0 SELECTED>(not set)\n";
      $l = glob( "dl/*.sig" );
      $found=0;
      foreach ( $l as $f )
      {
        $matches = array();
        if ( preg_match( "/([^\/]*).sig$/", $f, $matches ) )
        {
          $t = substr( $matches[0], 0, strlen($matches[0])-4 );
          if ( $t == $row['systype'] )
          {
            $found = 1;
            echo "<OPTION VALUE=\"$t\" SELECTED>$t\n";
          }
          else
            echo "<OPTION VALUE=\"$t\">$t\n";
  
        }
      }
      if ( !$found )
        echo "<OPTION VALUE=\"".$row['systype']."\" SELECTED>INVALID: ".$row['systype']."\n";
      #echo "<INPUT TYPE=TEXT SIZE=20 MAXLENGTH=50 id=\"systype".$row['id']."\" ".($val ? "CHECKED":"")." VALUE=\"".$row['systype']."\">";
      echo "</SELECT>";
    } else if ( $nm == "customer_id" )
    {
      echo "<SELECT ID=\"cust".$row['id']."\">";
      echo "<OPTION VALUE=0 ".($row['customer_id']==0 ? "SELECTED":"").">(not set)\n";
      foreach ( $customers as $c )
      {
        echo "<OPTION VALUE=".$c['id'].($c['id']==$row['customer_id']?" SELECTED":"").">".$c['name']."\n";
      }
      echo "</SELECT>";
    } else if ( $nm == "enabled" )
      echo "<INPUT TYPE=CHECKBOX id=\"enabled".$row['id']."\" ".($val ? "CHECKED":"").">";
    else
      echo "$val";
    echo "</TD>";
  }
  echo "<TD VALIGN=TOP>".
       "<BUTTON onClick=\"updateentry(".$row['id'].");\">Update</BUTTON>".
       "<BUTTON onClick=\"deleteentry(".$row['id'].");\">Delete</BUTTON>".
       "</TD>";
  echo "</TR>\n";
}
echo "</TABLE>";
if ( !$count )
  echo "Found no entries!";
else if ( !$show_all_columns )
  echo "<A HREF=\"?show_all=1\">Show all columns</A>";
echo "</BODY></HTML>";

?>
