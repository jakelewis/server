#!/usr/bin/perl

$op = shift;
#print "In script.\n";

if ( $op eq "start" )
{
  $cmd = "start";
} elsif ( $op eq "restart" )
{
  $cmd = "restart";
} elsif ( $op eq "stop" )
{
  $cmd = "stop";
} elsif ( $op eq "log" )
{
  print `cat /var/log/chemtrol_server.log`;
  die;
} elsif ( $op eq "test" )
{
  print `echo Server script works ; whoami ; service chemtrol_server status 2>&1`;
  die;
} else
{
  die "Unknown";
}

print `service chemtrol_server $cmd 2>&1 ; service chemtrol_server status 2>&1`;

