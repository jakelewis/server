/* 24 April 2018, Task 107 - Enigmo Software Solutions - Version 1.07 - Live */
$(document).ready(function () {
    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;
    
        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');
    
            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };

    var useStart = getUrlParameter("BEGIN");
    if ( !useStart ) useStart = '-1M';
    var useEnd = getUrlParameter("END");
    if ( !useEnd ) useEnd = '0';
    $("#startdate").datepicker({dateFormat: 'mm/dd/y', maxDate: '0'}).datepicker('setDate',useStart);
    $("#enddate").datepicker({dateFormat: 'mm/dd/y', maxDate: '0'}).datepicker('setDate',useEnd);
   // loadgraph(getUrlParameter("BEGIN"),getUrlParameter("END"));
    $("#refreshBtn").on("click",function(){
        var startdate= $("#startdate").val();
        var enddate= $("#enddate").val();
        if(startdate!="" && enddate !="") {
            $("#progress").show();
            $("#viewData").remove();
            $("#container1").show();
            
            loadgraph(startdate,enddate);

        }
        else alert("Please enter the date.");
    });
	function date( text )
	{
		var dateTime= new Date(text);
		return (dateTime.getTime()- dateTime.getTimezoneOffset(dateTime)*60*1000);
	}
    
   function loadgraph(begin,end){
    var myParams ={Unit:getUrlParameter("Unit"),BEGIN:begin,END:end,order:0}
    var myUrl = window.location.origin+ "/get_graph_data.php?"+ jQuery.param(myParams);
        $.ajax({
        url: myUrl,
        async:true,
        type: "GET",
        dataType: "json",
        contentType: "application/json"
           })
        .done(function (data) {
           
        var seriesdata= data;
		var dateArrays= [];
       var series =[];
	   var checkData=true;
       if(seriesdata[1].length>1){
       
      // $("#controllerName").html(seriesdata[0]);
	   $.each(seriesdata[1], function(i,v){
		   if(v.data.length<1){
				checkData=false
				console.log(checkData);
			}
		   $.each(v.data,function(j,value){
			var dateArray=[]
            
                dateArray.push(date(value[0]),value[1]);
                dateArrays.push(dateArray);
            
			
           });

           
           
             if(v.name=="Cond")  series.push({"name":v.name,color:'#96B05B',"data": dateArrays});
			 else if(v.name=="pH")  series.push({"name":v.name,yAxis: 1,color:'#E39345',"data": dateArrays});
			 else if(v.name=="ORP")  series.push({"name":v.name,yAxis: 3,color:'#4F81B4',"data": dateArrays});
             else if(v.name=="San")  series.push({"name":v.name,yAxis:2,color:'#B85750',"data": dateArrays});
           
		   dateArrays = [];
	   });
	   if(checkData){
          var chart1 = new Highcharts.Chart({
    
            chart: {
                renderTo: 'container1',
                zoomType: 'x'
            },
                  title: {
            text: ''
        },  
            xAxis: {
                   type: 'datetime',
                   startOnTick: true,
                   endOnTick: true,
            },
            yAxis: [
			
				{
					title: {
						text: 'Conductivity/ TDS',
						style: {
						color: '#96B05B'
						}
					},
					labels: {
						format: '{value}',
						style: {
						color: '#96B05B'
						}
					}

				},
				{ 
					title: {
						text: 'pH',
						style: {
						color: '#E39345'
						}
					},
					labels: {
						format: '{value}',
						style: {
						color: '#E39345'
						}
					}
				},
				{
                    title: {
                        text: 'San',
                        style:{
                            color:'#B85750'
                        }
                    
					},
                labels: {
                    format: '{value}',
                    style:{
                        color:'#B85750'
                    }
                    
                }
				}
				,{ 
			
           
					title: {
						text: 'ORP',
						style: {
						color: '#4F81B4'
						}
					},
					labels: {
						format: '{value}',
						style: {
						color: '#4F81B4'
						}
					}
    
				}
				
            
            ],
        tooltip: {
            shared: true
        },
        series:series
           
        
        });
	   }
	   else{
		   var html= "<h2 style='margin-top:15%;'>Sorry, no data found within the requested date range.</h2>";
        $("#container1").html("");
        $("#container1").append(html);
	   }
    }
    else{
        var html= "<h2 style='margin-top:15%;'>Sorry, no data found within the requested date range.</h2>";
        $("#container1").html("");
        $("#container1").append(html);
    }
        });
    
   
    }
    
});




