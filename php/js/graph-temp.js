/* 24 April 2018, Task 107 - Enigmo Software Solutions - Version 1.07 - Live */
$(document).ready(function () {
    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;
    
        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');
    
            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };
	function date( text )
	{
		var dateTime= new Date(text);
		return (dateTime.getTime()- dateTime.getTimezoneOffset(dateTime)*60*1000);
	}

   // loadgraph(getUrlParameter("BEGIN"),getUrlParameter("END"));
    
    $("#refreshBtn").on("click",function(){
        var startdate= $("#startdate").val();
        var enddate= $("#enddate").val();  
        $("#viewData").remove();
        $("#container2").show(); 
       loadgraph(startdate,enddate);

       
    });
    function loadgraph(begin,end){

    var myParams ={Unit:getUrlParameter("Unit"),BEGIN:begin,END:end,order:0}
    var myUrl = window.location.origin+ "/get_graph_data.php?"+ jQuery.param(myParams);
    $.ajax({
        url: myUrl,
        type: "GET",
        async:true,
        dataType: "json",
        contentType: "application/json"
           })
        .done(function (data) {
            
        var seriesdata= data;
		var dateArrays= [];
       var series =[];
	   var checkData= true;
       if(seriesdata[1].length>1){

	   $.each(seriesdata[1], function(i,v){
		   if(v.data.length<1){
				checkData=false
				
			}
		   $.each(v.data,function(j,value){
			var dateArray=[]
		
                dateArray.push(date(value[0]),value[1]);
                dateArrays.push(dateArray);
           
           });
		
            if(v.name =="ORP")  series.push({"name":v.name,color:'#4F81B4',"data": dateArrays,"visible":true});
			else if(v.name =="San")  series.push({"name":v.name,color:'#B85750',"data": dateArrays,"visible":false});
			else if(v.name =="pH")  series.push({"name":v.name,color:'#E39345',"data": dateArrays,"visible":false});
			else if(v.name =="Cond")  series.push({"name":v.name,color:'#96B05B',"data": dateArrays,"visible":false});
			else if(v.name =="Tmp")  series.push({"name":v.name,"color":"#CC6059","data": dateArrays,"visible":false});
		    else if(v.name =="MFlow")  series.push({"name":v.name,"color":"#886451","data": dateArrays,"visible":false});
			else if(v.name =="MFlow Tot")  series.push({"name":v.name,"color":"#885159","data": dateArrays,"visible":false});
			else if(v.name =="BFlow")  series.push({"name":v.name,"color":"#528881","data": dateArrays,"visible":false});
			else if(v.name =="ppmT")  series.push({"name":v.name,"color":"#96B05B","data": dateArrays,"visible":false});
			else if(v.name =="ppmA")  series.push({"name":v.name,"color":"#4F81B4","data": dateArrays,"visible":false});
			else if(v.name =="ppmC")  series.push({"name":v.name,"color":"#8F75AF","data": dateArrays,"visible":false});
			else if(v.name =="FEff")  series.push({"name":v.name,"color":"#b138ee","data": dateArrays,"visible":false});
            else  series.push({"name":v.name,"data": dateArrays,"visible":false});
          
           dateArrays = [];
           $("#progress").hide();
	   });
	   
	   if(checkData){
          var chart2 = new Highcharts.Chart({
    
            chart: {
                renderTo: 'container2',
                zoomType: 'x'
            },
                  title: {
            text: ''
        },  
            xAxis: {
                   type: 'datetime',
                   startOnTick: true,
                   endOnTick: true,
            },
            series:series
           
        
        });
	   }
	   else{
        $("#progress").hide();
        $("#container2").hide();
		}
	   
    }
	
    else{
        $("#progress").hide();
        $("#container2").hide();
    }
        });
   

    }
});




