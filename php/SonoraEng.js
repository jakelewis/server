var blinkIntervalId=-1;
var lcdIntervalId=-1;
var isIE = navigator.appName.indexOf("Internet Explorer")!=-1;
var requestUnitId = "";
function CreateRequestObject()
{
  if(window.XMLHttpRequest)
  {
    // pretty much all browswer except IE support this
    try {return new XMLHttpRequest();} catch(e) {}
  }
  else if(window.ActiveXObject)
  {
    // Internet Explorer 5+
    try {return new ActiveXObject('MSXML2.XMLHTTP');} catch(e) {}//IE5-6
    try {return new ActiveXObject('Microsoft.XMLHTTP');} catch(e) {}
    isIE = 1;
  }
  else if (window.createRequest)
  {
    try { return window.createRequest();} catch(e) {}
  }
  else
  {
    // There is an error creating the object,
    // just as an old browser is being used.
    alert('Problem creating the request object. Dynamic udpating will not work with this browser.');
  }
  return undefined;
}

var requestInProgress=0;
var scanBusy=0;
function AbortReq(obj) 
{
 if(obj != undefined)
 {
  if((obj.readyState>0)&&(obj.readyState<4)){obj.abort();}
 }
}
// this one does any URI, appends a time param to prevent caching
function DoReqURL(obj,url,success_func,fail_func)
{
  if ( requestInProgress ) return false; // don't put two out there at once
  if(obj==undefined) return false;
  requestInProgress = 1;
  AbortReq(obj);
  var dt = new Date();
  obj.open('get',url+"&t="+dt.getTime(),true);
  obj.onreadystatechange=function()
      {
       if(obj.readyState==4)
       {
         requestInProgress = 0;
         if(obj.status==200)
         { if ( success_func != undefined ) success_func(obj); }
         else if ( fail_func != undefined )
         { fail_func(obj); } 
       }
      }
  obj.send();
  return true;
}
// this one is specifically for ajax calls
function DoReq(obj,str,success_func,fail_func)
{
  return DoReqURL(obj,'ajax.php?' +
          ( requestUnitId != "" ? 'Unit='+requestUnitId+"&":"") 
          +str,success_func,fail_func);
}
var requestObjBtns = CreateRequestObject();
var requestObjTouch = CreateRequestObject();
var requestObjLCD = CreateRequestObject();
var lcdDiv = 'lcdDiv';
var lcdUpdateRate=2000;
var skippedUpdates=0;

// only gets called with a successful & completed return
function ProcessLCDResponse( reqObj )
{
  var answer = reqObj.responseText;
  if ( answer == "disconnected" )
  {
    window.location.replace( "select.php?msg=Controller%20was%20disconnected." );
    return;
  }
  if ( answer == "busy" )
  {
    window.location.replace( "select.php?msg=Controller%20is%20busy." );
    return;
  }
  if ( answer != ""  )
  {
    //ajaxText is the id of a DIV tag in index.htm
    document.getElementById(lcdDiv).innerHTML = answer;
  }
  if ( -1 != lcdIntervalId )
    clearTimeout(lcdIntervalId);
  lcdIntervalId = setTimeout(  "StartLCDUpdate()", lcdUpdateRate );
  
}

function ProcessLCDResponseFailure( reqObj )
{
  if ( -1 != lcdIntervalId )
    clearTimeout(lcdIntervalId);
  lcdIntervalId = setTimeout(  "StartLCDUpdate()", lcdUpdateRate );
}

function StartLCDUpdate()
{
  if ( scanBusy )
    console.log("LCD Update skipped due to scan");

  if ( !DoReq( requestObjLCD, "action=refresh", ProcessLCDResponse, ProcessLCDResponseFailure ) )
  {
    console.log("LCD Update failed");
    lcdIntervalId = setTimeout(  "StartLCDUpdate()", lcdUpdateRate );
  } else
    console.log("LCD Update started");
}

function setLCDUpdate( rate )
{
  // request LCD updates every 2 seconds
  lcdUpdateRate = rate;
  if ( rate > 0 )
    lcdIntervalId = setTimeout(  "StartLCDUpdate()", lcdUpdateRate );
}

function ProcessBtnResponse( reqObj )
{
  ProcessLCDResponse( reqObj );
/*
  // do nothing with the return
  if ( -1 != lcdIntervalId )
    clearTimeout(lcdIntervalId);
  lcdIntervalId = setTimeout(  "StartLCDUpdate()", 800 );
*/
}

function ProcessTouchResponse( reqObj )
{
  ProcessLCDResponse( reqObj );
/*
  // do nothing with the return
  if ( -1 != lcdIntervalId )
    clearTimeout(lcdIntervalId);
  lcdIntervalId = setTimeout(  "StartLCDUpdate()", 800 );
*/
}

//==============================================================================
// General purpose way to add events to objects without overwriting any
// existing events that might be on that object.
//==============================================================================
function addEvent(obj, eventType, functionToRun)
{
  if (obj.addEventListener)
  {
    obj.addEventListener(eventType, functionToRun, true);
    return true;
  } else if (obj.attachEvent)
  {
    var r = obj.attachEvent("on"+eventType, functionToRun);
    return r;
  } else 
  {
    return false;
  }
}

function doLoad()
{
  // do an immediate update
  StartLCDUpdate(); 

  // setup a timeout to request ajax updates every so often
  setLCDUpdate( lcdUpdateRate );
}

function insertLCD(uid)
{
  requestUnitId = uid;
  document.write( "<DIV CLASS=\"lcd\" ID=\"lcdDiv\">" );
	document.write( "1:_Waiting_for_update..<BR>" );
	document.write( "<BR>" );
	document.write( "<BR>" );
	document.write( "<BR>" );
	document.write( "<BR>" );
	document.write( "<BR>" );
	document.write( "<BR>" );
	document.write( "<BR>" );
	document.write( "</DIV>" );

  // blink rate = 1Hz
  blinkIntervalId = setInterval( updateBlink, 500, "JavaScript" );

  addEvent(window, 'load', doLoad);

}

function onButtonHot( btn,id )
{
  //btn.className="btnHot";
}

function onButtonCold( btn,id )
{
  //btn.className="btnCold";
}

function onButtonDown( btn,id )
{
  btn.className="btnHot";
}

function onButtonClicked( btn,id )
{
  onUserAction();
  if ( btn != 'null' )
    btn.className="btnCold";
  DoReq(requestObjBtns,"btn="+id,ProcessBtnResponse );
//  if ( id == "Refresh" )
//  {
//    StartAjaxUpdate( "refresh=1" );
//  } else
//    StartAjaxUpdate( "btn="+id );
  //document.getElementById('lcdDiv').innerHTML = "Clicked: "+id;
}

function fixEvent( e )
{
  if ( !isIE && !e.hasOwnProperty('offsetX') )
  {

    var obj = e.target;
    var curleft = 0;
    var curtop = 0;
    var foundUpperLeft = 0;
    var parWidth = -1;
    while (obj.offsetParent) 
    {
      obj = obj.offsetParent;
      if ( -1 == parWidth ) parWidth = obj.offsetWidth;
      curleft += obj.offsetLeft;
      curtop += obj.offsetTop;
    }
    e.offsetX=e.layerX-curleft;
    e.offsetY=e.layerY-curtop;
    e.parentWidth = parWidth;
  } else
  {

    var obj = e.srcElement;
    e.parentWidth = obj.offsetParent.offsetWidth;
  }
  return e;
}
var userActivityTimeoutHandle = window.setTimeout(60*10);
var dateLastActivity = new Date();
var inactivityTimeoutEnabled = 0;
function enableInactivityTimeout()
{
  inactivityTimeoutEnabled = 1;
  onUserAction();
}
function onUserAction()
{
  window.clearTimeout(userActivityTimeoutHandle);
  userActivityTimeoutHandle = window.setTimeout(60*10);
  dateLastActivity = new Date();
}
function checkUserActivityTimeout()
{
  if (!inactivityTimeoutEnabled ) return;

  var now = new Date();
  var diff = Math.trunc((now.getTime()-dateLastActivity.getTime())/1000);
  if ( diff < 60*9 ) 
  {
    // hide logout warning
     var obj = document.getElementById("LogoutWarning");
     if (obj != undefined)
       obj.style.visibility="hidden";


  } else if ( diff < 60*10 )
  {
    // show and update logout warning
     var obj = document.getElementById("LogoutWarning");
     if (obj != undefined)
     {
       var secs = 60*10-diff;
       obj.style.visibility="visible";
       obj.innerHTML = "You will be logged out in "+secs+" seconds with continued inaction.";
     }
  } else 
    window.location.replace( "/logout.php");
}

function onTouch( row,col,ev )
{
  onUserAction();
  if ( -1 == col ) // try to figure out x coord from mouse coordinates relative 
		  // to the size of the span
  {
    if ( !ev ) ev = window.event;
    ev = fixEvent(ev);
    var obj = ev.target || ev.srcElement;
    var x=1,w=200;
    x = ev.offsetX;
    w = ev.parentWidth;
    col = Math.round(x*22/w);
 //   col = x;//*22/obj.style.width.value;
//    alert( "onTouch("+row+","+col+");" );
  }
  DoReq(requestObjTouch,"touch=1&row="+row+"&col="+col,ProcessTouchResponse );
//  if ( id == "Refresh" )
//  {
//    StartAjaxUpdate( "refresh=1" );
//  } else
//    StartAjaxUpdate( "btn="+id );
  //document.getElementById('lcdDiv').innerHTML = "Clicked: "+id;
}

function onButtonUp( btn,id )
{
  onButtonClicked(btn,id);
}


function insertButton( txt, id, tdstyle )
{
  if ( !id ) id = txt;
  document.write( "<TD class=btn "+tdstyle+" ><DIV class=\"btnCold\" "+
                  "onMouseOver=\"onButtonHot(this,'"+id+"')\" "+
                  "onMouseOut=\"onButtonCold(this,'"+id+"')\" "+
                  //"onMouseDown=\"onButtonDown(this,'"+id+"')\" "+
                  //"onMouseUp=\"onButtonUp(this,'"+id+"')\" "+
                  //"onDblClick=\"onButtonClicked(this,'"+id+"')\" "+
                  "onClick=\"onButtonClicked(this,'"+id+"')\" "+
                  ">"+txt+"</DIV></TD>" );
}

var blinkState=0;
function updateBlink()
{
  checkUserActivityTimeout();
  blinkState=1-blinkState;
  // find all span objects whose stylename starts with blink
  var spans = document.getElementsByTagName('span');
  var patt=new RegExp("^blnk([0-1])(.*)","i");
  for ( i=0; i<spans.length; i++ )
  {
    // check classnames of all spans
    var mtch = ( patt.exec(spans[i].className ) );
    if ( mtch != null )
    {
      var newClass = "blnk"+blinkState+mtch[2];
      spans[i].className = newClass;
    } 
  }
}

function checkKey(e) {

    e = e || window.event;


    if (e.keyCode == '13') 
        onButtonClicked('null','enter');
    else if (e.keyCode == '37') 
        onButtonClicked('null','left');
    else if (e.keyCode == '38') 
    {
//      alert( "up hit");
        onButtonClicked('null','up');
    }
    else if (e.keyCode == '39') 
        onButtonClicked('null','right');
    else if (e.keyCode == '40')
        onButtonClicked('null','down');
    else if (e.keyCode == '48' || e.keyCode == '96' ) // 40 on reg keyboard, 96 on keypad
    {
//      alert( "0 hit")0i//;
        onButtonClicked('null','0');
    }
    else if (e.keyCode == '49' || e.keyCode == '97' )
    {
 //     alert( "1 hit");
        onButtonClicked('null','1');
    }
    else if (e.keyCode == '50' || e.keyCode == '98')
        onButtonClicked('null','2');
    else if (e.keyCode == '51' || e.keyCode == '99')
        onButtonClicked('null','3');
    else if (e.keyCode == '52' || e.keyCode == '100')
        onButtonClicked('null','4');
    else if (e.keyCode == '53' || e.keyCode == '101')
        onButtonClicked('null','5');
    else if (e.keyCode == '54' || e.keyCode == '102')
        onButtonClicked('null','6');
    else if (e.keyCode == '55' || e.keyCode == '103')
        onButtonClicked('null','7');
    else if (e.keyCode == '56' || e.keyCode == '104')
        onButtonClicked('null','8');
    else if (e.keyCode == '57' || e.keyCode == '105')
        onButtonClicked('null','9');
    
    else if (e.keyCode == '190' || e.keyCode == '110' ) // reg and numeric keypad
        onButtonClicked('null','dot');
    else
    {
      //alert( "Key code not used: "+e.keyCode );
    }
}
  
function insertKeypad()
{
  document.write("<TABLE WIDTH=\"100%\" HEIGHT=\"100%\" CELLSPACING=1><TR>" );
  insertButton("1"); insertButton("2"); insertButton("3"); insertButton("&#9650;","up");
  document.write( "</TR><TR>" );
  insertButton("4"); insertButton("5"); insertButton("6"); insertButton(".","dot");
  document.write( "</TR><TR>" );
  insertButton("7"); insertButton("8"); insertButton("9"); insertButton("&#9660;","down");
  document.write( "</TR><TR>" );
  insertButton("&#9668;","left"); insertButton("0"); insertButton("&#9658;","right");
// insertButton("&#10003;","enter"); // a Checkmark
 insertButton("OK","enter","STYLE=\"FONT-SIZE: 32;\"");
  //document.write( "</TR><TR>" );
  //insertButton("Refresh","Refresh","COLSPAN=4");
  document.write( "</TR></TABLE>" );

  document.onkeydown = checkKey;

}

function getObj(name)
{
  var ths = new Object();
  if (document.getElementById)
  {
    ths.obj = document.getElementById(name);
        if ( ths.obj.style )
          ths.style= ths.obj.style;
        else
    ths.style = new Object();
  }
  else if (document.all)
  {
  ths.obj = document.all[name];
  ths.style = document.all[name].style;
  }
  else if (document.layers)
  {
    ths.obj = document.layers[name];
    ths.style = document.layers[name];
  }
  return ths;
}

function hideScan(id)
{
  document.getElementById("scandata"+id).style.visibility="hidden";
  scanBusy = 0;
  console.log("scan hidden");
}
function TakeReadings(dest, id) 
{
  if (-1 == dest) return;
  if ( 3 == dest ) // view/download data scans from database
  {
    var win = window.open( "datascanhist.php?Unit="+id, "_blank" );
    win.focus();
    return;
  }
  if ( 2 == dest ) // view/download data log
  {
    var win = window.open( "datalog.php?Unit="+id, "_blank" );
    win.focus();
    return;
  }
  // dest=0 means show data scan, 
  // dest=1 means download data scan
  var drop = document.getElementById("dropbox" + id);
  if ( !drop ) alert( "Could not find dropbox"+ id );
  var div = document.getElementById("scandata" + id);
  if ( !div ) alert( "Could not find scandata"+ id );
  div.style.visibility = "visible";
  div.innerHTML = "";
  var requestObjScan = CreateRequestObject();
  var processScanFailure = function (reqObj) {
    div.innerHTML = "Scan Failed!";
    delete reqObj;
    setTimeout("hideScan(\""+id+"\");", 2000);
    console.log("Scan failed");
  };
  var processScanResponse = function (reqObj) {
    var answer = reqObj.responseText;
    if ( answer == "disconnected") {
      window.location.replace("select.php?msg=Controller%20was%20disconnected.");
      return;
    }
    if (answer != "") 
    {
      answer.trim();
      var prog_only = answer == "In Progress..." || answer == "Scan started...";
      answer = "<FONT SIZE=-1><PRE>"+answer+"</PRE></FONT>";
//      answer = answer.replace( "\n", "<BR>" );
      div.innerHTML = answer;
      if ( prog_only ) 
      {
        if ( !DoReq(reqObj, "action=getscan&Unit=" + id, processScanResponse, processScanFailure) )
        {
          console.log("getscan req failed");
          scanBusy = 0;
        }
        else
        {
          console.log("getscan started");
        }

      } else {
        setTimeout("hideScan(\""+id+"\");", 8000);
       // setTimeout("document.getElementById(\"scandata"+id+"\").style.visibility=\"hidden\";", 8000);
        delete reqObj;
        console.log("getscan response:"+answer);
        if (1 == dest) // want to download this scanned data
          window.location = "/ajax.php?action=downloadscan&Unit=" + id;
      }
    }
  };
  if ( !DoReq(requestObjLCD, "action=startscan&header=1&Unit=" + id, processScanResponse, processScanFailure ) )
    processScanFailure( requestObjLCD );
  else
    scanBusy = 1;

  drop.value = -1;
}

function UpdateSpan( spn_nm, url )
{
  var spn = document.getElementById(spn_nm);
  if (spn == undefined) 
    return -1;
/*
  var saveht = spn.style.height;
  spn.style.height=400;
  spn.innerHTML = "Loading...<BR><BR><BR><BR><BR>";
*/
  var spnreq = CreateRequestObject();
  var onSpanResponse = function(req)
  {
    spn.innerHTML = req.responseText;
 //   spn.style.height = saveht;
    var start = req.responseText.search(/<SCRIPT>/i);
    var end = req.responseText.search(/<\/SCRIPT>/i);
    if ( -1 != start && -1 != end && end > start )
    {
      start += 8;
      var script = req.responseText.substr(start,end-start);
//      alert( script );
//alert("Script:"+script);
      eval(script); // Execute javascript
    }
    var redir = ( req.getResponseHeader('TIMEOUT') == "1" );
      
    delete req;
    if ( redir ) window.location.href = 'index.php';
  }
  var onSpanFailure = function (req)
  {
    spn.style.height = saveht;
    spn.innerHTML = "Update failed:"+req.responseText;
    delete req;
  };
  DoReqURL(spnreq, url, onSpanResponse, onSpanFailure );
  return 0;
}

function UpdateControllerList() 
{
  if ( document.hidden || scanBusy ) return; // don't update this list unless the page is visible
  var spn = document.getElementById("ControllerList");
  if (spn != undefined) 
  {
    var req = CreateRequestObject();
    var onControllerList = function (treq) 
    {
      spn.innerHTML = treq.responseText;
      delete treq;
    };
    var onControllerListFailure = function (treq) 
    {
      spn.innerHTML = "Cannot get controller list!";
      delete treq;
    };
    DoReq(req, "action=getControllerList", onControllerList, onControllerListFailure);
  }
  var spng = document.getElementById("GSMControllerList");
  if (spng != undefined) {
    var reqg = CreateRequestObject();
    var onGSMControllerList = function (req) {
      spng.innerHTML = req.responseText;
      delete req;
    };
    var onGSMControllerListFailure = function (req) {
      spng.innerHTML = "Cannot get controller list!";
      delete req;
    };
    DoReq(reqg, "action=getGSMControllerList", onGSMControllerList, onGSMControllerListFailure);
  }
}

function RequestDataLog( id )
{
  var spn = document.getElementById("DataLog");
  if (spn != undefined) 
  {
    var req = CreateRequestObject();
    var onDataLogStart = function (req) 
    {
      spn.innerHTML = req.responseText;
      delete req;
      if (req.responseText.substring(0,14) == "In Progress..." || req.responseText == "Datalog retrieval started...")
        setTimeout( "CheckDataLog('"+id+"')", 1000 );
    };
    var onDataLogStartFailure = function (req) {
      spn.innerHTML = "Cannot get data log!";
      delete req;
    };
    DoReq(req, "action=startdatalog&Unit="+id, onDataLogStart, onDataLogStartFailure);
  }
}

function CheckDataLog(id) 
{
  // needs to re-up the interval to continue checking on the datalog...
  var spn = document.getElementById("DataLog");
  if (spn != undefined) 
  {
    var req = CreateRequestObject();
    var onDataLogCheck = function (req) 
    {
      var extra = "";
      var still_in_progress = req.responseText.substring(0, 14) == "In Progress...";
      if (!still_in_progress)
        extra = "<A HREF=\"ajax.php?action=downloaddatalog&Unit="+id+"\">Download this History to your computer</A>, or "+
             "<A HREF=\"javascript:window.close();\">close this page</A> when you're done with it.<br/>";
      else
        extra = "Please do not close this window or otherwise try to access this controller while the history is downloaded...<BR>\n";

      spn.innerHTML = extra+"<CENTER><TABLE><TR><TD ALIGN=LEFT><PRE>"+
                      req.responseText+
                      "</PRE></TD></TR></TABLE></CENTER>"+extra;

      delete req;
      
      var pageHeight = document.body.scrollHeight;

      window.scrollTo(0, pageHeight);

      if (still_in_progress )
        setTimeout("CheckDataLog('" + id + "')", 1000);

    };
    var onDataLogCheckFailure = function (req) {
      spn.innerHTML = "Cannot get data log!";
      delete req;
    };
    DoReq(req, "action=getdatalog&Unit=" + id, onDataLogCheck, onDataLogCheckFailure);
  }
}

function passwordsMatch( frm, n1, n2 ) 
{
// alert("validating form");
  var f = document.getElementById( frm );
  if ( null == f ) 
  { 
  //  alert( "Could not find form "+frm );
    return false;
  }
  var p1 = f.elements[n1].value;
  var p2 = f.elements[n2].value;
//  alert("p1="+p1+", p2="+p2 );
  if ( p1 != p2 )
  {
    alert("The password fields do not match, please try again.");
    return false;
  }
//  else f.submit(); 
  return true;
}

