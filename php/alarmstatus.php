<?php

// show status of various parts of our server setup:
//    Cluster health (should have 4 nodes)
//    Controller servers alive?
//    dispatcher alitve?
//    Apache alive at all nodes?

$controller_servers = array( "69.164.206.74" => 'chemcom.sbcontrol.com',
                             "45.33.94.233" => 'sa2.us',
                             "45.33.125.42" => 'sa3.us' );
date_default_timezone_set("America/Los_Angeles");
echo "<!DOCTYPE html><HTML lang=\"en\"><head><meta charset=\"utf-8\"/></head><BODY>";
$h;
    try {
      $h = new PDO('mysql:host=localhost;dbname=se','se','se' );
      if ( !$h ) die( "Could not connect to database using PDO" );
    } catch ( PDOException $exc )
    {
      die( "Could not connect to database: ".$exc->getMessage() );
    }
    $h->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $h->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

echo "<h3>Alarm Status</h3>\n";
$stmt = $h->prepare( "SELECT COUNT(id) from alarm_log WHERE TO_DAYS(NOW())-TO_DAYS(timestamp) < 1" );
$stmt->execute( array() );
$entry = $stmt->fetch();
echo "Received ".$entry[0]." alarms in the last 24 hours, ";

$stmt = $h->prepare( "SELECT COUNT(id) from alarm_log WHERE dispatched=1 AND TO_DAYS(NOW())-TO_DAYS(timestamp) < 1" );
$stmt->execute( array() );
$entry = $stmt->fetch();
echo "dispatched ".$entry[0]."\n";

$stmt = $h->prepare( "SELECT COUNT(id) from alarm_log WHERE dispatched=2 AND TO_DAYS(NOW())-TO_DAYS(timestamp) < 1" );
$stmt->execute( array() );
$entry = $stmt->fetch();
echo "( ".$entry[0]." were duplicates)\n";

$stmt = $h->prepare( "SELECT COUNT(id) from alarm_log WHERE dispatched=4 AND TO_DAYS(NOW())-TO_DAYS(timestamp) < 1" );
$stmt->execute( array() );
$entry = $stmt->fetch();
echo "( ".$entry[0]." were marked old)";
echo "<BR>\n";

$stmt = $h->prepare( "SELECT DISTINCT a.identifier,a.unit_num ".
          "from alarm_log a ".
          "WHERE a.dispatched=3 AND TO_DAYS(NOW())-TO_DAYS(a.timestamp) < 1" );
$stmt->execute( array() );
$results = $stmt->fetchAll();
if ( count($results) )
{
  echo "The following systems had alarms in the last 24 hours had no recipient and could not be dispatched:<BR>";
  echo "<TABLE BORDER=1 CELLPADDING=3 CELLSPACING=0><TR><TH>Name</TH><TH>MAC / unit#</TH><TH>Count</TH></TR>\n";
  $undispatchable=0;
  foreach ( $results as $row )
  {
    echo "<TR>";
    $stmt2 = $h->query("SELECT c.name as cname,u.name as uname from core_networkcontroller c, ".
                            "core_unit u where c.id=u.controller_id AND ".
                            "c.identifier='".$row['identifier']."' AND ".
                            "u.subunitid=".$row['unit_num']);
    if ( $stmt2 && $nameres=$stmt2->fetch() )
    {
      echo "<TD>".$nameres['cname'];
      if ( $nameres['cname'] != $nameres['uname'] )
        echo " (".$nameres['uname'].")";
      echo "</TD>";
    } else
      echo "<TD><I>[not configured]</I></TD>";

    echo "<TD>".$row['identifier']." #".$row['unit_num']."</TD>";
    $stmt2= $h->query( "SELECT COUNT(a.id) ".
          "from alarm_log a ".
          "WHERE a.dispatched=3 AND a.identifier='".$row['identifier']."'".
          " AND a.unit_num=".$row['unit_num'].
          " AND TO_DAYS(NOW())-TO_DAYS(a.timestamp) < 1" );

    $cres = $stmt2->fetch();
    if ( $cres )
    {
      $undispatchable += $cres[0];
      echo "<TD>".$cres[0]."</TD>";
    }
    echo "</TR>\n";
  }
  echo "<TR><TD COLSPAN=2 ALIGN=RIGHT>TOTAL:</TD><TD ALIGN=LEFT>$undispatchable</TD></TR>";
  echo "</TABLE>\n";
}

$stmt = $h->prepare( "SELECT DISTINCT a.identifier,a.unit_num,l.timestamp,l.result_code,l.result_message,a.message ".
          "from alarm_log a, alarm_dispatch_log l ".
          "WHERE a.id=l.alarm_id AND TO_DAYS(NOW())-TO_DAYS(a.timestamp) < 1 ORDER BY l.timestamp DESC" );
$stmt->execute( array() );
$results = $stmt->fetchAll();
if ( count($results) )
{
  echo "Dispatch results in the last 24 hours:<BR>";
  echo "<TABLE BORDER=1 CELLPADDING=3 CELLSPACING=0><TR><TH>Name</TH><TH>MAC / unit#</TH><TH>Dispatch Time</TH><TH>Dispatch Result</TH><TH>Alarm</TH></TR>\n";
  foreach ( $results as $row )
  {
    echo "<TR>";
    $stmt2 = $h->query("SELECT c.name as cname,u.name as uname from core_networkcontroller c, ".
                            "core_unit u where c.id=u.controller_id AND ".
                            "c.identifier='".$row['identifier']."' AND ".
                            "u.subunitid=".$row['unit_num']);
    $cname = "[not registered]";
    if ( $stmt2 && $nameres=$stmt2->fetch() )
    {
      $cname = $nameres['cname'];
      if ( $nameres['cname'] != $nameres['uname'] )
        $cname .= " (".$nameres['uname'].")";
    }
    echo "<TD>$cname</TD>";
    echo "<TD>".$row['identifier']." #".$row['unit_num']."</TD>";
    echo "<TD>".$row['timestamp']."</TD>\n";
    echo "<TD>".$row['result_message']." (".$row['result_code'].")</TD>";
    echo "<TD>".$row['message']."</TD>";
    echo "</TR>";
  }
  echo "</TABLE>\n";
}
echo "</BODY></HTML>";

?>
