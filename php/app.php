<?php // AjaxRequest 
error_reporting(E_ALL);
//ini_set( 'display_errors','1' );
  include_once "session.php";

  if ( $logged_in_as <= 0  && 
       !($_REQUEST['action'] === "auth" ) )
    die( "Not logged in!" );

function app_log( $txt )
{
  // keep it from showing passwords in the logs
  $txt = preg_replace( '/PW=[^&]+/','PW=**HIDDEN**',$txt );
  error_log( $txt, 3, "/var/log/apache2/app.log" );
}
  app_log( "REQUEST URI=".$_SERVER['REQUEST_URI'].", ".
                  "Agent=".$_SERVER['HTTP_USER_AGENT'].", ".
                  "IP=".$_SERVER['REMOTE_ADDR'].
                  "\n" );

// prevent caching of any of the responses from this file
  header("Cache-Control: no-cache,no-store,must-revalidate");
  header("Pragma: no-cache");
  header("Expires: 0");

function parse_csv( $csv )
{
    $lines = preg_split( "/[\r\n]+/", $csv ); // now it's split into lines, header is first
    $hdrs = explode( ",",$lines[0] );
    $data = array();
//    echo "CSV raw:\n$csv\n\n";
    $index = 0;
    for ( $i=1; $i<count($lines); $i++ )
    {
//      echo "RAW LINE: $lines[$i]\n";
      if ( $lines[$i] == "" )
      { continue; }
      $fields = explode(",",$lines[$i] );
//      echo count($fields).":".join("|",$fields )."\n";
      if ( count($hdrs) == count($fields) )
      {
        $row = array_combine($hdrs, $fields );
        $data[$index++] = $row;
      }
    }
    return $data;
}
  $ctx = stream_context_create(array(
    'http' => array(
        'timeout' => 10
        )
    )
  ); 
function file_contents( $path )
{
  global $ctx;
  try
  {
    $str = @file_get_contents( $path, 0, $ctx );
    if ( $str === FALSE )
    {
      error_log( print_r( "Failed to get URL contents '".$path."' :".get_headers($path), TRUE ) );
      return "Cannot get URL contents (1).";
    }
    return $str;
  } catch ( Exception $e )
  {
    error_log( print_r( "Exception getting URL contents '".$path."' ".$e->getMessage(), TRUE ) );
    return "Cannot get URL contents (2).";
  }
}
function get_url_base($unit)
{
  global $dbh;
  $stmt = $dbh->prepare( "Select dns.name FROM ".
                             "core_connection_status s,core_controller_servers dns WHERE ".
                             "s.connected is not null AND s.disconnected is null AND ".
                             "s.ip=dns.ip AND CONCAT(s.identifier,'_',s.unitnum)=:u" );
  if ( !$stmt->execute( array("u"=>$unit) ) )
    return "http://127.0.0.1:8077/";

  if ( !($ary = $stmt->fetch() ) )
    return "http://127.0.0.1:8077/";
  
  return "http://".$ary[0].":8077/";
}
  $url_base = "http://127.0.0.1:8077/";
  $params = "";
  if ( $logged_in_as > 0 )
  {
    $params.= "User=".$logged_in_as."&";
  }
  if ( isset($_SESSION["SelectedUnit"]) )
  {
    $params .= "Unit=".$_SESSION["SelectedUnit"]."&";
    $url_base = get_url_base( $_SESSION["SelectedUnit"] );
  } else if ( isset($_REQUEST["Unit"]) )
  {
    $params .= "Unit=".$_REQUEST["Unit"]."&";
    $url_base = get_url_base( $_REQUEST["Unit"] );
  }
  $ajax_base = $url_base."ajax.htm?".$params;
//echo $ajax_base;
  if ( !isset( $_REQUEST["action"] ) )
  {
    if ( isset( $_REQUEST["btn"]  ) )
    {
      header( "Content-Type: text/html; charset=ISO-8859-1" );
//error_log(print_r("Btn=".$_REQUEST['btn'],TRUE));
      $lcd_contents = file_contents( $ajax_base."btn=".$_REQUEST['btn'] );
      if ( !(substr($lcd_contents,0,5) === "<pre>" ) )
        error_log(print_r("Btn=".$_REQUEST['btn']." got bad LCD=".$lcd_contents,TRUE));
      echo $lcd_contents;
    } 
    else if ( isset($_REQUEST["touch"]) && $_REQUEST["touch"] )
    {
      header( "Content-Type: text/html; charset=ISO-8859-1" );
//error_log(print_r("touch=".$_REQUEST['touch'],TRUE));
      $lcd_contents = file_contents( $ajax_base."touch=".$_REQUEST['touch']."&row=".$_REQUEST['row']."&col=".$_REQUEST['col'] );
      echo $lcd_contents;
//error_log(print_r("LCD=".$lcd_contents,TRUE));
    } 
  } else
  {
    if ( $_REQUEST["action"] == "downloadscan" )
    {
      header( "Content-Type: application/octet-stream;charset=ISO-8859-1" );
      header( "Content-Disposition: attachment; filename=snapshot.txt" );
      echo file_contents( $ajax_base . "action=".$_REQUEST["action"] );
    } 
    else if ( $_REQUEST["action"] == "getscan" )
    {
      echo file_contents( $ajax_base . "action=".$_REQUEST["action"] );
    } 
    else if ( $_REQUEST["action"] == "startscan" )
    {
      echo file_contents( $ajax_base . "action=startscan".
            (isset($_REQUEST['header']) ? "&header=".$_REQUEST['header'] : "") );
    } 
    else if ( $_REQUEST["action"] == "refresh" )
    {
      header( "Content-Type: text/html; charset=ISO-8859-1" );
//header( "X-Used-Url: ".$ajax_base."action=refresh" );
      echo file_contents( $ajax_base."action=refresh" );
    } 
    else if ( $_REQUEST["action"] == "getdatalog" )
    {
      echo file_contents( $ajax_base . "action=".$_REQUEST["action"] );
    } 
    else if ( $_REQUEST["action"] == "startdatalog" )
    {
      echo file_contents( $ajax_base . "action=".$_REQUEST["action"] );
    } 
    else if ( $_REQUEST["action"] == "removeUnit" )
    {
//    echo $url_base . "removeUnit.htm?".$params."<BR>\n";
      echo file_contents( $url_base . "removeUnit.htm?".$params );
    
//    http_redirect( "${base_url}select.php", "", true, HTTP_REDIRECT_FOUND  );
    } 
    else if ( $_REQUEST["action"] == "downloaddatalog" )
    {
      header( "Content-Type: application/octet-stream;charset=us-ascii" );
      header( "Content-Disposition: attachment; filename=history.txt" );
      echo file_contents( $ajax_base . "action=".$_REQUEST["action"] );
    } 
    else if ( $_REQUEST["action"] == "auth" )
    {
      $pw = $_REQUEST['PW'];
      $un = $_REQUEST['UN'];
 // check if this is a real user, and if so, set their user ID in session
      header("Content-Type: application/json");
      $resp = "{ \"Login\": { ";
      if ( user_login( $un , $pw ) )
      {
        $logged_in_as = $_SESSION['UserId'];
        $resp .= "\"Result\":\"SUCCESS\",";
        $resp .= "\"PHPSESSID\":\"".session_id()."\"";
      } else 
      {
        $resp .= "\"Result\":\"FAILED\"";	
      }
      $resp .= "} }";
      echo $resp;
      app_log( "RESPONSE: $resp\n" );
      
    } 
    else if ( $_REQUEST["action"] == "getControllerList" )
    {
      // get list from database now,instead of HTTP request to ControllerServer
      global $dbh;
// first, clear this user from ownership of any units
      $stmt = $dbh->prepare( "UPDATE core_connection_status SET owner=0 WHERE owner=:owner" );
      $stmt->execute( array("owner"=>$logged_in_as ) );
      $stmt = $dbh->prepare( 
"Select DISTINCT s.identifier,s.unitnum,s.connected,s.owner,dns.name AS dnsname,s.flags,c.name ".
" FROM core_connection_status s,core_controller_servers dns,".
"(SELECT c.identifier,c.name ".
"FROM core_networkcontroller c,core_connection_status s ".
" WHERE c.identifier=s.identifier ".
"UNION ALL ".
"select DISTINCT s.identifier,'UNKNOWN' as name ".
"FROM core_networkcontroller c,core_connection_status s ".
" WHERE ".
"NOT EXISTS (Select * from core_networkcontroller c where c.identifier=s.identifier)) c ".
"WHERE s.connected is not null AND s.disconnected is null AND s.ip=dns.ip ".
"AND s.identifier=c.identifier" );
      $stmt->execute( array() );
      
    // clear the selected unit from this session
      unset( $_SESSION["SelectedUnit"] );

      header("Content-Type: application/json");
      echo "{ \"UnitList\": { \"Units\": [ \n";
    // go through the list and decide which ones should be visible to this user
      $rows = $stmt->fetchAll();
      $rowNum = 0;
      foreach ( $rows as $data )
      {
//      echo "<PRE>"; var_dump( $data[$i] ); echo "</PRE>";
        $cur_user = 0+$data["owner"];
        $mac = $data["identifier"];
        $unitnum = $data["unitnum"];
        $unitnumstr = $unitnum > 1 ? " #$unitnum" : "";
        $ip = $data["dnsname"];

        $id = $mac."_".$unitnum;
//        $unit_id = $data[$i]["UNITDBID"];
        $name = $data["name"];
        $flags = isset($data['flags']) ? $data['flags'] : -1;//isset( $data[$i]['FLAGS'] ) ? 0+$data[$i]["FLAGS"] : -1;
        $unknown = 0;//+$data[$i]["UNKNOWN"];
        // check access to this controller/unit by currently logged-in user
        if ( (!isset( $_SESSION['UserIsSuperuser']) || !$_SESSION['UserIsSuperuser']) &&
             !user_can_access_unit( $logged_in_as, $id ) 
           )
        {
          continue;
        }
        // see if we can find a name for this controller -- if not, it's unknown
        if ( $name === "UNKNOWN" ) $unknown = 1;
        if ( $rowNum ) echo ",\n";
        $rowNum++;
        echo "{ \"id\":\"$id\", \"name\":\"$name$unitnumstr\",".
             "\"owner\":\"$cur_user\",\"flags\":\"$flags\",".
             "\"connected_to\":\"$ip\" }";
      }
      echo "\n] } }";
    } 
    else if ( isset( $_REQUEST["action"] ) )
    {
      die("Invalid request for '".$_REQUEST["action"]."' by ".$logged_in_as );
    }
  } // end if set action
?>
