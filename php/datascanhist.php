<?php

global $debug;
$debug = isset($_REQUEST['debug']) && '1' == $_REQUEST['debug'];
if ( $debug )
{ 
error_reporting(E_ALL);
ini_set( 'display_errors','1' );
}

include_once "datalogutils.php";

  $require_login = 1;
  $no_menus = 1;
  $dl = isset( $_REQUEST['action'] ) && $_REQUEST["action"] == "Download";
  $dltype = isset( $_REQUEST['dltype']) ? $_REQUEST['dltype'] : "none";
  $clearhist = isset( $_REQUEST['action'] ) && $_REQUEST['action'] == "Delete Log";

  $start = isset( $_REQUEST['BEGIN'] ) ? $_REQUEST['BEGIN']:0;
  global $dbh;
  $end = isset( $_REQUEST['END'] ) ? $_REQUEST['END']:0;
  $def_start = $start;
  $oldest = 0;
  $def_end = $end;
  $db_start = 0;
  $db_end = 0;
  $matches = array();
  $ordering = isset($_REQUEST['order']) && $_REQUEST['order'] == '1' ? "DESC" : "ASC";
  if ( $start && preg_match( "/(\d+)\/(\d+)\/(\d+)/", $start, $matches ) )
    $db_start = sprintf( "%04d-%02d-%02d", 2000+$matches[3], $matches[1], $matches[2] );
  if ( $end && preg_match( "/(\d+)\/(\d+)\/(\d+)/", $end, $matches ) )
    $db_end = sprintf( "%04d-%02d-%02d", 2000+$matches[3], $matches[1], $matches[2] );
if ( $debug ) echo "Date range is $db_start to $db_end ($start - $end)<BR>\n";
  if ( isset( $_REQUEST["UID"] ) )
    $unit_id = $_REQUEST["UID"];
  if ( isset( $_REQUEST["Unit"] ) )
    $unit_code = $_REQUEST["Unit"];
  $params = array();
  $paramsa = array();
  $where = "";
  $awhere = "";
  if ( isset( $unit_id ) )
  {
    $params['uid'] = $unit_id;
    $paramsa['auid'] = $unit_id;
    $where = "u.id=:uid";
    $awhere = "u.id=:auid";
  } else if ( isset( $unit_code ) )
  {
    $parts = preg_split( "/_/", $unit_code );
    $where = " c.identifier=:mac AND u.subunitid=:unitnum ";
    $awhere = " c.identifier=:amac AND u.subunitid=:aunitnum ";
    $params['mac'] = $parts[0];
    $paramsa['amac'] = $parts[0];
    $params['unitnum'] = $parts[1];
    $paramsa['aunitnum'] = $parts[1];
  } else
    die( "Invalid parameters supplied!" );
  include_once "session.php";
  // first, need to find subunit's database row id and location for tz
  $tz = date_default_timezone_get();
  $stmt = $dbh->prepare( "select distinct u.id,l.tz from core_networkcontroller c, core_unit u, core_location l WHERE $where AND c.id=u.controller_id AND c.location_id=l.id" );
  if ( $stmt && $stmt->execute( $params ) )
  {
    $ary = $stmt->fetch(); 
    $unit_id = $ary[0];
    $tz = $ary[1];
    if ( isset( $params['mac']) )
    {
      // this will make select statements happen a lot faster if we don't have
      // to access controller and unit tables
      unset( $params['mac'] );
      unset( $paramsa['amac'] );
      unset( $params['unitnum'] );
      unset( $paramsa['aunitnum'] );
      $params['uid'] = $unit_id;
      $paramsa['auid'] = $unit_id;
      $where = "u.id=:uid";
      $awhere = "u.id=:auid";
    }  
  }
    $stmt = $dbh->prepare( "select c.name,cust.name ".
                           "FROM core_networkcontroller c, core_unit u, core_customer cust ".
                           " WHERE $where AND cust.id=c.customer_id AND u.controller_id=c.id" );
if ( $debug ) echo "-- DEBUG ON <BR>\n";
    $cont_name=$unit_code;
    $short_name="";
    if ( $stmt->execute( $params ) && ($results=$stmt->fetch()) )
    {
      $cont_name=$results[0]." ($unit_code) operated by ".$results[1];
      $short_name=$results[0];
    }
  if ( $dl )
  {
    $date = date('Y_m_d_H_i_s');
    $fileName = $short_name . '_' .$date;
    preg_replace( "/\s+/", "_", $fileName );


    if ( $dltype == "csv" )
    {
      header("Content-Type: application/vnd.ms-excel");
      header( "Content-Disposition: attachment; filename=$fileName".".csv" );
    } else
    {
      header( "Content-Type: application/octet-stream;charset=us-ascii" );
      header( "Content-Disposition: attachment; filename=$fileName".".txt" );
    }
  } else
  {
	include_once "graphcontent.php";
    include_once "header.php";
    if ( $clearhist )
    {
      $stmt = $dbh->prepare( "delete from core_unit_scans ".
       " WHERE unit_id=:uid ".
       " AND ts+0 >= (CONVERT_TZ(CONCAT(:start,' 00:00:00'),'$tz','UTC')+0) ".
       " AND ts+0 <= (CONVERT_TZ(CONCAT(:end,' 23:59:59'),'$tz','UTC')+0) " );
      $stmt->execute( array("uid"=>$unit_id,"start"=>$db_start,"end"=>$db_end ) );
      $stmt = $dbh->prepare( "delete from core_unit_scans_archive ".
       " WHERE unit_id=:uid ".
       " AND ts+0 >= (CONVERT_TZ(CONCAT(:start,' 00:00:00'),'$tz','UTC')+0) ".
       " AND ts+0 <= (CONVERT_TZ(CONCAT(:end,' 23:59:59'),'$tz','UTC')+0) " );
      $stmt->execute( array("uid"=>$unit_id,"start"=>$db_start,"end"=>$db_end ) );
      echo "<h2>DataScan History for $cont_name has been CLEARED between $start and $end</h2>\n";
      $start = 0;
    }
    echo "<h2>DataScan History for $cont_name</h2>\n";
if ( $debug) echo "(unit id = $unit_id)<BR>";
    {
      if ( !$def_start || !$def_end || !$oldest )
      {
       $sql = "select MIN(DATE(CONVERT_TZ(A.ts,'UTC','$tz'))),MAX(DATE(CONVERT_TZ(A.ts,'UTC','$tz'))) from (".
              "   SELECT s.ts from core_unit_scans s ".
//              "   SELECT s.ts from core_networkcontroller c, core_unit u, core_unit_scans s ".
//              " WHERE $where AND c.id=u.controller_id AND u.id=s.unit_id ".
              " WHERE s.unit_id=$unit_id ".
              "  UNION ".
              "   SELECT s.ts from core_unit_scans_archive s ".
//              "   SELECT s.ts from core_networkcontroller c, core_unit u, core_unit_scans_archive s ".
//              "  WHERE $awhere AND c.id=u.controller_id AND u.id=s.unit_id ".
              "  WHERE s.unit_id=$unit_id ".
              ") A;";
if ( $debug ) { echo "<PRE>SQL: $sql\nparams: "; var_dump(array_merge( $paramsa,$params)); echo " </PRE><BR>\n"; }
       $stmt = $dbh->prepare( $sql );
       if ( $stmt && $stmt->execute( array() ) && ($ary = $stmt->fetch() ) )
       {
if ( $debug ) { echo "<PRE>"; var_dump($ary); echo "\ndef_start=$def_start\ndef_end=$def_end</PRE><BR>\n"; }
        if ( preg_match( "/(\d+)-(\d+)-(\d+)/", $ary[0], $matches ) )
        {
          $oldest = sprintf( "%02d/%02d/%02d",0+$matches[2],0+$matches[3],$matches[1]-2000 );
          if ( !$def_start )
            $def_start = sprintf( "%02d/%02d/%02d",0+$matches[2],0+$matches[3],$matches[1]-2000 );
        }
        if ( !$def_end )
        {
          if ( preg_match( "/(\d+)-(\d+)-(\d+)/", $ary[1], $matches ) )
            $def_end = sprintf( "%02d/%02d/%02d",0+$matches[2],0+$matches[3],$matches[1]-2000 );
        }
if ( $debug) echo "<PRE>def_start=$def_start\ndef_end=$def_end</PRE>";
       }
      }
//echo "<FONT COLOR=\"#ff0000\">DATES STILL IN DEVELOPMENT!</FONT><BR>";
      if ( $def_start == 0 && $def_end == 0 )
      {
        echo "There is no scan data from this system saved on the server.<BR>";
      } else
      {
if ( $debug) echo "<PRE>def_start=$def_start\ndef_end=$def_end</PRE>";
        echo "<P>This page shows you the data scans that have been downloaded periodically ".
           "from the unit and stored in the server's database.<BR>  ".
           "The oldest record for this unit is dated $oldest.  Note all dates and times are displayed using the controller's time zone: <B>$tz</B>.</P>";
        echo "<FORM><INPUT TYPE=HIDDEN NAME=Unit VALUE=\"$unit_code\">";
        echo "Starting on <INPUT id='startdate' TYPE=TEXT MAXLENGTH=8 SIZE=9 NAME=BEGIN VALUE=\"$def_start\"> (MM/DD/YY)<BR>";
        echo "Ending on <INPUT id='enddate' TYPE=TEXT MAXLENGTH=8 SIZE=9 NAME=END VALUE=\"$def_end\"> (MM/DD/YY)<BR>";
        echo "Order: <SELECT NAME=order>".
             "<OPTION VALUE=0".($ordering=="ASC"?" SELECTED":"").">Chronological\n".
    "<OPTION VALUE=1".($ordering=="DESC"?" SELECTED":"").">Reverse chronological\n".
    "</SELECT><BR>\n";
        echo "<a target=\"_blank\"><input id='refreshBtn' type=\"button\" value=\"Display graph\"></input></a>";
         //echo "<input type=\"button\"><a href=\"graph.php?Unit=\".$unit_code.\"&BEGIN=\".$def_start.\"&END=\".$def_end.\"&order=0" target=\"_blank\">Display graph</a></input>";//A HREF=\"datascanhist.php?Unit=".$unit_code."&DL=1\">Download this data</A><BR>";
        echo "<INPUT TYPE=SUBMIT NAME=action VALUE=View> ";//A HREF=\"datascanhist.php?Unit=".$unit_code."&DL=1\">Download this data</A><BR>";
        echo "<SELECT NAME=dltype>\n".
"<OPTION VALUE='text'>Export Text\n".
    "<OPTION VALUE='csv'>Export CSV/Excel\n".
    "</SELECT>";

        echo "<INPUT TYPE=SUBMIT NAME=action VALUE=Download> ";//A HREF=\"datascanhist.php?Unit=".$unit_code."&DL=1\">Download this data</A><BR>";
        echo "<INPUT TYPE=SUBMIT NAME=action VALUE=\"Delete Log\" onclick=\"return confirm('Are you sure you want to delete the entries from this date range?');\"><BR>";//A HREF=\"datascanhist.php?Unit=".$unit_code."&Clear=1\" onclick=\"return confirm('Are you SURE you want to clear the server\'s stored data history?\\r\\n(This is the last opportunity you will have to view or download this data!)');\">Clear this data</A><BR>";
        echo "</FORM>";
      }
    }
    echo "<FONT SIZE=\"-2\">(<A HREF=\"javascript:window.close();\">close this page</A> when you're done with it)</FONT><br />\n<br />\n";
	  if ( !$dl ) { 
    echo "<div id='progress' style='display:none;'>";
    echo "<div class='preloader' style=''><img src='./img/ajax-loader.gif' tooltip='loading...'></div>";
    echo "</div>";
    echo "<div id='container1' style='display:none;height: 400px' class='col_80 boxBorder'></div>";
    echo "<div id='container2' style='display:none;height: 400px;margin-top:50px;' class='col_80 boxBorder'></div>";
	  }
  }
  if ( $start && $end )
  {
    $hdr = array();
    $all_data = get_unit_scans( $db_start, $db_end, $unit_id, $ordering, $tz, $hdr );
    if ( $dl )
    {
      $delim = ",";//$dltype == "csv" ? "\t" : ",";
      if ( count( $hdr ) )
      {
        echo join( $delim, $hdr )."\r\n";
      }
      foreach ( $all_data as $row )
      {
        echo join( $delim, $row)."\r\n";
      }
    } else
    {
      echo "<PRE><TABLE ID='viewData' CELLPADDING=0>";
      if ( isset($hdr) && count( $hdr ) > 0 )
      {
        echo "<TR>";
        foreach ( $hdr as $colh )
          echo "<TH ALIGN=RIGHT>$colh&nbsp;</TH>\n";
        echo "</TR>";
      }
      foreach ( $all_data as $row )
      {
        echo "<TR>";
        foreach ( $row as $col )
          echo "<TD ALIGN=RIGHT>$col&nbsp;</TD>\n";
        echo "</TR>";
      }
      echo "</TABLE></PRE>";
//      echo "<A HREF=datascanhist.php?Unit=".$unit_code."&DL=1>Click here to download</A><BR>";
    }
  } else
  {
    echo "Found no scan history in the database for this controller.<BR>";
  }
  //}


  if ( !$dl ) { 
    @include "footer.php"; 
  } ?>
