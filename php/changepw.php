<?php
  $require_login = 1;

  include "header.php";

  $user_to_change = $logged_in_as;
  if ( $is_superuser )
    $user_to_change = $_REQUEST["UID"];
  if ( ( $is_superuser || isset( $_REQUEST["OP"] ) )
       && isset( $_REQUEST["P"] ) && isset( $_REQUEST["PC"] ) )
  {
    if ( $_REQUEST['P'] == "" )
    {
      echo "New password was empty!<BR>";
    }
    else if ( $_REQUEST['PC'] == "" )
    {
      echo "Confirmation password was empty!<BR>";
    }
    else if ( $_REQUEST["P"] != $_REQUEST["PC"] )
    {
      echo "Confirmation password did not match!<BR>";
    } else if ( !$is_superuser && !user_check_password( $logged_in_as, $_REQUEST["OP"] ) )
    {
      echo "You did not enter the correct 'Old Password.' <BR>";
    } else if ( user_change_password( $user_to_change, $_REQUEST["P"]  ) )
    {
      echo "Password changed successfully!<BR>";
      if ( isset( $_REQUEST["returnToAdmin"] ) )
        echo "<SCRIPT LANGUAGE=JavaScript>window.location.replace('admin.php?tab=user&user=$user_to_change');</SCRIPT>";
    } else
    {
      echo "Unknown error changing password!<BR>";
    }
  }
?>
<H2>To change password, please enter the information below:</H2>
<TABLE>
<FORM METHOD=POST AUTOCOMPLETE=off>
<?php if ( $is_superuser ) 
{ ?><TR><TD ALIGN=RIGHT>User to Change:</TD><TD><SELECT NAME=UID><?php 
// list all users on the system so admin can select them to change their pw
    global $dbh;
    $stmt = $dbh->query( "SELECT id,first_name,last_name,username from auth_user ORDER BY last_name,first_name" );
    foreach ( $stmt as $entry )
    {
      if ( $user_to_change == $entry[0] )
        echo "<OPTION SELECTED ";
      else
        echo "<OPTION ";
      if ( !$entry[1] && !$entry[2] )
        echo "VALUE=\"".$entry[0]."\">UID ".$entry[0]." (".$entry[3].")\n";
      else
        echo "VALUE=\"".$entry[0]."\">".$entry[2].", ".$entry[1]." (".$entry[3].")\n";
    }



?></SELECT></TD></TR><?php } else { ?>
<TR><TD ALIGN=RIGHT>Old Password:</TD><TD><INPUT TYPE=PASSWORD NAME=OP></TD></TR>
<?php } ?>
<TR><TD ALIGN=RIGHT>New Password:</TD><TD><INPUT TYPE=PASSWORD NAME=P></TD></TR>
<TR><TD ALIGN=RIGHT>Confirm New Password:</TD><TD><INPUT TYPE=PASSWORD NAME=PC></TD></TR>
<TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
<TR><TD ALIGN=CENTER COLSPAN=2><INPUT TYPE=SUBMIT VALUE="Change Password"></TD></TR>
</FORM>
</TABLE>
<br>
<?php @include "footer.php"?>

