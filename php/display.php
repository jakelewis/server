<?php
  $head_script = "<SCRIPT LANGUAGE=\"JavaScript\"><!--\n".
                 "function doSetupForDisplayPage() {\n".
                 "  setLCDUpdate(5000);\n".
                 "  enableInactivityTimeout();\n".
                 //"//  window.onbeforeunload=befunload;\n".
                 "}\n".
                 //"function befunload() {\n".
                 //"  return \"This controller may be inaccessible to others if you don't logout first.  Are you sure?\"; }\n".
                 "--></SCRIPT>\n";
  $page_onload="doSetupForDisplayPage();";
  $require_login = 1;

  include_once "header.php"; // also includes session.php & dbconn.php

  $selectedUnit="";
  if ( isset($_REQUEST["Unit"] ) )
  {
    $_SESSION["SelectedUnit"] = $_REQUEST["Unit"];
    $selectedUnit = $_REQUEST["Unit"];
  } else
    $selectedUnit = $_SESSION["SelectedUnit"];
  if ( !$selectedUnit )
  {
    die("No unit selected");
  }
  if ( !user_can_access_unit( $logged_in_as, $selectedUnit ) )
    die("Invalid unit selected");
  $cid="";
  $cname="";
  $tzn="";
  $dbid = 0;
  global $dbh;
  $stmt = $dbh->prepare( "SELECT c.name,c.identifier,u.subunitid,l.tz,c.id ".
                         "FROM core_networkcontroller c,core_unit u,core_location l ".
                         "WHERE u.controller_id=c.id AND l.id=c.location_id ".
                         "AND CONCAT(c.identifier,'_',u.subunitid)=:unit " );
  if ( $stmt->execute(array("unit"=>$selectedUnit)) )
  {
    $entry = $stmt->fetch();
    $cname = $entry[0];
    $cid = $entry[1];
    $tzn = $entry[3];
    $dbid = $entry[4];
    if ( 1 != $entry[2] )
      $cid .= " #".$entry[2]; 
  }
  if ( $cname != "" )
  {
    echo "<FONT SIZE=\"+2\"><B>Remote Control of '$cname' ($cid)</B></FONT><BR>\n";
    if ( $tzn != "" )
    {
      echo "Began on ";
      $timestamp = time();
      $dt = new DateTime("now", new DateTimeZone($tzn)); //first argument "must" be a string
      $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
      echo $dt->format('m/d/Y')." at ".$dt->format('H:i:s');
      echo "<BR>\n";
    } else
    {
      if ( $_SESSION['UserIsSuperuser'] )
        echo "<FONT COLOR=\"#ff0000\">Timezone unknown for this controller.  <A HREF=\"https://chemcom.sbcontrol.com/admin.php?tab=cont&cont=$dbid\" TITLE=\"If you set the timezone, we can help keep the controller's clock accurate!\">Set it here.</A></FONT><BR>";
    }
  }

?>
<DIV ID="LogoutWarning"></DIV>
<TABLE><TR>
<TD WIDTH="400" HEIGHT="200"  ALIGN=LEFT CELLSPACING=1>
<SCRIPT LANGUAGE="JavaScript"><!--
insertLCD('<?= $selectedUnit ?>');
//--></SCRIPT>
</TD>
<TD VALIGN=CENTER ALIGN=LEFT>
<SCRIPT LANGUAGE="JavaScript"><!--
insertKeypad();
//--></SCRIPT>
</TD>
</TR></TABLE>
<?php @include "footer.php" ?>
