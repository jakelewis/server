<?php 

  $require_login = 1;
  include "session.php";

  $cid = $_REQUEST["cust"];
  if ( !isset($cid) ) die( "Invalid request" );

  // TODO: make it so a customer admin can edit their own info
  if ( !$is_superuser && $user_level < 2 ) die( "Superuser or admin rights required!" );

  if ( !$is_superuser && $cid != $user_customer )
    die( "You cannot access other customer info!" );
  $not_null = array( "name" => 1 );
  $fields = array( "name"=>"Name","address1"=>"Address 1","address2"=>"Address 2",
			"city"=>"City", "state"=>"State", "zip"=>"Zip",
			"phone"=>"Phone", "fax" =>"Fax",
			"contactname" => "Contact Name",
			"contactphone" => "Contact Phone",
			"contactemail" => "Contact Email"
                  );
// TODO: see if a form has been submitted, and if so, do the appropriate thing
  if ( isset( $_REQUEST["SUBMIT"] ) )
  {
     $errmsg;
     $retryparams = "";
     if ( $_REQUEST["SUBMIT"] == "ADD NEW" && $is_superuser )
     {
       
       echo "Performing ADD NEW<BR>" ;
       $fldlist = "";
       $vallist = "";
       $params = array( );
       foreach ( $fields as $nm=>$val )
       {
         if ( $fields_ro[$nm] ) continue;
         if ( ( !isset( $_REQUEST[$nm] )  || 
                "" === $_REQUEST[$nm] ) && $not_null[$nm]  ) 
	 {
            $errmsg .= "Need value for '".$fields[$nm]."'<BR>";
            continue;
         }
         if ( "" !== $_REQUEST[$nm] )
           $retryparams .= "&_".$nm."=".urlencode($_REQUEST[$nm]);
         if ( $fldlist != "" )
           $fldlist .= ", ";
         $fldlist .= $nm;
         if ( $vallist != "" )
           $vallist .= ", ";
         if ( "" == $_REQUEST[$nm] )
           $vallist .= "NULL";
         else
         {
           $vallist .= ":".$nm;
           if ( $fields_bool[$nm] )
             $params[$nm] = $_REQUEST[$nm] == "on" ? 1 : 0;
           else
             $params[$nm] = $_REQUEST[$nm];
         }
       }
//echo $vallist;
//var_dump( $params );
       if ( !isset( $errmsg) )
       {
         $stmt = $dbh->prepare( "INSERT INTO core_customer (".$fldlist.") ".
  		"VALUES (".$vallist.")" );
         $res = $stmt->execute( $params );
         // need to get ID of the customer we just added, and make it the current selection
         $stmt = $dbh->query( "SELECT @@IDENTITY" );
         $ary = $stmt->fetch();
         $cid = $ary[0];
       }
     }
     else if ( $_REQUEST["SUBMIT"] == "UPDATE" )
     {
       echo "Performing UPDATE<BR>" ;
       $vallist = "";
       $params = array( "id" => $_REQUEST['cust'] );
       foreach ( $fields as $nm=>$val )
       {
         if ( $fields_ro[$nm] ) continue;
         if ( !isset( $_REQUEST[$nm] ) ) continue;
         if ( $vallist != "" )
           $vallist .= ", ";
         if ( "" == $_REQUEST[$nm] )
           $vallist .= $nm.="=NULL";
         else
         {
           $vallist .= $nm."=:".$nm;
           if ( $fields_bool[$nm] )
             $params[$nm] = $_REQUEST[$nm] == "on" ? 1 : 0;
           else
             $params[$nm] = $_REQUEST[$nm];
         }
       }
//echo $vallist;
//var_dump( $params );
       $stmt = $dbh->prepare( "UPDATE core_customer SET ".$vallist." ".
		"WHERE id=:id" );
       $res = $stmt->execute( $params );
     } else if ( $_REQUEST["SUBMIT"] == "DELETE" && isset( $_REQUEST['cust']) 
            && $is_superuser )
     {
       echo "Performing DELETE " ;
       $params = array( "id" => $_REQUEST['cust'] );
       // TODO: needs to delete the customer and anything that points to it:
       //   For each customer unit:
       //    core_unit, core_networkcontroller, core_location, core_unit_notifications, core_unit_users
       $stmt = $dbh->prepare( "DELETE l.* ".
                              " FROM core_unit u, core_networkcontroller c, ".
                              "      core_location l ".
                              "WHERE u.controller_id=c.id AND c.customer_id=:id ".
                              "  AND c.location_id=l.id" );
       $stmt->execute( $params );
       $stmt = $dbh->prepare( "DELETE uu.* ".
                              " FROM core_unit u, core_networkcontroller c, ".
                              "      core_unit_users uu ".
                              "WHERE u.controller_id=c.id AND c.customer_id=:id ".
                              "  AND u.id=uu.unit_id " );
       $stmt->execute( $params );
       $stmt = $dbh->prepare( "DELETE u.*,c.* ".
                              " FROM core_unit u, core_networkcontroller c ".
                              "WHERE u.controller_id=c.id AND c.customer_id=:id " );
       $stmt->execute( $params );
       
       //   For each customer user:
       //    core_unit_users, auth_user_groups, core_useraccount, 
       //    auth_user_user_permission
       $stmt = $dbh->prepare( "DELETE up.* ".
                              "FROM auth_user u,core_useraccount ua, ".
                              "     auth_user_user_permissions up ".
                              "WHERE ua.customer_id=:id AND ua.user_ptr_id=u.id AND ".
                              "      up.user_id=u.id" );
       $stmt->execute( $params );
       $stmt = $dbh->prepare( "DELETE ug.* ".
                              "FROM auth_user u,core_useraccount ua, ".
                              "     auth_user_groups ug ".
                              "WHERE ua.customer_id=:id AND ua.user_ptr_id=u.id AND ".
                              "      ug.user_id=u.id" );
       $stmt->execute( $params );
       $stmt = $dbh->prepare( "DELETE uu.* ".
                              "FROM auth_user u,core_useraccount ua, ".
                              "     core_unit_users uu ".
                              "WHERE ua.customer_id=:id AND ua.user_ptr_id=u.id AND ".
                              "      uu.user_id=u.id" );
       $stmt->execute( $params );
       $stmt = $dbh->prepare( "DELETE u.*,ua.* ".
                              "FROM auth_user u,core_useraccount ua ".
                              "WHERE ua.customer_id=:id AND ua.user_ptr_id=u.id ");
       $stmt->execute( $params );

       $stmt = $dbh->prepare( "DELETE FROM core_customer WHERE id=:id " );
       $stmt->execute( $params );
     } else
       echo "Unknown operation!\n";

//     echo "<A HREF=\"admin.php?tab=cust&cust=$cid\">go here</A>";
     echo "<SCRIPT LANGUAGE=JavaScript>window.location.replace( \"admin.php?tab=cust&cust=$cid".
     (isset($errmsg) ? $retryparams."&err=".urlencode($errmsg) : "")."\");</SCRIPT>";
     exit();
  }
  global $dbh;
  $stmt = $dbh->prepare( "SELECT ".join(",",array_keys($fields))." FROM core_customer WHERE id=:id" );
  if ( !$stmt || !$stmt->execute( array( 'id' => $cid ) ) )
      return 0;

  $entry = $stmt->fetch();
  if ( -99 != $cid )
  {
    echo "<A HREF=\"admin.php?tab=user&cust=$cid\">Users...</A> ";
    echo "<A HREF=\"admin.php?tab=cont&cust=$cid\">Controllers...</A><BR>";
  }
  echo "<FORM METHOD=GET ACTION=\"get_cust.php\" NAME=\"custform\"><INPUT TYPE=HIDDEN NAME=cust VALUE=$cid>";
  echo "<TABLE>";
  foreach ( $fields as $nm=>$desc )
  {
    echo "<TR><TD ALIGN=RIGHT>$desc:</TD><TD ALIGN=LEFT><INPUT TYPE=TEXT SIZE=50 NAME=$nm VALUE='".(-99 == $cid ? $_REQUEST[$nm] : stripslashes($entry[$nm]))."'>";
    echo (isset($not_null[$nm]) ? " *":"")."</TD></TR>";
  }
  echo "<TR><TD ALIGN=CENTER COLSPAN=2>";
  if ( -99 == $cid )
    echo "<INPUT TYPE=SUBMIT NAME=SUBMIT VALUE=\"ADD NEW\">";
  else
  {
    echo "<INPUT TYPE=SUBMIT NAME=SUBMIT VALUE=UPDATE>";
    if ( $is_superuser )
      echo "<INPUT TYPE=SUBMIT NAME=SUBMIT VALUE=DELETE>";
  }
  echo "</TD></TR>";
  echo "</TABLE>";
  echo "</FORM>";
?>
