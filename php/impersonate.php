<?php

  $page_title = "Chemtrol Remote Monitoring Impersonate";

 // this is here so if an ajax call gets redirected to login page, 
 // JS code can detect this and correctly redirect to the login page
 include "header.php";

 if ( $is_superuser )
 {
   $new_uid = $_REQUEST['UID'];
   // set all the session variables that make this the current user
   user_impersonate( $new_uid );
 }
 // just go to the select page with a replace, so this disappears from history
 echo "<SCRIPT LANGUAGE=\"JavaScript\">window.location.replace( \"select.php\" );</SCRIPT>";

 @include "footer.php"?>

