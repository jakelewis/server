<?php 

$debug = 0;
if ( $debug )
{
error_reporting(E_ALL);
ini_set( 'display_errors','1' );
}

  $require_login = 1;
  include "session.php";
  include "timezone.php";

//error_reporting(E_ALL);
//ini_set( 'display_errors','1' );

  $cid = $_REQUEST["cont"];
  if ( !isset($cid) ) die( "Invalid request" );

  global $dbh;
  if ( preg_match( "/:/", $cid ) )
  {
    $stmt = $dbh->prepare( "SELECT id from core_networkcontroller WHERE identifier=:ident" );
    if ( $stmt->execute( array( "ident"=>$cid ) ) && ($ary = $stmt->fetch()) )
      $cid = $ary[0];
    else
      die( "Could not find controller $cid" );
  }
  // make it so a customer admin can edit their own info
  if ( !$is_superuser && $user_level < 2 ) die( "Superuser or admin rights required!" );

  $cnot_null = array( "name"=>1, "identifier" => 1 );
  $cdbfields = array( "name"=>"c.name","identifier"=>"c.identifier",
			"notes"=>"c.notes",
			"email"=>"c.email",
			"msisdn"=>"c.msisdn",
                        "ip"=>"c.ip",
                        "gateway"=>"c.gateway",
                        "dns"=>"c.dns" );
  $lnot_null = array( "locnotes" => 1, "locname" => 1, "loctz" => 1 );
  $ldbfields = array (  "locname"=>"l.name",
       			"locnotes"=>"l.notes",
      			"loclat"=>"l.latitude",
			"loclong"=>"l.longitude",
			"locaddr1"=>"l.address1",
			"locaddr2"=>"l.address2",
			"loccity"=>"l.city",
			"locstate"=>"l.state",
			"loczip"=>"l.zip",
			"loctz"=>"l.tz"
                  );
  $cfields = array( "name"=>"Name","identifier"=>"Identifier",
			"notes"=>"Notes",
			"email"=>"Email",
			"msisdn"=>"Msisdn",
			"ip"=>"IP Address",
			"gateway"=>"Gateway",
			"dns"=>"DNS Server" );
  $lfields = array(     "locname"=>"Location Nickname",
       			"locnotes"=>"Location notes",
      			"loclat"=>"Latitude",
			"loclong"=>"Longitude",
			"locaddr1"=>"Address 1",
			"locaddr2"=>"Address 2",
			"loccity"=>"City",
			"locstate"=>"State",
			"loczip"=>"Zip",
			"loctz"=>"Time Zone"
                  );

  // see if a form has been submitted, and if so, do the appropriate thingrams = "";
  $xparams="";
  foreach ( $_REQUEST as $n=>$v )
  {
    if ( substr( $n, 0,1) !== "_" ) continue;
    $xparams .= "&".substr( $n, 1 )."=".urlencode($v);
  }
  if ( isset( $_REQUEST["cust"] ) )
    $xparams .= "&cust=".$_REQUEST["cust"];

  if ( isset( $_REQUEST["SUBMIT"] ) 
       || isset( $_REQUEST["add_user_to_unit"]) 
       || isset( $_REQUEST["remove_user_from_unit"]) 
     )
  {
     $retryparams="";
     $errmsg;
     $_REQUEST["identifier"] = preg_replace( "/ /", "", $_REQUEST["identifier"]  );
     if ( $_REQUEST["SUBMIT"] == 'ADD NEW' )
     {
       echo "Performing ADD NEW<BR>" ;
       $lvallist = ""; $lfldlist ="";
       $cvallist = ""; $cfldlist ="";
       $cparams = array( "cust" => $_REQUEST["cust"] );
       $lparams = array( );
       if ( "" !== $_REQUEST["identifier"] ) // do a query to make sure unique
       {
         $stmt = $dbh->prepare("SELECT * from core_networkcontroller WHERE identifier=:id" );
         $stmt->execute( array( "id" => $_REQUEST["identifier"] ) );
         if ( $stmt->rowCount() > 0 )
         {
           $errmsg .= "The identifier is already in the database<BR>";
         }
       }
       foreach ( $cfields as $nm=>$val )
       {
         if ( ( !isset( $_REQUEST[$nm] ) ||
              "" === $_REQUEST[$nm] ) && $cnot_null[$nm] ) 
         {
           echo "Need a value for '".$cfields[$nm]."'<BR>";
           $errmsg .= "Need a value for '".$cfields[$nm]."'<BR>";
           continue;
         }
         if ( isset( $_REQUEST[$nm] ) && "" !== $_REQUEST[$nm])
           $retryparams .= "&_".$nm."=".urlencode($_REQUEST[$nm]);
         if ( $cfldlist != "" )
           $cfldlist .= ", ";
         $cfldlist .= substr($cdbfields[$nm],1+strpos($cdbfields[$nm],"."));
         if ( $cvallist != "" )
           $cvallist .= ", ";
         if ( "" == $_REQUEST[$nm] ) //empty field
           $cvallist .= "NULL";
         else
         {
           $cvallist .= ":".$nm;
           $cparams[$nm] = $_REQUEST[$nm];
         }
       }
       foreach ( $lfields as $nm=>$val )
       {
         if ( ( !isset( $_REQUEST[$nm] ) ||
              "" === $_REQUEST[$nm] ) && $lnot_null[$nm] ) 
         {
           echo "Need a value for '".$lfields[$nm]."'<BR>";
           $errmsg .= "Need a value for '".$lfields[$nm]."'<BR>";
           continue;
         }
         if ( isset( $_REQUEST[$nm] ) && "" !== $_REQUEST[$nm])
           $retryparams .= "&_".$nm."=".urlencode($_REQUEST[$nm]);
         if ( $lfldlist != "" )
           $lfldlist .= ", ";
         $lfldlist .= substr($ldbfields[$nm],1+strpos($ldbfields[$nm],"."));
         if ( $lvallist != "" )
           $lvallist .= ", ";
         if ( "" == $_REQUEST[$nm] ) //empty field
           $lvallist .= "NULL";
         else
         {
           $lvallist .= ":".$nm;
           $lparams[$nm] = $_REQUEST[$nm];
         }
       }
//echo $cvallist;
//var_dump( $cparams );
//var_dump( $lparams );
//die( "BLAH!" );

       if ( !isset( $errmsg ) || "" === $errmsg )
       {
         $stmt = $dbh->prepare( "INSERT INTO core_location ".
                  " (".$lfldlist.") ".
  		"VALUES (".$lvallist.")" );
         $res = $stmt->execute( $lparams );
         
         $stmt = $dbh->query( "SELECT @@IDENTITY" );
         $ary = $stmt->fetch();
         $lid = $ary[0];
  
         $stmt = $dbh->prepare( "INSERT INTO core_networkcontroller ".
                  " (".$cfldlist.",location_id,customer_id) ".
  		"VALUES (".$cvallist.",".$lid.",:cust)" );
         $res = $stmt->execute( $cparams );

         $stmt = $dbh->query( "SELECT @@IDENTITY" );
         $ary = $stmt->fetch();
         $cid = $ary[0];
         // add controller's main unit #1
         $stmt = $dbh->prepare( "INSERT INTO core_unit ".
                                "(controller_id,subunitid,name) ".
                                "VALUES(:cid,1,'main')" );
         $stmt->execute( array( "cid"=>$cid ) );
         $stmt = $dbh->query( "SELECT @@IDENTITY" );
         $ary = $stmt->fetch();
         $unitid = $ary[0];
         // if current user is not superuser, add them as a user of this unit
         if ( !$is_superuser )
         {
           $stmt = $dbh->prepare( "INSERT INTO core_unit_users ".
                                  "(unit_id,user_id) ".
                                  "VALUES (".$unitid.",".$logged_in_as.")" );
           $stmt->execute( array() );
         }
         $res = file_get_contents( "http://127.0.0.1:8077/ajax.htm?action=refreshController&Unit=".$_REQUEST["identifier"]."_*" );
       }
     } else if ( $_REQUEST["remove_user_from_unit"] == 'X' )
     {
       $userid = $_REQUEST['user_to_remove'];
       $stmt = $dbh->prepare( "DELETE FROM core_unit_users ".
                    "WHERE user_id=:userid and unit_id=:unitid" );
       $stmt->execute( array( "userid"=>$userid, "unitid"=>$cid ) );
     }
     else if ( $_REQUEST["add_user_to_unit"] == '+' )
     {
       $userid = $_REQUEST['user_to_add'];
       $stmt = $dbh->prepare( "INSERT INTO core_unit_users ".
                    "(user_id,unit_id) ".
                    "VALUES (:userid,:unitid)" );
       $stmt->execute( array( "userid"=>$userid, "unitid"=>$cid ) );
     }
     else if ( $_REQUEST["SUBMIT"] == "UPDATE" )
     {
       echo "Performing UPDATE<BR>" ;
       $cvallist = "";
       $lvallist = "";
       $cparams = array( "cid" => $_REQUEST['cont'] );
       $lparams = array( "cid" => $_REQUEST['cont'] );
       foreach ( $cfields as $nm=>$val )
       {
         if ( ( !isset( $_REQUEST[$nm] ) ||
              "" === $_REQUEST[$nm] ) && $cnot_null[$nm] ) 
         {
           echo "Need a value for '".$cfields[$nm]."'<BR>";
           $errmsg .= "Need a value for '".$cfields[$nm]."'<BR>";
           continue;
         }
         if ( !isset( $_REQUEST[$nm] ) ) continue;
         if ( $cvallist != "" )
           $cvallist .= ", ";
         if ( "" == $_REQUEST[$nm] ) //empty field
           $cvallist .= $cdbfields[$nm]."=NULL";
         else
         {
           $cvallist .= $cdbfields[$nm]."=:".$nm;
           $cparams[$nm] = $_REQUEST[$nm];
         }
       }
       $lfldlist = ""; $lvallist2 = "";
       foreach ( $lfields as $nm=>$val )
       {
         if ( ( !isset( $_REQUEST[$nm] ) ||
              "" === $_REQUEST[$nm] ) && $lnot_null[$nm] ) 
         {
           echo "Need a value for '".$lfields[$nm]."'<BR>";
           $errmsg .= "Need a value for '".$lfields[$nm]."'<BR>";
           continue;
         }
         if ( !isset( $_REQUEST[$nm] ) ) continue;
         if ( $lfldlist != "" )
           $lfldlist .= ", ";
         if ( $lvallist != "" )
           $lvallist .= ", ";
         if ( $lvallist2 != "" )
           $lvallist2 .= ", ";
         $lfldlist .= substr($ldbfields[$nm],1+strpos($ldbfields[$nm],"."));
         if ( "" == $_REQUEST[$nm] ) //empty field
         {
           $lvallist .= $ldbfields[$nm]."=NULL";
           $lvallist2 .= "NULL";
         } else
         {
           $lvallist .= $ldbfields[$nm]."=:".$nm;
           $lvallist2 .= ":".$nm;
           $lparams[$nm] = $_REQUEST[$nm];
         }
       }
//echo $vallist;
//var_dump( $params );
       if ( !isset( $errmsg ) || "" === $errmsg )
       {
         if ( isset( $_REQUEST['locid'] ) )
         {
           unset( $lparams['cid'] );
           $lparams['locid'] = $_REQUEST['locid'];
           $stmt = $dbh->prepare( "UPDATE core_location l ".
                  " SET ".$lvallist." ".
  		"WHERE l.id=:locid" );
           $res = $stmt->execute( $lparams );
         } else
         {
           $stmt = $dbh->prepare( "INSERT INTO core_location ".
                  " ($lfldlist) VALUES ( $lvallist2)" );
  //echo "$lfldlist => $lvallist2";
  //var_dump( $lparams );
           unset( $lparams["cid"] ); // not used here
           $res = $stmt->execute( $lparams );
           
           $stmt = $dbh->query( "SELECT @@IDENTITY" );
           $ary = $stmt->fetch();
           $cvallist .= ",location_id=".$ary[0];
         }
         $stmt = $dbh->prepare( "UPDATE core_networkcontroller c ".
                  " SET ".$cvallist." ".
  		"WHERE c.id=:cid" );
         $res = $stmt->execute( $cparams );
         $res = file_get_contents( "http://127.0.0.1:8077/ajax.htm?action=refreshController&Unit=".$_REQUEST["identifier"]."_*" );
       }
     } else if ( $_REQUEST["SUBMIT"] == "DELETE" && isset( $_REQUEST['cont']) )
     {
       echo "Performing DELETE " ;
       $params = array( "cid" => $_REQUEST['cont'] );
       // needs to delete the unit/controller and anything that points to it:
       //    core_unit_users, core_location, core_unit_notifications, 
       //    core_networkcontroller, core_unit
       $stmt = $dbh->prepare( "DELETE uu.* from core_unit_users uu, core_unit u ".
                              "WHERE uu.unit_id=u.id AND u.controller_id=:cid" );
       $stmt->execute( $params );
       $stmt = $dbh->prepare( "DELETE un.* from core_unit_notifications un, core_unit u ".
                              "WHERE un.unit_id=u.id AND u.controller_id=:cid" );
       $stmt->execute( $params );
       $stmt = $dbh->prepare( "DELETE u.* from core_unit u ".
                              "WHERE u.controller_id=:cid" );
       $stmt->execute( $params );
       $stmt = $dbh->prepare( "DELETE l.* ".
                              "FROM core_networkcontroller c," .
                              " core_location l ".
                              " WHERE l.id=c.location_id AND c.id=:cid" );
       $stmt->execute( $params );
       $stmt = $dbh->prepare( "DELETE c.* ".
                              "FROM core_networkcontroller c " .
                              " WHERE c.id=:cid" );
       $stmt->execute( $params );
       $res = file_get_contents( "http://127.0.0.1:8077/ajax.htm?action=refreshController&Unit=".$_REQUEST["identifier"]."_*" );
       $cid = -1; // we just removed the one this pointed to before
     } else
       echo "Unknown operation!\n";

//     echo "<A HREF=\"admin.php?tab=unit&unit=$cid".(isset($_REQUEST['cust']) ? "&cust=".$_REQUEST['cust']:"")."\">go here</A>";
     echo "<SCRIPT LANGUAGE=JavaScript>window.location.replace( \"admin.php?tab=cont&cont=$cid".
        (isset($_REQUEST['cust']) ? "&cust=".$_REQUEST['cust']:"").
        (isset($errmsg) ? $retryparams."&err=".urlencode($errmsg):"").
        "\");</SCRIPT>";
     exit();
  }
  global $dbh;
  $stmt = $dbh->prepare( "SELECT ".join(",",array_values($ldbfields)).
          " FROM core_networkcontroller c, core_location l ".
          " WHERE c.id=:id AND c.location_id=l.id" );
  if ( !$stmt || !$stmt->execute( array( 'id' => $cid ) ) )
      return 0;
  $lentry = $stmt->fetch();
  $stmt = $dbh->prepare( "SELECT ".join(",",array_values($cdbfields)).
          ", customer_id, location_id".
          " FROM core_networkcontroller c ".
          " WHERE c.id=:id " );
  if ( !$stmt || !$stmt->execute( array( 'id' => $cid ) ) )
      return 0;
  $centry = $stmt->fetch();
  if ( !$centry && -99 != $cid )
  {
    die( "Nothing found for $cid!" );
  }
  $custid = $centry["customer_id"];
  $stmt = $dbh->prepare( "SELECT name FROM core_customer WHERE id=:custid" );
  if ( !$stmt || !$stmt->execute( array( 'custid' => $custid ) ) )
      return 0;
  $cust_name = $stmt->fetch()[0];

  echo "<FORM METHOD=GET ACTION=\"get_cont.php\" NAME=\"contform\">".
       "<INPUT TYPE=HIDDEN NAME=cont VALUE=$cid>";
  if ( isset( $centry['location_id'] ) )
    echo "<INPUT TYPE=HIDDEN NAME=locid VALUE=".$centry['location_id'].">\n";
  if ( isset( $_REQUEST['cust'] ) )
    echo "<INPUT TYPE=HIDDEN NAME=cust VALUE=".$_REQUEST['cust'].">";
  echo "<A HREF=\"admin.php?tab=cust&cust=$custid\">Operated by $cust_name</A><BR>";
  echo "<TABLE>"; $index=0;
//var_dump( $_REQUEST );

  
  foreach ( $cfields as $nm=>$desc )
  {
    if ( $nm == "notes" || $nm == "locnotes" )
      echo "<TR><TD VALIGN=TOP ALIGN=RIGHT>$desc:</TD><TD ALIGN=LEFT><TEXTAREA COLS=37 ROWS=6 NAME=$nm>".(-99 == $cid ? $_REQUEST[$nm] : stripslashes($centry[$index++]))."</TEXTAREA>";
    else
      echo "<TR><TD ALIGN=RIGHT>$desc:</TD><TD ALIGN=LEFT><INPUT TYPE=TEXT SIZE=50 NAME=$nm VALUE=\"".(-99 == $cid ? $_REQUEST[$nm] : stripslashes($centry[$index++]))."\">";
    echo (isset($cnot_null[$nm]) ? " *":"")."</TD></TR>";
  }
  $index = 0;
  foreach ( $lfields as $nm=>$desc )
  {
    if ( $nm == "notes" || $nm == "locnotes" )
      echo "<TR><TD VALIGN=TOP ALIGN=RIGHT>$desc:</TD><TD ALIGN=LEFT><TEXTAREA COLS=37 ROWS=6 NAME=$nm>".(-99 == $cid ? $_REQUEST[$nm] : stripslashes($lentry[$index++]))."</TEXTAREA>";
    else if ( $nm == "loctz" )
    {
      echo "<TR><TD VALIGN=TOP ALIGN=RIGHT>$desc:</TD><TD ALIGN=LEFT>";
      emit_timezone_select($nm,(-99 == $cid ? $_REQUEST[$nm] : stripslashes($lentry[$index++])) );
    }
    else
      echo "<TR><TD ALIGN=RIGHT>$desc:</TD><TD ALIGN=LEFT><INPUT TYPE=TEXT SIZE=50 NAME=$nm VALUE=\"".(-99 == $cid ? $_REQUEST[$nm] : stripslashes($lentry[$index++]))."\">";
    echo (isset($lnot_null[$nm]) ? " *":"")."</TD></TR>";
  }
  echo "<TR><TD ALIGN=CENTER COLSPAN=2>";
  if ( -99 == $cid )
    echo "<INPUT TYPE=SUBMIT NAME=SUBMIT VALUE=\"ADD NEW\">";
  else
  { 
    echo "<INPUT TYPE=SUBMIT NAME=SUBMIT VALUE=UPDATE>";
    echo "<INPUT TYPE=SUBMIT NAME=SUBMIT TITLE=\"Delete this controller, all subunits, location info, and user access\" VALUE=DELETE>";
  }
  echo "</TD></TR>";
  echo "</TABLE>";
  echo "</FORM>";

 if ( $cid > 0 ) // valid controller ID?
 {
  $pstmt = "";
  $params = array("cid"=>$cid);
  $pstmt = $dbh->prepare( "SELECT u.id,u.subunitid,u.name FROM core_unit u WHERE u.controller_id=:cid" );
  echo "<H3>Subunit : ";
  $stmt = $pstmt->execute( $params );

  echo "<SELECT NAME=unit onChange='UpdateSpan(\"UnitInfo\",\"get_unit.php?cont=$cid&unit=\"+this.value+\"$xparams\");'>";
  $rows = $pstmt->fetchAll();
  $selId;
  if ( isset( $_REQUEST['unit'] ) )
    $selId = $_REQUEST['unit'];
  else if ( count($rows)>=1  )
    $selId = $rows[0][0];
  else
  {
    $selId = -99;
//    echo "<OPTION VALUE=-1 selected>Select Subunit here...\n";
  }
  foreach ( $rows as $entry )
  {
    echo "<OPTION VALUE=\"".$entry[0],"\"".($selId == $entry[0] ? " SELECTED" : "").">#$entry[1] ($entry[2] $entry[0])\n";
  }
  echo "<OPTION VALUE=-99 ".($selId==-99?"SELECTED":"").">===> Add New Subunit...\n";
  echo "</SELECT></H3>";
  echo "<SPAN ID=UnitInfo></SPAN>";
  if ( -1 != $selId )
  {
    // this doesn't work, probably because the JS request tries to happen at the same time as the controller update.  Probably means there's shared global affecting it somehow...
    echo "<SCRIPT>UpdateSpan(\"UnitInfo\",\"get_unit.php?cont=$cid&unit=$selId$xparams\" );</SCRIPT>\n";
  }

 }
?>
