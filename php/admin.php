<?php
  $require_login = 1;
  include_once "header.php";

  if ( !$is_superuser && $user_level < 2 ) die( "This page requires superuser or admin access!" );

  global $dbh;

  $admin_tabs = array( );
  $def_tab = "unit";
  $admin_tabs["cust"]   = "Customer";
  $def_tab = "cust";
  $admin_tabs["cont"] = "Controllers";
  $admin_tabs["user"] = "Users";
  if ( $is_superuser )
    $admin_tabs["cstate"] = "Controller Status";
  $tab = isset($_REQUEST['tab']) ? $_REQUEST['tab'] : $def_tab;
  echo "<H2>Admin menu:</H2><TABLE CELLSPACING=10><TR>";
  foreach ( $admin_tabs as $nm => $desc)
  {
    if ( $tab == $nm )
      echo "<TD ALIGN=CENTER BGCOLOR=\"#0096fa\"><FONT COLOR=\"#f0f000\"><B>$desc</B></FONT></TD>";
    else
      echo "<TD ALIGN=CENTER><A HREF=\"admin.php?tab=$nm\">$desc</A></TD>";
  }
  echo "</TR></TABLE>";

  if ( isset( $_REQUEST['err'] ) )
    echo "<P><FONT SIZE=\"+2\" COLOR=\"#ff0000\">".$_REQUEST['err']."</FONT></P>";
  $xparams = "";
  foreach ( $_REQUEST as $n=>$v )
  {
    if ( substr( $n, 0,1) !== "_" ) continue;
    $xparams .= "&".substr( $n, 1 )."=".urlencode($v);
  }
  if ( $tab == "cstate" )
    echo file_get_contents( "http://127.0.0.1:8077/ajax.htm?action=AdminPage" );
  else if ( $tab == "cont" )
  {
    $pstmt = "";
    $params = array();
    if ( !$is_superuser )
      $_REQUEST['cust'] = $user_customer;
    if ( !isset($_REQUEST["sort"]) )
      $_REQUEST["sort"] = "name";
    echo "<FONT SIZE=+2 STYLE=\"font-style:bold;\">";
    if ( isset( $_REQUEST['cust'] ) )
    {
      $params['cid']=$_REQUEST['cust'];
      $xparams .= "&cust=".$_REQUEST['cust'];
      $pstmt = $dbh->prepare( "SELECT name from core_customer where id=:cid" );
      $stmt = $pstmt->execute( $params );
      foreach ( $pstmt as $entry )
      {
        $cust_name = $entry[0];
        echo $entry[0]." Controller: ";
      }
      $pstmt = $dbh->prepare( "SELECT c.id,c.name,c.identifier FROM core_networkcontroller c WHERE c.customer_id=:cid ORDER BY c.".$_REQUEST["sort"] );
    }
    else
    {
      $pstmt = $dbh->prepare( "SELECT c.id,c.name,c.identifier FROM core_networkcontroller c ORDER BY c.".$_REQUEST["sort"] );
      echo "Controller: ";
    }
    $stmt = $pstmt->execute( $params );
    echo "<SELECT NAME=cont onChange='UpdateSpan(\"ContInfo\",\"get_cont.php?cont=\"+this.value+\"$xparams\");'>";
    $rows = $pstmt->fetchAll();
    $selId;
    if ( isset( $_REQUEST['cont'] ) )
    {
      $selId = $_REQUEST['cont'];
      if ( preg_match( "/:/", $selId ) )
      {
        $stmt = $dbh->prepare( "SELECT id from core_networkcontroller WHERE identifier=:ident" );
        if ( $stmt->execute( array( "ident"=>$selId ) ) && ($ary = $stmt->fetch()) )
          $selId = $ary[0];
        else
          die( "Could not find controller $selId" );
      }

    } else if ( count($rows)==1  )
      $selId = $rows[0][0];
    else
    {
      $selId = -1;
      echo "<OPTION VALUE=-1 selected>Select Controller here...\n";
    }
    foreach ( $rows as $entry )
    {
      echo "<OPTION VALUE=\"".$entry[0],"\"".($selId == $entry[0] ? " SELECTED" : "").">$entry[1] ($entry[2])\n";
    }
    if ( isset( $_REQUEST['cust'] ) )
      echo "<OPTION VALUE=-99 ".($selId==-99?"SELECTED":"").">===> Add New Controller...\n";
    echo "</SELECT></FONT>";
    echo " (Sort by ";
    if ( $_REQUEST["sort"] == "name" )
      echo "<B>Name</B> ";
    else
      echo "<A HREF=\"admin.php?tab=cont&sort=name\">Name</A> ";
    if ( $_REQUEST["sort"] == "identifier" )
      echo "<B>MAC</B>)<BR>";
    else
      echo "<A HREF=\"admin.php?tab=cont&sort=identifier\">MAC</A>)<BR>";
    if ( isset( $cust_name ) )
    {
      echo "<A HREF=\"admin.php?tab=user&cust=".$_REQUEST["cust"]."\">$cust_name Users...</A><BR>";
    }
    echo "<SPAN ID=ContInfo></SPAN>";
    if ( -1 != $selId )
    {
      if ( isset( $_REQUEST["unit"] ) )
        $xparams .= "&unit=".$_REQUEST["unit"];
      echo "<SCRIPT LANGUAGE=JavaScript>UpdateSpan(\"ContInfo\",\"get_cont.php?cont=".$selId.$xparams."\" );</SCRIPT>\n";
    }
  }
  else if ( $tab == "user" )
  {
    $pstmt = "";
    $params = array();
    if ( !$is_superuser )
      $_REQUEST['cust'] = $user_customer;
    if ( !isset($_REQUEST["sort"]) )
      $_REQUEST["sort"] = "last_name";
    echo "<FONT SIZE=+2 STYLE=\"font-style:bold\">";
    if ( isset( $_REQUEST['cust'] ) )
    {
      $params['cid']=$_REQUEST['cust'];
      $xparams .= "&cust=".$_REQUEST['cust'];
      $pstmt = $dbh->prepare( "SELECT name from core_customer where id=:cid" );
      $stmt = $pstmt->execute( $params );
      foreach ( $pstmt as $entry )
      {
        $cust_name = $entry[0];
        echo $entry[0]." User: ";
      }
      $pstmt = $dbh->prepare( "SELECT id,last_name,first_name,username,is_superuser FROM auth_user u,core_useraccount ua WHERE u.id=ua.user_ptr_id AND ua.customer_id=:cid ORDER BY ".$_REQUEST["sort"] );
    }
    else
    {
      $pstmt = $dbh->prepare( "SELECT id,last_name,first_name,username,is_superuser FROM auth_user ORDER BY ".$_REQUEST["sort"] );
      echo "User: ";
    }
    $stmt = $pstmt->execute( $params );
    $rows = $pstmt->fetchAll();
    $selId =-1;
    echo "<SELECT NAME=user onChange='UpdateSpan(\"UserInfo\",\"get_user.php?user=\"+this.value+\"$xparams\");'>";
    if ( isset( $_REQUEST['user'] ) )
      $selId = $_REQUEST['user'];
    else if ( count($rows)==1 )
      $selId = $rows[0][0];
    else
    {
      echo "<OPTION VALUE=-1 selected>Select User here...\n";
      $selId = -1;
    }
    $selIdx = 0;
    foreach ( $rows as $entry )
    {
      $nm = ($entry[1] && $entry[2] ) ? $entry[1].", ".$entry[2].", " : "";
      if ( $entry[4] ) $nm .= "(SUPERUSER), ";
      echo "<OPTION VALUE=\"".$entry[0],"\"".($selId == $entry[0] ? " SELECTED" : "").">".$nm."user_id=".$entry[0].", username=".$entry[3]."\n";
      $selIdx++;
    }

    if ( isset( $_REQUEST['cust'] ) )
      echo "<OPTION VALUE=-99 ".($selId==-99?"SELECTED":"").">===> Add New User...\n";
    echo "</SELECT></FONT>";
    echo " (Sort by ";
    if ( $_REQUEST["sort"] == "last_name" )
      echo "<B>Last name</B> ";
    else
      echo "<A HREF=\"admin.php?tab=user&sort=last_name\">Last name</A> ";
    if ( $_REQUEST["sort"] == "username" )
      echo "<B>Username</B>)<BR>";
    else
      echo "<A HREF=\"admin.php?tab=user&sort=username\">Username</A>)<BR>";
//    echo "<FONT COLOR=\"#c02020\">Note: Changes are NOT RECOVERABLE!</FONT><BR>";
    if ( isset( $cust_name ) )
    {
      echo "<A HREF=\"admin.php?tab=cont&cust=".$_REQUEST["cust"]."\">$cust_name Controllers...</A><BR>";
    }
    echo "<SPAN ID=UserInfo></SPAN>";
    if ( $selId != -1 )
      echo "<SCRIPT LANGUAGE=JavaScript>UpdateSpan(\"UserInfo\",\"get_user.php?user=".$selId.$xparams."\" );</SCRIPT>\n";
  }
  else if ( $tab == "cust" )
  {
    echo "<H3>Customer: ";
    $stmt = $dbh->query( "SELECT id,name,contactname FROM core_customer ORDER BY name" );
    $cid = isset( $_REQUEST["cust"] ) ? $_REQUEST["cust"] : -1;
    if ( !$is_superuser )
      $cid = $user_customer;
    else
    {
      echo "<SELECT NAME=cust onChange='UpdateSpan(\"CustomerInfo\",\"get_cust.php?cust=\"+this.value);'>";
      if ( -1 == $cid )
        echo "<OPTION VALUE=-1 ".((-1 == $cid) ? "selected":"").">Select Customer here...\n";
    }
    foreach ( $stmt as $entry )
    {
      if ( !$is_superuser )
      {
        if ( $cid != $entry[0] )
          continue;
        echo "$entry[1]";
      } else
        echo "<OPTION VALUE=\"$entry[0]\" ".($cid==$entry[0]?"selected":"").">$entry[1] ($entry[2])\n";
    }
    if ( $is_superuser )
    {
      echo "<OPTION VALUE=-99 ".($cid==-99?"SELECTED":"").">===> Add New Customer...\n";
      echo "</SELECT>";
    }
    echo "</H3><SPAN ID=CustomerInfo></SPAN>";
    if ( -1 != $cid )
      echo "<SCRIPT LANGUAGE=JavaScript>UpdateSpan(\"CustomerInfo\",\"get_cust.php?cust=$cid$xparams\");</SCRIPT>";
  }

  include "footer.php";
?>


