<?php

function parse_header( $hdr )
{
  $out = array();
  $cols = preg_split( "/\s+/",$hdr );
  foreach ( $cols as $col )
  {
    if ( $col === "" ) continue;
    if ($col ==="Tot" )
      $out[count($out)-1] .= " Tot";
    else
      array_push( $out, $col );
  }
  if ( !count($out) ) return NULL;
  return $out;
}

function make_date_from_ts($in,$ts)
{
  if ( !preg_match( '/(\\d+)\-(\\d+)\-([\\d\\*]+)/', $ts, $matches ) )
  { //echo "TS='$ts'<BR>\n";
    $out = $in;
  } else
    $out = sprintf( "%02d/%02d/%02d", $matches[2]+0, $matches[3]+0, $matches[1]-2000 );
  return $out;
}

function make_time_from_ts($in,$ts)
{
  if ( !preg_match( '/(\\d+)\:(\\d+)\:([\\d\\*]+)/', $ts, $matches ) )
  { //echo "TS='$ts'<BR>\n";
    $out = $in;
  } else
    $out = sprintf( "%02d:%02d", $matches[1]+0, $matches[2]+0 );
global $debug;
if ( $debug ) $out .= " ($in)";

  return $out;//." ($in)";
}

function parse_row( $row,$ts )
{
  $out = array();
  $cols = preg_split( "/\s+/",$row );
  foreach ( $cols as $col )
  {
    if ( $col === "" ) continue;
    if ( preg_match( '/\d+\/\d+\/[\*\d]+/', $col ) ) // a date, need to replace
      $col = make_date_from_ts($col,$ts);
    else if ( preg_match( '/\d+\:\d+/', $col ) ) // a time, need to replace
      $col = make_time_from_ts($col,$ts);
    array_push( $out, $col );
  }
  if ( !count($out) ) return NULL;
  return $out;
}

function get_unit_scans( $start, $end, $unit_id, $ordering, $tz, &$hdr )
{
  $params = array();
  $all_data = array();
  global $dbh;
  $params['start'] = $start;
  $params['astart'] = $start;
  $params['end'] = $end;
  $params['aend'] = $end;
  $params['uid'] = $unit_id;
  $params['auid'] = $unit_id;
  $sql = "select A.scandata,CONVERT_TZ(A.ts,'UTC','$tz') from ( ".
         "    select s.scandata,s.ts from core_unit_scans s WHERE s.unit_id=:uid ".
     "   AND (s.ts+0) >= CONVERT_TZ(CONCAT(:start,' 00:00:00'),'$tz','UTC')+0 ".
     "   AND (s.ts+0) <= CONVERT_TZ(CONCAT(:end,' 23:59:59'),'$tz','UTC')+0 ".
         "  UNION  ".
         "    select s.scandata,s.ts from core_unit_scans_archive s WHERE s.unit_id=:auid ".
    "   AND (s.ts+0) >= CONVERT_TZ(CONCAT(:astart,' 00:00:00'),'$tz','UTC')+0 ".
    "   AND (s.ts+0) <= CONVERT_TZ(CONCAT(:aend,' 23:59:59'),'$tz','UTC')+0 ".
         ") A ORDER BY A.ts $ordering";

  $stmt = $dbh->prepare($sql);
  if ( $stmt && $stmt->execute( $params ) )
  {
    $all_rows = $stmt->fetchAll();
    $hdr = array();
    $all_data = array();
    foreach ( $all_rows as $entry )
    {
      $hdr_and_line = array();
      $hasHdr = preg_match( '/([^\\n\\r]+)[\\r\\n]+(.*+)/', $entry[0],$hdr_and_line );
      $thisHdr = $hasHdr ? parse_header($hdr_and_line[1]) : NULL;
      $thisRow = $hasHdr ? parse_row($hdr_and_line[2],$entry[1]) : parse_row($entry[0],$entry[1]);

      if ( count ( $thisRow ) )
        array_push( $all_data, $thisRow );
      if ( !count($hdr) && count ( $thisHdr ) )
        $hdr = $thisHdr;
    }

    return $all_data;
  }
  return NULL;
}


?>
