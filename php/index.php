<?php

//error_reporting(E_ALL);
//ini_set( 'display_errors','1' );

  $page_title = "Chemtrol Remote Monitoring Login";

 // this is here so if an ajax call gets redirected to login page, 
 // JS code can detect this and correctly redirect to the login page
 header( "TIMEOUT: 1" );
 $logging_in = 1;
 include "header.php";

 $dest = isset( $_REQUEST['dest'] ) ? $_REQUEST['dest'] : "select.php";
 if ( $logged_in_as > 0 )
 {
   echo "<SCRIPT LANGUAGE=\"JavaScript\">window.location.replace( \"".$dest."\" );</SCRIPT>";
 }
?>

<H2>Please enter your login information below</H2>

<TABLE>
<FORM ACTION="<?= $base_url_secure ?>index.php" METHOD="POST">
<?php
  if ( isset( $_REQUEST['dest'] ) )
    echo "<INPUT TYPE=HIDDEN NAME=dest VALUE=\"".$dest."\">\n";
?>
<TR><TD>Username:</TD><TD><INPUT TYPE=TEXT NAME=UN></TD></TR>
<TR><TD>Password:</TD><TD><INPUT TYPE=PASSWORD NAME=PW></TD></TR>
<TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
<TR><TD ALIGN=CENTER COLSPAN=2><INPUT TYPE=SUBMIT VALUE="LOGIN"></TD></TR>
</FORM>
</TABLE>
<BR>
<BR>
<!-- New Customers: <A HREF="setupController.php">Click here to register your controller</A>
<BR> -->
<BR>

<?php @include "footer.php"?>

