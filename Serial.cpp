#ifndef _included_Serial_cpp_
#define _included_Serial_cpp_

#include "predef.h"
#include <stdio.h>
#include <errno.h>
#include <termios.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include "Serial.h"

Serial::Serial( const char *fn, int baud)
{
  int fd = open( fn, O_RDWR|O_NOCTTY|O_NONBLOCK|O_NDELAY );
  while ( fd >= 0 )
  {
    int res;
    if ( (res = flock( fd, LOCK_NB|LOCK_EX ) ) )
    {
      close(fd);
      printf( "Cannot lock serial port: %d (%s)\n", res, strerror(errno) );
      break;
    }
    //fcntl(fd,F_SETFL, FNDELAY ); // don't block serial read
    
    struct termios tios;
    tcgetattr( fd, &tios );
    memset( tios.c_cc, 0, sizeof(tios.c_cc) ); // clear out any special characters
    cfsetispeed( &tios, baud );//baud );
    cfsetospeed( &tios, baud ); //baud );
    cfmakeraw( &tios );
    tios.c_cflag = baud|CS8|CLOCAL|CREAD; // baud, 8n1, no modem control, enable receiving chars
    tios.c_iflag = IGNBRK;
    tios.c_oflag = 0; // raw output
    tios.c_lflag = 0; // no echo, no signals to calling program
  //printf("tcsetattr\n" );
    tcflush( fd, TCIOFLUSH );
    tcsetattr( fd, TCSANOW, &tios );
    //tcflow( fd, TCOON );

    SetFD(fd);
    break;
  }
}
  
Serial::~Serial()
{
  // will be closed by Stream object
printf( "Serial port closing\n" );
  int fd = GetFD();
  if ( -1 != fd )
    flock( fd, LOCK_UN ); // release the lock
  Close();
}

// not valid for a listening socket
int Serial::Read( void *data, int nBytes )
{
  int fd = GetFD();
  int retries = 100;
  ssize_t res;
  if ( fd <= 0 ) return -1;
  do
  {
//printf( "Serial reading from %d\n", fd );
    res = read( fd, data, nBytes );
    if ( res >= 0 ) return res; // no error
  } while ( errno == EAGAIN && --retries );
  return -errno;
}
int Serial::Write( const void *data, int nBytes )
{
  int fd = GetFD();
  ssize_t res;
  if ( fd <= 0 ) return -1;
//printf( "Serial writing to %d\n", fd );
  res = write( fd, data, nBytes );
  if ( res < 0 ) return -errno;
  return res;
}
// how many bytes are available to be read
int Serial::DataReadable()
{
  int ret;
  int nReady;
  int fd = GetFD();
  if ( fd <= 0 ) 
  {
    printf( "[Serial::DataReadable] has no fd\n" );
    return -1;
  }
  ret = ioctl( fd, TIOCINQ, &nReady );
  if ( ret >= 0 )
  {
//    printf( "[Serial::DataReadable] returning %d for %d\n", nReady, fd );
    return nReady;
  }
  printf( "[Serial::DataReadable] error %d (%s)\n", errno, strerror(errno) );
  return -errno;
}

#endif //ndef _included_Serial_cpp_
