#ifndef _included_predef_h_
#define _included_predef_h_

#ifdef _WIN32

// Windows support
#define _CRT_SECURE_NO_WARNINGS
#define _WINSOCKAPI_
#define OSTimeDly Sleep
#define iprintf printf
#define IPADDR DWORD
#define AsciiToIp inet_addr
#define IS_VALID_SOCKET(s) ((s) != INVALID_SOCKET)
#include <windows.h>
#include <winsock2.h>
#ifdef _DEBUG
#define assert(x) if ( !(x) ) __asm int 3
#else
#define assert(x)
#endif
#define THREADHANDLE HANDLE
#define THREADRETURN DWORD
#define THREADCALLTYPE WINAPI

#define ADDR_FROM_SOCKADDR(sa) sa.sin_addr.S_un.S_addr
#define socklen_t int

#define OS_SLASH_STR "\\"

#define IS_INVALID_SOCKET(s) (INVALID_SOCKET == (s))

#else

#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <time.h> // for nanosleep
#include <sys/time.h>

// should this be CLOCK_REALTIME?
#define GTOD_CLOCK CLOCK_MONOTONIC

// linux-like system
#define OSTimeDly(x) { struct timespec ts={((x)/1000L),((x)%1000L)*1000000L }; nanosleep(&ts,NULL); }
#define iprintf printf
#define IPADDR unsigned long
// defined in sqltypes.h: typedef unsigned long DWORD;
#define WINAPI 
#define AsciiToIp inet_addr
#define IS_VALID_SOCKET(s) ((s) > 0 )
#define SOCKET int
#define closesocket close
#ifdef _DEBUG
#define assert(x) // if ( !(x) ) __asm int 3
#else
#define assert(x)
#endif
#define THREADHANDLE pthread_t
#define THREADRETURN void *
#define THREADCALLTYPE
typedef struct sockaddr_in SOCKADDR_IN;
#define GetLastError() errno
#define WSAGetLastError() errno
#define WSAEWOULDBLOCK EAGAIN
#define INVALID_SOCKET -1
#define IS_INVALID_SOCKET(s) ((signed long)(s)<0)

#define ADDR_FROM_SOCKADDR(sa) sa.sin_addr.s_addr

#define _strnicmp strncasecmp
#define _stricmp strcasecmp

#define MAX_PATH 300

#define ioctlsocket ioctl

#define OS_SLASH_STR "/"

#endif

//#define _NO_DBCONN

// a way to track what function the program is in... without a debugger attached

//#define TRACK_FUNCTION_CALLS
#ifdef TRACK_FUNCTION_CALLS
#define INSTRUMENT_THIS_FUNCTION ThisFunction here( __FUNCTION__, __FILE__, __LINE__ );
#include <vector>
#include <string>
class ThisFunction
{
  std::vector< ThisFunction* > *m_pFunctionStack;
  std::string m_fcn,m_fn;
  int m_line;
  void LogCurrentPosition()
  {
    // stash the info from the back of the vector in a file, replacing it each time
    ThisFunction *bk = m_pFunctionStack->back();
    FILE *fp = fopen( "chemtrol_server.function", "w" );
    if ( fp )
    {
      fprintf( fp, "%s(%d) : %s\n", m_fn.c_str(), m_line, m_fcn.c_str() );
      fclose( fp );
    }
  }
public:
  ThisFunction( const char *fcnName, const char *srcFN, int lineNo )
  : m_fcn(fcnName), m_fn(srcFN), m_line(lineNo)
  {
    static std::vector< ThisFunction* > FunctionStack;
    m_pFunctionStack = &FunctionStack;
    m_pFunctionStack->push_back( this );
    LogCurrentPosition();
  }
  ~ThisFunction()
  {
    m_pFunctionStack->pop_back();
    LogCurrentPosition();
  }
};
#else
#define INSTRUMENT_THIS_FUNCTION 
#endif

#endif //ndef _included_predef_h_
