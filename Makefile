# makefile fto build webserver

GIT_VERSION:=$(shell git --no-pager describe --tags --always --dirty)
GIT_DATE:=$(firstword $(shell git --no-pager show --date=short --format="%ad" --name-only))

XOBJS:=
XLIBS:=
EXTRA:=
#-D_DO_SYNC 

#NO_DBCONN:=0
ifeq ($(strip $(NO_DBCONN)),)
XOBJS += DBConn.o
#XLIBS += -lmyodbc3
XLIBS += -lmyodbc5a
else
EXTRA+=-D_NO_DBCONN
endif

LIBOBJS=Connection.o Socket.o sha1.o Serial.o Stream.o \
	SignalList.o HttpResponse.o HttpRequest.o WebRequestHandler.o \
	MessageReader.o
OBJS=$(LIBOBJS) Controller.o LCD.o main.o Server.o \
	$(XOBJS) Alarm.o Forwarder.o Bus.o ErrorLog.o 
#	OSThread_linux.o Socket.o DBConn.o sha1.o
#	WebClient.o # this file was never used

SA2=sa2.us
CHEMCOM=chemcom.sbcontrol.com

LD_OPTS=-lc -lrt -lstdc++ -lpthread -g -Wl,-Map=chemtrol_server.map \
	$(XLIBS)
#	-L/usr/lib/x86_64-linux-gnu/odbc/ -lodbc -lmyodbc

.PHONY: deployall force


CC=gcc -O0 -g -D_LINUX_ $(EXTRA)
CXX=g++ -O0 -g -D_LINUX_ $(EXTRA)
LD=g++ 

all: chemtrol_server

clean:
	rm -f $(OBJS) .version git_version.h
	-rm -f libChemtrol.a
	rm -f chemtrol_server

.version: force
	@echo '$(GIT_VERSION) $(GIT_DATE)' | cmp -s - $@ || echo '$(GIT_VERSION) $(GIT_DATE)' > $@

git_version.h: .version
	@echo '#define GIT_VERSION "$(GIT_VERSION)"' >$@
	@echo '#define GIT_DATE "$(GIT_DATE)"' >>$@
	@echo '<?php $$GIT_VERSION="$(GIT_VERSION)"; $$GIT_DATE="$(GIT_DATE)";' >php/git_version.php
	@echo 'echo "v$$GIT_VERSION/$$GIT_DATE"; ?>' >>php/git_version.php
	@echo Git version $(GIT_VERSION) $(GIT_DATE)
	@rm -f $(OBJS)

pull:
	git pull origin master

deploychemcom: chemtrol_server
	@echo Be sure to stop the chemtrol_server before trying to replace it!
	scp -P 4000 chemtrol_server todd@$(CHEMCOM):/home/todd/

deploysa2: chemtrol_server
	@echo Be sure to stop the chemtrol_server before trying to replace it!
	scp -P 4000 chemtrol_server todd@$(SA2):/home/todd/

deploysa3: chemtrol_server
	@echo Be sure to stop the chemtrol_server before trying to replace it!
	scp -P 4000 chemtrol_server todd@sa3.us:/home/todd/

deployall: deploysa2 deploysa3 deploychemcom

chemtrol_server: git_version.h $(OBJS)
	$(LD) $(OBJS) $(LD_OPTS) -o $@

lib: git_version.h $(LIBOBJS)
	$(AR) -rv libChemtrol.a $(LIBOBJS)

