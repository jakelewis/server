#include "predef.h"
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <time.h>
#include "Server.h"
#include "git_version.h"
#ifdef _WIN32
#include <conio.h>
#else
#define _getch getchar
#endif

// class to do some init and shutdown tasks specific to the OS
class ModuleStartupShutdown
{
public:
  ModuleStartupShutdown()
  {
#ifdef _WIN32
    WSADATA wsa;
    WSAStartup( 0x0202, &wsa );
#else
    if ( clock() == (clock_t) -1 )
      printf( "clock() does not work!\n" );
    {
      struct timespec ts;
      if ( clock_gettime(GTOD_CLOCK,&ts) == -1 )
        printf( "clock_gettime(%d) does not work!\n", GTOD_CLOCK );
    }
#endif
  }
  ~ModuleStartupShutdown()
  {
#ifdef _WIN32
    WSACleanup();
#endif
  }
};

static int g_shouldQuit = 0;
#ifndef _WIN32
#include <signal.h>
void handleQuit(int sig)
{
  if ( g_shouldQuit ) 
  {
    printf( "Received second sig %d, exiting...\n", sig );
    exit(0);
  } else
  {
    printf( "Received sig %d, quitting...\n", sig );
    g_shouldQuit = 1;
  }
}
void handleSigpipe(int signum)
{
   printf( "SIGPIPE %d ignored\n", signum );
}
#endif

#define MY_PID_FILE "/tmp/chemtrol.pid"

int main(int argc, char **argv)
{
  int opt_d=0;
  char *opt_f = NULL;
  int arg;
  for ( arg=1; arg<argc; ++arg )
  {
    if ( !strcmp( argv[arg], "-d" ) )
      opt_d = 1;
    else if ( !strcmp( argv[arg], "-l" ) )
      Connection::EnableCommLogging(1);
    else if ( !strcmp( argv[arg], "-f" ) )
    {
      if ( arg+1 >= argc ) 
      {
        printf( "Error: -f argument needs a parameter: the forwarding address\n" );
        return -1;
      }
      opt_f = argv[++arg];
    } else
    {
      printf( "Unknown argument: %s\n", argv[arg] );
    }
  }
#ifndef _WIN32
  setvbuf(stdout, NULL, _IONBF, 0); // make stdout unbuffered, so stuff goes out to screen or redirected file immediately
  signal(SIGPIPE, handleSigpipe);
  signal(SIGINT, handleQuit);
  signal(SIGQUIT, handleQuit);

  FILE *pf = fopen( MY_PID_FILE, "r" );
  if ( pf ) // file exists, try to kill that process
  {
    int other_pid;
    if ( 1 == fscanf( pf, "%d", &other_pid ) )
    {
      printf( "Attempting to kill already-running instance\n" );
      if ( !kill( (pid_t)other_pid, SIGINT ) )
      {
        printf("Waiting 10sec to try kill again\n");
        OSTimeDly(10);
        // send a second one just in case
        kill( (pid_t)other_pid, SIGINT );
      }
    }
    fclose(pf);
  }
  
  if ( argc > 1 && !strcmp( argv[1], "-d" ) )
  {
    printf( "Daemonizing... bye!\n" );
    daemon(1,0);

    pid_t pid = getpid();
    
    char logfn[200];
    sprintf( logfn, "/var/log/chemtrol_server.log" );
    FILE *logout = fopen( logfn, "a" );
    if ( logout ) stdout = logout;
    setvbuf(stdout, NULL, _IONBF, 0); // make stdout unbuffered, so stuff goes out to screen or redirected file immediately

  }
  pf = fopen(MY_PID_FILE, "w" ); // TODO: maybe this should be a lock file?
  if ( pf )
  {
    fprintf( pf, "%d", getpid() );
    fclose(pf);
  }  
#endif

  ModuleStartupShutdown initializer;
  
#ifdef _WIN32
  printf("Chemtrol server started (ESC to quit)\n");
#else
  printf("Chemtrol server started (CTRL-C to quit)\n");
#endif
  
  struct tm *info;
  time_t rawtime;
  time(&rawtime);
  info = gmtime(&rawtime );
  printf( "Started at GMT %02d:%02d:%02d on %02d/%02d/%04d\n",
           info->tm_hour, info->tm_min, info->tm_sec, 1+info->tm_mon, info->tm_mday, info->tm_year+1900 );
           
  printf( "Built %s at %s (v%s/%s)\n", __DATE__, __TIME__, GIT_VERSION, GIT_DATE );
  Server server( opt_f );

printf( "Starting main loop\n" );
  int res = 0;
  while (!g_shouldQuit)
  {

#ifdef _WIN32
    if ( _kbhit() )
    {
      int c = _getch();
      {  
        if ( 0x1b == c )
        {
          printf( "ESC hit, server exiting\n" );
          break;
        }
      }
    }
#endif      
  	while ( (res=server.Service()) > 0 )
  		; // keep looping while controller says it did something
    if ( res < 0 ) // server said to quit
      break;
    OSTimeDly(10);
  }
#ifndef _WIN32
  remove( MY_PID_FILE );
#endif
  printf( "[Main loop exited... shutting down]\n" );
  
  return res;
}


